[TOC]

### 敏感词模块
![](https://img.shields.io/badge/release-1.0.0-blue.svg)
#### 使用说明
使用了DFA算法实现实现敏感词过滤，内置了字典库，也可以自己实现数据库方式保存敏感词，字典库可以使用自定义的。

##### 验证敏感词
直接调用工具类的check方法即可，默认使用字典库文件验证。
```java

//验证敏感词，
SensitiveWordResult result = SensitiveWordHandlerUtil.check(text);

SensitiveWordResult包含是否包含敏感词，原始文本，处理后的文本（进行了敏感词替换），名词此集合，替换词替换，禁用词集合
public boolean contains = false; // 是否包含敏感词
public String originalContent;// 原始字符串
public String filteredContent;// 处理后的字符串，屏蔽敏感词后的文本内容

public Set<String> sensitiveWords; // 所有的敏感词
public Map<String, String> replaceWords; // 替换的敏感词，替换前=替换后
public Set<String> sensitiveWordsActive; // 排除替换后的敏感词
……

```

##### 配置参数

默认加载了xinhua.dic（新华社：新闻信息报道中的禁用词和慎用词(2016年7月修订)）和web.dic（收集自网络的敏感词库），可以通过配置文件sb-sensitiveword.properties重新定义。

```properties
### 字典类型，file代表dic字典文件，database代表数据库，更多支持方式待扩展
sensitiveword.type=file
### 加载默认字典（可不带后缀.dic）
sensitiveword.dict=xinhua,web
### 用户扩展字典，会覆盖默认字典,写文件路径，多个文件用英文逗号分隔
sensitiveword.ext_dict=
### 敏感词处理器算法，内置DFA算法的实现，其他算法可自己实现
sensitiveword.handler=DFA
```

常用方法
```java
SensitiveWordHandlerUtil.reloadService(); //重新加载服务，一般用于服务类修改时
SensitiveWordHandlerUtil.loadDict(); // 重新加载字典库（敏感词）

// 删除词条，dictFileName为字典名称，word为敏感词
Result result = SensitiveWordHandlerUtil.delDictItem(String dictFileName, String word);
// 删除词条，extendModel可以放一些扩展对象
SensitiveWordHandlerUtil.delDictItem(String dictFileName, String word, Map<String, Object> extendModel);

// 添加词条，extendModel可以放一些扩展对象
SensitiveWordHandlerUtil.addDictItem(String dictFileName, DictionaryItem item, Map<String, Object> extendModel);
```
##### 自定义敏感词处理器
实现接口com.ld.shieldsb.sensitiveword.handler.SensitiveWordHandler,注册到com.ld.shieldsb.sensitiveword.handler.SensitiveWordHandlerFactory中，并修改配置文件为新增的类即可

```java
// 1、新建处理器
public class SensitiveWordDFAHandler implements SensitiveWordHandler {

	/**
     * 初始化
     */
    public void init(Set<String> sensitiveWordSet, Map<String, String> replaceWords);

    /**
     * 是否包含敏感词
     */
    public boolean contains(String txt);

    /**
     * 返回检查结果
     */
    public SensitiveWordResult check(String text);
}

// 2、注册到工厂类
SensitiveWordHandlerFactory.registerServiceClass(String key, Class<? extends SensitiveWordHandler> clses);

// 3、修改配置文件
### 敏感词处理器算法，内置DFA算法的实现，其他算法可自己实现
sensitiveword.handler=上一步中注册的key
```
##### 自定义敏感词库服务类
继承com.ld.shieldsb.sensitiveword.service.SensitiveWordService,注册到com.ld.shieldsb.sensitiveword.service.SensitiveWordServiceFactory中，并修改配置文件为新增的类即可

```java
// 1、新建处理器
public class SensitiveWordDBService extends SensitiveWordService {
	……
}

// 2、注册到工厂类
SensitiveWordServiceFactory中.registerServiceClass(String key, Class<? extends SensitiveWordService> clses);
 SensitiveWordServiceFactory.registerServiceClass(SensitiveWordServiceType.DATABASE.getValue(), SensitiveWordDBService.class);

// 3、修改配置文件
### 字典类型，file文件database数据库
sensitiveword.type=database【上一步注册的key】
```

#### 版本说明

##### v1.0.0（2019-11-01）
- 初版发布;
- ;