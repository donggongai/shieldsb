package com.ld.shieldsb.sensitiveword.service;

public enum SensitiveWordServiceType {
    // 支持的几种类型，只有file实现了，其他需要自己实现，file表示字典库文件，后缀为.dic
    FILE("file"), DATABASE("database"), XML("xml"), EXT1("ext1"), EXT2("ext2"), EXT3("ext3");

    private final String value;

    SensitiveWordServiceType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
