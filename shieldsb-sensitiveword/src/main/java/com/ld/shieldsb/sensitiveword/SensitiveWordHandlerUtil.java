package com.ld.shieldsb.sensitiveword;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.ld.shieldsb.common.core.model.PageBean;
import com.ld.shieldsb.common.core.model.Result;
import com.ld.shieldsb.common.core.util.StringUtils;
import com.ld.shieldsb.common.core.util.properties.PropertiesPojo;
import com.ld.shieldsb.common.core.util.sensitiveWord.SensitiveWordResult;
import com.ld.shieldsb.sensitiveword.handler.SensitiveWordHandler;
import com.ld.shieldsb.sensitiveword.handler.SensitiveWordHandlerFactory;
import com.ld.shieldsb.sensitiveword.service.SensitiveWordService;
import com.ld.shieldsb.sensitiveword.service.SensitiveWordServiceFactory;

/**
 * 敏感词处理器工具
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2019年10月28日 上午8:41:08
 *
 */
public class SensitiveWordHandlerUtil {

    private static SensitiveWordService service;

    public static boolean load = false; // 项目配置，可覆盖默认配置

    static {
        // 初始化获取服务，加载字典库
        loadDict();
    }

    /**
     * 获取服务
     * 
     * @Title getService
     * @author 吕凯
     * @date 2019年10月31日 上午10:21:48
     * @return SensitiveWordService
     */
    public static SensitiveWordService getService() {
        if (service == null) {
            String key = getServiceType();
            service = SensitiveWordServiceFactory.getInstance().getService(key);
        }
        return service;
    }

    public static String getServiceType() {
        String key = SensitiveWordService.getServiceType();
        return key;
    }

    /**
     * 获取所有所有服务的key
     * 
     * @Title getServiceTypeKeys
     * @author 吕凯
     * @date 2019年10月31日 上午10:21:17
     * @return Set<String>
     */
    public static Set<String> getServiceTypeKeys() {
        return SensitiveWordServiceFactory.getInstance().getAllKey();
    }

    /**
     * 获取所有处理器的key
     * 
     * @Title getHandlerTypeKeys
     * @author 吕凯
     * @date 2019年11月1日 下午3:31:23
     * @return Set<String>
     */
    public static Set<String> getHandlerTypeKeys() {
        return SensitiveWordHandlerFactory.getInstance().getAllKey();
    }

    /**
     * 重新加载service，service类型变化时
     * 
     * @Title reloadService
     * @author 吕凯
     * @date 2019年10月30日 下午6:10:28
     * @return SensitiveWordService
     */
    public static SensitiveWordService reloadService() {
        String key = SensitiveWordService.getServiceType();
        service = SensitiveWordServiceFactory.getInstance().getService(key);
        return service;
    }

    public static Map<String, String> getReplaceWordMap() {
        return service.getReplaceWordMap();
    }

    /**
     * 加载handler
     * 
     * @Title loadHandler
     * @author 吕凯
     * @date 2019年11月1日 下午3:22:46
     * @return SensitiveWordHandler
     */
    public static SensitiveWordHandler loadHandler() {
        return service.loadHandler();
    }

    public static Map<String, String> getWordDictionaryMap() {
        return service.getWordDictionaryMap();
    }

    public static Set<String> getSensitiveWordSet() {
        return service.getSensitiveWordSet();
    }

    public static Set<String> getDictionarysAllEmbed() {
        return service.getDictionarysAllEmbed();
    }

    public static Map<String, Dictionary> getDictionarys() {
        return service.getDictionarys();
    }

    /**
     * 从配置文件中加载字典
     * 
     * @Title loadDict
     * @author 吕凯
     * @date 2019年10月28日 上午9:16:25 void
     */
    public static void loadDict() {
        getService();
        service.loadDict();

    }

    public static String getDictName(String dictName) {
        return service.getDictName(dictName);
    }

    /**
     * 更新字典库
     * 
     * @Title updateDict
     * @author 吕凯
     * @date 2019年10月28日 下午5:10:01
     * @param file
     *            void
     */
    public static void updateDict(File file) {
        service.updateDict(file);

    }

    public static String getString(String paramName, String defaultV) {
        return SensitiveWordService.getString(paramName, defaultV);
    }

    public static SensitiveWordResult check(String text) {
        return service.check(text);
    }

    /**
     * 获取配置参数对象
     * 
     * @Title getProperties
     * @author 吕凯
     * @date 2019年10月31日 上午8:21:04
     * @param key
     * @return PropertiesPojo
     */
    public static PropertiesPojo getProperties(String key) {
        PropertiesPojo model = new PropertiesPojo();
        model.setKey(key);
        if (StringUtils.isNotBlank(key)) {
            String value = getString(key, null);
            model.setValue(value);
        }
        return model;
    }

    /**
     * 更新配置参数对象
     * 
     * @Title updateProperties
     * @author 吕凯
     * @date 2019年10月31日 上午8:21:19
     * @param key
     * @param value
     * @return Result
     */
    public static Result updateProperties(String key, String value) {
        Result result = new Result();
        String valueOld = getString(key, null);
        if (valueOld == null) { // 属性不存在
            result.setMessage("属性不支持！");
            return result;
        }
        result = SensitiveWordService.CONFIG.save(key, value);
        if (!result.getSuccess()) {
            return result;
        }
        result.setSuccess(true);
        result.setMessage("更新配置成功！");
        return result;
    }

    public static void reloadConfig() {
        service.reloadConfig(); // 项目配置，可覆盖默认配置
    }

    public static Result delDictItem(String dictFileName, String word) {
        return service.delDictItem(dictFileName, word, null);
    }

    public static Result delDictItem(String dictFileName, String word, Map<String, Object> extendModel) {
        return service.delDictItem(dictFileName, word, extendModel);
    }

    public static Result addDictItem(String dictFileName, DictionaryItem item, Map<String, Object> extendModel) {
        return service.addDictItem(dictFileName, item, extendModel);
    }

    public static List<PropertiesPojo> getSetInfo() {
        return service.getSetInfo();
    }

    public static Result createConfigFile() {
        return service.createConfigFile();
    }

    public static PageBean<DictionaryItem> getDictionaryItemPageBean(DictionaryItem queryModel, Map<String, Object> extendQueryModel,
            int pageNum, int pageSize) {
        return service.getDictionaryItemPageBean(queryModel, extendQueryModel, pageNum, pageSize);
    }

    public static void main(String[] args) {
        String string = "【傻逼】太多的伤感情怀也许只局限于饲养基地 荧幕中的情节。" + "然后我们的扮演的角色就是跟随着主人公的喜红客联盟 怒哀乐而过于牵强的把自己的情感也附加于银幕情节中，然后感动就流泪，"
                + "难过就躺在某一个人的怀里尽情的阐述心扉或者手机卡复制器一个贱人一杯红酒一部电影在夜 深人静的晚上，关上电话静静的发呆着，来了个天王巨星。";
        System.out.println("=============================================");
        SensitiveWordResult result = check(string);
        System.out.println("最大匹配规则处理：" + result);
    }
}
