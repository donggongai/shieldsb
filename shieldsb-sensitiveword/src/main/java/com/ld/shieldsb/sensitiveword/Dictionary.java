package com.ld.shieldsb.sensitiveword;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import lombok.Data;

@Data
public class Dictionary {
    private String fileName; // 文件名
    private String filePath; // 文件路径
    private String name; // 名称
    private String version; // 版本
    private String description; // 描述

    private List<DictionaryItem> items; // 词条

    /**
     * 获取item，如果没有则创建
     * 
     * @Title getOrCreate
     * @author 吕凯
     * @date 2019年10月28日 上午10:35:08
     * @return List<DictionaryItem>
     */
    public List<DictionaryItem> getOrCreate() {
        List<DictionaryItem> items = getItems();
        if (items == null) {
            items = new ArrayList<>();
            setItems(items);
        }
        return items;
    }

    public DictionaryItem getItem(String key) {
        if (items == null) {
            return null;
        }
        Optional<DictionaryItem> flag = items.stream().filter(item -> item.getWord().equals(key)).findFirst();
        if (flag.isPresent()) {
            return flag.get();
        }
        return null;
    }

}
