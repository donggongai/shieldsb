package com.ld.shieldsb.sensitiveword.handler;

import java.util.Map;
import java.util.Set;

import com.ld.shieldsb.common.core.util.sensitiveWord.SensitiveWordResult;

public interface SensitiveWordHandler {

    /**
     * 初始化
     * 
     * @Title init
     * @author 吕凯
     * @date 2019年11月1日 下午2:55:36
     * @param sensitiveWordSet
     * @param replaceWords
     *            void
     */
    public void init(Set<String> sensitiveWordSet, Map<String, String> replaceWords);

    /**
     * 是否包含敏感词
     * 
     * @Title contains
     * @author 吕凯
     * @date 2019年11月1日 下午2:55:11
     * @param txt
     * @return boolean
     */
    public boolean contains(String txt);

    /**
     * 返回检查结果
     * 
     * @Title check
     * @author 吕凯
     * @date 2019年11月1日 下午2:55:24
     * @param text
     * @return SensitiveWordResult
     */
    public SensitiveWordResult check(String text);

}
