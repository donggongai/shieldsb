package com.ld.shieldsb.sensitiveword.handler;

public enum SensitiveWordHandlerType {
    // 支持的几种类型，只有dfa实现了，其他需要自己实现
    DFA("DFA"), OTHER("other");

    private final String value;

    SensitiveWordHandlerType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
