package com.ld.shieldsb.sensitiveword.handler;

import com.ld.shieldsb.common.core.ServiceFactory;
import com.ld.shieldsb.sensitiveword.handler.impl.SensitiveWordDFAHandler;

public class SensitiveWordHandlerFactory extends ServiceFactory<SensitiveWordHandler> {
    public static final String DEFAULT_TYPE = SensitiveWordHandlerType.DFA.getValue();

    static {
        // 注意工厂类不要一开始就注册对象，否则可能缺少必要的jar包不能实例化报错，应该注册类
        getInstance().registerServiceClass(DEFAULT_TYPE, SensitiveWordDFAHandler.class);
    }

    public static ServiceFactory<SensitiveWordHandler> getInstance() {
        return getInstance(SensitiveWordHandlerFactory.class);
    }

}
