package com.ld.shieldsb.sensitiveword;

import lombok.Data;

@Data
public class DictionaryItem {
    private String word;
    private String replaceWord;
    private int type = 0; // 0禁止使用1替换
    private boolean remove = false; // 移除

    private String dictionaryName; // 字典名称
    private Integer dictionaryType; // 字典类型0内置1扩展

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof DictionaryItem)) {
            return false;
        }
        DictionaryItem other = (DictionaryItem) o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object thisWord = getWord();
        Object otherWord = other.getWord();
        if (thisWord == null ? otherWord != null : !thisWord.equals(otherWord)) {
            return false;
        }
        return true;
    }

    protected boolean canEqual(Object other) {
        return other instanceof DictionaryItem;
    }

    @Override
    public int hashCode() {
        int PRIME = 59;
        int result = 1;
        Object word = getWord();
        result = result * PRIME + (word == null ? 43 : word.hashCode());
        return result * PRIME + 79;
    }

}
