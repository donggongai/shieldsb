package com.ld.shieldsb.sensitiveword.service;

import com.ld.shieldsb.common.core.ServiceFactory;
import com.ld.shieldsb.sensitiveword.service.impl.SensitiveWordFileService;

public class SensitiveWordServiceFactory extends ServiceFactory<SensitiveWordService> {

    static {
        // 注意工厂类不要一开始就注册对象，否则可能缺少必要的jar包不能实例化报错，应该注册类
        getInstance().registerServiceClass(SensitiveWordServiceType.FILE.getValue(), SensitiveWordFileService.class);
    }

    public static ServiceFactory<SensitiveWordService> getInstance() {
        return getInstance(SensitiveWordServiceFactory.class);
    }

}
