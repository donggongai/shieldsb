package com.ld.shieldsb.sensitiveword;

public enum SensitiveWordType {
    FORBID(0), REPLACE(1), REMOVE(-1);

    private final int value;

    SensitiveWordType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
