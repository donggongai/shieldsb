package com.ld.shieldsb.sensitiveword.handler.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import com.ld.shieldsb.common.core.util.StringUtils;
import com.ld.shieldsb.common.core.util.sensitiveWord.SensitiveWordResult;
import com.ld.shieldsb.sensitiveword.handler.SensitiveWordHandler;

/**
 * 
 * 敏感词处理工具 - DFA算法实现
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2019年10月29日 上午9:35:07
 *
 */
public class SensitiveWordDFAHandler implements SensitiveWordHandler {

    /**
     * 敏感词匹配规则
     */
    public static final int MIN_MATCH_TYPE = 1; // 最小匹配规则，如：敏感词库["中国","中国人"]，语句："我是中国人"，匹配结果：我是[中国]人
    public static final int MAX_MATCH_TYPE = 2; // 最大匹配规则，如：敏感词库["中国","中国人"]，语句："我是中国人"，匹配结果：我是[中国人]

    /**
     * 敏感词集合
     */
    public Map<Object, Object> sensitiveWordMap;
    /**
     * 替换词集合
     */
    public Map<String, String> replaceWordMap;

    /**
     * 初始化敏感词库，构建DFA算法模型
     *
     * @param sensitiveWordSet
     *            敏感词库
     */
    public void init(Set<String> sensitiveWordSet) {
        init(sensitiveWordSet, null);
    }

    @Override
    public void init(Set<String> sensitiveWordSet, Map<String, String> replaceWords) {
        initSensitiveWordMap(sensitiveWordSet);
        replaceWordMap = replaceWords;
    }

    /**
     * 添加额外的敏感词
     * 
     * @Title addSensitiveWord
     * @author 吕凯
     * @date 2019年5月29日 下午3:46:58
     * @param sensitiveWordSet
     *            void
     */
    public void addSensitiveWord(Set<String> sensitiveWordSet) {
        addSensitiveWord(sensitiveWordSet, null);
    }

    public void addSensitiveWord(Set<String> sensitiveWordSet, Map<String, String> replaceWords) {
        addSensitiveWordMap(sensitiveWordSet);
        if (replaceWords != null && !replaceWords.isEmpty()) {
            if (replaceWordMap == null) {
                replaceWordMap = new HashMap<>();
            }
            replaceWordMap.putAll(replaceWords);
        }
    }

    /**
     * 初始化敏感词库，构建DFA算法模型
     *
     * @param sensitiveWordSet
     *            敏感词库
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void initSensitiveWordMap(Set<String> sensitiveWordSet) {
        // 初始化敏感词容器，减少扩容操作
        sensitiveWordMap = new HashMap(sensitiveWordSet.size());
        addSensitiveWordMap(sensitiveWordSet);
    }

    /**
     * 添加敏感词的处理
     * 
     * @Title addSensitiveWordMap
     * @author 吕凯
     * @date 2019年5月29日 下午3:47:19
     * @param sensitiveWordSet
     *            void
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private void addSensitiveWordMap(Set<String> sensitiveWordSet) {
        // 初始化敏感词容器，减少扩容操作
        String key;
        Map nowMap;
        Map<String, String> newWordMap;
        // 迭代sensitiveWordSet
        Iterator<String> iterator = sensitiveWordSet.iterator();
        while (iterator.hasNext()) {
            // 关键字
            key = iterator.next();
            nowMap = sensitiveWordMap;
            for (int i = 0; i < key.length(); i++) {
                // 转换成char型
                char keyChar = key.charAt(i);
                // 库中获取关键字
                Object wordMap = nowMap.get(keyChar);
                // 如果存在该key，直接赋值，用于下一个循环获取
                if (wordMap != null) {
                    nowMap = (Map) wordMap;
                } else {
                    // 不存在则，则构建一个map，同时将isEnd设置为0，因为他不是最后一个
                    newWordMap = new HashMap<>();
                    // 不是最后一个
                    newWordMap.put("isEnd", "0");
                    nowMap.put(keyChar, newWordMap);
                    nowMap = newWordMap;
                }

                if (i == key.length() - 1) {
                    // 最后一个
                    nowMap.put("isEnd", "1");
                }
            }
        }
    }

    /**
     * 判断文字是否包含敏感字符
     *
     * @param txt
     *            文字
     * @param matchType
     *            匹配规则 1：最小匹配规则，2：最大匹配规则
     * @return 若包含返回true，否则返回false
     */
    public boolean contains(String txt, int matchType) {
        boolean flag = false;
        for (int i = 0; i < txt.length(); i++) {
            int matchFlag = checkSensitiveWord(txt, i, matchType); // 判断是否包含敏感字符
            if (matchFlag > 0) { // 大于0存在，返回true
                flag = true;
            }
        }
        return flag;
    }

    /**
     * 判断文字是否包含敏感字符
     *
     * @param txt
     *            要判断的文字
     * @return 若包含返回true，否则返回false
     */
    @Override
    public boolean contains(String txt) {
        return contains(txt, MAX_MATCH_TYPE);
    }

    /**
     * 获取文字中的敏感词
     *
     * @param txt
     *            文字
     * @param matchType
     *            匹配规则 1：最小匹配规则，2：最大匹配规则
     * @return
     */
    public Set<String> getSensitiveWord(String txt, int matchType) {
        Set<String> sensitiveWordList = new LinkedHashSet<>(); // 有序

        for (int i = 0; i < txt.length(); i++) {
            // 判断是否包含敏感字符
            int length = checkSensitiveWord(txt, i, matchType);
            if (length > 0) {// 存在,加入list中
                sensitiveWordList.add(txt.substring(i, i + length));
                i = i + length - 1;// 减1的原因，是因为for会自增
            }
        }

        return sensitiveWordList;
    }

    /**
     * 获取文字中的敏感词
     *
     * @param txt
     *            文字
     * @return
     */
    public Set<String> getSensitiveWord(String txt) {
        return getSensitiveWord(txt, MAX_MATCH_TYPE);
    }

    /**
     * 返回敏感词字符串
     * 
     * @Title getSensitiveWordStr
     * @author 吕凯
     * @date 2019年5月30日 上午9:38:03
     * @param txt
     * @return String
     */
    public String getSensitiveWordStr(String txt) {
        if (StringUtils.isEmpty(txt)) {
            return null;
        }
        return String.join(",", getSensitiveWord(txt));
    }

    /**
     * 替换敏感字字符
     *
     * @param txt
     *            文本
     * @param replaceChar
     *            替换的字符，匹配的敏感词以字符逐个替换，如 语句：我爱中国人 敏感词：中国人，替换字符：*， 替换结果：我爱***
     * @param matchType
     *            敏感词匹配规则
     * @return
     */
    public String replaceSensitiveWord(String txt, char replaceChar, int matchType) {
        String resultTxt = txt;
        // 获取所有的敏感词
        Set<String> set = getSensitiveWord(txt, matchType);
        Iterator<String> iterator = set.iterator();
        String word;
        String replaceString;
        while (iterator.hasNext()) {
            word = iterator.next();
            replaceString = getReplaceChars(replaceChar, word.length());
            resultTxt = resultTxt.replaceAll(word, replaceString);
        }

        return resultTxt;
    }

    /**
     * 替换敏感字字符
     *
     * @param txt
     *            文本
     * @param replaceChar
     *            替换的字符，匹配的敏感词以字符逐个替换，如 语句：我爱中国人 敏感词：中国人，替换字符：*， 替换结果：我爱***。
     * @return
     */
    public String replaceSensitiveWord(String txt, char replaceChar) {
        return replaceSensitiveWord(txt, replaceChar, MAX_MATCH_TYPE);
    }

    /**
     * 替换敏感字字符
     *
     * @param txt
     *            文本
     * @param replaceStr
     *            替换的字符串，匹配的敏感词以字符逐个替换，如 语句：我爱中国人 敏感词：中国人，替换字符串：[屏蔽]，替换结果：我爱[屏蔽]
     * @param matchType
     *            敏感词匹配规则
     * @return
     */
    public String replaceSensitiveWord(String txt, String replaceStr, int matchType) {
        String resultTxt = txt;
        // 获取所有的敏感词
        Set<String> set = getSensitiveWord(txt, matchType);
        Iterator<String> iterator = set.iterator();
        String word;
        while (iterator.hasNext()) {
            word = iterator.next();
            resultTxt = resultTxt.replaceAll(word, replaceStr);
        }

        return resultTxt;
    }

    public String replaceSensitiveWord(String txt) {
        return replaceSensitiveWord(txt, MAX_MATCH_TYPE);
    }

    /**
     * 
     * 替换敏感字字符，使用内置的替换词库
     * 
     * @Title replaceSensitiveWord
     * @author 吕凯
     * @date 2019年10月26日 上午11:03:44
     * @param txt
     *            替换的字符串，匹配的敏感词以字符逐个替换
     * @param matchType
     *            敏感词匹配规则
     * @return String
     */
    public String replaceSensitiveWord(String txt, int matchType) {
        String resultTxt = txt;
        if (replaceWordMap != null && !replaceWordMap.isEmpty()) {
            // 获取所有的敏感词
            Set<String> set = getSensitiveWord(txt, matchType);
            Iterator<String> iterator = set.iterator();
            String word;
            while (iterator.hasNext()) {
                word = iterator.next(); // 敏感词
                if (replaceWordMap.containsKey(word)) {
                    resultTxt = resultTxt.replaceAll(word, replaceWordMap.get(word));
                }
            }
        }

        return resultTxt;
    }

    /**
     * 替换敏感字字符
     *
     * @param txt
     *            文本
     * @param replaceStr
     *            替换的字符串，匹配的敏感词以字符逐个替换，如 语句：我爱中国人 敏感词：中国人，替换字符串：[屏蔽]，替换结果：我爱[屏蔽]
     * @return
     */
    public String replaceSensitiveWord(String txt, String replaceStr) {
        return replaceSensitiveWord(txt, replaceStr, MAX_MATCH_TYPE);
    }

    /**
     * 获取替换字符串
     *
     * @param replaceChar
     * @param length
     * @return
     */
    private String getReplaceChars(char replaceChar, int length) {
//        String resultReplace = String.valueOf(replaceChar);
        StringBuilder resultReplaceSb = new StringBuilder(replaceChar);
        for (int i = 1; i < length; i++) {
//            resultReplace += replaceChar;
            resultReplaceSb.append(replaceChar);
        }

        return resultReplaceSb.toString();
    }

    /**
     * 检查文字中是否包含敏感字符，检查规则如下：<br>
     *
     * @param txt
     * @param beginIndex
     * @param matchType
     * @return 如果存在，则返回敏感词字符的长度，不存在返回0
     */
    @SuppressWarnings("rawtypes")
    private int checkSensitiveWord(String txt, int beginIndex, int matchType) {
        // 敏感词结束标识位：用于敏感词只有1位的情况
        boolean flag = false;
        // 匹配标识数默认为0
        int matchFlag = 0;
        char word;
        Map nowMap = sensitiveWordMap;
        for (int i = beginIndex; i < txt.length(); i++) {
            word = txt.charAt(i);
            // 获取指定key
            nowMap = (Map) nowMap.get(word);
            if (nowMap != null) {// 存在，则判断是否为最后一个
                // 找到相应key，匹配标识+1
                matchFlag++;
                // 如果为最后一个匹配规则,结束循环，返回匹配标识数
                if ("1".equals(nowMap.get("isEnd"))) {
                    // 结束标志位为true
                    flag = true;
                    // 最小规则，直接返回,最大规则还需继续查找
                    if (MIN_MATCH_TYPE == matchType) {
                        break;
                    }
                }
            } else {// 不存在，直接返回
                break;
            }
        }
        if (matchFlag < 2 || !flag) {// 长度必须大于等于1，为词
            matchFlag = 0;
        }
        return matchFlag;
    }

    /**
     * 检查文本
     * 
     * @Title check
     * @author 吕凯
     * @date 2019年10月26日 上午11:48:38
     * @param text
     * @return SensitiveWordResult
     */
    @Override
    public SensitiveWordResult check(String text) {
        SensitiveWordResult result = new SensitiveWordResult();
        boolean contains = false;
        if (StringUtils.isNotEmpty(text)) {
            contains = contains(text);
            if (contains) {
                Set<String> sensitiveWordSet = getSensitiveWord(text.toString());
                result.setSensitiveWords(sensitiveWordSet); // 所有敏感词
                Map<String, String> replaceWords = new LinkedHashMap<>(); // 保留顺序
                if (sensitiveWordSet != null) {
                    Set<String> sensitiveWordSetActive = new LinkedHashSet<>(); // 保留顺序
                    String sensitiveWord = String.join(",", sensitiveWordSet);
                    result.setSensitiveWordStr(sensitiveWord); // 敏感词
                    sensitiveWordSet.stream().forEach(key -> {
                        if (replaceWordMap.containsKey(key)) {
                            replaceWords.put(key, replaceWordMap.get(key));
                        } else {
                            sensitiveWordSetActive.add(key);
                        }
                    });
                    result.setReplaceWords(replaceWords); // 替换词语
                    result.setSensitiveWordsActive(sensitiveWordSetActive); // 排除替换后的敏感词
                }
                result.setFilteredContent(replaceSensitiveWord(text));
            }
        }
        result.setOriginalContent(text); // 原始文本
        result.setContains(contains); // 是否包含敏感词
        return result;
    }

    public static void main(String[] args) {

        SensitiveWordDFAHandler util = new SensitiveWordDFAHandler();

        Set<String> sensitiveWordSet = new HashSet<>();
        sensitiveWordSet.add("太多");
        sensitiveWordSet.add("太多的伤感"); // 注意最大匹配与最小匹配的差别
        sensitiveWordSet.add("爱恋");
        sensitiveWordSet.add("静静");
        sensitiveWordSet.add("哈哈");
        sensitiveWordSet.add("啦啦");
        sensitiveWordSet.add("感动");
        sensitiveWordSet.add("发呆");
        // 初始化敏感词库
        util.init(sensitiveWordSet);

        Set<String> extendWordSet = new HashSet<>();
        extendWordSet.add("饲养");
        util.addSensitiveWord(extendWordSet);

        System.out.println("敏感词的数量：" + util.sensitiveWordMap.size());
        String string = "太多的伤感情怀也许只局限于饲养基地 荧幕中的情节。" + "然后我们的扮演的角色就是跟随着主人公的喜红客联盟 怒哀乐而过于牵强的把自己的情感也附加于银幕情节中，然后感动就流泪，"
                + "难过就躺在某一个人的怀里尽情的阐述心扉或者手机卡复制器一个贱人一杯红酒一部电影在夜 深人静的晚上，关上电话静静的发呆着。";
        System.out.println("待检测语句字数：" + string.length());

        // 是否含有关键字
        boolean contains = util.contains(string);
        System.out.println("最大匹配规则：" + contains);
        contains = util.contains(string, SensitiveWordDFAHandler.MIN_MATCH_TYPE);
        System.out.println("最小匹配规则：" + contains);

        // 获取语句中的敏感词
        Set<String> set = util.getSensitiveWord(string);
        System.out.println("最大匹配规则语句中包含敏感词的个数为：" + set.size() + "。包含：" + set);
        set = util.getSensitiveWord(string, SensitiveWordDFAHandler.MIN_MATCH_TYPE);
        System.out.println("最小匹配规则语句中包含敏感词的个数为：" + set.size() + "。包含：" + set);

        // 替换语句中的敏感词
        String filterStr = util.replaceSensitiveWord(string, '*');
        System.out.println("最大匹配规则替换：" + filterStr);
        filterStr = util.replaceSensitiveWord(string, '*', SensitiveWordDFAHandler.MIN_MATCH_TYPE);
        System.out.println("最小匹配规则替换：" + filterStr);

        String filterStr2 = util.replaceSensitiveWord(string, "[*敏感词*]");
        System.out.println("最大匹配规则替换：" + filterStr2);
        filterStr2 = util.replaceSensitiveWord(string, "[*敏感词*]", SensitiveWordDFAHandler.MIN_MATCH_TYPE);
        System.out.println("最小匹配规则替换：" + filterStr2);

        // 初始化敏感词库
        System.out.println("=============================================");
        Map<String, String> map = new HashMap<>();
        map.put("饲养", "[*饲养*]");
        map.put("静静", "[*胡汉三*]");
        util.init(sensitiveWordSet);
        util.addSensitiveWord(extendWordSet, map);
        filterStr2 = util.replaceSensitiveWord(string);
        System.out.println("最大匹配规则替换：" + filterStr2);
        filterStr2 = util.replaceSensitiveWord(string, SensitiveWordDFAHandler.MIN_MATCH_TYPE);
        System.out.println("最小匹配规则替换：" + filterStr2);
        System.out.println("=============================================");
        SensitiveWordResult result = util.check(string);
        System.out.println("最大匹配规则处理：" + result);

    }

}
