package com.ld.shieldsb.common.core.encryptor;

import org.testng.Assert;
import org.testng.annotations.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * EorEncryptor的单元测试类
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月26日 上午9:29:32
 *
 */
@Slf4j
public class EorEncryptorTest {

    /**
     * 测试
     * 
     * @Title test
     * @author 吕凯
     * @date 2021年26日 上午9:29:32
     * @return void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void test() {
        int key0 = 1231;
        String str = "test";
        String encodeStr = EorEncryptor.encode(str, key0);
        String decodeStr = EorEncryptor.decode(encodeStr, key0);
        Assert.assertEquals(decodeStr, str);

        String decodeStr1 = EorEncryptor.decode(encodeStr, 12);
        Assert.assertNotEquals(decodeStr1, str);

        log.warn("EorEncryptorTest加密后：{},是否有非法字符：{}", encodeStr, EorEncryptor.hasValidChar(encodeStr));
        log.warn("EorEncryptorTest解密后：{},是否有非法字符：{}", decodeStr, EorEncryptor.hasValidChar(decodeStr));
        log.warn("EorEncryptorTest使用错误密钥解密后：{},是否有非法字符：{}", decodeStr1, EorEncryptor.hasValidChar(decodeStr1));

    }

}
