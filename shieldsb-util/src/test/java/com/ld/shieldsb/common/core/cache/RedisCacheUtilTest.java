package com.ld.shieldsb.common.core.cache;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ld.shieldsb.common.core.cache.redis.RedisCacheUtil;

/**
 * redis缓存工具类的单元测试类，需要配合src/test/resources/properties/config_test.properties，因为存在密码泄露的风险，该文件需要自己创建，设置参数<br>
 * # 是否启用redis<br>
 * shieldsb.redis.use=true<br>
 * # 是否启用redis缓存<br>
 * cache_redis_use=true<br>
 * # 主机ip<br>
 * shieldsb.redis.host=192.168.1.248<br>
 * # 端口<br>
 * shieldsb.redis.port=6379<br>
 * # 密码，没有可不写<br>
 * shieldsb.redis.password=<br>
 * # 存放的库下标<br>
 * shieldsb.redis.database=11<br>
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月26日 上午9:29:32
 *
 */
public class RedisCacheUtilTest {
    private String testCacheName = "test_redis_cache";

    /**
     * 测试前运行，只执行1次
     * 
     * @Title before
     * @author 吕凯
     * @date 2021年4月27日 上午10:16:43 void
     */
    @BeforeClass
    public void before() {
        RedisCacheUtil.createCache(testCacheName, 2); // 创建缓存,缓存2秒

    }

    @AfterClass
    public void after() {
        RedisCacheUtil.close(); // 关闭缓存
    }

    /**
     * 测试
     * 
     * @Title test
     * @author 吕凯
     * @date 2021年4月26日 上午9:29:32
     * @return void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void test() {
        put("test", "testvalue");
        String cacheValue = get("test");
        Assert.assertEquals(cacheValue, "testvalue");
        // 验证超时
        try {
            Thread.sleep(2100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        cacheValue = get("test"); // 缓存取不到了
        Assert.assertNull(cacheValue);

        put("test2", "testvalue1");
        cacheValue = get("test2");
        Assert.assertEquals(cacheValue, "testvalue1");
        // 验证不超时
        try {
            Thread.sleep(1900);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        cacheValue = get("test2"); // 没到时间还能取到
        Assert.assertEquals(cacheValue, "testvalue1");

    }

    private boolean put(String key, Object value) {
        return RedisCacheUtil.put(testCacheName, key, value);
    }

    private <T> T get(String key) {
        return RedisCacheUtil.get(testCacheName, key);
    }

}
