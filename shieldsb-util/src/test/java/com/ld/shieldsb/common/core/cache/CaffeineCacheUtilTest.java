package com.ld.shieldsb.common.core.cache;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ld.shieldsb.common.core.cache.caffeine.CaffeineCacheUtil;

/**
 * CaffeineCache缓存工具类的单元测试类
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月26日 上午9:29:32
 *
 */
public class CaffeineCacheUtilTest {
    private String testCacheName = "test_caffeine_cache";

    /**
     * 测试前运行，只执行1次
     * 
     * @Title before
     * @author 吕凯
     * @date 2021年4月27日 上午10:16:43 void
     */
    @BeforeClass
    public void before() {
        CaffeineCacheUtil.createCache(testCacheName, 2L); // 创建缓存,缓存2秒

    }

    /**
     * 测试
     * 
     * @Title test
     * @author 吕凯
     * @date 2021年4月26日 上午9:29:32
     * @return void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void test() {
        put("test", "testvalue");
        String cacheValue = get("test");
        Assert.assertEquals(cacheValue, "testvalue");
        // 验证超时
        try {
            Thread.sleep(2100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        cacheValue = get("test"); // 缓存取不到了
        Assert.assertNull(cacheValue);

        put("test2", "testvalue1");
        cacheValue = get("test2");
        Assert.assertEquals(cacheValue, "testvalue1");
        // 验证不超时
        try {
            Thread.sleep(1900);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        cacheValue = get("test2"); // 没到时间还能取到
        Assert.assertEquals(cacheValue, "testvalue1");

        String keyRemove = "testremove";
        put(keyRemove, "testvalue");
        remove(keyRemove);
        cacheValue = get(keyRemove); // 缓存取不到了
        Assert.assertNull(cacheValue);

    }

    private boolean put(String key, Object value) {
        return CaffeineCacheUtil.put(testCacheName, key, value);
    }

    private <T> T get(String key) {
        return CaffeineCacheUtil.get(testCacheName, key);
    }

    /**
     * 删除缓存
     * 
     * @Title remove
     * @author 吕凯
     * @date 2021年4月28日 上午10:18:01
     * @param key
     * @return boolean
     */
    private boolean remove(String key) {
        return CaffeineCacheUtil.remove(testCacheName, key);
    }

}
