package com.ld.shieldsb.common.core.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.math.RandomUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.alibaba.fastjson.JSONObject;
import com.ld.shieldsb.common.core.reflect.ClassUtilTest;
import com.ld.shieldsb.common.core.reflect.ClassUtilTest.ModelT;

import lombok.extern.slf4j.Slf4j;

/**
 * FastJsonUtil的单元测试类
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年5月7日 下午2:05:47
 *
 */
@Slf4j
public class FastJsonUtilTest {

    /**
     * 
     * 测试
     * 
     * @Title test
     * @author 吕凯
     * @date 2021年5月7日 上午9:04:17 void
     */
    @SuppressWarnings("unchecked")
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void test() {
        List<ModelT> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            ModelT model = new ClassUtilTest().new ModelT();
            model.setName("name" + i);
            model.setSex(i % 2 == 0 ? "男" : "女");
            model.setAge(RandomUtils.nextInt(99));
            list.add(model);
        }
        log.warn("处理前的对象为：{}", list);
        List<JSONObject> returnList = (List<JSONObject>) FastJsonUtil.filterFieldJson(list, ModelT.class, "name", "age");
        log.warn("处理后的对象为：{}", returnList);
        Assert.assertEquals(returnList.size(), 5); // 期望结果：：相等
        Assert.assertNotNull(returnList.get(0).get("age")); // 期望结果：：非空，
        Assert.assertNull(returnList.get(0).get("sex")); // 期望结果：：null，被排除了
    }

}
