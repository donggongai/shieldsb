package com.ld.shieldsb.common.core.cache.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class TestModel implements Serializable {
    /**
     * @Fields serialVersionUID
     */
    private static final long serialVersionUID = 6943797383316913095L;
    private String name;
    private Integer age;

    private TestModelInner innerModel;

}
