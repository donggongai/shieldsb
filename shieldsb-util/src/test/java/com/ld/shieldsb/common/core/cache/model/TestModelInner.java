package com.ld.shieldsb.common.core.cache.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class TestModelInner implements Serializable {
    /**
     * @Fields serialVersionUID
     */
    private static final long serialVersionUID = -8433231642485202193L;
    private String key;
    private Boolean isUse;

}
