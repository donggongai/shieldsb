package com.ld.shieldsb.common.core.encryptor;

import org.testng.Assert;
import org.testng.annotations.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * BlowfishEncryptor的单元测试类
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月26日 上午9:29:32
 *
 */
@Slf4j
public class BlowfishEncryptorTest {
    public static final String PASS_KEY = "test";
    public static final BlowfishEncryptor BFISH = new BlowfishEncryptor(PASS_KEY);

    /**
     * 测试
     * 
     * @Title test
     * @author 吕凯
     * @date 2021年26日 上午9:29:32
     * @return void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void test() {
        String value = "mykeevin";
        String encryptStr = BFISH.encrypt(value);
        String decryptStr = BFISH.decrypt(encryptStr);
        log.warn(encryptStr);
        Assert.assertEquals(decryptStr, value);

        value = "事发地点1111123吾问无为谓无111111111111111111111111111111111111111第三方";
        encryptStr = BFISH.encrypt(value);
        decryptStr = BFISH.decrypt(encryptStr);
        log.warn(encryptStr);
        Assert.assertEquals(decryptStr, value);

        String passkey = "test11111111111111111111111111111111111111";
        BlowfishEncryptor bfish1 = new BlowfishEncryptor(passkey);
        value = "事发地点";
        encryptStr = bfish1.encrypt(value);
        decryptStr = bfish1.decrypt(encryptStr);
        log.warn(encryptStr);
        Assert.assertEquals(decryptStr, value);

    }

}
