package com.ld.shieldsb.common.core.util.notice.email;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import org.apache.commons.mail.EmailAttachment;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.ld.shieldsb.common.core.util.notice.mail.EmailInfo;
import com.ld.shieldsb.common.core.util.notice.mail.EmailUtil;

/**
 * 测试邮件发送工具类的测试用例，需要配合src/test/resources/properties/config_test.properties，因为存在密码泄露的风险，该文件需要自己创建
 * 
 * @ClassName EmailUtilTest
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2018年12月26日 下午6:06:02
 *
 */
public class EmailUtilTest {
    private String sendTo = "291490365@qq.com"; // 接收方邮箱

    @Test
    public void test() {
        Reporter.log("EmailUtilTest……测试发送邮件", true);
        EmailInfo mailInfo = new EmailInfo();
        mailInfo.addToAddress(sendTo);
        mailInfo.setSubject("发送一封普通邮件测的试"); // 注意标题只有“测试”使用163发送邮件时可能被163服务器拦截提示非法内容
        mailInfo.setContent("测试内容 ");

        boolean flag = EmailUtil.sendEmail(mailInfo).getSuccess();
        Assert.assertEquals(flag, true);
    }

    @Test
    public void testFromName() {
        Reporter.log("EmailUtilTest……测试发送自定义from的邮件", true);
        try {
            Thread.sleep(1000); // 休眠一下，防止发送过快
        } catch (InterruptedException e) {
        }
        EmailInfo mailInfo = new EmailInfo();
        mailInfo.addToAddress(sendTo);
        mailInfo.setSubject("发送一封自定义fromName的邮件测的试");
        mailInfo.setContent("测试内容 ");
        mailInfo.setFromName("老吕啊");

        boolean flag = EmailUtil.sendEmail(mailInfo).getSuccess();
        Assert.assertEquals(flag, true);
    }

    // 根据模板获取内容
    @Test
    public void testTmpl() {
        Reporter.log("EmailUtilTest……测试发送模板邮件", true);
        try {
            Thread.sleep(1000); // 休眠一下，防止发送过快
        } catch (InterruptedException e) {
        }
        EmailInfo mailInfo = new EmailInfo();
        mailInfo.addToAddress(sendTo);
        mailInfo.setSubject("根据模板发送内容测的试");
        mailInfo.setContent("测试内容 ");
        mailInfo.setFromName("老吕啊");

        boolean flag = EmailUtil.sendEmailByTempl(mailInfo, "sendEmail.html", new HashMap<>()).getSuccess();
        Assert.assertEquals(flag, true);
    }

    // 带附件的测试
    @Test
    public void testAttachment() {
        Reporter.log("EmailUtilTest……测试带附件的邮件", true);
        try {
            Thread.sleep(1000); // 休眠一下，防止发送过快
        } catch (InterruptedException e) {
        }
        EmailInfo mailInfo = new EmailInfo();
        mailInfo.addToAddress(sendTo);
        mailInfo.setSubject("发送附件测的试");
        mailInfo.setContent("测试内容 ");
        mailInfo.setFromName("老吕啊");

        // 本地附件
        EmailAttachment att = new EmailAttachment();
        URL fileUrl = Thread.currentThread().getContextClassLoader().getResource("emailatta.txt");

        att.setPath(fileUrl.getPath()); // 本地存在的文件
        att.setName("测试.txt");
        att.setDescription("测试赛");
        mailInfo.addAttachments(att);
        // 远程附件
        EmailAttachment attUrl = new EmailAttachment();
        try {
            attUrl.setURL(new URL("https://ss0.bdstatic.com/5aV1bjqh_Q23odCf/static/superman/img/logo_top_86d58ae1.png")); // 远程地址
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        attUrl.setName("图片.png");
        attUrl.setDescription("这是一个图片");
        mailInfo.addAttachments(attUrl);

        boolean flag = EmailUtil.sendEmail(mailInfo).getSuccess();
        Assert.assertEquals(flag, true);
    }

}
