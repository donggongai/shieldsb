package com.ld.shieldsb.common.core.cache;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * 聚合后的缓存工具类的单元测试类,需要配合src/test/resources/properties/config_test.properties，因为存在密码泄露的风险，该文件需要自己创建，设置参数shielsb.cache = caffeine
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月26日 上午9:29:32
 *
 */
public class CacheUtilTest {
    private String testCacheName = "first";

    /**
     * 测试前运行，只执行1次
     * 
     * @Title before
     * @author 吕凯
     * @date 2021年4月27日 上午10:16:43 void
     */
    @BeforeClass
    public void before() {
        // CaffeineCacheUtil.createCache(testCacheName, 2L); // 如果缓存库不是使用的默认的，则需要先创建缓存

    }

    /**
     * 测试
     * 
     * @Title test
     * @author 吕凯
     * @date 2021年4月26日 上午9:29:32
     * @return void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void test() {
        String key = "testUtil";
        String value = "value";
        put(key, value);
        String cacheValue = getFirstLevel(key);
        Assert.assertEquals(cacheValue, value);

        // 默认不开启2级缓存
        String cacheValueSecond = getSecondLevel(key);
        Assert.assertNull(cacheValueSecond);

        // 开启2级缓存
        put(key, value, true);
        cacheValue = getFirstLevel(key);
        Assert.assertEquals(cacheValue, value);

        // 默认不开启2级缓存
        cacheValueSecond = getSecondLevel(key);
        Assert.assertEquals(cacheValueSecond, value);

    }

    private void put(String key, Object value) {
        put(key, value, false);
    }

    private void put(String key, Object value, Boolean useSecondLevel) {
        CacheUtil.put(testCacheName, key, value, useSecondLevel);
    }

    private <T> T getFirstLevel(String key) {
        return CacheUtil.getFirstLevel(testCacheName, key);
    }

    private <T> T getSecondLevel(String key) {
        return CacheUtil.getSecondLevel(testCacheName, key);
    }

}
