package com.ld.shieldsb.common.core.cache;

import java.io.Serializable;
import java.time.Duration;
import java.util.Iterator;

import org.ehcache.Cache;
import org.ehcache.config.CacheConfiguration;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.MemoryUnit;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.ld.shieldsb.common.core.cache.ehcache.EhcacheCache;
import com.ld.shieldsb.common.core.cache.ehcache.EhcacheUtil;
import com.ld.shieldsb.common.core.cache.model.TestModel;
import com.ld.shieldsb.common.core.cache.model.TestModelInner;

import lombok.extern.slf4j.Slf4j;

/**
 * ehCache缓存工具类的单元测试类
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月26日 上午9:29:32
 *
 */
@Slf4j
public class EhcacheUtilTest {
    private String testCacheName = "test_ehcache_cache";
    private String testCacheNameHeap = "test_ehcache_cache1";

    /**
     * 测试前运行，只执行1次
     * 
     * @Title before
     * @author 吕凯
     * @date 2021年4月27日 上午10:16:43 void
     */
    @BeforeClass
    public void before() {
        // 因为持久化的原因，需要先判断一下缓存是否存在
        if (EhcacheUtil.getCache(testCacheName) == null) {
            EhcacheUtil.createCache(testCacheName, 2L); // 创建缓存,缓存2秒
        } else {
            log.warn(EhcacheUtil.getPersistencePath());
        }
    }

    @AfterClass
    public void after() {
        EhcacheUtil.close(); // 关闭缓存
    }

    /**
     * 测试正常存取
     * 
     * @Title test
     * @author 吕凯
     * @date 2021年4月26日 上午9:29:32
     * @return void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void test() {
        put("test", "testvalue");
        put("test-1", "testvalue-1");
        String cacheValue = get("test");
        Assert.assertEquals(cacheValue, "testvalue");
        String cacheValue1 = get("test-1");
        Assert.assertEquals(cacheValue1, "testvalue-1");
        // 验证超时
        try {
            Thread.sleep(2100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        cacheValue = get("test"); // 缓存取不到了
        Assert.assertNull(cacheValue);

        put("test2", "testvalue1");
        cacheValue = get("test2");
        Assert.assertEquals(cacheValue, "testvalue1");
        // 验证不超时
        try {
            Thread.sleep(1900);
        } catch (InterruptedException e) {
            log.error("", e);
        }
        cacheValue = get("test2"); // 没到时间还能取到
        Assert.assertEquals(cacheValue, "testvalue1");

    }

    /**
     * 测试超出数量时抛弃
     * 
     * @Title testHeap
     * @author 吕凯
     * @date 2021年4月27日 下午4:57:16 void
     */
    @Test(priority = 1) // 不指定顺序时默认按字母顺序执行
    public void testSize() {
        CacheConfiguration<String, Serializable> configuration = CacheConfigurationBuilder.newCacheConfigurationBuilder(
                // 缓存数据K和V的数值类型
                // 在ehcache3.3以后必须指定缓存键值类型,如果使用中类型与配置的不同,会报类转换异常
                String.class, Serializable.class,
                // 设置缓存堆容纳元素个数(JVM内存空间)超出个数后会存到offheap中
                ResourcePoolsBuilder.heap(2L)).build();

        if (EhcacheUtil.getCache(testCacheNameHeap) != null) {
            EhcacheUtil.removeCache(testCacheNameHeap);
        }
        EhcacheUtil.createCache(testCacheNameHeap, configuration); // 创建缓存

        EhcacheUtil.put(testCacheNameHeap, "test", "testvalue");
        String cacheValue = EhcacheUtil.get(testCacheNameHeap, "test");
        Assert.assertEquals(cacheValue, "testvalue");

        for (int i = 0; i < 2; i++) {

            boolean put = EhcacheUtil.put(testCacheNameHeap, "test-" + i, "testvalue" + i);
            Assert.assertTrue(put);
            cacheValue = EhcacheUtil.get(testCacheNameHeap, "test-" + i);
            log.warn("test-{} = {}", i, cacheValue);
            Assert.assertEquals(cacheValue, "testvalue" + i);
        }

        cacheValue = EhcacheUtil.get(testCacheNameHeap, "test1"); // 取出并输出第一条数据,因为heap的个数设置为2所以当存入后2条数据时,第一条会被淘汰
        Assert.assertNull(cacheValue);

    }

    /**
     * 测试的对象缓存，注意如果缓存的value为对象，要求里面所有的字段都必需序列化
     * 
     * @Title testModel
     * @author 吕凯
     * @date 2021年4月30日 上午10:03:26 void
     */
    @Test(priority = 3) // 不指定顺序时默认按字母顺序执行
    public void testModel() {
        String key = "testPersistence";
        TestModel model = new TestModel();
        model.setName("kevin");
        model.setAge(22);
        TestModel value = model;
        TestModelInner innerModel = new TestModelInner();
        innerModel.setKey("kk");
        innerModel.setIsUse(true);
        value.setInnerModel(innerModel);
        put(key, value);

        TestModel cacheValue = get(key);
        log.warn("存储的model：{}", cacheValue);
        log.warn("原来的model：{}", model);
        Assert.assertEquals(cacheValue, value);

    }

    /**
     * 测试有效期，使用临时缓存
     * 
     * @Title testModel
     * @author 吕凯
     * @date 2021年4月30日 上午10:03:26 void
     */
    @Test(priority = 4) // 不指定顺序时默认按字母顺序执行
    public void testTime() {
        String cacheName = "test_tmp_kevin111";
        // 同时设置了ttl和tti，tti会覆盖ttl
        CacheConfiguration<String, Serializable> configuration = CacheConfigurationBuilder.newCacheConfigurationBuilder(
                // 缓存数据K和V的数值类型
                // 在ehcache3.3中必须指定缓存键值类型,如果使用中类型与配置的不同,会报类转换异常
                String.class, Serializable.class,
                // 设置缓存堆容纳元素个数(JVM内存空间)超出个数后会存到offheap中
                ResourcePoolsBuilder.heap(10000L)
                        // 设置堆外储存大小(内存存储) 超出offheap的大小会淘汰规则被淘汰
                        .offheap(100L, MemoryUnit.MB)
                        // 配置磁盘持久化储存(硬盘存储)用来持久化到磁盘,这里设置为false不启用
                        .disk(500L, MemoryUnit.MB, false))
                // no expiry : 永不过期
                // time-to-live ：创建后一段时间过期
                // time-to-idle ： 访问后一段时间过期
                // 存活时间是duration秒，过期后自动清除
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(
                        // 设置缓存过期时间
                        Duration.ofSeconds(10)))
                .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(
                        // 设置被访问后过期时间(同时设置和TTL和TTI之后会被覆盖,这里TTI生效,之前版本xml配置后是两个配置了都会生效)
                        Duration.ofSeconds(3))
                // 缓存淘汰策略 默认策略是LRU（最近最少使用）。你可以设置为FIFO（先进先出）或是LFU（较少使用）。
                ).build();

        EhcacheUtil.createCache(cacheName, configuration);
        EhcacheUtil.put(cacheName, "test", "testvalue");
        EhcacheUtil.put(cacheName, "test-1", "testvalue-1");
        String cacheValue = EhcacheUtil.get(cacheName, "test");
        Assert.assertEquals(cacheValue, "testvalue");
        String cacheValue1 = EhcacheUtil.get(cacheName, "test-1");
        Assert.assertEquals(cacheValue1, "testvalue-1");
        // 验证超时
        try {
            Thread.sleep(3100); // 静待3秒钟，超过tti，但是没有超过ttl的情况，看是哪个生效
        } catch (InterruptedException e) {
            log.error("", e);
        }

        cacheValue = EhcacheUtil.get(cacheName, "test"); // 缓存取不到了
        Assert.assertNull(cacheValue);// 期望结果：：空，若未空则证明tti把ttl覆盖了

        // 只设置ttl的情况
        String cacheName2 = "test_tmp_kevin112";
        CacheConfiguration<String, Serializable> configuration2 = CacheConfigurationBuilder.newCacheConfigurationBuilder(
                // 缓存数据K和V的数值类型
                // 在ehcache3.3中必须指定缓存键值类型,如果使用中类型与配置的不同,会报类转换异常
                String.class, Serializable.class,
                // 设置缓存堆容纳元素个数(JVM内存空间)超出个数后会存到offheap中
                ResourcePoolsBuilder.heap(10000L)
                        // 设置堆外储存大小(内存存储) 超出offheap的大小会淘汰规则被淘汰
                        .offheap(100L, MemoryUnit.MB)
                        // 配置磁盘持久化储存(硬盘存储)用来持久化到磁盘,这里设置为false不启用
                        .disk(500L, MemoryUnit.MB, false))
                // no expiry : 永不过期
                // time-to-live ：创建后一段时间过期
                // time-to-idle ： 访问后一段时间过期
                // 存活时间是duration秒，过期后自动清除
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(
                        // 设置缓存过期时间
                        Duration.ofSeconds(4)))
                // 缓存淘汰策略 默认策略是LRU（最近最少使用）。你可以设置为FIFO（先进先出）或是LFU（较少使用）。
                .build();

        EhcacheUtil.createCache(cacheName2, configuration2);
        EhcacheUtil.put(cacheName2, "test", "testvalue");
        EhcacheUtil.put(cacheName2, "test-1", "testvalue-1");
        String cacheValue2 = EhcacheUtil.get(cacheName2, "test");
        Assert.assertEquals(cacheValue2, "testvalue");// 期望结果：：相等
        String cacheValue12 = EhcacheUtil.get(cacheName2, "test-1");
        Assert.assertEquals(cacheValue12, "testvalue-1"); // 期望结果：：相等
        // 验证超时
        try {
            Thread.sleep(3100);
        } catch (InterruptedException e) {
            log.error("", e);
        }
        cacheValue = EhcacheUtil.get(cacheName2, "test"); // 缓存取不到了
        Assert.assertNotNull(cacheValue); // 期望结果：：非空

        // 验证超时
        try {
            Thread.sleep(900);
        } catch (InterruptedException e) {
            log.error("", e);
        }
        cacheValue = EhcacheUtil.get(cacheName2, "test"); // 缓存可以取到
        Assert.assertNull(cacheValue); // 期望结果：：空
    }

    /**
     * 测试持久化
     * 
     * @Title testPersistence
     * @author 吕凯
     * @date 2021年4月27日 下午5:00:20 void
     */
    @Test(priority = 99) // 不指定顺序时默认按字母顺序执行
    public void testPersistence() {
        String key = "testPersistence";
        String value = "testvaluePersistence123";
        put(key, value);
        String cacheValue = get(key);
        Assert.assertEquals(cacheValue, value);

        EhcacheUtil.close(); // 关闭

        // 重新new对象，如果还能读到说明已经持久化到本地了
        EhcacheCache cache = new EhcacheCache();
        cacheValue = cache.get(testCacheName, key);
        Assert.assertEquals(cacheValue, value);

        int count = 0;
        Cache<String, Serializable> threeTieredCache = cache.getCache(testCacheName).getCache();
        Iterator<Cache.Entry<String, Serializable>> iterator = threeTieredCache.iterator();
        while (iterator.hasNext()) {
            ++count;
            Cache.Entry<String, Serializable> entry = iterator.next();
            log.warn("threeTieredCache,主键：{}，值：{}", entry.getKey(), entry.getValue());
        }
        log.info("信息的总条数为：{}", count);
        // 断言输出的信息结果条数，如果大于0，则验证正确。
        Assert.assertTrue(count > 0);

        cache.close(); // 关闭
    }

    private boolean put(String key, Object value) {
        return EhcacheUtil.put(testCacheName, key, value);
    }

    private <T> T get(String key) {
        return EhcacheUtil.get(testCacheName, key);
    }

}
