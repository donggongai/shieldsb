package com.ld.shieldsb.common.core.util.properties;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ld.shieldsb.common.core.util.pwdchecker.PwdChecker;
import com.ld.shieldsb.common.core.util.pwdchecker.impl.AlphanumericSymbolPwdChecker;
import com.ld.shieldsb.common.core.util.pwdchecker.impl.SimplePwdChecker;

/**
 * 密码验证工具的测试用例
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月14日 下午4:56:43
 *
 */
public class PwdCheckerTest {

    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void testAlphanumericSymbolPwdChecker() {
        PwdChecker checker = new AlphanumericSymbolPwdChecker();
        // 判定false
        Assert.assertFalse(checker.checkIsSafe("test").getSuccess());
        Assert.assertFalse(checker.checkIsSafe("12345678").getSuccess());
        Assert.assertFalse(checker.checkIsSafe("admin31511cs").getSuccess());
        Assert.assertFalse(checker.checkIsSafe("admin31511csA").getSuccess());
        // 判定true
        Assert.assertTrue(checker.checkIsSafe("admin31511csA-").getSuccess());
        Assert.assertTrue(checker.checkIsSafe("admin31511csA!").getSuccess());
        Assert.assertTrue(checker.checkIsSafe("shieldsb_key_admin31511csA!").getSuccess());
        Assert.assertTrue(checker.checkIsSafe("shieldsb-key-admin31511csA!").getSuccess());
    }

    @Test(priority = 1) // 不指定顺序时默认按字母顺序执行
    public void testSimplePwdChecker() {
        PwdChecker checker = new SimplePwdChecker();
        // 判定false
        Assert.assertFalse(checker.checkIsSafe("admin").getSuccess());
        Assert.assertFalse(checker.checkIsSafe("123456").getSuccess());
        Assert.assertFalse(checker.checkIsSafe("12345678").getSuccess());
        Assert.assertFalse(checker.checkIsSafe("111111111").getSuccess());
        Assert.assertFalse(checker.checkIsSafe("root123").getSuccess());
        Assert.assertFalse(checker.checkIsSafe("123123").getSuccess());
        Assert.assertFalse(checker.checkIsSafe("admin31").getSuccess()); // 少于8位
        // 判定true
        Assert.assertTrue(checker.checkIsSafe("admin31511cs").getSuccess());
    }

}
