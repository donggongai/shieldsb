package com.ld.shieldsb.common.core.eventbus;

import com.google.common.eventbus.EventBus;

public class TestMultipleEvents {

    public static void main(String[] args) throws Exception {

        EventBus eventBus = new EventBus("test");
        MultipleListener multiListener = new MultipleListener();

        eventBus.register(multiListener);

        eventBus.post(Integer.valueOf(100));
        eventBus.post(Integer.valueOf(200));
        eventBus.post(Integer.valueOf(300));
        eventBus.post(Long.valueOf(800));
        eventBus.post(Long.valueOf(800990));
        eventBus.post(Long.valueOf(800882934));

        System.out.println("LastInteger:" + multiListener.getLastInteger());
        System.out.println("LastLong:" + multiListener.getLastLong());
    }
}
