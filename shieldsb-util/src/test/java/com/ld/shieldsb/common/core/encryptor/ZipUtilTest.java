package com.ld.shieldsb.common.core.encryptor;

import org.testng.Assert;
import org.testng.annotations.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * zipUtil的单元测试类
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月30日 下午4:21:01
 *
 */
@Slf4j
public class ZipUtilTest {

    /**
     * 测试
     * 
     * @Title test
     * @author 吕凯
     * @date 2021年26日 上午9:29:32
     * @return void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void test() {
        String str = "这是一个用于测试的字符串";
        String hex = ZipUtils.gzip(str);
        String unstr = ZipUtils.ungzip(hex);
        log.warn(hex);
        log.warn(unstr);
        Assert.assertEquals(unstr, str);

    }

}
