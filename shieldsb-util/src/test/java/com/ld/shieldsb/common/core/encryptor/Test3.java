package com.ld.shieldsb.common.core.encryptor;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Test3 {
    /**
     * 用JAVA实现字符串压缩算法。 将字符串 aaabcdda 编程实现将其转换为 3 a1b1c2d1a；**
     * 
     * @param str
     *            需要转换的字符串
     * @return string
     * @throws IOException
     */
    public static String encode(final String str) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ZipOutputStream zout = new ZipOutputStream(out);
        zout.putNextEntry(new ZipEntry("0"));
        zout.write(str.getBytes());
        zout.closeEntry();
        byte[] compressed = out.toByteArray(); // --返回压缩后的字符串的字节数组
        return EncodeUtils.encodeBase642Str(compressed);
    }

    public static String decode(final String compressed) throws IOException {
        byte[] compbytes = EncodeUtils.decodeBase64(compressed);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ByteArrayInputStream in = new ByteArrayInputStream(compbytes);
        ZipInputStream zin = new ZipInputStream(in);

        zin.getNextEntry();

        byte[] buffer = new byte[1024];

        int offset = -1;

        while ((offset = zin.read(buffer)) != -1) {
            out.write(buffer, 0, offset);
        }

        byte[] uncompressed = out.toByteArray(); // --返回解压缩后的字符串的字节数组
        return new String(uncompressed);
    }

    public static String decompressionString(String tempString) {
        char[] tempBytes = tempString.toCharArray();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < tempBytes.length; i++) {
            char c = tempBytes[i];
            char firstCharacter = (char) (c >>> 8);
            char secondCharacter = (char) ((byte) c);
            sb.append(firstCharacter);
            if (secondCharacter != 0) {
                sb.append(secondCharacter);
            }
        }
        return sb.toString();
    }

    public static String compactString(String tempString) {
        StringBuffer sb = new StringBuffer();
        byte[] tempBytes = tempString.getBytes();
        for (int i = 0; i < tempBytes.length; i += 2) {
            char firstCharacter = (char) tempBytes[i];
            char secondCharacter = 0;
            if (i + 1 < tempBytes.length) {
                secondCharacter = (char) tempBytes[i + 1];
            }
            firstCharacter <<= 8;
            sb.append((char) (firstCharacter + secondCharacter));
        }
        return sb.toString();
    }

    /**
     * @param sourceString
     * @param password
     * @return 密文
     */
    public static String encrypt(String sourceString, String password) {
        char[] p = password.toCharArray(); // 字符串转字符数组
        int n = p.length; // 密码长度
        char[] c = sourceString.toCharArray();
        int m = c.length; // 字符串长度
        for (int k = 0; k < m; k++) {
            int mima = c[k] + p[k / n]; // 加密
            c[k] = (char) mima;
        }
        return new String(c);
    }

    /**
     *
     * @param sourceString
     * @param password
     * @return 明文
     */
    public static String decrypt(String sourceString, String password) {
        char[] p = password.toCharArray(); // 字符串转字符数组
        int n = p.length; // 密码长度
        char[] c = sourceString.toCharArray();
        int m = c.length; // 字符串长度
        for (int k = 0; k < m; k++) {
            int mima = c[k] - p[k / n]; // 解密
            c[k] = (char) mima;
        }
        return new String(c);
    }

    public static void main(String[] args) {
        String ss = "website";
        String decode;
        try {
            String encode = encode(ss);
            decode = encode(encode);
            System.out.println(encode);
            System.out.println(decode);
            encode = compactString(ss);
            decode = decompressionString(encode);
            System.out.println(encode);
            System.out.println(decode);
            encode = encrypt(ss, "abcd");
            decode = decrypt(encode, "abcd");
            System.out.println(encode);
            System.out.println(decode);
        } catch (IOException e) {
            log.error("", e);
        }
    }
}
