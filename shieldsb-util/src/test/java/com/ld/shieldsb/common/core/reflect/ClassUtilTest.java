package com.ld.shieldsb.common.core.reflect;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ld.shieldsb.common.core.io.IOUtils;
import com.ld.shieldsb.common.core.io.img.ImageUtil;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * ClassUtil工具类的单元测试类
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年5月7日 上午8:54:54
 *
 */
@Slf4j
public class ClassUtilTest {

    /**
     * 
     * 测试
     * 
     * @Title test
     * @author 吕凯
     * @date 2021年5月7日 上午9:04:17 void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void test() {

        // 标识是否要遍历该包路径下子包的类名
        boolean recursive = true;
        // 指定的包名
//      String pkg = "javax.crypto.spec";// 为java/jre6/lib/jce.jar，普通的java工程默认已引用  
        String pkg = "com.ld.shieldsb.common.core.io";
        List<?> list = null;
//      list = getClassList(pkg, recursive, null);  
        // 增加 author.class的过滤项，即可只选出ClassTestDemo
        list = ClassUtil.getClassList(pkg, null, Object.class, recursive);

        for (int i = 0; i < list.size(); i++) {
            log.warn(i + ":" + list.get(i));
        }
        Assert.assertTrue(list.contains(IOUtils.class)); // 期望结果：：true，包含此类
        Assert.assertTrue(list.contains(ImageUtil.class)); // 期望结果：：true，包含子包下的此类
        log.warn("类的个数：{}", list.size());
        Assert.assertTrue(list.size() > 12); // 期望结果：：true，个数超过12个

        list = ClassUtil.getClassList(pkg, "img", Object.class, recursive);

        for (int i = 0; i < list.size(); i++) {
            log.warn(i + ":" + list.get(i));
        }
        Assert.assertTrue(list.contains(ImageUtil.class)); // 期望结果：：true，包含子包下的此类
        log.warn("类的个数：{}", list.size());
        Assert.assertEquals(list.size(), 5); // 期望结果：：相等，个数为5，包含一个内部类

    }

    /**
     * 测试类型转换
     * 
     * @Title testTypeParse
     * @author 吕凯
     * @date 2021年5月7日 上午9:40:14 void
     */
    @Test(priority = 1) // 不指定顺序时默认按字母顺序执行
    public void testTypeParse() {
        String tt = null;
        Assert.assertEquals(ClassUtil.obj2int(tt), 0); // 期望结果：：相等，默认0
        Assert.assertNull(ClassUtil.obj2int(tt, null)); // 期望结果：：null，
        Assert.assertEquals(ClassUtil.obj2long(tt), 0); // 期望结果：：相等，默认0

        String tt2 = "ss";
        Assert.assertEquals(ClassUtil.obj2long(tt2), 0); // 期望结果：：相等，默认0
        Assert.assertEquals(ClassUtil.obj2int(tt2), 0); // 期望结果：：相等，默认0
        Assert.assertEquals(ClassUtil.obj2doulbe(tt2), 0D); // 期望结果：：相等，默认0

        Assert.assertEquals(ClassUtil.obj2BigDecimal("123").doubleValue(), 123D); // 期望结果：：相等，默认0
        Assert.assertNull(ClassUtil.obj2BigDecimal("t123", null)); // 期望结果：：null，转换错误

        Assert.assertNull(ClassUtil.obj2T(tt, int.class)); // 期望结果：：null，
        Assert.assertNull(ClassUtil.obj2T(tt, long.class)); // 期望结果：：null，
        Assert.assertNull(ClassUtil.obj2T(tt, float.class)); // 期望结果：：null，
        Assert.assertNull(ClassUtil.obj2T(tt, double.class)); // 期望结果：：null，
        Assert.assertNull(ClassUtil.obj2T(tt, boolean.class)); // 期望结果：：null，

        Assert.assertNull(ClassUtil.obj2T(tt2, int.class)); // 期望结果：：null，
        Assert.assertNull(ClassUtil.obj2T(tt2, long.class)); // 期望结果：：null，
        Assert.assertNull(ClassUtil.obj2T(tt2, float.class)); // 期望结果：：null，
        Assert.assertNull(ClassUtil.obj2T(tt2, double.class)); // 期望结果：：null，
        Assert.assertNull(ClassUtil.obj2T(tt2, boolean.class)); // 期望结果：：null，
        Assert.assertTrue(ClassUtil.obj2T("tRue", boolean.class)); // 期望结果：：true，
        Assert.assertFalse(ClassUtil.obj2T("false", boolean.class)); // 期望结果：：false，

        Date date = ClassUtil.obj2T("2021-05-01", Date.class);
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        log.warn("转换后的日期：{}", date);
        Assert.assertEquals(calendar.get(Calendar.DAY_OF_MONTH), 1); // 期望结果：：相等，
        Assert.assertEquals(calendar.get(Calendar.MONTH), 4); // 期望结果：：相等，从0开始

        date = ClassUtil.obj2T("2021-06-05 12:13:14", Date.class);
        calendar.setTime(date);
        log.warn("转换后的日期：{}", date);
        Assert.assertEquals(calendar.get(Calendar.DAY_OF_MONTH), 5); // 期望结果：：相等，
        Assert.assertEquals(calendar.get(Calendar.MONTH), 5); // 期望结果：：相等，从0开始
        Assert.assertEquals(calendar.get(Calendar.HOUR_OF_DAY), 12); // 期望结果：：相等
        Assert.assertEquals(calendar.get(Calendar.MINUTE), 13); // 期望结果：：相等
        Assert.assertEquals(calendar.get(Calendar.SECOND), 14); // 期望结果：：相等

        ModelT model = ClassUtil.getInstance("com.ld.shieldsb.common.core.reflect.ClassUtilTest$ModelT");
        log.warn("model实例：{}", model);
        log.warn("model中test：{}", ModelT.test);
        Assert.assertEquals(model.getClass(), ModelT.class); // 期望结果：：相等

        ModelTest modeltest = ClassUtil.getInstance("com.ld.shieldsb.common.core.reflect.ModelTest");
        log.warn("model实例：{}", modeltest);
        log.warn("model中test：{}", ModelTest.test);
        Assert.assertEquals(modeltest.getClass(), ModelTest.class); // 期望结果：：相等

    }

    @Data
    public class ModelT {
        private String name;
        private String sex;
        private Integer age;

        public static final String test = "test";

    }

}

@Data
class ModelTest {
    private String name;
    private String sex;
    private Integer age;

    public static String test = "test";

    static {
        test = "testafter";
    }

}
