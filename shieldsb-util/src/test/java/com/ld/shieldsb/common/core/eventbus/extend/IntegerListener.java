package com.ld.shieldsb.common.core.eventbus.extend;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;

public class IntegerListener {

    private Integer lastMessage;

    @Subscribe
    @AllowConcurrentEvents // 支持同步
    public void listen(Integer integer) {
        lastMessage = integer;
        System.out.println("Message:" + lastMessage);
    }

    public Integer getLastMessage() {
        return lastMessage;
    }
}
