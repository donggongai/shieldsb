package com.ld.shieldsb.common.core.reflect;

import java.lang.reflect.Field;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ld.shieldsb.common.core.reflect.FunctionUtil.Property;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * Function工具类的单元测试类
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月26日 上午9:29:32
 *
 */
@Slf4j
public class FunctionUtilTest {

    /**
     * 测试
     * 
     * @Title test
     * @author 吕凯
     * @date 2021年26日 上午9:29:32
     * @return void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void test() {

        String fieldName = getFieldName(Model::getName);
        Assert.assertEquals(fieldName, "name"); // 期望结果：：true

        String functionName = FunctionUtil.getFunctionsName(Model::getSex);
        Assert.assertEquals(functionName, "getSex"); // 期望结果：：true

        Field field = FunctionUtil.getFiled(Model::getAge);
        log.warn(field + "");
        Assert.assertEquals(field.getName(), "age"); // 期望结果：：相等，名称为name

        // 方法
        Field fieldShow = FunctionUtil.getFiled(Model::getShow);
        log.warn(fieldShow + "");
        Assert.assertNull(fieldShow); // 期望结果：：true，包含此类

        String classMethodName = FunctionUtil.getFunctionClassMethod(Model::getSex);
        log.warn(classMethodName);
        Assert.assertTrue(classMethodName.contains("getSex") && classMethodName.contains(Model.class.getName())); // 期望结果：：true，完整方法名包含getSex并且包含类名

    }

    public <T> String getFieldName(Property<T, ?> property) {
        return FunctionUtil.getFieldName(property);
    }

    /**
     * 内部类，测试用
     * 
     * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
     * @date 2021年5月7日 上午9:05:59
     *
     */
    @Data
    class Model {
        private String name;
        private String sex;
        private Integer age;

        public String getShow() {
            return "show";
        }
    }

}
