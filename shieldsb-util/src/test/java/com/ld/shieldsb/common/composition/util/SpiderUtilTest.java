package com.ld.shieldsb.common.composition.util;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * 蜘蛛工具类的单元测试类
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月16日 下午4:18:45
 *
 */
@Slf4j
public class SpiderUtilTest {

    @BeforeTest
    public void setUp() {
    }

    /**
     * 测试
     * 
     * @Title test
     * @author 吕凯
     * @date 2021年4月29日 下午2:05:37 void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void test() {

        log.warn(SpiderUtil.getRemoteURLContent("http://ip.ws.126.net/ipquery?ip="));
        String url = "https://www.toutiao.com/";
        log.warn(SpiderUtil.getRemoteURLContent(url));
//        log.warn(SpiderUtil.getRemoteURLContent4Ajax(url).toString());

    }

}
