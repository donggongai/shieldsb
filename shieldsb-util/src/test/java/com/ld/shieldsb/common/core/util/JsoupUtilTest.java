package com.ld.shieldsb.common.core.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.alibaba.fastjson.JSONObject;
import com.ld.shieldsb.common.core.io.IOUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * jsoup工具类的单元测试类
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月16日 下午4:18:45
 *
 */
@Slf4j
public class JsoupUtilTest {
    private static final String URL_UPLOAD = "http://124.42.8.131:1134/upload/ueditor/upload/redirect?action=uploadimage&encode=utf-8&sendRedirect=http://work.11315.cn:28800/uploadCallback&uploadGroup=test";

    /**
     * 上传图片
     * 
     * @Title testSenTextByIds
     * @author 吕凯
     * @date 2021年4月16日 上午9:17:34 void
     */
    @Test(priority = 10) // 不指定顺序时默认按字母顺序执行
    public void testUploadFile() {

        URL fileUrl = Thread.currentThread().getContextClassLoader().getResource("test.png");
        File file = new File(fileUrl.getFile());
        System.out.println(file.exists());
        JSONObject result = JsoupUtil.uploadFile(URL_UPLOAD, file, "file");
        System.out.println(result);
        // {"affixId":"170808993768275968","original":"test.png","size":"6072","state":"SUCCESS","title":"1618560837563075072.png","type":".png","url":"http://test.11315.com/ueditorgroup/softwarecenter/ueditor/jsp/upload/image/2021/04/16/1618560837563075072.png"}
        Assert.assertEquals(result.getString("state"), "SUCCESS");

        try (InputStream uploadIs = IOUtils.readAsInputStream("test.png")) {
            result = JsoupUtil.upload(URL_UPLOAD, uploadIs, "file", "test.png");
            System.out.println(result);
            Assert.assertEquals(result.getString("state"), "SUCCESS");
        } catch (IOException e) {
            log.error("单元测试出错！", e);
        }
    }

    @Test(priority = 9) // 不指定顺序时默认按字母顺序执行
    public void testClean() {
        String str = "<a style=\"font-size:12px; color:#0066cc;\" download=\"111.docx\" href=\"http://test.11315.com/ueditorgroup/laiyang/ueditor/jsp/upload/file/20210615/1623735968011050871.png\" title=\"1618562470919.png\">1618562470919.png</a>";

        String value = JsoupUtil.clean(str);
        System.err.println(value);
    }

}
