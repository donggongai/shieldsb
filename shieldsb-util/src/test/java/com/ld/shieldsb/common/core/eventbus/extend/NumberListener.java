package com.ld.shieldsb.common.core.eventbus.extend;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;

public class NumberListener {

    private Number lastMessage;

    @Subscribe
    @AllowConcurrentEvents // 支持同步
    public void listen(Number integer) {
        lastMessage = integer;
        System.out.println("Message:" + lastMessage);
    }

    public Number getLastMessage() {
        return lastMessage;
    }
}
