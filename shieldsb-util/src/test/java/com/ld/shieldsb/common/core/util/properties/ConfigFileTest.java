package com.ld.shieldsb.common.core.util.properties;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

/**
 * 测试邮件发送工具类的测试用例
 * 
 * @ClassName EmailUtilTest
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2018年12月26日 下午6:06:02
 *
 */
public class ConfigFileTest {

    @Test
    public void testYaml() {
        String key = "test.tt";
        Reporter.log("ConfigFileTest……yaml", true);
        ConfigFile config = ConfigFileBuilder.build("test.yml");
        String testVal = config.getString(key);
        Reporter.log("yaml test.tt=" + testVal, true);

        config.save(key, "mysql");
        testVal = config.getString(key);
        Reporter.log("yaml test.tt=" + testVal, true);
        Assert.assertEquals("mysql", testVal);
    }

    @Test
    public void testPropertes() {
        String key = "test.tt";
        Reporter.log("ConfigFileTest……properties", true);
        ConfigFile config = ConfigFileBuilder.build("test.properties");
        String testVal = config.getString(key);
        Reporter.log("properties test=" + testVal, true);

        config.save(key, "mysql");
        testVal = config.getString(key);
        Reporter.log("properties test=" + testVal, true);
        Assert.assertEquals("mysql", testVal);
    }

}
