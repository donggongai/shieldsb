package com.ld.shieldsb.common.core.util;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * IPUtil的单元测试类
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年5月7日 下午2:05:47
 *
 */
public class IPUtilTest {

    /**
     * 
     * 测试
     * 
     * @Title test
     * @author 吕凯
     * @date 2021年5月7日 上午9:04:17 void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void test() {
        Assert.assertFalse(IPUtil.isInRange("10.153.48.127", "10.153.48.0/26")); // 期望结果：：false
        // 10.153.48.0/26，26代表26个1，写成二进制就是11111111.11111111.11111111.11000000，即
        // 子网掩码255.255.255.192（11111111.11111111.11111111.11000000）
        // 子网掩码的最后一位为192，其对应的二进制为11000000，一共有6个0，但主机号不能全为1或0，所以可以容纳的机器数目为2的六次方减去2共62台计算机（网络地址与广播地址，头尾的IP），减去网关（第一个可用IP），剩余61个可用IP。
        // ip段为（124.42.8.0-124.42.8.63）
        Assert.assertTrue(IPUtil.isInRange("10.153.48.61", "10.153.48.0/26")); // 期望结果：：true

        // 10.168.0.224/23，23代表23个1，写成二进制就是11111111.11111111.11111110.00000000，即子网掩码255.255.254.0（11111111.11111111.11111110.00000000）2的9次方-2，加一个广播地址，511
        // ip段为（10.168.0.224-10.168.0.715）=（10.168.0.0-10.168.1.255）
        Assert.assertTrue(IPUtil.isInRange("10.168.1.2", "10.168.0.224/23")); // 期望结果：：true
        Assert.assertTrue(IPUtil.isInRange("10.168.0.25", "10.168.0.224/23")); // 期望结果：：true

        Assert.assertTrue(IPUtil.isInRange("192.168.1.211", "192.168.1.211")); // 期望结果：：true// 255.255.255.0

        // 10.168.0.0/32，32代表32个1，写成二进制就是11111111.11111111.11111111.11111111，
        // ip段为（10.168.0.0-10.168.0.0）
        Assert.assertTrue(IPUtil.isInRange("10.168.0.0", "10.168.0.0/32")); // 期望结果：：true
        Assert.assertFalse(IPUtil.isInRange("10.168.0.1", "10.168.0.0/32")); // 期望结果：：false

        Assert.assertFalse(IPUtil.isInRange("192.168.1.211", "192.168.1.100-192.168.1.210")); // 期望结果：：false
        Assert.assertTrue(IPUtil.isInRange("192.168.1.211", "192.168.1.100-192.168.1.211")); // 期望结果：：true
    }

}
