package com.ld.shieldsb.common.core.model;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * 配置文件测试类，需要配合src/test/resources/properties/config_test.properties，因为存在密码泄露的风险，该文件需要自己创建
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年5月6日 下午4:15:21
 *
 */
@Slf4j
public class PropertiesModelTest {

    /**
     * 测试
     * 
     * @Title testBase
     * @author 吕凯
     * @date 2021年5月6日 下午4:16:03 void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void testBase() {
        PropertiesModel.CONFIG.save("tt.my", "100^120^110");
        List<Object> values = PropertiesModel.CONFIG.getList("tt.my");
        log.warn("{}", values);
        Assert.assertEquals(values.size(), 3); // 期望结果：：相等，集合个数为3

        PropertiesModel.CONFIG.save("tt.my1", "100-120-110");
        String valueStr = PropertiesModel.CONFIG.getString("tt.my1");
        Assert.assertEquals(valueStr, "100-120-110"); // 期望结果：：相等

        String valueStr2 = PropertiesModel.CONFIG.getString("tt.my2", "nothing");
        Assert.assertEquals(valueStr2, "nothing"); // 期望结果：：相等,等于默认值

        log.warn("{}", PropertiesModel.CONFIG.getPropertys());
    }

}
