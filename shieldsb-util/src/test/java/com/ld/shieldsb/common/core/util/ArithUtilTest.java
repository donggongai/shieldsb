package com.ld.shieldsb.common.core.util;

import org.testng.Assert;
import org.testng.annotations.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * 算术工具类的单元测试类，由于Java的简单类型不能够精确的对浮点数进行运算，这个工具类提供精 确的浮点数运算，包括加减乘除和四舍五入。
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年5月7日 下午2:05:47
 *
 */
@Slf4j
public class ArithUtilTest {

    /**
     * 
     * 测试
     * 
     * @Title test
     * @author 吕凯
     * @date 2021年5月7日 上午9:04:17 void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void test() {

        double[] a = { 0.84, 0.42, 0.14, 0.36, 0.36 };
        double result = 0;
        double result1 = 0;
        for (int i = 0; i < a.length; i++) {
            if (i == 0) {
                result = a[0];
                result1 = a[0];
            } else {
                result += a[i];
                result1 = ArithUtil.add(result1, a[i]);
            }
        }
        log.warn("计算数组{}的和", a);
        log.warn("直接计算结果为：{}", result);
        log.warn("使用ArithUtil结果为：{}", result1);
        Assert.assertEquals(result1, 2.12); // 期望结果：：相等

        double addV = 0.1 + 0.2;
        log.warn("0.1 + 0.2 = {}", addV);
        Assert.assertNotEquals(addV, 0.3); // 期望结果：：不相等，和为0.30000000000000004
        Assert.assertEquals(ArithUtil.add(0.1, 0.2), 0.3); // 期望结果：：相等，

        double subV = 0.3 - 0.1;
        log.warn("0.3 - 0.1 = {}", subV);
        Assert.assertNotEquals(subV, 0.2); // 期望结果：：不相等，差为0.19999999999999998
        Assert.assertEquals(ArithUtil.sub(0.2, 0.1), 0.1); // 期望结果：：相等，

        double mulV = 0.1 * 0.2;
        log.warn("0.1 * 0.2 = {}", mulV);
        Assert.assertNotEquals(mulV, 0.02); // 期望结果：：不相等，乘积为0.020000000000000004
        Assert.assertEquals(ArithUtil.mul(0.1, 0.2), 0.02); // 期望结果：：相等，

        double divV = 3 / 2;
        log.warn("3 / 2 = {}", divV);
        Assert.assertNotEquals(divV, 1.5D); // 期望结果：：不相等，结果为1
        Assert.assertEquals(ArithUtil.div(3, 2), 1.5); // 期望结果：：相等，

        String ss = "0.100";
        String resultStr = ArithUtil.subZeroAndDot(ss);
        log.warn("去掉多余的0和小数点后 = {}", resultStr);
        Assert.assertEquals(resultStr, "0.1"); // 期望结果：：相等，最后是多余的0

        ss = "15.";
        resultStr = ArithUtil.subZeroAndDot(ss);
        log.warn("去掉多余的0和小数点后 = {}", resultStr);
        Assert.assertEquals(resultStr, "15"); // 期望结果：：相等，最后一位是点

        ss = "15.1";
        resultStr = ArithUtil.subZeroAndDot(ss);
        log.warn("去掉多余的0和小数点后 = {}", resultStr);
        Assert.assertEquals(resultStr, ss); // 期望结果：：相等，正常的不受影响

    }

}
