package com.ld.shieldsb.common.core.util.notice.wechat;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.ld.shieldsb.common.core.io.IOUtils;
import com.ld.shieldsb.common.core.model.Result;
import com.ld.shieldsb.common.core.util.notice.wechat.application.EweChatApplicationUtil;
import com.ld.shieldsb.common.core.util.notice.wechat.application.EwechatResponse;
import com.ld.shieldsb.common.core.util.notice.wechat.application.taskcard.EweChatTaskCardBtn;
import com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.TmplCardActionModel;
import com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.TmplCardHorizontalContentModel;
import com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.TmplCardMainTitleModel;
import com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.TmplCardSourceModel;
import com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.news.TemplateCardNewsModel;
import com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.news.TmplCardImageModel;
import com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.text.TemplateCardTextModel;
import com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.text.TmplCardEmphasisContentModel;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 用于企业微信自定义或者第三方应用的工具类的测试类,需要配合src/test/resources/properties/config_test.properties，因为存在密码泄露的风险，该文件需要自己创建
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月19日 下午3:16:03
 *
 */
@Slf4j
public class EweChatApplicationUtilTest {
    private String sendTo = "lvkai";

    /*   id转译
     *  1.支持的消息类型和对应的字段
    消息类型    支持字段
    文本（text）    content
    文本卡片（textcard）  title、description
    图文（news）    title、description
    图文（mpnews）  title、digest、content
    任务卡片（taskcard）  title、description
    小程序通知（miniprogram_notice）   title、description、content_item.value    
    
    2.id转译模版语法
    $departmentName=DEPARTMENT_ID$
    $userName=USERID$
    其中 DEPARTMENT_ID 是数字类型的部门id，USERID 是成员帐号。
    譬如，
    将$departmentName=1$替换成部门id为“1”对应的部门名，如“企业微信产品部”；
    将$userName=lisi007$替换成userid为“lisi007”对应的用户名，如“李四”；*/

    /**
     * 
     * 测试发送文本消息
     * 
     * @Title testSendText
     * @author 吕凯
     * @date 2021年4月19日 下午3:19:51 void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void testSendTextMsg() {

        String msg = "单元测试：向自定义应用发送文本消息。打开<a href=\"http://work.11315.cn:28800\">论坛</a>";
        Result result = EweChatApplicationUtil.sendTextMsg(sendTo, msg); // 注意不带参数的话，默认10分钟内不发送重复消息，想更改重复信息设置
        log.warn(result.toString());
        Assert.assertTrue(result.getSuccess());

        // 重复不会发送
        result = EweChatApplicationUtil.sendTextMsg(sendTo, msg); // 注意不带参数的话，默认10分钟内不发送重复消息，想更改重复信息设置
        log.warn(result.toString());
        Assert.assertTrue(result.getSuccess());

        // 重复也会发送
        result = EweChatApplicationUtil.sendTextMsg(sendTo, msg, -1); // 内容重复也会发送,其他格式的信息也会检查重复，参照此例子
        log.warn(result.toString());
        Assert.assertTrue(result.getSuccess());

        msg = "单元测试：向自定义应用发送文本消息（id转名字）$userName=lvkai$。打开<a href=\"http://work.11315.cn:28800\">论坛</a>";
        result = EweChatApplicationUtil.sendTextMsg(sendTo, msg);
        log.warn(result.toString());
        Assert.assertTrue(result.getSuccess());

    }

    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void testRecallMsg() {

//        String msgid = "Dv0oBVNA9p2BIWPODPqgkgcWV2Uj6-oAO-cDgpVsMJ4KLPHKaAyrRyhPMjIic1J7V4iWiS4tODr3b1Ak_7qI0A";
//        Result result = EweChatApplicationUtil.recallMsg(msgid); // 撤回
//        log.warn(result.toString());
//        Assert.assertTrue(result.getSuccess());

        // 撤回
        String msg = "单元测试：测试撤回";
        Result result = EweChatApplicationUtil.sendTextMsg(sendTo, msg); // 注意不带参数的话，默认10分钟内不发送重复消息，想更改重复信息设置
        log.warn(result.toString());
        Assert.assertTrue(result.getSuccess());
        if (result.getSuccess()) {
            // 为了观察效果延迟1.5秒
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                log.error("", e);
            }
            EwechatResponse response = (EwechatResponse) result.getData();
            result = EweChatApplicationUtil.recallMsg(response.getMsgid()); // 撤回
            log.warn(result.toString());
            Assert.assertTrue(result.getSuccess());
        }

    }

    /**
     * 发送文本卡片格式的消息
     * 
     * @Title testSendNewsCardMsg
     * @author 吕凯
     * @date 2021年4月20日 上午8:55:16 void
     */
    @Test(priority = 1) // 不指定顺序时默认按字母顺序执行
    public void testSendTextCardMsg() {

        String msg = "<div class=\"gray\">2021年4月19日</div><br/>单元测试：向自定义应用发送文本卡片消息。<div class=\"normal\">恭喜你抽中iPhone 7一台，领奖码：xxxx</div><div class=\"highlight\">请于2016年10月10日前联系行政同事领取</div>";
        Result result = EweChatApplicationUtil.sendTextCardMsg(sendTo, "文本卡片", msg, "http://work.11315.cn:28800");
        log.warn(result.toString());
        System.out.println(result.toString());
        Assert.assertTrue(result.getSuccess());

    }

    /**
     * 发送图文消息
     * 
     * @Title testSendNewsCardMsg
     * @author 吕凯
     * @date 2021年4月20日 上午8:55:16 void
     */
    @Test(priority = 2) // 不指定顺序时默认按字母顺序执行
    public void testSendNewsMsg() {

        String msg = "单元测试：向自定义应用发送图文消息。今年中秋节公司有豪礼相送";
        Result result = EweChatApplicationUtil.sendNewsMsg(sendTo, "中秋节礼品领取", msg, "http://work.11315.cn:28800",
                "http://res.mail.qq.com/node/ww/wwopenmng/images/independent/doc/test_pic_msg1.png");
        log.warn(result.toString());
        System.out.println(result.toString());
        Assert.assertTrue(result.getSuccess());

    }

    /**
     * 发送markdown格式的消息，不支持id转译人名
     * 
     * @Title testSendMarkdownMsg
     * @author 吕凯
     * @date 2021年4月20日 上午8:54:58 void
     */
    @Test(priority = 4) // 不指定顺序时默认按字母顺序执行
    public void testSendMarkdownMsg() {

        // info 绿色 comment 灰色 warning 橙红色
        String content = "单元测试：向自定义应用发送Markdown消息，测试结果反馈<font color=\"warning\">24</font>个，请注意修改。\n" //
                + " >BUG:<font color=\"comment\">10个</font>\n" //
                + " >建议:<font color=\"comment\">12个</font>\n" //
                + " >咨询:<font color=\"comment\">2个</font>"; // markdown信息

        Result result = EweChatApplicationUtil.sendMarkdownMsg(sendTo, content);
        log.warn(result.toString());
        System.out.println(result.toString());
        Assert.assertTrue(result.getSuccess());

        // 加粗，a链接
        content = "单元测试：向自定义应用发送Markdown消息\n" //
                + "# xx功能需求分析完成\n" //
                + "请尽快安排开发，此功能需要在**7月7日**前完成<@lvkai>\n" //
                + "`Assert.assertEquals(result.getInteger(EWeChatWebHookUtil.ERRCODE), (Integer) 0);`\n" //
                + "参考代码：[鲸落论坛](http://work.11315.cn:28800)\n"; // markdown信息

        result = EweChatApplicationUtil.sendMarkdownMsg(sendTo, content);
        log.warn(result.toString());
        System.out.println(result.toString());
        Assert.assertTrue(result.getSuccess());

        // 引用文字
        content = "单元测试：向自定义应用发送Markdown消息，您的会议室已经预定，稍后会同步到`邮箱` \n" + //
                "   >**事项详情** \n" + //
                "   >事　项：<font color=\"info\">开会</font> \n" + //
                "   >组织者：吕凯 \n" + //
                "   >参与者：吕凯、武杨、张和祥、刘金浩 \n" + //
                "   > \n" + //
                "   >会议室：<font color=\"info\">广州TIT 1楼 301</font> \n" + //
                "   >日　期：<font color=\"warning\">2018年5月18日</font> \n" + //
                "   >时　间：<font color=\"comment\">上午9:00-11:00</font> \n" + //
                "   > \n" + //
                "   >请准时参加会议。 \n" + //
                "   > \n" + //
                "   >如需修改会议信息，请点击：[修改会议信息](https://work.weixin.qq.com)"; // markdown信息

        result = EweChatApplicationUtil.sendMarkdownMsg(sendTo, content);
        log.warn(result.toString());
        System.out.println(result.toString());
        Assert.assertTrue(result.getSuccess());

    }

    /**
     * 发送TaskCard消息
     * 
     * @Title testSendTaskCardMsg
     * @author 吕凯
     * @date 2021年4月20日 上午9:44:58 void
     */
    @Test(priority = 5) // 不指定顺序时默认按字母顺序执行
    public void testSendTaskCardMsg() {

        // info 绿色 comment 灰色 warning 橙红色
        String content = "单元测试：向自定义应用发送任务卡片消息，测试结果反馈<font color=\"warning\">24</font>个，请注意修改。\n" //
                + " >BUG:<font color=\"comment\">10个</font>\n" //
                + " >建议:<font color=\"comment\">12个</font>\n" //
                + " >咨询:<font color=\"comment\">2个</font>"; // markdown信息
        List<EweChatTaskCardBtn> btns = new ArrayList<>();
        EweChatTaskCardBtn btnOk = new EweChatTaskCardBtn();
        btnOk.setKey("keyok");
        btnOk.setName("批准");
        btnOk.setIsBold(true);
        EweChatTaskCardBtn btnCancel = new EweChatTaskCardBtn();
        btnCancel.setKey("keyCancel");
        btnCancel.setName("驳回");
        btnCancel.setColor("red");
        btns.add(btnOk);
        btns.add(btnCancel);

        Result result = EweChatApplicationUtil.sendTaskCardMsg(sendTo, UUID.randomUUID().toString(), "测试任务卡片", content,
                "http://work.11315.cn:28800/", btns);
        log.warn(result.toString());
        System.out.println(result.toString());
        Assert.assertTrue(result.getSuccess());

    }

    /**
     * 测试更新任务卡片
     * 
     * @Title testSendTaskCardMsg
     * @author 吕凯
     * @date 2021年4月21日 上午9:22:51 void
     */
    @Test(priority = 6) // 不指定顺序时默认按字母顺序执行
    public void testUpdateTaskCardMsg() {

        // info 绿色 comment 灰色 warning 橙红色
        String content = "单元测试：向自定义应用发送任务卡片消息并更新"; // markdown信息
        List<EweChatTaskCardBtn> btns = new ArrayList<>();
        EweChatTaskCardBtn btnOk = new EweChatTaskCardBtn();
        btnOk.setKey("keyok");
        btnOk.setName("通过");
        btnOk.setColor("red");
        btnOk.setIsBold(true);
        EweChatTaskCardBtn btnCancel = new EweChatTaskCardBtn();
        btnCancel.setKey("keyCancel");
        btnCancel.setName("退回");
        btns.add(btnOk);
        btns.add(btnCancel);

        String taskId = UUID.randomUUID().toString();
        Result result = EweChatApplicationUtil.sendTaskCardMsg(sendTo, taskId, "测试任务卡片", content, "http://work.11315.cn:28800/", btns);
        log.warn(result.toString());
        System.out.println(result.toString());
        Assert.assertTrue(result.getSuccess());

        // 为了观察效果延迟1.5秒
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            log.error("", e);
        }
        result = EweChatApplicationUtil.updateTaskcard(sendTo, taskId, "已完成");
        log.warn(result.toString());
        System.out.println(result.toString());
        Assert.assertTrue(result.getSuccess());

    }

    /**
     * 
     * 测试上传文件
     * 
     * @Title testUploadFile
     * @author 吕凯
     * @date 2021年4月21日 上午10:35:06 void
     */
    @Test(priority = 7) // 不指定顺序时默认按字母顺序执行
    public void testUploadFile() {
        // 通过流上传
        try (InputStream uploadIs = IOUtils.readAsInputStream("upload.txt")) {

            JSONObject result = EweChatApplicationUtil.uploadFile(uploadIs, "test.txt");
            System.out.println(result.toString());
            // {"errcode":0,"errmsg":"ok","media_id":"3OUFL2ceut5mI4-SjhVEzYwkuugJYCdu0J-uEoBHF_c0","created_at":"1618972558","type":"file"}
            Assert.assertEquals(result.getInteger(EweChatTool.ERRCODE), (Integer) 0);
        } catch (IOException e) {
            log.error("单元测试出错！", e);
        }
        // 通过文件上传
        URL fileUrl = Thread.currentThread().getContextClassLoader().getResource("test.png");
        File file = new File(fileUrl.getFile());
        System.out.println(file.exists());
        JSONObject result = EweChatApplicationUtil.uploadFile(file);
        System.out.println(result.toString());
        // {"errcode":0,"errmsg":"ok","media_id":"3OUFL2ceut5mI4-SjhVEzYwkuugJYCdu0J-uEoBHF_c0","created_at":"1618972558","type":"file"}

        Assert.assertEquals(result.getInteger(EweChatTool.ERRCODE), (Integer) 0);

    }

    /**
     * 
     * 测试文件信息，注意文件有效期为3天，过期无效
     * 
     * @Title testSendFileMsg
     * @author 吕凯
     * @date 2021年4月21日 上午10:36:30 void
     */
    @Test(priority = 8) // 不指定顺序时默认按字母顺序执行
    public void testSendFileMsg() {
        // 通过流上传
        try (InputStream uploadIs = IOUtils.readAsInputStream("upload.txt")) {

            Result result = EweChatApplicationUtil.sendFileMsg(sendTo, uploadIs, "test.txt");
            System.out.println(result.toString());
            // {"errcode":0,"errmsg":"ok","media_id":"3QtSsDv5R-shKSFuMM0BWFCauDwhbBmWqbz_uqGT9xO0","created_at":"1618812990","type":"file"}
            Assert.assertTrue(result.getSuccess());
        } catch (IOException e) {
            log.error("单元测试出错！", e);
        }

        // 通过文件上传
        URL fileUrl = Thread.currentThread().getContextClassLoader().getResource("test.png");
        File file = new File(fileUrl.getFile());
        System.out.println(file.exists());
        Result result = EweChatApplicationUtil.sendFileMsg(sendTo, file);
        System.out.println(result.toString());
        // {"errcode":0,"errmsg":"ok","media_id":"3QtSsDv5R-shKSFuMM0BWFCauDwhbBmWqbz_uqGT9xO0","created_at":"1618812990","type":"file"}

        Assert.assertTrue(result.getSuccess());

    }

    /**
     * 发送存储在企业微信的图文消息(类似于新闻)，跟普通的图文消息一致，唯一的差异是图文内容存储在企业微信。多次发送mpnews，会被认为是不同的图文，阅读、点赞的统计会被分开计算。
     * 
     * @Title testSendMpNewsMsg
     * @author 吕凯
     * @date 2021年4月21日 上午10:52:28 void
     */
    @Test(priority = 9) // 不指定顺序时默认按字母顺序执行
    public void testSendMpNewsMsg() {
        // 通过文件上传
        URL fileUrl = Thread.currentThread().getContextClassLoader().getResource("test.png");
        File file = new File(fileUrl.getFile());
        System.out.println(file.exists());
        String title = "单元测试";
        String author = "Kevin Lv";
        String url = "http://work.11315.cn:28800/"; // 阅读原文的地址
        String content = "发送存储在企业微信的图文消息，跟普通的图文消息一致，唯一的差异是图文内容存储在企业微信。多次发送mpnews，会被认为是不同的图文，阅读、点赞的统计会被分开计算。";
        String digest = "这是一个谜";
        Result result = EweChatApplicationUtil.sendMpNewsMsg(sendTo, file, title, author, url, content, digest);
        System.out.println(result.toString());

        Assert.assertTrue(result.getSuccess());

    }

    /**
     * 工作台模板设置,测试发现只在手机端生效
     * 
     * @Title testSetWorkbenchTemplate
     * @author 吕凯
     * @date 2021年5月20日 上午9:07:37 void
     */
//    @Test(priority = 10) // 不指定顺序时默认按字母顺序执行
    public void testSetWorkbenchTemplate() {
        // 参考文档：https://work.weixin.qq.com/api/doc/90000/90135/92535
        // 图片型
        Map<String, Object> data = new HashMap<>();
        data.put("type", "image");
        data.put("image", ImmutableMap.of("url", "http://work.11315.cn:28800/bbs/v2/images/ind-banner.jpg?20210304184008", "jump_url",
                "http://work.11315.cn:28800"));
        Result result = EweChatApplicationUtil.setWorkbenchTemplate(data);
        System.out.println(result.toString());

        Assert.assertTrue(result.getSuccess());

    }

    /**
     * 发送模块卡片-文本通知型消息
     * 
     * @Title testSendTaskCardMsg
     * @author 吕凯
     * @date 2021年4月20日 上午9:44:58 void
     */
    @Test(priority = 11) // 不指定顺序时默认按字母顺序执行
    public void testSendTemplCardTextMsg() {

        String content = "单元测试：向自定义应用发送任务卡片消息，测试结果反馈24，请注意修改。\n" //
                + " BUG:10个\n" //
                + " 建议:12个\n" //
                + " 咨询:2个"; // 文本信息

        Result result = EweChatApplicationUtil.sendTemplateCardTextMsg(sendTo, "测试模板卡片文本通知", content, "http://work.11315.cn:28800/");
        log.warn(result.toString());
        System.out.println(result.toString());
        Assert.assertTrue(result.getSuccess());

        // mainTitle、subTitleText必须有，其他可无
        TmplCardSourceModel source = TmplCardSourceModel.builder().iconUrl("http://work.11315.cn:28800/favicon.ico").desc("工作平台").build();
        TmplCardActionModel cardAction = TmplCardActionModel.builder().type(EweChatTool.ACTION_TYPE_URL)
                .url("http://work.11315.cn:28800/a/#/oaManage/oa_attendance_my").build();
        TmplCardMainTitleModel mainTitle = TmplCardMainTitleModel.builder().title("一级标题说明").desc("一级标题说明").build();
        TmplCardEmphasisContentModel emphasisContent = TmplCardEmphasisContentModel.builder().title("一级标题下核心数据").desc("一级标题下核心数据说明")
                .build();
        // 二级标题+文本列表，该字段可为空数组，但有数据的话需确认对应字段是否必填，列表长度不超过6
        TmplCardHorizontalContentModel horizontalContent1 = TmplCardHorizontalContentModel.build("主办单位", "谁知道呢？");
        TmplCardHorizontalContentModel horizontalContent2 = TmplCardHorizontalContentModel.buildUrl("网址", "点击查看",
                "http://work.11315.cn:28800/");
        // 跳转链接，最多3个，可无
        TmplCardActionModel jump = TmplCardActionModel.buildUrl("出勤管理", "http://work.11315.cn:28800/a/#/oaManage");

        TemplateCardTextModel model = TemplateCardTextModel.builder().source(source)
                // 标题必须有，但是里面的描述可以无
                .mainTitle(mainTitle).emphasisContent(emphasisContent)
                // 内容必须有
                .subTitleText(content).horizontalContentList(ImmutableList.of(horizontalContent1, horizontalContent2))
                .jumpList(ImmutableList.of(jump)).cardAction(cardAction).build();

        result = EweChatApplicationUtil.sendTemplateCardTextMsg(sendTo, model);
        log.warn(result.toString());
        System.out.println(result.toString());
        Assert.assertTrue(result.getSuccess());

    }

    /**
     * 
     * 发送模块卡片-图文展示型消息
     * 
     * @Title testSendTemplCardNewsMsg
     * @author 吕凯
     * @date 2021年9月13日 下午2:56:35 void
     */
    @Test(priority = 12) // 不指定顺序时默认按字母顺序执行
    public void testSendTemplCardNewsMsg() {

        String imagePath = "http://img.zcool.cn/community/0292505760bc830000018c1b0a8941.jpg@800w_1l_2o"; // 文本信息

        Result result = EweChatApplicationUtil.sendTemplateCardNewsMsg(sendTo, "测试模板卡片图片展示型", imagePath, "http://work.11315.cn:28800/");
        log.warn(result.toString());
        System.out.println(result.toString());
        Assert.assertTrue(result.getSuccess());

        // mainTitle、subTitleText必须有，其他可无
        TmplCardSourceModel source = TmplCardSourceModel.builder().iconUrl("http://work.11315.cn:28800/favicon.ico").desc("工作平台").build();
        TmplCardActionModel cardAction = TmplCardActionModel.builder().type(EweChatTool.ACTION_TYPE_URL)
                .url("http://work.11315.cn:28800/a/#/oaManage/oa_attendance_my").build();
        TmplCardMainTitleModel mainTitle = TmplCardMainTitleModel.builder().title("一级标题说明").desc("一级标题说明").build();
        TmplCardImageModel cardImage = TmplCardImageModel.builder()
                .url("http://img.zcool.cn/community/0292505760bc830000018c1b0a8941.jpg@800w_1l_2o").build();
        // 二级标题+文本列表，该字段可为空数组，但有数据的话需确认对应字段是否必填，列表长度不超过6
        TmplCardHorizontalContentModel horizontalContent1 = TmplCardHorizontalContentModel.build("主办单位", "谁知道呢？");
        TmplCardHorizontalContentModel horizontalContent2 = TmplCardHorizontalContentModel.buildUrl("网址", "点击查看",
                "http://work.11315.cn:28800/");
        // 跳转链接，最多3个，可无
        TmplCardActionModel jump = TmplCardActionModel.buildUrl("出勤管理", "http://work.11315.cn:28800/a/#/oaManage");

        TemplateCardNewsModel model = TemplateCardNewsModel.builder().source(source)
                // 标题必须有，但是里面的描述可以无
                .mainTitle(mainTitle)
                // 图片
                .cardImage(cardImage).horizontalContentList(ImmutableList.of(horizontalContent1, horizontalContent2))
                .jumpList(ImmutableList.of(jump)).cardAction(cardAction).build();

        result = EweChatApplicationUtil.sendTemplateCardNewsMsg(sendTo, model);
        log.warn(result.toString());
        System.out.println(result.toString());
        Assert.assertTrue(result.getSuccess());

    }

    /**
     * 
     * 发送模块卡片-按钮交互型消息
     * 
     * @Title testSendTemplCardButtonMsg
     * @author 吕凯
     * @date 2021年9月13日 下午3:49:00 void
     */
    @Test(priority = 12) // 不指定顺序时默认按字母顺序执行
    public void testSendTemplCardButtonMsg() {

        String content = "这是一个高尚的按钮，一个纯粹的按钮，一个有道德的按钮，一个脱离了低级趣味的按钮，一个有益于程序员的按钮。"; // 文本信息
        String taskId = UUID.randomUUID().toString();
        Result result = EweChatApplicationUtil.sendTemplateCardButtonMsg(sendTo, taskId, "测试模板卡片按钮交互型", content,
                "http://work.11315.cn:28800/", "确认");
        log.warn(result.toString());
        System.out.println(result.toString());
        Assert.assertTrue(result.getSuccess());

    }
}
