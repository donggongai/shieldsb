package com.ld.shieldsb.common.core.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ld.shieldsb.common.core.util.date.DateFormatterUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 测试日期格式化
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月25日 上午8:47:46
 *
 */
@Slf4j
public class DateFormatterUtilTest {

    /**
     * 测试日期
     * 
     * @Title testDate
     * @author 吕凯
     * @date 2021年4月25日 上午9:27:22 void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void testDate() {
        String source = "2021年05月01号";

        Date fmtDate = DateFormatterUtil.convert(source);
        System.out.println(fmtDate);
        Assert.assertNotNull(fmtDate);

        Date initDate = new GregorianCalendar(2021, 4, 1).getTime(); // 注意月份从0开始，5月即为4
        System.out.println(initDate);
        Assert.assertEquals(fmtDate, initDate);

        source = "2021年5月1号";
        fmtDate = DateFormatterUtil.convert(source);
        System.out.println(fmtDate);
        Assert.assertNotNull(fmtDate);

        Assert.assertEquals(fmtDate, initDate);

        // 单长度月份检验
        source = "2021-5-1";
        fmtDate = DateFormatterUtil.convert(source);
        System.out.println(fmtDate);
        Assert.assertNotNull(fmtDate);

        Assert.assertEquals(fmtDate, initDate);

        // 双长度月份检验
        source = "2021-05-1";
        fmtDate = DateFormatterUtil.convert(source);
        System.out.println(fmtDate);
        Assert.assertNotNull(fmtDate);

        Assert.assertEquals(fmtDate, initDate);

        // 双长度月份、双长度日期检验
        source = "2021-05-01";
        fmtDate = DateFormatterUtil.convert(source);
        System.out.println(fmtDate);
        Assert.assertNotNull(fmtDate);

        Assert.assertEquals(fmtDate, initDate);

        // 双长度月份、双长度日期检验
        source = "2021-05-01";
        fmtDate = DateFormatterUtil.convert(source);
        System.out.println(fmtDate);
        Assert.assertNotNull(fmtDate);

        Assert.assertEquals(fmtDate, initDate);

    }

    /**
     * 测试时间
     * 
     * @Title testDateTime
     * @author 吕凯
     * @date 2021年4月25日 上午9:27:35 void
     */
    @Test(priority = 1) // 不指定顺序时默认按字母顺序执行
    public void testTime() {
        String source = "2021年05月01号13点14分52秒";

        Date fmtDate = DateFormatterUtil.convert(source);
        System.out.println(fmtDate);
        Assert.assertNotNull(fmtDate);

        Date initDate = new GregorianCalendar(2021, 4, 1, 13, 14, 52).getTime(); // 注意月份从0开始，5月即为4
        System.out.println(initDate);
        Assert.assertEquals(fmtDate, initDate);

        source = "2021年5月1号13点14分52秒";
        fmtDate = DateFormatterUtil.convert(source);
        System.out.println(fmtDate);
        Assert.assertNotNull(fmtDate);

        Assert.assertEquals(fmtDate, initDate);

        // 单长度月份检验
        source = "2021-5-1 13:14:52";
        fmtDate = DateFormatterUtil.convert(source);
        System.out.println(fmtDate);
        Assert.assertNotNull(fmtDate);

        Assert.assertEquals(fmtDate, initDate);

        // 双长度月份检验
        source = "2021-05-1 13:14:52";
        fmtDate = DateFormatterUtil.convert(source);
        System.out.println(fmtDate);
        Assert.assertNotNull(fmtDate);

        Assert.assertEquals(fmtDate, initDate);

        // 双长度月份、双长度日期检验
        source = "2021-05-01 13:14:52";
        fmtDate = DateFormatterUtil.convert(source);
        System.out.println(fmtDate);
        Assert.assertNotNull(fmtDate);

        Assert.assertEquals(fmtDate, initDate);

        // 双长度月份、双长度日期检验
        source = "2021-05-01 13:14:52";
        fmtDate = DateFormatterUtil.convert(source);
        System.out.println(fmtDate);
        Assert.assertNotNull(fmtDate);

        Assert.assertEquals(fmtDate, initDate);

    }

    /**
     * 测试时间到分
     * 
     * @Title testDateTime
     * @author 吕凯
     * @date 2021年4月25日 上午9:27:35 void
     */
    @Test(priority = 2) // 不指定顺序时默认按字母顺序执行
    public void testTimeMinute() {
        String source = "2021年05月01号13点14分";

        Date fmtDate = DateFormatterUtil.convert(source);
        System.out.println(fmtDate);
        Assert.assertNotNull(fmtDate);

        Date initDate = new GregorianCalendar(2021, 4, 1, 13, 14).getTime(); // 注意月份从0开始，5月即为4
        System.out.println(initDate);
        Assert.assertEquals(fmtDate, initDate);

        source = "2021年5月1号13点14分";
        fmtDate = DateFormatterUtil.convert(source);
        System.out.println(fmtDate);
        Assert.assertNotNull(fmtDate);

        Assert.assertEquals(fmtDate, initDate);

        source = "2021年5月1号13时14分";
        fmtDate = DateFormatterUtil.convert(source);
        System.out.println(fmtDate);
        Assert.assertNotNull(fmtDate);

        Assert.assertEquals(fmtDate, initDate);

        source = "2021年5月1日13时14分";
        fmtDate = DateFormatterUtil.convert(source);
        System.out.println(fmtDate);
        Assert.assertNotNull(fmtDate);

        Assert.assertEquals(fmtDate, initDate);

        // 单长度月份检验
        source = "2021-5-1 13:14";
        fmtDate = DateFormatterUtil.convert(source);
        System.out.println(fmtDate);
        Assert.assertNotNull(fmtDate);

        Assert.assertEquals(fmtDate, initDate);

        // 双长度月份检验
        source = "2021-05-1 13:14";
        fmtDate = DateFormatterUtil.convert(source);
        System.out.println(fmtDate);
        Assert.assertNotNull(fmtDate);

        Assert.assertEquals(fmtDate, initDate);

        // 双长度月份、双长度日期检验
        source = "2021-05-01 13:14";
        fmtDate = DateFormatterUtil.convert(source);
        System.out.println(fmtDate);
        Assert.assertNotNull(fmtDate);

        Assert.assertEquals(fmtDate, initDate);

        // 双长度月份、双长度日期检验
        source = "2021-05-01 13:14";
        fmtDate = DateFormatterUtil.convert(source);
        System.out.println(fmtDate);
        Assert.assertNotNull(fmtDate);

        Assert.assertEquals(fmtDate, initDate);

    }

    /**
     * 测试带毫秒的时间
     * 
     * @Title testDateTime
     * @author 吕凯
     * @date 2021年4月25日 上午9:27:35 void
     */
    @Test(priority = 3) // 不指定顺序时默认按字母顺序执行
    public void testTimeMillis() {
        String source = "2021-04-23 15:24:19.1";

        Date fmtDate = DateFormatterUtil.convert(source);
        System.out.println(fmtDate);
        Assert.assertNotNull(fmtDate);

        Calendar calendar = new GregorianCalendar(2021, 3, 23, 15, 24, 19);
//        calendar.set(Calendar.MILLISECOND, 1); 毫秒数忽略
        Date initDate = calendar.getTime(); // 注意月份从0开始，4月即为3
        System.out.println(initDate);
        Assert.assertEquals(fmtDate, initDate);

    }
}
