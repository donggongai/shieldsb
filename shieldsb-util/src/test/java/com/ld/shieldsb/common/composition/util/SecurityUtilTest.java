package com.ld.shieldsb.common.composition.util;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.ld.shieldsb.common.core.encryptor.AesEncryptor;
import com.ld.shieldsb.common.core.encryptor.BlowfishEncryptor;

import lombok.extern.slf4j.Slf4j;

/**
 * 加密工具类的单元测试类
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月16日 下午4:18:45
 *
 */
@Slf4j
public class SecurityUtilTest {

    @BeforeTest
    public void setUp() {
        SecurityUtil.setSimpleSecretKeyRw("wefoundlove"); // "wefoundlove"; // 简单加密的密钥（读写），在具体项目中赋值
        SecurityUtil.setSimpleSecretKeyR("wefoundlove_r"); // "wefoundlove_r"; // 简单加密的密钥（只读），在具体项目中赋值
        SecurityUtil.setDesSecretKey("we_found_love"); // "we_found_love"; // DES加密的密钥，在具体项目中赋值
        SecurityUtil.setEorSecretKeyR(123412); // // 异或运算的密钥（只读），在具体项目中赋值
    }

    /**
     * 测试
     * 
     * @Title test
     * @author 吕凯
     * @date 2021年4月29日 下午2:05:55 void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void test() {

        String sha1String = SecurityUtil.sha1("test");
        log.warn("SHA1加密后：{}", sha1String);
        Assert.assertEquals(sha1String, "a94a8fe5ccb19ba61c4c0873d391e987982fbbd3");

        String md5String = SecurityUtil.md5("test");
        log.warn("MD5加密后：{}", md5String);
        Assert.assertEquals(md5String, "098f6bcd4621d373cade4e832627b4f6");

        String desEncode = SecurityUtil.desEncrypt("test");
        log.warn("DES加密后：{}", desEncode);
        String desDecode = SecurityUtil.desDecrypt(desEncode);
        log.warn("DES解密后：{}", desDecode);
        Assert.assertEquals(desDecode, "test");

        String strBefore = "511702198209158764";
        AesEncryptor aes = new AesEncryptor("1l23o0g9s5aesl11l1315");
        aes.setIvParameters("123");
        String strAesEncode = aes.encrypt(strBefore);
        log.warn("AES加密后：{}", strAesEncode);
        String ssv = aes.decrypt(strAesEncode);
        log.warn("AES解密后：{}", ssv);
        Assert.assertTrue(aes.match(strBefore, strAesEncode, null));

        BlowfishEncryptor blowfishEncryptor = new BlowfishEncryptor("123");
        String strEncode = blowfishEncryptor.encrypt(strBefore);
        log.warn("Blowfish加密后：{}", strEncode);
        String strDecode = blowfishEncryptor.decrypt(strEncode);
        log.warn("Blowfish解密后：{}", strDecode);
        Assert.assertTrue(blowfishEncryptor.match(strBefore, strEncode, null));

        desEncode = SecurityUtil.queryEncrypt("test我");
        log.warn("EOR加密后：{}", desEncode);
        desDecode = SecurityUtil.queryDecrypt(desEncode);
        log.warn("EOR解密后：{}", desDecode);
        Assert.assertEquals(desDecode, "test我");

        desEncode = SecurityUtil.simpleEncrypt("test我");
        log.warn("simpleEncrypt后：{}", desEncode);
        desDecode = SecurityUtil.simpleDecrypt(desEncode);
        log.warn("simpleDecrypt后：{}", desDecode);
        Assert.assertEquals(desDecode, "test我");

        desEncode = SecurityUtil.simpleReadEncrypt("test我");
        log.warn("simpleReadEncrypt后：{}", desEncode);
        desDecode = SecurityUtil.simpleReadDecrypt(desEncode);
        log.warn("simpleReadDecrypt后：{}", desDecode);
        Assert.assertEquals(desDecode, "test我");

        String encrypt;
        String bcryptStr = "test";
        try {
            encrypt = SecurityUtil.bcryptEncrypt("test"); // 相同字符每次加密结果不同
            log.warn("Bcrypt加密字符{}后：{}", bcryptStr, encrypt);
            log.warn("Bcrypt加密字符{}后：{}", bcryptStr, SecurityUtil.bcryptEncrypt("test"));
            log.warn("Bcrypt加密字符{}后：{}", bcryptStr, SecurityUtil.bcryptEncrypt("test"));
            log.warn("Bcrypt加密字符{}后：{}", encrypt, SecurityUtil.bcryptEncrypt(encrypt));
            Assert.assertTrue(SecurityUtil.bcryptMatch("test", encrypt)); // 验证是否匹配
        } catch (Exception e) {
            log.error("", e);
        }

    }

}
