package com.ld.shieldsb.common.core.io.image;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ld.shieldsb.common.core.io.FileUtils;
import com.ld.shieldsb.common.core.io.img.ImageUtil;
import com.ld.shieldsb.common.core.io.img.WaterMaskSettings;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 图片工具类的单元测试类
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年5月6日 上午9:17:29
 *
 */
@Slf4j
public class ImageUtilTest {

    /**
     * 测试图片操作
     * 
     * @Title testBase
     * @author 吕凯
     * @date 2021年5月6日 上午9:17:15 void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void testBase() {
        URL fileUrl = Thread.currentThread().getContextClassLoader().getResource("test.png");
        String filePath = fileUrl.getFile();
        String filePathCopy = filePath.replace("test.png", "test-copy.png");
        FileUtils.copyFile(filePath, filePathCopy);
        File imageCopyFile = new File(filePathCopy);
        Assert.assertTrue(imageCopyFile.exists()); // 期望结果：：true，文件存在

        ImageUtil.setImageSize(imageCopyFile, 1200, 350);

        try {
            BufferedImage bImage = ImageUtil.read(imageCopyFile);
            Assert.assertEquals(bImage.getWidth(), 1200); // 期望结果：：相等
        } catch (IOException e) {
            log.error("读取图片失败", e);
        }

        try {
            String format = ImageUtil.getImageFormatName(imageCopyFile);
            Assert.assertEquals(format, "png"); // 期望结果：：相等
        } catch (IOException e) {
            log.error("获取格式失败", e);
        }

        boolean isSuppert = ImageUtil.isSupportImage(imageCopyFile);
        Assert.assertTrue(isSuppert); // 期望结果：：true

        FileUtils.delete(filePathCopy);

    }

    /**
     * 测试水印方法
     * 
     * @Title testWaterMark
     * @author 吕凯
     * @date 2021年5月6日 上午9:20:54 void
     */
    @Test(priority = 1) // 不指定顺序时默认按字母顺序执行
    public void testWaterMark() {
        URL fileUrl = Thread.currentThread().getContextClassLoader().getResource("test-f.jpg");
        String filePath = fileUrl.getFile();
        String filePathCopy = filePath.replace("test-f.jpg", "test-f-copy.png");
        FileUtils.copyFile(filePath, filePathCopy);
        File imageFile = new File(filePathCopy); // 原图
        Assert.assertTrue(imageFile.exists()); // 期望结果：：true，文件存在
        String filePathCopy2 = filePath.replace("test-f.jpg", "test-f-copy2.png");
        FileUtils.copyFile(filePath, filePathCopy2);
        File imageFile2 = new File(filePathCopy2); // 原图2
        Assert.assertTrue(imageFile2.exists()); // 期望结果：：true，文件存在

        String filePathWaterMask = filePath.replace("test-f.jpg", "test.png");
        File markFile = new File(filePathWaterMask);
        String waterMaskDir = filePath.replace("test-f.jpg", "waterMark");
        // ==================== 水印加到底部 =========================
        File returnFile = ImageUtil.setWaterMarkRightBottom(imageFile, markFile, waterMaskDir, 255);
        try {
            BufferedImage bImage = ImageUtil.read(returnFile);
            Assert.assertTrue(returnFile.getAbsolutePath().contains("test-f-copy.png")); // 期望结果：：true，文件名包含
            Assert.assertEquals(bImage.getWidth(), 255); // 期望结果：：相等，原图256，压缩了1像素
        } catch (IOException e) {
            log.error("读取图片失败", e);
        }

        WaterMaskSettings settings = WaterMaskSettings.builder().distanceX(10).build();
        returnFile = ImageUtil.setWaterMarkRightBottom(imageFile, markFile, waterMaskDir, 650, settings);
        try {
            BufferedImage bImage = ImageUtil.read(returnFile);
            Assert.assertTrue(returnFile.getAbsolutePath().contains("test-f-copy.png")); // 期望结果：：true，文件名包含
            Assert.assertEquals(bImage.getWidth(), 256); // 期望结果：：相等，原图256，小于650不压缩
        } catch (IOException e) {
            log.error("读取图片失败", e);
        }

        // ==================== 水印斜铺在原图 =======================
        File returnFile2 = ImageUtil.setWaterMark(imageFile2, markFile, waterMaskDir, 500);
        try {
            BufferedImage bImage = ImageUtil.read(returnFile2);
            Assert.assertTrue(returnFile2.getAbsolutePath().contains("test-f-copy2.png")); // 期望结果：：true，文件名包含
            Assert.assertEquals(bImage.getWidth(), 256); // 期望结果：：相等，原图256，小于500不压缩
        } catch (IOException e) {
            log.error("读取图片失败", e);
        }
        // 水印透明度0.2，最大宽度500，x轴距离10，y轴距离20
        returnFile2 = ImageUtil.setWaterMark(imageFile2, markFile, waterMaskDir, 0.2F, 500, 10, 20);
        try {
            BufferedImage bImage = ImageUtil.read(returnFile2);
            Assert.assertTrue(returnFile2.getAbsolutePath().contains("test-f-copy2.png")); // 期望结果：：true，文件名包含
            Assert.assertEquals(bImage.getWidth(), 256); // 期望结果：：相等，原图256，小于500不压缩
        } catch (IOException e) {
            log.error("读取图片失败", e);
        }
        // 水印透明度0.1，x轴距离50，还可以设定y轴距离，水印长宽
        settings = WaterMaskSettings.builder().opacity(0.1F).distanceX(50).build();
        returnFile2 = ImageUtil.setWaterMark(imageFile2, markFile, waterMaskDir, 650, settings);
        // returnFile2 = ImageUtil.setWaterMark(imageFile2, markFile, waterMaskDir, settings); //也可以不限定最小宽度
        try {
            BufferedImage bImage = ImageUtil.read(returnFile2);
            Assert.assertTrue(returnFile2.getAbsolutePath().contains("test-f-copy2.png")); // 期望结果：：true，文件名包含
            Assert.assertEquals(bImage.getWidth(), 256); // 期望结果：：相等，原图256，小于650不压缩
        } catch (IOException e) {
            log.error("读取图片失败", e);
        }

        FileUtils.delete(filePathCopy); // 删除文件
        FileUtils.delete(filePathCopy2); // 删除文件
        FileUtils.deleteCaseParent(returnFile.getAbsolutePath());
        FileUtils.deleteCaseParent(returnFile2.getAbsolutePath());
    }

    /**
     * 小图测试
     * 
     * @Title testSmallImage
     * @author 吕凯
     * @date 2021年5月6日 下午1:47:11 void
     */
    @Test(priority = 2) // 不指定顺序时默认按字母顺序执行
    public void testSmallImage() {
        URL fileUrl = Thread.currentThread().getContextClassLoader().getResource("test-f.jpg");
        String filePath = fileUrl.getFile();
        File imageFile = new File(filePath);
        String waterMaskDir = filePath.replace("test-f.jpg", "imageSmall");

        File imageSmallFile = ImageUtil.createSmallPhoto(imageFile, waterMaskDir);

        try {
            BufferedImage bImage = ImageUtil.read(imageSmallFile);
            log.warn("图片宽度：{}", bImage.getWidth());
            Assert.assertTrue(imageSmallFile.getAbsolutePath().contains("test-f_min.jpg")); // 期望结果：：true，文件名包含
            Assert.assertEquals(bImage.getWidth(), 50); // 期望结果：：相等，原图256，小于500不压缩
        } catch (IOException e) {
            log.error("读取图片失败", e);
        }

        String filePathPng = filePath.replace("test-f.jpg", "test.png");
        File imageFilePng = new File(filePathPng);
        File imageSmallFilePng = ImageUtil.createSmallPhoto(imageFilePng, waterMaskDir);

        try {
            BufferedImage bImage = ImageUtil.read(imageSmallFilePng);
            log.warn("图片宽度：{}", bImage.getWidth());
            Assert.assertTrue(imageSmallFilePng.getAbsolutePath().contains("test_min.png")); // 期望结果：：true，文件名包含
            Assert.assertEquals(bImage.getWidth(), 50); // 期望结果：：相等，原图256，小于500不压缩
        } catch (IOException e) {
            log.error("读取图片失败", e);
        }

        FileUtils.delete(imageSmallFile.getAbsolutePath()); // 删除文件
        FileUtils.deleteCaseParent(waterMaskDir + "/test-f.jpg"); // 删除文件
        FileUtils.delete(imageSmallFilePng.getAbsolutePath()); // 删除文件
        FileUtils.deleteCaseParent(waterMaskDir + "/test.png"); // 删除文件
    }

    /**
     * 测试图片旋转
     * 
     * @Title testRotateImg
     * @author 吕凯
     * @date 2021年5月6日 下午4:04:10 void
     */
    @Test(priority = 3) // 不指定顺序时默认按字母顺序执行
    public void testRotateImg() {
        URL fileUrl = Thread.currentThread().getContextClassLoader().getResource("test-f.jpg");
        String filePath = fileUrl.getFile();
        String rotateImgPath = filePath.replace("test-f.jpg", "imageRotate/test-f.jpg");
        FileUtils.copyFile(filePath, rotateImgPath);
        File imageFile = new File(rotateImgPath);
        Assert.assertTrue(imageFile.exists()); // 期望结果：：true，文件存在
        boolean retateResult = ImageUtil.rotateImg(imageFile, 90);
        Assert.assertTrue(retateResult); // 期望结果：：true，

        // TODO gif的旋转还是有问题的，会变黑
        String filePathPng = filePath.replace("test-f.jpg", "test.png");
        String filePathPngCopy = filePath.replace("test-f.jpg", "imageRotate/test.png");
        FileUtils.copyFile(filePathPng, filePathPngCopy);
        File imageFilePng = new File(filePathPngCopy);
        Assert.assertTrue(imageFilePng.exists()); // 期望结果：：true，文件存在
        retateResult = ImageUtil.rotateImg(imageFilePng, 45);
        Assert.assertTrue(retateResult); // 期望结果：：true，

        // 非正方形图形的处理

        FileUtils.delete(rotateImgPath); // 删除文件
        FileUtils.deleteCaseParent(filePathPngCopy); // 删除文件
    }

    /**
     * 测试检查图片大小
     * 
     * @Title testCheckFixedSize
     * @author 吕凯
     * @date 2021年5月6日 下午4:03:56 void
     */
    @Test(priority = 4) // 不指定顺序时默认按字母顺序执行
    public void testCheckFixedSize() {
        URL fileUrl = Thread.currentThread().getContextClassLoader().getResource("test-f.jpg");
        String filePath = fileUrl.getFile();
        File imageFile = new File(filePath);
        Assert.assertTrue(imageFile.exists()); // 期望结果：：true，文件存在
        boolean retateResult = ImageUtil.checkFixedSize(imageFile, 90, 90);
        Assert.assertFalse(retateResult); // 期望结果：：false，尺寸应该是256x256
        retateResult = ImageUtil.checkFixedSize(imageFile, 256, 256);
        Assert.assertTrue(retateResult); // 期望结果：：true，

        String fileRealPath = ImageUtil.getRealSuffixPath(filePath);
        log.warn(fileRealPath);

    }

    /**
     * 测试修改图片大小
     * 
     * @Title testResize
     * @author 吕凯
     * @date 2021年5月6日 下午4:03:38 void
     */
    @Test(priority = 5) // 不指定顺序时默认按字母顺序执行
    public void testResize() {
        URL fileUrl = Thread.currentThread().getContextClassLoader().getResource("test-f.jpg");
        String filePath = fileUrl.getFile();
        File imageFile = new File(filePath);
        Assert.assertTrue(imageFile.exists()); // 期望结果：：true，文件存在

        String saveImageFile = filePath.replace("test-f.jpg", "test-f-resize.jpg");
        try {
            ImageUtil.resize(imageFile, saveImageFile, 125, 125);
            BufferedImage bImage = ImageUtil.read(new File(saveImageFile));
            Assert.assertEquals(bImage.getWidth(), 125); // 期望结果：：相等
        } catch (IOException e) {
            log.error("", e);
        }

        // png测试
        String pngImgPath = filePath.replace("test-f.jpg", "test.png");
        String pngImgPathSave = filePath.replace("test-f.jpg", "test-resize-png.png");
        try {
            ImageUtil.resize(new File(pngImgPath), pngImgPathSave, 125, 125);
            BufferedImage bImage = ImageUtil.read(new File(pngImgPathSave));
            Assert.assertEquals(bImage.getWidth(), 125); // 期望结果：：相等
        } catch (IOException e) {
            log.error("", e);
        }

        // gif测试
        String gifImgPath = filePath.replace("test-f.jpg", "test-gif.gif");
        String gifImgPathSave = filePath.replace("test-f.jpg", "test-resize-gif.png");
        try {
            ImageUtil.resize(new File(gifImgPath), gifImgPathSave, 125, 125);
            BufferedImage bImage = ImageUtil.read(new File(gifImgPathSave));
            Assert.assertEquals(bImage.getWidth(), 125); // 期望结果：：相等
        } catch (IOException e) {
            log.error("", e);
        }

        FileUtils.delete(saveImageFile); // 删除文件
        FileUtils.delete(pngImgPathSave); // 删除文件
        FileUtils.delete(gifImgPathSave); // 删除文件
    }

    @Test(priority = 6) // 不指定顺序时默认按字母顺序执行
    public void testCrop() {
        URL fileUrl = Thread.currentThread().getContextClassLoader().getResource("test-f.jpg");
        String filePath = fileUrl.getFile();
        File imageFile = new File(filePath);
        Assert.assertTrue(imageFile.exists()); // 期望结果：：true，文件存在

        // jpg
        String saveImageFile = filePath.replace("test-f.jpg", "test-crop.jpg");
        try {
            ImageUtil.crop(imageFile, new File(saveImageFile), 0, 0, 125, 125);
            BufferedImage bImage = ImageUtil.read(new File(saveImageFile));
            Assert.assertEquals(bImage.getWidth(), 125); // 期望结果：：相等
        } catch (IOException e) {
            log.error("", e);
        }

        // png测试
        String pngImgPath = filePath.replace("test-f.jpg", "test.png");
        String pngImgPathSave = filePath.replace("test-f.jpg", "test-crop-png.png");
        try {
            ImageUtil.crop(new File(pngImgPath), new File(pngImgPathSave), 0, 0, 125, 125);
            BufferedImage bImage = ImageUtil.read(new File(pngImgPathSave));
            Assert.assertEquals(bImage.getWidth(), 125); // 期望结果：：相等
        } catch (IOException e) {
            log.error("", e);
        }

        // gif测试
        String gifImgPath = filePath.replace("test-f.jpg", "test-gif.gif");
        String gifImgPathSave = filePath.replace("test-f.jpg", "test-crop-gif.gif");
        try {
            ImageUtil.crop(new File(gifImgPath), new File(gifImgPathSave), 0, 0, 125, 125);
            BufferedImage bImage = ImageUtil.read(new File(gifImgPathSave));
            Assert.assertEquals(bImage.getWidth(), 125); // 期望结果：：相等
        } catch (IOException e) {
            log.error("", e);
        }

        FileUtils.delete(saveImageFile); // 删除文件
        FileUtils.delete(pngImgPathSave); // 删除文件
        FileUtils.delete(gifImgPathSave); // 删除文件
    }

}
