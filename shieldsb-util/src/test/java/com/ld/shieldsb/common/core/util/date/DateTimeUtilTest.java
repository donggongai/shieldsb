package com.ld.shieldsb.common.core.util.date;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

import org.testng.Assert;
import org.testng.annotations.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * java8的日期时间工具类单元测试类
 * 
 * @ClassName DateTimeUtilTest
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年6月1日 上午9:29:29
 *
 */
@Slf4j
public class DateTimeUtilTest {

    /**
     * 
     * 测试
     * 
     * @Title test
     * @author 吕凯
     * @date 2021年6月1日 上午9:29:29
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void test() {
        long syslong = System.currentTimeMillis();
        log.warn(DateTimeUtil.getDateTimeString(syslong));
        LocalDate date = LocalDate.now();
        // 将日期转换成Epoch 天，也就是相对于1970-01-01（ISO）开始的天数，和时间戳是一个道理，时间戳是秒数。显然，该方法是有一定的局限性的。
        log.warn("{}", date.toEpochDay());

        String fromNow = DateTimeUtil.fromNow(DateUtil.str2dateTime("2021-06-01 09:00:00"));
        log.warn(fromNow);

        Date nowDate = new Date();
        Date dataStart = DateTimeUtil.getDate(nowDate);
        log.warn("时间去除时分秒后{} 原来{}", dataStart, nowDate);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dataStart);
        Assert.assertEquals(calendar.get(Calendar.HOUR_OF_DAY), 0);
        Assert.assertEquals(calendar.get(Calendar.MINUTE), 0);

        // localdate

    }

    @Test(priority = 1) // 不指定顺序时默认按字母顺序执行
    public void testLocalDate2Date() {

        Date nowDate = new Date();
        LocalDate localDate = DateTimeUtil.date2localDate(nowDate);
        log.warn("Date转LocalDate LocalDate={} Date={}", localDate, nowDate);

        LocalDate nowLocalDate = LocalDate.now();
        Date date = DateTimeUtil.localDate2Date(nowLocalDate);
        log.warn("LocalDate转Date Date={} LocalDate={}", date, nowLocalDate);
        // localdate
        LocalDateTime localDateTime = DateTimeUtil.date2localDateTime(nowDate);
        log.warn("Date转LocalDateTime LocalDateTime={} Date={}", localDateTime, nowDate);

        LocalDateTime nowLocalDateTime = LocalDateTime.now();
        date = DateTimeUtil.localDateTime2Date(nowLocalDateTime);
        log.warn("LocalDateTime转Date Date={} LocalDateTime={}", date, nowLocalDateTime);

    }

    @Test(priority = 2) // 不指定顺序时默认按字母顺序执行
    public void testWeekOfYear() {

        Date nowDate = new Date();
        LocalDate localDate = DateTimeUtil.date2localDate(nowDate);
        log.warn("今天是本年度的第{}周", DateTimeUtil.getWeekOfYear(localDate));

        // 需要注意的是，ISO8601的标准中，至少需要4天才能算作1周，如果某年的第一天是周五、周六或周日(比如2021年)，那么这一年的第一周是从下一周开始的
        /*可以看到一年的第一个自然周应当满足：
        
        1,有第一个星期四
        2,包含1月4号
        3,第一个自然周应当有4个或者4个以上的天数
        4,这个星期开始的时间（即周一）在去年的12月29号（星期一）到今年的1月4号之间
        所以如果1月1号是周一、周二、周三或者周四，它属于第一个自然周，如果不是，他属于去年的52周或者53周。*/
        // 2021年第一天
        LocalDate time1 = LocalDate.of(2021, 1, 1);
        int time1Weekly1 = DateTimeUtil.getWeekOfYear(time1);
        Assert.assertEquals(53, time1Weekly1); // 预期结果：：相等，第2020年第53周
        // 2021年最后一天
        LocalDate time2 = LocalDate.of(2021, 12, 31);
        int time1Weekly2 = DateTimeUtil.getWeekOfYear(time2);
        Assert.assertEquals(52, time1Weekly2); // 预期结果：：相等，第52周
        // 2019年第一天
        LocalDate time3 = LocalDate.of(2020, 1, 1);
        int time1Weekly3 = DateTimeUtil.getWeekOfYear(time3);
        Assert.assertEquals(1, time1Weekly3); // 预期结果：：相等，第1周
        // 2019年最后一天
        LocalDate time4 = LocalDate.of(2020, 12, 31);
        int time1Weekly4 = DateTimeUtil.getWeekOfYear(time4);
        Assert.assertEquals(53, time1Weekly4); // 预期结果：：相等，第53周

        LocalDate today = LocalDate.now();
        log.warn("本周周一：{}", DateTimeUtil.date2Str(DateTimeUtil.getStartDayOfWeek(today)));
        log.warn("本周周日：{}", DateTimeUtil.date2Str(DateTimeUtil.getEndDayOfWeek(today)));

        // 共多少周
        // 2021年
        int weeknum = DateTimeUtil.getMaxWeekNumOfYear(time1);
        Assert.assertEquals(52, weeknum); // 预期结果：：相等，共52周

        weeknum = DateTimeUtil.getMaxWeekNumOfYear(2021);
        Assert.assertEquals(52, weeknum); // 预期结果：：相等，共52周

        // 第一周时间
        log.warn("2021年第29周周一 {}", DateTimeUtil.date2Str(DateTimeUtil.getStartDayOfWeek(2021, 29)));
        log.warn("2021年第29周周日 {}", DateTimeUtil.date2Str(DateTimeUtil.getEndDayOfWeek(2021, 29)));
    }

}
