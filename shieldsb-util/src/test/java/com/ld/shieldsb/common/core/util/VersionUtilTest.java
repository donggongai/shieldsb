package com.ld.shieldsb.common.core.util;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.zafarkhaja.semver.Version;
import com.ld.shieldsb.common.core.model.PropertiesModel;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * VersionUtil的单元测试类
 * 
 * @ClassName VersionUtilTest
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年9月1日 上午8:57:23
 *
 */
@Slf4j
public class VersionUtilTest {

    /**
     * 
     * 测试
     * 
     * @Title test
     * @author 吕凯
     * @date 2021年9月1日 上午9:15:21 void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void test() {
        // 1.0.0-alpha < 1.0.0-alpha.1 < 1.0.0-beta.2 < 1.0.0-beta.11 < 1.0.0-rc.1 < 1.0.0-rc.1+build.1 < 1.0.0 < 1.0.0+0.3.7 < 1.3.7+build
        // < 1.3.7+build.2.b8f12d7 < 1.3.7+build.11.e0f985a.
        String v1 = "1.0.0-alpha";
        String v2 = "1.0.0-alpha.1";
        String v3 = "1.0.0-beta.2";
        String v4 = "1.0.0-beta.11";
        String v5 = "1.0.0-rc.1";
        String v6 = "1.0.0-rc.1+build.1";
        String v7 = "1.0.0";
        String v8 = "1.0.0+0.3.7";
        String v9 = "1.3.7+build";
        String v10 = "1.3.7+build.2.b8f12d7";
        String v11 = "1.3.7+build.11.e0f985a";
        Assert.assertEquals(VersionUtil.compareVersion(v2, v1), VersionUtil.V_BIG); // 期望结果：：相等
        Assert.assertEquals(VersionUtil.compareVersion(v3, v2), VersionUtil.V_BIG); // 期望结果：：相等
        Assert.assertEquals(VersionUtil.compareVersion(v4, v3), VersionUtil.V_BIG); // 期望结果：：相等
        Assert.assertEquals(VersionUtil.compareVersion(v5, v4), VersionUtil.V_BIG); // 期望结果：：相等
        Assert.assertEquals(VersionUtil.compareVersion(v6, v5), VersionUtil.V_BIG); // 期望结果：：相等
        Assert.assertEquals(VersionUtil.compareVersion(v7, v6), VersionUtil.V_BIG); // 期望结果：：相等
        Assert.assertEquals(VersionUtil.compareVersion(v8, v7), VersionUtil.V_BIG); // 期望结果：：相等
        Assert.assertEquals(VersionUtil.compareVersion(v9, v8), VersionUtil.V_BIG); // 期望结果：：相等
        Assert.assertEquals(VersionUtil.compareVersion(v10, v9), VersionUtil.V_BIG); // 期望结果：：相等
        Assert.assertEquals(VersionUtil.compareVersion(v11, v10), VersionUtil.V_BIG); // 期望结果：：相等

        Assert.assertTrue(VersionUtil.lessThan(v7, v8)); // 期望结果：：true,v7 < v8

        Version v = Version.valueOf("1.0.0-rc.1+build.1");

        int major = v.getMajorVersion(); // 1
        int minor = v.getMinorVersion(); // 0
        int patch = v.getPatchVersion(); // 0
        log.warn("版本号major:{}，minor:{}，patch:{}", major, minor, patch);

        String normal = v.getNormalVersion(); // "1.0.0"
        String preRelease = v.getPreReleaseVersion(); // "rc.1"
        String build = v.getBuildMetadata(); // "build.1"

        String str = v.toString(); // "1.0.0-rc.1+build.1"
        log.warn("版本号:{}", str);
        log.warn("normal:{}", normal);
        Assert.assertEquals(normal, "1.0.0"); // 期望结果：：相等
        log.warn("preRelease:{}", preRelease);
        Assert.assertEquals(preRelease, "rc.1"); // 期望结果：：相等
        log.warn("build:{}", build);
        Assert.assertEquals(build, "build.1"); // 期望结果：：相等

        // 根据参数构建版本号
        Version vv1 = Version.forIntegers(1);
        Version vv2 = Version.forIntegers(1, 2);
        Version vv3 = Version.forIntegers(1, 2, 3);

        System.out.println("vv1:" + vv1);
        System.out.println("vv2:" + vv2);
        System.out.println("vv3:" + vv3);

        Version vvv1 = Version.valueOf("1.0.0-rc.1+build.1");
        Version vvv2 = Version.valueOf("1.3.7+build.2.b8f12d7");

        int result = vvv1.compareTo(vvv2); // < 0
        System.out.println("vvv1-vvv2:" + result);
    }

    public static void main(String[] args) {
        System.out.println(PropertiesModel.CONFIG.getBoolean("test", false));
    }

}
