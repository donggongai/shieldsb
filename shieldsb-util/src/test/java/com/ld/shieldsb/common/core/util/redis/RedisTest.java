package com.ld.shieldsb.common.core.util.redis;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

/**
 * @author: flychao88 Time: 2012.5.7 16:23:15
 */
@Slf4j
public class RedisTest {
    Jedis jedis;

    @BeforeTest
    public void setUp() {
//        pool = new JedisPool(new JedisPoolConfig(), "124.42.8.133");
//        Config config = new JedisPoolConfig();
//        int port = 6379;
//        pool = new JedisPool(config, "localhost", port);
//
//        jedis = pool.getResource();
//        jedis.auth("admin31511redis");
        // 通过工具类获取
        jedis = RedisUtil.getJedis();
        // log.warn("::::" + jedis.incr("commonid"));// id生成器
    }

    /**
     * 测试基本字符串CRUD操作
     * 
     * @Title testBasicString
     * @author 吕凯
     * @date 2021年4月28日 下午6:15:23 void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void testBasicString() {
        // -----添加数据----------
        String key = "name";
        String valueSet = "minxr";
        jedis.set(key, valueSet);// 向key-->name中放入了value-->minxr
        String nameGet = jedis.get(key);
        log.warn(nameGet);// 执行结果：minxr
        Assert.assertEquals(nameGet, valueSet);

        // -----修改数据-----------
        // 1、在原来基础上修改]
        String valueUpdate = "-jarorwar";
        jedis.append(key, valueUpdate); // 很直观，类似map 将jarorwar值append到已经有的value之后
        nameGet = jedis.get(key);
        log.warn(nameGet);// 执行结果:minxr-jarorwar
        Assert.assertEquals(nameGet, valueSet + valueUpdate);

        // 2、直接覆盖原来的数据
        valueSet = "李寻欢";
        jedis.set(key, valueSet);
        nameGet = jedis.get(key);
        log.warn(nameGet);// 执行结果：李寻欢
        Assert.assertEquals(nameGet, valueSet);

        // 删除key对应的记录
        jedis.del(key);
        nameGet = jedis.get(key); // 执行结果：null
        Assert.assertNull(nameGet);

        /**
         * mset相当于 <br>
         * jedis.set("name","minxr"); <br>
         * jedis.set("jarorwar","李寻欢");
         */
        valueSet = "minxr";
        String key2 = "jarorwar";
        String valueSet2 = "李寻欢";
        jedis.mset(key, valueSet, key2, valueSet2);
        List<String> valueList = jedis.mget(key, key2); // list
        log.warn("{}", valueList);
        Assert.assertEquals(valueList.size(), 2);

        jedis.del(key, key2);

    }

    /**
     * 测试操作Map
     * 
     * @Title testMap
     * @author 吕凯
     * @date 2021年4月28日 下午6:15:02 void
     */
    @Test(priority = 1) // 不指定顺序时默认按字母顺序执行
    public void testMap() {
        Long userId = jedis.incr("userId"); // 获取自增序列

        String key = "user:" + userId;
        Map<String, String> user = new HashMap<>();
        String keyName = "name";
        String keyPwd = "pwd";
        String valueName = "惊鸿仙子";
        String valuePwd = "lili";
        user.put(keyName, valueName);
        user.put(keyPwd, valuePwd);

        jedis.hmset(key, user);
        jedis.set("username:" + valueName, userId + ""); // 如果要根据用户名搜索需要创建索引

        // jedis.bgsave();
        // jedis.del("user");
        // 取出user中的name，执行结果:[minxr]-->注意结果是一个泛型的List
        // 第一个参数是存入redis中map对象的key，后面跟的是放入map中的对象的key，后面的key可以跟多个，是可变参数
        List<String> mapList = jedis.hmget(key, keyName, keyPwd);
//        jedis.hgetAll(key); //获取全部
        log.warn("key为 {} 的map{}和{}的值：{}", key, keyName, keyPwd, mapList);
        Assert.assertEquals(mapList.get(0), valueName);
        Assert.assertEquals(mapList.get(1), valuePwd);

        // 删除map中的某个键值
        jedis.hdel(key, keyPwd);
        List<String> pwdList = jedis.hmget(key, keyPwd);
        log.warn("key为 {} 的map中{}值：{}", key, keyPwd, pwdList); // 因为上一步del删除了，所以返回的是null
        Assert.assertNull(pwdList.get(0));
        long length = jedis.hlen(key);
        log.warn("key为 {} 的map中元素个数：{}", key, length); // 返回key为user的键中存放的值的个数
        Assert.assertEquals(length, 1L);
        boolean exists = jedis.exists(key);
        log.warn("key为 {} 的map是否存在：{}", key, exists);// 是否存在key为user的记录 返回true
        Assert.assertTrue(exists);
        Set<String> keys = jedis.hkeys(key);
        log.warn("key为 {} 的map对象中的所有key：{}", key, keys);// 返回map对象中的所有key [pwd, name]
        List<String> values = jedis.hvals(key);
        log.warn("key为 {} 的map对象中的所有value：{}", key, values);// 返回map对象中的所有value [kevin,password]

        Iterator<String> iter = jedis.hkeys(key).iterator(); // 迭代所有的key
        while (iter.hasNext()) {
            String mykey = iter.next();
            log.warn(mykey + ":" + jedis.hmget(key, mykey));
        }

        jedis.del("userId");
        jedis.del(key);
        jedis.del("username:" + valueName);
    }

    @Test(priority = 2) // 不指定顺序时默认按字母顺序执行
    public void getCompanyList() {
        Long companyId = 1L;
        String key = "companyId:" + companyId;
        String companyKey = "company:" + companyId;
        Long count = jedis.llen(key); // 企业总数
        log.warn("getCompanyList count:::{}", count);

        List<String> idList = jedis.lrange(key, 0, 2); // 分页
        for (String id : idList) {
            log.warn("list::: {}=={}", id, jedis.hgetAll(companyKey + ":" + id));
        }
    }

    /**
     * 测试操作List
     * 
     * @Title testList
     * @author 吕凯
     * @date 2021年4月28日 下午6:14:41 void
     */
    @Test(priority = 4) // 不指定顺序时默认按字母顺序执行
    public void testList() {
        // 开始前，先移除所有的内容
        String key = "java framework";
        jedis.del(key);
        List<String> dataList = jedis.lrange(key, 0, -1);
        log.warn("key为{}的list值为：{}", key, dataList);
        Assert.assertEquals(dataList.size(), 0);

        // 先向key java framework中存放三条数据
        jedis.lpush(key, "spring");
        jedis.lpush(key, "struts");
        jedis.lpush(key, "hibernate");
        // 再取出所有数据jedis.lrange是按范围取出，
        // 第一个是key，第二个是起始位置，第三个是结束位置，jedis.llen获取长度 -1表示取得所有
        dataList = jedis.lrange(key, 0, -1);
        log.warn("key为{}的list值为：{}", key, dataList);
        Assert.assertEquals(dataList.get(0), "hibernate"); // 后入先出
        Assert.assertEquals(dataList.size(), 3);

        jedis.del(key);
    }

    /**
     * 测试操作Set,会去重
     * 
     * @Title testSet
     * @author 吕凯
     * @date 2021年4月28日 下午6:14:23 void
     */
    @Test(priority = 5) // 不指定顺序时默认按字母顺序执行
    public void testSet() {
        String key = "sname";
        Transaction transaction = jedis.multi(); // 事务
        // 添加
        transaction.sadd(key, "orange");
        transaction.sadd(key, "apple");
        transaction.sadd(key, "李寻欢");
        transaction.sadd(key, "西门吹雪");
        transaction.sadd(key, "西门吹雪"); // 重复不会添加
        // 移除noname
        transaction.srem(key, "西门吹雪");
        transaction.exec();
        jedis.sadd(key, "李寻欢"); // 重复不会添加

        log.warn("testSet中key为{}的set所有的值：{}", key, jedis.smembers(key));// 获取所有加入的value
        log.warn("testSet中key为{}的set中存在apple：{}", key, jedis.sismember(key, "apple"));// 判断 apple
        // 是否是sname集合的元素
        log.warn("testSet中key为{}的set随机获取一个值：{}", key, jedis.srandmember(key)); // 随机取
        log.warn("testSet中key为{}的set元素个数：{}", key, jedis.scard(key));// 返回集合的元素个数

        jedis.del(key); // 删除
        Assert.assertEquals((long) jedis.scard(key), 0L);

    }

    /**
     * 测试操作有序集合Set
     * 
     * @Title testZSet
     * @author 吕凯
     * @date 2021年4月28日 下午6:14:00 void
     */
    @Test(priority = 6) // 不指定顺序时默认按字母顺序执行
    public void testZSet() {
        String key = "zname";
        // 添加
        Map<String, Double> valueMap = new LinkedHashMap<>();
        valueMap.put("test", 5d);
        valueMap.put("发送", 1d);
        valueMap.put("apple", 3d);
        valueMap.put("noname", 0d);
        valueMap.put("noname", 2d); // 覆盖前面的
        jedis.zadd(key, valueMap);
        // 移除noname
//        jedis.zrem("zname", "noname");
        log.warn("testZSet中可以为{}的所有值：{}", key, jedis.zrange(key, 0, -1)); // 获取所有加入的value
        log.warn("testZSet中可以为{}的对象中{}的值：{}", key, "noname", jedis.zscore(key, "noname")); // 返回score
        log.warn("testZSet中可以为{}的元素个数：{}", key, jedis.zcard(key));// 返回集合的元素个数

        jedis.del(key);
    }

    /**
     * 测试排序
     * 
     * @Title testSort
     * @author 吕凯
     * @date 2021年4月28日 下午6:13:41 void
     */
    @Test(priority = 7) // 不指定顺序时默认按字母顺序执行
    public void testSort() {
        // jedis 排序
        // 注意，此处的rpush和lpush是List的操作。是一个双向链表l r左右
        String key = "table";
        jedis.del(key);// 先清除数据，再加入数据进行测试
        jedis.rpush(key, "1");
        jedis.lpush(key, "6");
        jedis.lpush(key, "3");
        jedis.lpush(key, "9");
        List<String> tableList = jedis.lrange(key, 0, -1);
        log.warn("排序::key为{}的所有数据：{}", key, tableList);// [9, 3, 6, 1]
        Assert.assertEquals(tableList.get(0), "9");
        tableList = jedis.sort(key);
        log.warn("排序::key为{}的数据排序：{}", key, tableList); // [1, 3, 6, 9] //输入排序后结果
        Assert.assertEquals(tableList.get(0), "1");
        tableList = jedis.lrange(key, 0, -1); // 原数据为改变
        log.warn("排序::key为{}的所有数据：{}", key, tableList);
        Assert.assertEquals(tableList.get(0), "9");

        jedis.del(key);
    }

    /**
     * 测试有效期
     * 
     * @Title testTtl
     * @author 吕凯
     * @date 2021年4月28日 下午6:13:30 void
     */
    @Test(priority = 8) // 不指定顺序时默认按字母顺序执行
    public void testTtl() {
        String key = "timekey";
        log.warn("TTL当前库中可以为name的有效时间为：{}", jedis.ttl("name"));// 返回给定key的有效时间，如果是-1则表示永远有效-2表示数据不存在
        jedis.setex(key, 10, "min");// 通过此方法，可以指定key的存活（有效时间） 时间为秒
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            log.error("", e);
        } // 睡眠5秒后，剩余时间将为<=5
        long ttl = jedis.ttl(key);
        log.warn("TTL当前库中可以为timekey的有效时间为：{}", ttl); // 输出结果为5
        Assert.assertEquals(ttl, 5);

        jedis.setex(key, 1, "min"); // 设为1后，下面再看剩余时间就是1了
        ttl = jedis.ttl(key);
        log.warn("TTL当前库中可以为timekey的有效时间为：{}", ttl); // 输出结果为5
        Assert.assertEquals(ttl, 1);

        jedis.del(key);
    }

    /**
     * 测试重命名
     * 
     * @Title testRename
     * @author 吕凯
     * @date 2021年4月28日 下午6:13:20
     * @throws InterruptedException
     *             void
     */
    @Test(priority = 9) // 不指定顺序时默认按字母顺序执行
    public void testRename() throws InterruptedException {
        String key = "testzong";
        String valueSet = "val";
        jedis.set(key, valueSet);
        log.warn("testRename{}", jedis.rename(key, "time"));
        log.warn("testRename{}", jedis.get(key));// 因为移除，返回为null
        log.warn("testRename{}", jedis.get("time")); // 因为将timekey 重命名为time
        // 所以可以取得值val
        jedis.del("time");
    }

    @Test(priority = 10) // 不指定顺序时默认按字母顺序执行
    public void test() {
        // keys中传入的可以用通配符
        log.warn("当前库所有的key：{}", jedis.keys("*")); // 返回当前库中所有的key [sose,
        log.warn("当前库所有的数据格式：{}", jedis.dbSize());// 数据的个数

        log.warn("当前库所有包含name的key：{}", jedis.keys("*name"));// 返回的sname [sname, name]

        Assert.assertEquals((long) jedis.del("sanmdde"), 0); // 删除key为sanmdde的对象 删除成功返回1，删除失败（或者不存在）返回 0

        log.warn("当前库中是否存在key为key的数据：{}", jedis.exists("key"));// 检查key是否存在
        Assert.assertFalse(jedis.exists("key"));

    }

    @AfterTest
    public void close() {
        if (jedis != null) {
            RedisUtil.returnResource(jedis);
            RedisUtil.close();
            log.warn("====== 关闭redis ======");
        }
    }

}