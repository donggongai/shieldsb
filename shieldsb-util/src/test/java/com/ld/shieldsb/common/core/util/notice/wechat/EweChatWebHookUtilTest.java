package com.ld.shieldsb.common.core.util.notice.wechat;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.ImmutableList;
import com.ld.shieldsb.common.core.io.IOUtils;
import com.ld.shieldsb.common.core.model.Result;
import com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.TmplCardActionModel;
import com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.TmplCardHorizontalContentModel;
import com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.TmplCardMainTitleModel;
import com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.TmplCardSourceModel;
import com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.news.TemplateCardNewsModel;
import com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.news.TmplCardImageModel;
import com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.text.TemplateCardTextModel;
import com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.text.TmplCardEmphasisContentModel;
import com.ld.shieldsb.common.core.util.notice.wechat.model.NewsMsgModel;
import com.ld.shieldsb.common.core.util.notice.wechat.webhook.EweChatWebHookMsgParams;
import com.ld.shieldsb.common.core.util.notice.wechat.webhook.EweChatWebHookUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 用于企业微信群机器人的工具类的测试类,需要配合src/test/resources/properties/config_test.properties，因为存在密码泄露的风险，该文件需要自己创建
 * 
 * @ClassName EmailUtilTest
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月16日 下午2:16:03
 *
 */
@Slf4j
public class EweChatWebHookUtilTest {

    /**
     * 测试通过id发送文本消息
     * 
     * @Title testSenTextByIds
     * @author 吕凯
     * @date 2021年4月16日 上午9:17:34 void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void testSendText() {

//        Map<String, Object> dataMap ;
//              dataMap = parse2TextDataMap("测试@全部", true); //@全体
//              dataMap = parse2TextDataMap("测试@某个人", "LvKai", null); // 用户id@用户
//              dataMap = parse2TextDataMap("测试@某个人", null, "15253640441"); //手机号@用户
//        dataMap = EWeChatWebHookUtil.parse2TextDataMap("单元测试：测试在内容中通过id@某个人，sho<@LvKai>123"); // 内容中@用户，<@userId>
//              dataMap = parse2TextDataMap("测试@某个人", null, "15253640441,@all"); //逗号连接，@多个人
//            JSONObject result = EWeChatWebHookUtil.sendMsg(dataMap);

        String content = "单元测试：普通消息";
        Result result = EweChatWebHookUtil.sendTextMsg(content);
        log.warn(result.toString());
        // {"errcode":0,"errmsg":"ok"}
        Assert.assertTrue(result.getSuccess());

        content = "单元测试：测试@全部";
        result = EweChatWebHookUtil.sendTextMsg(content, true);
        log.warn(result.toString());
        // {"errcode":0,"errmsg":"ok"}
        Assert.assertTrue(result.getSuccess());

        content = "单元测试：测试@某个人";
        result = EweChatWebHookUtil.sendTextMsg(EweChatWebHookMsgParams.builder().content(content).noticeIds("lvkai").build());
        log.warn(result.toString());
        // {"errcode":0,"errmsg":"ok"}
        Assert.assertTrue(result.getSuccess());

        content = "单元测试：测试在内容中通过id@某个人，sho<@LvKai>123";
        result = EweChatWebHookUtil.sendTextMsg(content);
        log.warn(result.toString());
        // {"errcode":0,"errmsg":"ok"}
        Assert.assertTrue(result.getSuccess());
    }

    /**
     * 测试通过手机号发送文本消息
     * 
     * @Title testSenTextByMobile
     * @author 吕凯
     * @date 2021年4月16日 上午9:17:46 void
     */
    @Test(priority = 1) // 不指定顺序时默认按字母顺序执行
    public void testSendTextByMobile() {

//        Map<String, Object> dataMap = EWeChatWebHookUtil
//                .parse2TextDataMap(EWeChatWebHookMsgParams.builder().content("单元测试：测试通过手机号@某个人").noticeMobiles("15253640441,@all").build()); // 逗号连接，@all表示全体，如果只发送给一个人则写那个人的手机号即可
//
//        JSONObject result = EWeChatWebHookUtil.sendMsg(dataMap);

        // 逗号连接多个手机号，@all表示全体，如果只发送给一个人则写那个人的手机号即可
        Result result = EweChatWebHookUtil
                .sendTextMsg(EweChatWebHookMsgParams.builder().content("单元测试：测试通过手机号@某个人").noticeMobiles("15253640441,@all").build());
        System.out.println(result.toString());
        // {"errcode":0,"errmsg":"ok"}
        Assert.assertTrue(result.getSuccess());

    }

    /**
     * 测试发送图片信息
     * 
     * @Title testImageMsg
     * @author 吕凯
     * @date 2021年4月16日 上午9:18:09 void
     */
    @Test(priority = 2) // 不指定顺序时默认按字母顺序执行
    public void testSendImageMsg() {
        try {
//          EWeChatWebHookUtil.sendImageMsg("D:\\图片视频\\092002.92725844_620X620.jpg"); //本地图片
            Result result = EweChatWebHookUtil.sendImageMsg(new URL("https://s1.ax1x.com/2020/07/02/Nb5hb4.png")); // 图片信息
            System.out.println(result.toString());
            // {"errcode":0,"errmsg":"ok"}
            Assert.assertTrue(result.getSuccess());
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    /**
     * 发送图文信息
     * 
     * @Title testSendImageMsg
     * @author 吕凯
     * @date 2022年3月14日 上午11:40:52 void
     */
    @Test(priority = 2) // 不指定顺序时默认按字母顺序执行
    public void testSendNewsMsg() {
        Result result = EweChatWebHookUtil.sendNewsMsg("biaoti标题", "描述miaoshu", "http:www.baidu.com",
                "https://s1.ax1x.com/2020/07/02/Nb5hb4.png"); // 图片信息
        System.out.println(result.toString());
        // {"errcode":0,"errmsg":"ok"}
        Assert.assertTrue(result.getSuccess());

        // 测试多组图文的情况
        List<NewsMsgModel> newsMsgList = new ArrayList<>();
        NewsMsgModel model1 = new NewsMsgModel();
        model1.setTitle("潍坊最新疫情情况");
        model1.setDescription("士大夫所发生的辅导费斯蒂芬三的放松放松的发随碟附送的发生的分所发生的浮士德发士大夫是放松放松的发时代氛围浮士德发的");
        model1.setUrl("http://work.11315.cn:28800");
        model1.setPicurl(
                "http://img-qn-5.51miz.com/preview/muban/00/00/42/60/M-426085-45E50755.jpg!/quality/90/unsharp/true/compress/true/fw/830/clip/830x500a0a500");
        // http://5b0988e595225.cdn.sohucs.com/images/20200222/9515f649acb24b06b25d9ca9182cd315.jpeg

        NewsMsgModel model2 = new NewsMsgModel();
        model2.setTitle("测试标题");
        model2.setDescription("士大夫所发生的辅导费斯蒂芬三的放松放松的发随碟附送的发生的分所发生的浮士德发士大夫是放松放松的发时代氛围浮士德发的");
        model2.setUrl("http://work.11315.cn:28800");
        model2.setPicurl("https://s1.ax1x.com/2020/07/02/Nb5hb4.png");

        newsMsgList.add(model1);
        newsMsgList.add(model2);

        result = EweChatWebHookUtil.sendNewsMsg(newsMsgList); // 图片信息
        System.out.println(result.toString());
        // {"errcode":0,"errmsg":"ok"}
        Assert.assertTrue(result.getSuccess());

    }

    /**
     * 测试markdown格式的消息
     * 
     * @Title testMarkdownMsg
     * @author 吕凯
     * @date 2021年4月16日 上午10:11:05 void
     */
    @Test(priority = 3) // 不指定顺序时默认按字母顺序执行
    public void testSendMarkdownMsg() {

        // info 绿色 comment 灰色 warning 橙红色
        String content = "单元测试：测试结果反馈<font color=\"warning\">24</font>个，请注意修改。\n" //
                + " >BUG:<font color=\"comment\">10个</font>\n" //
                + " >建议:<font color=\"comment\">12个</font>\n" //
                + " >咨询:<font color=\"comment\">2个</font>"; // markdown信息

        /*参数    是否必填    说明
        msgtype 是   消息类型，此时固定为markdown
        content 是   markdown内容，最长不超过4096个字节，必须是utf8编码*/
        /*标题 （支持1至6级标题，注意#与文字中间要有空格）
            # 标题一
            ## 标题二
            ### 标题三
            #### 标题四
            ##### 标题五
            ###### 标题六
        加粗
         **bold**
        链接
            [这是一个链接](http://work.weixin.qq.com/api/doc)
        行内代码段（暂不支持跨行）
            `code`
        引用
            > 引用文字
        字体颜色(只支持3种内置颜色)
            <font color="info">绿色</font>
            <font color="comment">灰色</font>
            <font color="warning">橙红色</font>*/

        Result result = EweChatWebHookUtil.sendMarkdownMsg(content);
        log.warn(result.toString());
        // {"errcode":0,"errmsg":"ok"}
        Assert.assertTrue(result.getSuccess());

        // 内容中个@某人
        content = "单元测试：内容中@某人\n" //
                + "# xx功能需求分析完成\n" //
                + "请尽快安排开发，此功能需要在**7月7日**前完成<@lvkai>\n" //
                + "`Assert.assertEquals(result.getInteger(EWeChatWebHookUtil.ERRCODE), (Integer) 0);`\n" //
                + "参考代码：[鲸落论坛](http://work.11315.cn:28800)\n"; // markdown信息

        result = EweChatWebHookUtil.sendMarkdownMsg(content);
        System.out.println(result.toString());
        // {"errcode":0,"errmsg":"ok"}
        Assert.assertTrue(result.getSuccess());

    }

    /**
     * 测试上传文件
     * 
     * @Title testFileMsg
     * @author 吕凯
     * @date 2021年4月16日 上午10:11:05 void
     */
    @Test(priority = 4) // 不指定顺序时默认按字母顺序执行
    public void testUploadFile() {
        // 通过流上传
        try (InputStream uploadIs = IOUtils.readAsInputStream("upload.txt")) {

            JSONObject result = EweChatWebHookUtil.uploadFile(uploadIs, "test.txt");
            System.out.println(result.toString());
            // {"errcode":0,"errmsg":"ok","media_id":"3QtSsDv5R-shKSFuMM0BWFCauDwhbBmWqbz_uqGT9xO0","created_at":"1618812990","type":"file"}
            Assert.assertEquals(result.getInteger(EweChatTool.ERRCODE), (Integer) 0);
        } catch (IOException e) {
            log.error("单元测试出错！", e);
        }
        // 通过文件上传
        URL fileUrl = Thread.currentThread().getContextClassLoader().getResource("test.png");
        File file = new File(fileUrl.getFile());
        System.out.println(file.exists());
        Result result = EweChatWebHookUtil.uploadFile(file);
        System.out.println(result.toString());
        // {"errcode":0,"errmsg":"ok","media_id":"3QtSsDv5R-shKSFuMM0BWFCauDwhbBmWqbz_uqGT9xO0","created_at":"1618812990","type":"file"}
        Assert.assertTrue(result.getSuccess());

//        uploadByHttpClient();
    }

    /**
     * 测试文件信息，注意文件有效期为3天，过期无效
     * 
     * @Title testSendFileMsg
     * @author 吕凯
     * @date 2021年4月19日 下午2:45:12 void
     */
    @Test(priority = 5) // 不指定顺序时默认按字母顺序执行
    public void testSendFileMsg() {
        // 通过流上传
        try (InputStream uploadIs = IOUtils.readAsInputStream("upload.txt")) {

            Result result = EweChatWebHookUtil.sendFileMsg(uploadIs, "test.txt");
            System.out.println(result.toString());
            // {"errcode":0,"errmsg":"ok","media_id":"3QtSsDv5R-shKSFuMM0BWFCauDwhbBmWqbz_uqGT9xO0","created_at":"1618812990","type":"file"}
            Assert.assertTrue(result.getSuccess());
        } catch (IOException e) {
            log.error("单元测试出错！", e);
        }

        // 通过文件上传
        URL fileUrl = Thread.currentThread().getContextClassLoader().getResource("test.png");
        File file = new File(fileUrl.getFile());
        System.out.println(file.exists());
        Result result = EweChatWebHookUtil.sendFileMsg(file);
        System.out.println(result.toString());
        // {"errcode":0,"errmsg":"ok","media_id":"3QtSsDv5R-shKSFuMM0BWFCauDwhbBmWqbz_uqGT9xO0","created_at":"1618812990","type":"file"}

        Assert.assertTrue(result.getSuccess());

//        uploadByHttpClient();
    }

    /**
     * jsoup上传文件有问题，改用httpclient方式测试，未使用
     * 
     * @Title uploadByHttpClient
     * @author 吕凯
     * @date 2021年4月19日 下午2:42:18 void
     */
    @SuppressWarnings("unused")
    private void uploadByHttpClient() {
        HttpPost post = new HttpPost(
                "https://qyapi.weixin.qq.com/cgi-bin/webhook/upload_media?key=e4c6370f-9daa-40aa-97a9-141bb71dfc84&type=file");
        //
        HttpClient client = HttpClients.createDefault();

        // 代理为弱网测试内容
        HttpHost proxy = new HttpHost("127.0.0.1", 8866, "http");
        RequestConfig config = RequestConfig.custom().setProxy(proxy).build();
        post.setConfig(config);

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        URL fileUrl = Thread.currentThread().getContextClassLoader().getResource("test.png");
        File file = new File(fileUrl.getFile());
        System.out.println(file.exists());
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        // @2017-06-28 在文件上传中，有些系统不支持指定字符集(企业微信)
        // builder.setCharset(Charset.forName(FsSpec.Charset_Default) );
        String name = "media";

        // 先添加文件部分(无需指定编码)
        if (file != null && file.exists()) {
            builder.addBinaryBody(name, file, ContentType.DEFAULT_BINARY, file.getName());
        }

        post.setEntity(builder.build());
        HttpResponse response = null;
        try {
            response = client.execute(post);
            System.out.println(response.getEntity());
            // 判断返回状态是否为200
            if (response.getStatusLine().getStatusCode() == 200) {
                String resultString = EntityUtils.toString(response.getEntity(), "UTF-8");
                System.out.println(resultString);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 发送模块卡片-文本通知型消息
     * 
     * @Title testSendTaskCardMsg
     * @author 吕凯
     * @date 2021年4月20日 上午9:44:58 void
     */
    @Test(priority = 11) // 不指定顺序时默认按字母顺序执行
    public void testSendTemplCardTextMsg() {

        String content = "单元测试：向自定义应用发送任务卡片消息，测试结果反馈24，请注意修改。\n" //
                + " BUG:10个\n" //
                + " 建议:12个\n" //
                + " 咨询:2个"; // 文本信息

        Result result = EweChatWebHookUtil.sendTemplateCardTextMsg("测试模板卡片文本通知", content, "http://work.11315.cn:28800/");
        log.warn(result.toString());
        System.out.println(result.toString());
        Assert.assertTrue(result.getSuccess());

        // mainTitle、subTitleText必须有，其他可无
        TmplCardSourceModel source = TmplCardSourceModel.builder().iconUrl("http://work.11315.cn:28800/favicon.ico").desc(" 工作平台").build();
        TmplCardActionModel cardAction = TmplCardActionModel.builder().type(EweChatTool.ACTION_TYPE_URL)
                .url("http://work.11315.cn:28800/a/#/oaManage/oa_attendance_my").build();
        TmplCardMainTitleModel mainTitle = TmplCardMainTitleModel.builder().title("一级标题说明").desc("一级标题说明").build();
        TmplCardEmphasisContentModel emphasisContent = TmplCardEmphasisContentModel.builder().title("一级标题下核心数据").desc("一级标题下核心数据说明")
                .build();
        // 二级标题+文本列表，该字段可为空数组，但有数据的话需确认对应字段是否必填，列表长度不超过6
        TmplCardHorizontalContentModel horizontalContent1 = TmplCardHorizontalContentModel.build("主办单位", "谁知道呢？");
        TmplCardHorizontalContentModel horizontalContent2 = TmplCardHorizontalContentModel.buildUrl("网址", "点击查看",
                "http://work.11315.cn:28800/");
        // 跳转链接，最多3个，可无
        TmplCardActionModel jump = TmplCardActionModel.buildUrl("出勤管理", "http://work.11315.cn:28800/a/#/oaManage");

        TemplateCardTextModel model = TemplateCardTextModel.builder().source(source)
                // 标题必须有，但是里面的描述可以无
                .mainTitle(mainTitle).emphasisContent(emphasisContent)
                // 内容必须有
                .subTitleText(content).horizontalContentList(ImmutableList.of(horizontalContent1, horizontalContent2))
                .jumpList(ImmutableList.of(jump)).cardAction(cardAction).build();

        result = EweChatWebHookUtil.sendTemplateCardTextMsg(model);
        log.warn(result.toString());
        System.out.println(result.toString());
        Assert.assertTrue(result.getSuccess());

    }

    /**
     * 
     * 发送模块卡片-图文展示型消息
     * 
     * @Title testSendTemplCardNewsMsg
     * @author 吕凯
     * @date 2021年9月13日 下午2:56:35 void
     */
    @Test(priority = 12) // 不指定顺序时默认按字母顺序执行
    public void testSendTemplCardNewsMsg() {

        String imagePath = "http://img.zcool.cn/community/0292505760bc830000018c1b0a8941.jpg@800w_1l_2o"; // 文本信息

        Result result = EweChatWebHookUtil.sendTemplateCardNewsMsg("测试模板卡片图片展示型", imagePath, "http://work.11315.cn:28800/");
        log.warn(result.toString());
        System.out.println(result.toString());
        Assert.assertTrue(result.getSuccess());

        // mainTitle、subTitleText必须有，其他可无
        TmplCardSourceModel source = TmplCardSourceModel.builder().iconUrl("http://work.11315.cn:28800/favicon.ico").desc("工作平台").build();
        TmplCardActionModel cardAction = TmplCardActionModel.builder().type(EweChatTool.ACTION_TYPE_URL)
                .url("http://work.11315.cn:28800/a/#/oaManage/oa_attendance_my").build();
        TmplCardMainTitleModel mainTitle = TmplCardMainTitleModel.builder().title("一级标题说明").desc("一级标题说明").build();
        TmplCardImageModel cardImage = TmplCardImageModel.builder()
                .url("http://img.zcool.cn/community/0292505760bc830000018c1b0a8941.jpg@800w_1l_2o").build();
        // 二级标题+文本列表，该字段可为空数组，但有数据的话需确认对应字段是否必填，列表长度不超过6
        TmplCardHorizontalContentModel horizontalContent1 = TmplCardHorizontalContentModel.build("主办单位", "谁知道呢？");
        TmplCardHorizontalContentModel horizontalContent2 = TmplCardHorizontalContentModel.buildUrl("网址", "点击查看",
                "http://work.11315.cn:28800/");
        // 跳转链接，最多3个，可无
        TmplCardActionModel jump = TmplCardActionModel.buildUrl("出勤管理", "http://work.11315.cn:28800/a/#/oaManage");

        TemplateCardNewsModel model = TemplateCardNewsModel.builder().source(source)
                // 标题必须有，但是里面的描述可以无
                .mainTitle(mainTitle)
                // 图片
                .cardImage(cardImage).horizontalContentList(ImmutableList.of(horizontalContent1, horizontalContent2))
                .jumpList(ImmutableList.of(jump)).cardAction(cardAction).build();

        result = EweChatWebHookUtil.sendTemplateCardNewsMsg(model);
        log.warn(result.toString());
        System.out.println(result.toString());
        Assert.assertTrue(result.getSuccess());

    }

    public static void main(String[] args) {
        EweChatWebHookUtilTest test = new EweChatWebHookUtilTest();
//        test.testSendTemplCardTextMsg();
        test.testSendTemplCardNewsMsg();

    }

}
