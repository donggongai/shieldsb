package com.ld.shieldsb.common.core.io;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * 文件工具类的单元测试类
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月26日 上午9:29:32
 *
 */
@Slf4j
public class FileUtilsTest {

    /**
     * 测试复制文件
     * 
     * @Title testCopyFile
     * @author 吕凯
     * @date 2021年26日 上午9:29:32
     * @return void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void testCopyFile() {
        log.warn(FileUtils.CURRENT_WORK_DIR);
        File tempDirectory = FileUtils.getTempDirectory();
        File userDirectory = FileUtils.getUserDirectory();
        log.warn("临时目录：{}，File：{}", FileUtils.getTempDirectoryPath(), tempDirectory);
        log.warn("用户目录：{}，File：{}", FileUtils.getUserDirectoryPath(), userDirectory);
        System.out.println();
        System.out.println();
        log.warn("========================== 测试复制文件 =============================");
        URL fileUrl = Thread.currentThread().getContextClassLoader().getResource("test.png");
        String filePath = fileUrl.getFile();
        String filePathCopy = filePath.replace("test.png", "test-copy.png");
        FileUtils.delete(filePathCopy); // 先删除

        boolean result = FileUtils.copyFile(filePath, filePathCopy);
        Assert.assertTrue(result); // 期望结果：：成功

        String filePathNo = fileUrl.getFile().replace("test.png", "test-copy--.png"); // 文件不存在
        result = FileUtils.copyFile(filePathNo, filePathCopy);
        Assert.assertFalse(result); // 期望结果：：失败

        String filePathCopy2 = filePath.replace("test.png", "filecopy/test-copy.png"); // 带目录
        result = FileUtils.copyFile(filePath, filePathCopy2);
        Assert.assertTrue(result); // 期望结果：：成功，自动创建目录

        FileUtils.delete(filePathCopy); // 删除复制文件
        FileUtils.deleteCaseParent(filePathCopy2); // 删除复制文件，并且将上级的空目录也删除
        File file = new File(filePathCopy2);
        Assert.assertFalse(file.getParentFile().exists()); // 期望结果：：false，上级目录不存在，使用默认delete方法不会删除上级的空目录

        // 批量删除
        List<File> files = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            String fileListPath = fileUrl.getFile().replace("test.png", i + "-test-copy--.png"); // 文件不存在
            FileUtils.copyFile(filePath, fileListPath);
            File fileListItem = new File(fileListPath);
            files.add(fileListItem);
        }
        Assert.assertEquals(files.size(), 10);
        FileUtils.delete(files);
        Assert.assertFalse(new File("0-test-copy--.png").exists()); // 期望结果：：false，文件不存在
    }

    /**
     * 测试复制目录
     * 
     * @Title testCopyDir
     * @author 吕凯
     * @date 2021年4月29日 下午5:29:53 void
     */
    @Test(priority = 1) // 不指定顺序时默认按字母顺序执行
    public void testCopyDir() {
        System.out.println();
        System.out.println();
        log.warn("========================== 测试复制目录 =============================");
        URL dirUrl = Thread.currentThread().getContextClassLoader().getResource("properties");
        String dirPath = dirUrl.getFile();
        String dirPathCopy = dirPath.replace("properties", "properties-copy");
        boolean result = FileUtils.copyDirectory(dirPath, dirPathCopy); // 复制
        Assert.assertTrue(result); // 期望结果：：成功

        String dirPathNo = dirPath.replace("properties", "properties-copy--"); // 目录不存在
        result = FileUtils.copyDirectory(dirPathNo, dirPathCopy);
        Assert.assertFalse(result); // 期望结果：：失败，目录不存在

        dirPathNo = dirPath.replace("properties", "test.png"); // 目标为文件
        result = FileUtils.copyDirectory(dirPath, dirPathNo);
        Assert.assertFalse(result); // 期望结果：：失败，目标不是目录

        FileUtils.delete(dirPathCopy); // 删除复制目录
    }

    /**
     * 
     * 测试创建文件和目录
     * 
     * @Title testCreate
     * @author 吕凯
     * @date 2021年4月29日 下午6:51:15 void
     */
    @Test(priority = 2) // 不指定顺序时默认按字母顺序执行
    public void testCreate() {
        System.out.println();
        System.out.println();
        log.warn("========================== 创建文件目录 =============================");
        URL dirUrl = Thread.currentThread().getContextClassLoader().getResource("properties");
        String dirPath = dirUrl.getFile();
        String filePathCreate = dirPath.replace("properties", "createfile.txt");
        boolean result = FileUtils.createFile(filePathCreate); // 创建
        Assert.assertTrue(result); // 期望结果：：成功

        FileUtils.delete(filePathCreate); // 删除文件

        String filePathCreate1 = dirPath.replace("properties", "createdir/createfile.txt");
        result = FileUtils.createFile(filePathCreate1); // 创建
        Assert.assertTrue(result); // 期望结果：：成功,带目录

        FileUtils.deleteCaseParent(filePathCreate1); // 删除文件，并且将上级的空目录也删除

        String filePathCreate2 = dirPath.replace("properties", "createdir2");
        File file = new File(filePathCreate2);
        filePathCreate2 = file.getAbsolutePath();
        result = FileUtils.createDirectory(filePathCreate2); // 创建
        Assert.assertTrue(result); // 期望结果：：成功,带目录

        FileUtils.delete(filePathCreate2); // 删除文件

    }

    /**
     * 
     * 测试文件的写入和读取
     * 
     * @Title testReadWrite
     * @author 吕凯
     * @date 2021年4月29日 下午6:51:06 void
     */
    @Test(priority = 3) // 不指定顺序时默认按字母顺序执行
    public void testReadWrite() {
        System.out.println();
        System.out.println();
        log.warn("========================== 测试文件的写入和读取 =============================");
        URL dirUrl = Thread.currentThread().getContextClassLoader().getResource("properties");
        String dirPath = dirUrl.getFile();
        File file = new File(dirPath);
        String filePathCreate = file.getAbsolutePath().replace("properties", "createfile.txt");
        FileUtils.createFile(filePathCreate); // 创建
        String content = "测试";
        FileUtils.writeToFile(filePathCreate, content);
        try {
            FileUtils.append(filePathCreate, "append");
            String contentRead = FileUtils.read(filePathCreate);
            Assert.assertEquals(contentRead, content + "append"); // 期望结果：：成功,读取与写入结果一致

            FileUtils.write(filePathCreate, content); // 写入，会直接覆盖内容，文件如果不存在会抛出异常
            contentRead = FileUtils.read(filePathCreate);
            Assert.assertEquals(contentRead, content); // 期望结果：：成功,内容被覆盖
        } catch (IOException e) {
            log.error("", e);
        }
        FileUtils.delete(filePathCreate); // 删除文件

    }

    /**
     * 
     * 测试移动文件（重命名）
     * 
     * @Title testMove
     * @author 吕凯
     * @date 2021年4月30日 下午3:12:37 void
     */
    @Test(priority = 4) // 不指定顺序时默认按字母顺序执行
    public void testMove() {
        System.out.println();
        System.out.println();
        log.warn("========================== 测试移动文件（重命名） =============================");
        URL dirUrl = Thread.currentThread().getContextClassLoader().getResource("test.png");
        String dirPath = dirUrl.getFile();
        File file = new File(dirPath);
        String filePathBefore = file.getAbsolutePath().replace("test.png", "test-move-before.png");
        FileUtils.copyFile(dirPath, filePathBefore);
        String filePathAfter = file.getAbsolutePath().replace("test.png", "test-move-after.png");
        try {
            FileUtils.moveFile(filePathBefore, filePathAfter);
            Assert.assertTrue(FileUtils.exists(filePathAfter)); // 期望结果：：true,文件存在
        } catch (IOException e) {
            log.error("", e);
        }

        FileUtils.delete(filePathAfter); // 删除文件

    }

    /**
     * 
     * 获取某个前缀或后缀的文件
     * 
     * @Title testFilesByPrefix
     * @author 吕凯
     * @date 2021年4月30日 下午3:12:37 void
     */
    @Test(priority = 5) // 不指定顺序时默认按字母顺序执行
    public void testFilesByPrefix() {
        System.out.println();
        System.out.println();
        log.warn("========================== 获取某个前缀或后缀的文件 =============================");
        URL fileUrl = Thread.currentThread().getContextClassLoader().getResource("test.png");
        String filePath = fileUrl.getFile();
        File file = new File(filePath);
        // 批量删除
        List<File> files = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            String fileListPath = fileUrl.getFile().replace("test.png", "prefix-test-" + i + ".png"); // 文件不存在
            FileUtils.copyFile(filePath, fileListPath);
            File fileListItem = new File(fileListPath);
            files.add(fileListItem);
        }

        List<File> filesGet0 = FileUtils.getDirFilesByPrefix(file.getParent(), "prefix-test");
        Assert.assertEquals(filesGet0.size(), 3); // 期望结果：：true,个数为3

        List<File> filesGet1 = FileUtils.getDirFilesByPrefix(file.getParent(), "prefix-test", "prefix-test-0");
        Assert.assertEquals(filesGet1.size(), 2); // 期望结果：：true,排除了一个，剩余2个

        List<File> filesGet2 = FileUtils.getDirFilesByPrefixAndSurfix(file.getParent(), "prefix-test", "2.png");
        Assert.assertEquals(filesGet2.size(), 1); // 期望结果：：true,个数为1

        FileUtils.delete(files);
        Assert.assertFalse(new File("prefix-test-0.png").exists()); // 期望结果：：false，文件不存在

    }

    /**
     * 测试编码等其他
     * 
     * @Title testOther
     * @author 吕凯
     * @date 2021年4月30日 下午2:38:18 void
     */
    @Test(priority = 99) // 不指定顺序时默认按字母顺序执行
    public void testEncodeOthers() {
        System.out.println();
        System.out.println();
        log.warn("========================== 测试编码文件写入和读取 =============================");
        URL fileUrl = Thread.currentThread().getContextClassLoader().getResource("test.png");
        File file = new File(fileUrl.getFile());
        String filePath = file.getAbsolutePath();
        String imgBase64Str = FileUtils.convertFile2Base64(filePath); // 文件转换为base64格式字符串
        log.warn("本地图片转换Base64:{}", imgBase64Str);
        log.warn("Base64字符串length={} md5={}", imgBase64Str.length(), FileUtils.getFileMD5Hex(filePath));
        String filePathOut = file.getAbsolutePath().replace("test.png", "test-out.png");
        FileUtils.convertBase64ToFile(filePathOut, imgBase64Str); // 将base64格式的字符串转储为文件
        Assert.assertTrue(new File(filePathOut).exists()); // 期望结果：：true,文件存在

        FileUtils.delete(filePathOut); // 删除文件

        // xml格式文件转储
        String filePathXml = file.getAbsolutePath().replace("test.png", "ehcache.xml");
        String fileBase64Str = FileUtils.convertFile2Base64(filePathXml);
        log.warn("本地xml转换Base64:{}", fileBase64Str);
        log.warn("Base64字符串length={}", fileBase64Str.length());
        String filePathOutXmlOut = file.getAbsolutePath().replace("test.png", "ehcache-out.xml");
        FileUtils.convertBase64ToFile(filePathOutXmlOut, fileBase64Str);
        Assert.assertTrue(new File(filePathOutXmlOut).exists()); // 期望结果：：true,文件存在

        FileUtils.delete(filePathOutXmlOut); // 删除文件

        // 测试远程文件的转储
        String imgUrlStr = "https://s1.ax1x.com/2020/07/02/Nb5hb4.png";
        try {
            URL imgURL = new URL(imgUrlStr);
            imgBase64Str = FileUtils.convertFile2Base64(imgURL);
            log.warn("本地图片转换Base64:", imgBase64Str);
            log.warn("Base64字符串length={}", imgBase64Str.length());
            String filePathOut2 = file.getAbsolutePath().replace("test.png", "test-out2.png");
            FileUtils.convertBase64ToFile(filePathOut2, imgBase64Str);
            Assert.assertTrue(new File(filePathOut2).exists()); // 期望结果：：true,文件存在

            FileUtils.delete(filePathOut2); // 删除文件
        } catch (MalformedURLException e) {
            log.error("", e);
        }

        File tempFile = FileUtils.createTempDirectory(); // 测试临时目录
        log.warn("{}", tempFile);

        filePathOut = file.getAbsolutePath().replace("test.png", "a/b/c/d/test-out.png");
        File parDirsFile = new File(filePathOut);
        FileUtils.createDirs(filePathOut); // 测多级目录的创建
        Assert.assertTrue(parDirsFile.getParentFile().exists());
        FileUtils.deleteCaseParent(parDirsFile.getParentFile().getAbsolutePath()); // 删除文件

    }

}
