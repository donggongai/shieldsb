package com.ld.shieldsb.common.core.io.zip;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ld.shieldsb.common.core.io.FileUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 文件压缩的单元测试类
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年5月6日 下午4:15:21
 *
 */
@Slf4j
public class FileZipTest {

    /**
     * 测试
     * 
     * @Title testBase
     * @author 吕凯
     * @date 2021年5月6日 下午4:16:03 void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void testBase() {
        URL fileUrl = Thread.currentThread().getContextClassLoader().getResource("test-f.jpg");
        String filePath = fileUrl.getFile();
        File file = new File(filePath);
        log.warn(file.getName());
        String filePath2 = filePath.replace("test-f.jpg", "test.png");
        File file2 = new File(filePath2);
        String filePathCn = filePath.replace("test-f.jpg", "测试.png"); // 测试中文名的
        FileUtils.createFile(filePathCn);
        File fileCn = new File(filePathCn);
        List<File> srcFiles = new ArrayList<File>();
        srcFiles.add(file);
        srcFiles.add(file2);
        srcFiles.add(fileCn);

        String filePathOut = filePath.replace("test-f.jpg", "testout.zip");
        String filePathUnzip = filePath.replace("test-f.jpg", "testunzip");
        File zipFile = new File(filePathOut);// 最终打包的压缩包

        try {
            boolean result = FileZip.zipFiles(srcFiles, zipFile);
            Assert.assertTrue(result); // 期望结果：：true，文件压缩成功
            boolean unzip = FileZip.unZipFiles(filePathOut, filePathUnzip);
            Assert.assertTrue(unzip); // 期望结果：：true，文件解压成功
            File filePathUnzipDir = new File(filePathUnzip);
            List<String> filelist = Arrays.asList(filePathUnzipDir.list());
            Assert.assertEquals(filelist.size(), 3); // 期望结果：：3，3个文件
            Assert.assertTrue(filelist.contains("测试.png")); // 期望结果：：true，文件存在

        } catch (FileNotFoundException e) {
            log.error("", e);
        }
        FileUtils.delete(filePathOut); // 删除文件
        FileUtils.delete(filePathUnzip); // 删除文件

        // 压缩目录
        String filePathOut2 = filePath.replace("test-f.jpg", "testout2.zip");
        try {
            String filePathDir = filePath.replace("test-f.jpg", "properties");
            FileZip.zipFiles(filePathDir, "", new FileOutputStream(filePathOut2));
        } catch (FileNotFoundException e) {
            log.error("", e);
        }
        FileUtils.delete(filePathOut2); // 删除文件
    }

}
