package com.ld.shieldsb.common.core.util;

import org.testng.annotations.Test;

import com.github.zafarkhaja.semver.Version;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * TypeUtil的单元测试类
 * 
 * @ClassName TypeUtilTest
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年9月1日 上午8:42:09
 *
 */
@Slf4j
public class TypeUtilTest {

    /**
     * 
     * 测试
     * 
     * @Title test
     * @author 吕凯
     * @date 2021年5月7日 上午9:04:17 void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void test() {
        boolean flag = TypeUtil.isGeJdk8();
        log.warn("java版本是否大于java8：{}", flag);
        Version version = TypeUtil.getJavaVersion();
        log.warn("java版本：{}", version);
        log.warn("java版本：{}", version.getMajorVersion());
        log.warn("java版本：{}", version.getMinorVersion());
        log.warn("java版本：{}", version.getNormalVersion());
    }

}
