package com.ld.shieldsb.common.core.eventbus;

import com.google.common.eventbus.DeadEvent;
import com.google.common.eventbus.Subscribe;

public class DeadEventListener {
    boolean notDelivered = false;

    /**
     * 非监听范围内的对象
     * 
     * @Title listen
     * @author 吕凯
     * @date 2019年9月16日 下午5:12:52
     * @param event
     *            void
     */
    @Subscribe
    public void listen(DeadEvent event) {
        notDelivered = true;
    }

    public boolean isNotDelivered() {
        return notDelivered;
    }
}