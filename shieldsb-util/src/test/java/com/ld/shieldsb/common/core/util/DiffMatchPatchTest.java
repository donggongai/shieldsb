package com.ld.shieldsb.common.core.util;

import org.testng.annotations.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * 文本差异比较工具类的单元测试类
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月16日 下午4:18:45
 *
 */
@Slf4j
public class DiffMatchPatchTest {

    /**
     * 测试
     * 
     * @Title test
     * @author 吕凯
     * @date 2021年4月29日 上午11:16:15 void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void test() {

        DiffMatchPatch dmp = new DiffMatchPatch();
        String text1 = "稿件内容首次提交审核通过了，哈哈哈稿件审核的撒法通过过了，哈哈哈哈";
        String text2 = "稿件内容打算发生首次提交审核通过了，哈哈哈稿件审核通过初审通过了，哈哈哈哈";
        String diff = dmp.getHtmlDiffString(text1, text2);
        log.warn(diff);
    }

}
