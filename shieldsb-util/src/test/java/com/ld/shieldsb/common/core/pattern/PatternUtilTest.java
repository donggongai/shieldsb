package com.ld.shieldsb.common.core.pattern;

import java.util.List;
import java.util.regex.Pattern;

import org.testng.Assert;
import org.testng.annotations.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 正则表达式测试类
 * 
 * @ClassName PatternUtilTest
 * @author 刘金浩
 * @date 2021年6月23日 上午11:03:54
 *
 */
@Slf4j
public class PatternUtilTest {

    /**
     * 
     * 测试是否符合正则
     * 
     * @Title isMatchTest
     * @author 刘金浩
     * @date 2021年6月25日 上午8:06:14 void
     */
    @Test(priority = 0)
    public void isMatchTest() {
        String content = "ZZZaaabbbccc中文1234";
        // 给定字符串是否匹配给定正则
        boolean isMatch = PatternUtil.isMatch("\\w+[\u4E00-\u9FFF]+\\d+", content);
        Assert.assertTrue(isMatch);
    }

    /**
     * 
     * 测试获取第一个匹配项数据
     * 
     * @Title getTest
     * @author 刘金浩
     * @date 2021年6月25日 上午8:06:40 void
     */

    @Test(priority = 1)
    public void getTest() {
        String content = "550827627@qq.com中文1234@126.com";
        String resultGet = PatternUtil.get(PatternPool.EMAIL, content, 0);
        System.err.println(resultGet);
    }

    /**
     * 
     * 测试获取所有匹配项数据
     * 
     * @Title getTest
     * @author 刘金浩
     * @date 2021年6月25日 上午8:06:40 void
     */

    @Test(priority = 2)
    public void findAll() {
        String content = "550827627@qq.com中文1234@126.com";
        List<String> list = PatternUtil.findAllGroup0(PatternPool.EMAIL, content);
        System.err.println(list);
    }

    /**
     * 
     * 测试正则分组
     * 
     * @Title getGroups
     * @author 刘金浩
     * @date 2021年6月25日 上午8:07:15 void
     */
    @Test(priority = 3)
    public void getGroups() {
        Pattern pattern = Pattern.compile("(\\d+)-(\\d+)-(\\d+)");
        List<String> allGroups = PatternUtil.getAllGroups(pattern, "192-168-1-1");
        System.err.println(allGroups);
        allGroups = PatternUtil.getAllGroups(pattern, "192-168-1-1", false);
        System.err.println(allGroups);
    }

    /**
     * 
     * 测试内容中获取图片地址
     * 
     * @Title test
     * @author 刘金浩
     * @date 2021年6月25日 上午8:07:31 void
     */
    @Test(priority = 4)
    public void test() {
        String content = "dsafdf<img src=\"https://www.baidu.com/1/2gsa/333.jpg\" ></img><img src=\\\"http://www.baidu.com/1/2gsa/222.jpg\" ></img>sadfsadfdsafdsaf";
        String regex = "<img\\b[^<>]*?\\bsrc[\\s\\t\\r\\n]*=[\\s\\t\\r\\n]*[\"\"']?[\\s\\t\\r\\n]*(?<imgUrl>[^\\t\\r\\n\"\"'<>]*)[^<>]*?/?[\\s\\t\\r\\n]*>";
        List<String> listImg = PatternUtil.findAllGroup0(regex, content);
        for (String img : listImg) {
            String reg_html_img_src_http = "\\\"http?(.*?)(\\\"|>|\\\\s+)";
            System.err.println(PatternUtil.get(reg_html_img_src_http, img, 0));
        }
    }
}
