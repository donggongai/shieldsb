package com.ld.shieldsb.common.composition.util;

import org.testng.Assert;
import org.testng.annotations.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * 格式化工具类的单元测试类
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月16日 下午4:18:45
 *
 */
@Slf4j
public class FormatUtilTest {

    /**
     * 上传图片
     * 
     * @Title testSenTextByIds
     * @author 吕凯
     * @date 2021年4月16日 上午9:17:34 void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void test() {

        long size = 1031;
        String fmtStr = FormatUtil.byteFormat(size, 2);
        log.warn(fmtStr);
        Assert.assertEquals(fmtStr, "1.01 KB");

        String lengthNum = "1234";
        fmtStr = FormatUtil.formatIntLength(lengthNum, 2);
        log.warn(fmtStr);
        Assert.assertEquals(fmtStr, lengthNum);

        fmtStr = FormatUtil.formatIntLength(lengthNum, 5);
        log.warn(fmtStr);
        Assert.assertEquals(fmtStr, "0" + lengthNum);
    }

}
