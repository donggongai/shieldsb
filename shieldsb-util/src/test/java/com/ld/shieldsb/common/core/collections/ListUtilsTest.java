package com.ld.shieldsb.common.core.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ld.shieldsb.common.core.cache.model.TestModel;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * ListUtils的单元测试类
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年5月18日 上午10:52:44
 *
 */
@Slf4j
public class ListUtilsTest {

    /**
     * 测试
     * 
     * @Title test
     * @author 吕凯
     * @date 2021年26日 上午9:29:32
     * @return void
     */
    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void test() {
        TestModel model = new TestModel();
        model.setAge(10);
        model.setName("kevin");

        TestModel model1 = new TestModel();
        model1.setAge(11);
        model1.setName("kevin1");

        TestModel model2 = new TestModel();
        model2.setAge(12);
        model2.setName("kevin2");

        List<TestModel> list = new ArrayList<>();
        Assert.assertTrue(ListUtils.isEmpty(list)); // 期望结果：：true，为空

        list.add(model);
        list.add(model1);
        list.add(model2);
        Assert.assertTrue(ListUtils.isNotEmpty(list)); // 期望结果：：true，不为空
        Map<Integer, String> map = ListUtils.listField2Map(list, TestModel::getAge, TestModel::getName);
        log.warn("{}", map);
        Assert.assertEquals(map.keySet().size(), 3); // 期望结果：：相等，3个元素
        Assert.assertEquals(map.get(10), "kevin"); // 期望结果：：相等，10对应kevin

        TestModel model3 = new TestModel();
        model3.setAge(10);
        model3.setName("kevin3");

        list.add(model3);

        try {
            map = new HashMap<>();
            map = ListUtils.listField2Map(list, TestModel::getAge, TestModel::getName);
        } catch (Exception e) {
            Assert.assertTrue(MapUtils.isEmpty(map)); // 期望结果：：true，为空
            log.error("", e);
        }
        log.warn("{}", map);
        Assert.assertEquals(map.keySet().size(), 3); // 期望结果：：相等，3个元素
        Assert.assertEquals(map.get(10), "kevin3"); // 期望结果：：相等，10对应kevin

        map = ListUtils.listField2Map(list, TestModel::getAge, TestModel::getName, true); // 如果存在重复则保留第一个，默认保留最后一个
        log.warn("{}", map);
        Assert.assertEquals(map.keySet().size(), 3); // 期望结果：：相等，3个元素
        Assert.assertEquals(map.get(10), "kevin"); // 期望结果：：相等，10对应kevin

        Map<Integer, TestModel> modelmap = ListUtils.listField2Map(list, TestModel::getAge, true); // 如果存在重复则保留第一个，默认保留最后一个
        log.warn("{}", modelmap);
        Assert.assertEquals(modelmap.keySet().size(), 3); // 期望结果：：相等，3个元素
        Assert.assertEquals(modelmap.get(10).getName(), "kevin"); // 期望结果：：相等，10对应kevin

    }

}
