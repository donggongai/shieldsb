package com.ld.shieldsb.eventbus;

import com.google.common.eventbus.Subscribe;

public class SimpleListener {

    /**
     * 一个简单的Listener方法
     * 
     * @param event
     *            Guava规定此处只能有一个参数
     */
    @Subscribe
    public void doAction(final String event) {
        System.out.println(String.format("Received event [%s] and will take a action", event));
    }
}