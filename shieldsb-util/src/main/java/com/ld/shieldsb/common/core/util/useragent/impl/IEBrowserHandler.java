package com.ld.shieldsb.common.core.util.useragent.impl;

import com.ld.shieldsb.common.core.util.useragent.BrowserHandler;
import com.ld.shieldsb.common.core.util.useragent.BrowserModel;

public class IEBrowserHandler implements BrowserHandler {
    private final static String IE11 = "rv:11.0";
    private final static String IE10 = "MSIE 10.0";
    private final static String IE9 = "MSIE 9.0";
    private final static String IE8 = "MSIE 8.0";
    private final static String IE7 = "MSIE 7.0";
    private final static String IE6 = "MSIE 6.0";

    /**
     * 获取IE版本
     * 
     * @param userAgent
     * @return
     */
    public static Double getIEversion(String userAgent) {
        Double version = 0.0;
        if (BrowserHandler.getBrowserType(userAgent, IE11)) {
            version = 11.0;
        } else if (BrowserHandler.getBrowserType(userAgent, IE10)) {
            version = 10.0;
        } else if (BrowserHandler.getBrowserType(userAgent, IE9)) {
            version = 9.0;
        } else if (BrowserHandler.getBrowserType(userAgent, IE8)) {
            version = 8.0;
        } else if (BrowserHandler.getBrowserType(userAgent, IE7)) {
            version = 7.0;
        } else if (BrowserHandler.getBrowserType(userAgent, IE6)) {
            version = 6.0;
        }
        return version;
    }

    @Override
    public BrowserModel deal(String userAgent) {
        BrowserModel browserModel = new BrowserModel();
        if (checkBrowse(userAgent)) {
            browserModel.setBrowserType("Internet Explorer");
            browserModel.setBrowserVersion(getIEversion(userAgent) + "");
        }
        return browserModel;
    }

    @Override
    public boolean checkBrowse(String userAgent) {
        return isIE(userAgent);
    }

    // 判断是否是IE
    public static boolean isIE(String userAgent) {
        userAgent = userAgent.toLowerCase();
        return (userAgent.indexOf("msie") > 0 || userAgent.indexOf("rv:11.0") > 0) ? true : false;
    }

}
