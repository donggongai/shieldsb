package com.ld.shieldsb.common.core.util.notice.wechat.model;

import lombok.Data;

@Data
public class NewsMsgModel {
    private String title; // 标题
    private String description; // 描述
    private String url; // url
    private String picurl;// 图片url

}
