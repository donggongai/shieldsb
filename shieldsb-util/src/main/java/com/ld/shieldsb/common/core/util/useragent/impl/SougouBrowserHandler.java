package com.ld.shieldsb.common.core.util.useragent.impl;

import com.ld.shieldsb.common.core.util.useragent.BrowserHandler;
import com.ld.shieldsb.common.core.util.useragent.BrowserModel;
import com.ld.shieldsb.common.core.util.useragent.BrowserUtils;

public class SougouBrowserHandler implements BrowserHandler {
    private static final String BROWSER_TYPE = BrowserUtils.SOUGOU;

    @Override
    public BrowserModel deal(String userAgent) {
        BrowserModel browserModel = new BrowserModel();
        if (checkBrowse(userAgent)) {
            browserModel.setBrowserType(BROWSER_TYPE);
            browserModel.setBrowserVersion(BrowserUtils.getBrowserVersion(BROWSER_TYPE, userAgent));
        }
        return browserModel;
    }

    @Override
    public boolean checkBrowse(String userAgent) {
        return BrowserUtils.matchIn("SE 2.X", userAgent);
    }

}
