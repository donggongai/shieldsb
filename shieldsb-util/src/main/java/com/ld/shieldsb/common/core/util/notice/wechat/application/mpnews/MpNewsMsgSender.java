package com.ld.shieldsb.common.core.util.notice.wechat.application.mpnews;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.ld.shieldsb.common.core.model.Result;
import com.ld.shieldsb.common.core.util.StringUtils;
import com.ld.shieldsb.common.core.util.notice.wechat.EweChatTool;
import com.ld.shieldsb.common.core.util.notice.wechat.Type;
import com.ld.shieldsb.common.core.util.notice.wechat.application.EweChatApplicationParams;
import com.ld.shieldsb.common.core.util.notice.wechat.application.EwechatBasicMsgSender;

/**
 * 新闻
 * 
 * @ClassName NewsMsgSender
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年9月14日 上午9:42:23
 *
 */
public class MpNewsMsgSender extends EwechatBasicMsgSender {
    /**
     * 转换图文dataMap，mpnews类型的图文消息，跟普通的图文消息一致，唯一的差异是图文内容存储在企业微信。<br>
     * 多次发送mpnews，会被认为是不同的图文，阅读、点赞的统计会被分开计算。
     * 
     * @Title parse2MpNewsDataMap
     * @author 吕凯
     * @date 2021年4月19日 下午5:02:59
     * @param agentid
     * @param sendTo
     * @param title
     * @param msg
     * @param url
     * @param btnTxt
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Map<String,Object>
     */

    public static Map<String, Object> parse2MpNewsDataMap(String agentid, String sendTo, String title, String thumbMediaId, String author,
            String url, String content, String digest, Integer duplicateCheckInterval) {
        /*{
            "touser" : "UserID1|UserID2|UserID3",
            "toparty" : "PartyID1 | PartyID2",
            "totag" : "TagID1 | TagID2",
            "msgtype" : "mpnews",
            "agentid" : 1,
            "mpnews" : {
               "articles" : [
               {
                   "title" : "中秋节礼品领取",
                   "thumb_media_id": "MEDIA_ID",
                   "author": "Author",
                   "content_source_url": "URL",
                   "content": "Content",
                   "digest": "Digest description"
               }]
            },
            "safe":0,
            "enable_id_trans": 0,
            "enable_duplicate_check": 0,
            "duplicate_check_interval": 1800
         }*/

        Map<String, Object> dataMap = defaultDataMap(agentid, sendTo, duplicateCheckInterval);

        dataMap.put(EweChatTool.MSG_TYPE, Type.MPNEWS.value);

        Map<String, List<Map<String, String>>> dataNewsMap = new HashMap<>();
        List<Map<String, String>> newsArtiList = new ArrayList<>();
        Map<String, String> dataContentMap = new HashMap<>();
        dataContentMap.put(TITLE, title); // 必须有，标题，不超过128个字节，超过会自动截断（支持id转译）
        dataContentMap.put("thumb_media_id", thumbMediaId); // 必须有，图文消息缩略图的media_id, 可以通过素材管理接口获得。此处thumb_media_id即上传接口返回的media_id
        if (StringUtils.isNotEmpty(author)) {
            dataContentMap.put("author", author); // 可无,图文消息的作者，不超过64个字节
        }
        if (StringUtils.isNotEmpty(url)) {
            dataContentMap.put("content_source_url", url); // 可无,图文消息点击“阅读原文”之后的页面链接
        }
        dataContentMap.put(CONTENT, content); // 必须有，图文消息的内容，支持html标签，不超过666 K个字节（支持id转译）
        if (StringUtils.isNotEmpty(digest)) {
            dataContentMap.put("digest", digest); // 可无，图文消息的描述，不超过512个字节，超过会自动截断（支持id转译）
        }

        newsArtiList.add(dataContentMap);

        dataNewsMap.put("articles", newsArtiList);
        dataMap.put(Type.MPNEWS.value, dataNewsMap);

        return dataMap;
    }

    public static Map<String, Object> parse2MpNewsDataMap(String agentid, String sendTo, String title, String thumbMediaId, String author,
            String url, String content, String digest) {
        return parse2MpNewsDataMap(agentid, sendTo, title, thumbMediaId, author, url, content, digest, null);
    }

    public static Map<String, Object> parse2MpNewsDataMap(String agentid, String sendTo, String title, String thumbMediaId, String content,
            Integer duplicateCheckInterval) {
        return parse2MpNewsDataMap(agentid, sendTo, title, thumbMediaId, null, null, content, null, duplicateCheckInterval);
    }

    public static Map<String, Object> parse2MpNewsDataMap(String agentid, String sendTo, String title, String thumbMediaId,
            String content) {
        return parse2MpNewsDataMap(agentid, sendTo, title, thumbMediaId, null, null, content, null);
    }

    public static Result sendMpNewsMsg(String corpid, String corpsecret, String agentid, String sendTo, String title, String thumbMediaId,
            String author, String url, String content, String digest, Integer duplicateCheckInterval) {
        return sendMsg(corpid, corpsecret, agentid,
                parse2MpNewsDataMap(agentid, sendTo, title, thumbMediaId, author, url, content, digest, duplicateCheckInterval));

    }

    public static Result sendMpNewsMsg(String corpid, String corpsecret, String agentid, String sendTo, String title, String thumbMediaId,
            String author, String url, String content, String digest) {
        return sendMpNewsMsg(corpid, corpsecret, agentid, sendTo, title, thumbMediaId, author, url, content, digest, null);

    }

    /**
     * 发送存储在企业微信的图文消息
     * 
     * @Title parse2NewsDataMap
     * @author 吕凯
     * @date 2021年4月19日 下午4:32:10
     * @param sendTo
     * @param title
     * @param msg
     * @param url
     * @param btnTxt
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Result
     */
    public static Result sendMpNewsMsg(String sendTo, String title, String thumbMediaId, String author, String url, String content,
            String digest, Integer duplicateCheckInterval) {
        // 注意下面参数需要到企业微信中获取
        EweChatApplicationParams params = getConfig();
        return sendMpNewsMsg(params.getCorpId(), params.getCorpSecret(), params.getAgentId(), sendTo, title, thumbMediaId, author, url,
                content, digest, duplicateCheckInterval);
    }

    public static Result sendMpNewsMsg(String sendTo, String title, String thumbMediaId, String author, String url, String content,
            String digest) {
        return sendMpNewsMsg(sendTo, title, thumbMediaId, author, url, content, digest, null);
    }

    /**
     * 发送存储在企业微信的图文消息
     * 
     * @Title sendMpNewsMsg
     * @author 吕凯
     * @date 2021年4月21日 上午10:48:30
     * @param corpid
     * @param corpsecret
     * @param agentid
     * @param sendTo
     *            发送给
     * @param file
     *            文件
     * @param title
     *            必填，标题，不超过128个字节，超过会自动截断（支持id转译）
     * @param author
     *            非必填，图文消息的作者，不超过64个字节
     * @param url
     *            非必填，图文消息点击“阅读原文”之后的页面链接
     * @param content
     *            必填，图文消息的内容，支持html标签，不超过666 K个字节（支持id转译）
     * @param digest
     *            非必填，图文消息的描述，不超过512个字节，超过会自动截断（支持id转译）
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Result
     */
    public static final Result sendMpNewsMsg(String corpid, String corpsecret, String agentid, String sendTo, File file, String title,
            String author, String url, String content, String digest, Integer duplicateCheckInterval) {
        Result result = getAccessToken(corpid, corpsecret, agentid);
        if (result.getSuccess()) { // 获取成功
            String accessToken = result.getMessage();
            JSONObject uploadResult = uploadFile(accessToken, file);
            if (uploadResult != null) {
                Integer errCode = uploadResult.getInteger(EweChatTool.ERRCODE);
                if (Integer.valueOf(0).equals(errCode)) {
                    Map<String, Object> dataMap = parse2MpNewsDataMap(agentid, sendTo, title, uploadResult.getString(UPLOAD_RESULT_FILE_ID),
                            author, url, content, digest, duplicateCheckInterval); // map转换
                    return sendMsg(corpid, corpsecret, agentid, dataMap);
                }
            }
        }
        return result;
    }

    public static final Result sendMpNewsMsg(String corpid, String corpsecret, String agentid, String sendTo, File file, String title,
            String author, String url, String content, String digest) {
        return sendMpNewsMsg(corpid, corpsecret, agentid, sendTo, file, title, author, url, content, digest, null);
    }

    /**
     * 
     * 发送存储在企业微信的图文消息
     * 
     * @Title sendMpNewsMsg
     * @author 吕凯
     * @date 2021年4月21日 上午10:51:07
     * @param sendTo
     *            发送给
     * @param file
     *            文件
     * @param title
     *            必填，标题，不超过128个字节，超过会自动截断（支持id转译）
     * @param author
     *            非必填，图文消息的作者，不超过64个字节
     * @param url
     *            非必填，图文消息点击“阅读原文”之后的页面链接
     * @param content
     *            必填，图文消息的内容，支持html标签，不超过666 K个字节（支持id转译）
     * @param digest
     *            非必填，图文消息的描述，不超过512个字节，超过会自动截断（支持id转译）
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Result
     */
    public static Result sendMpNewsMsg(String sendTo, File file, String title, String author, String url, String content, String digest,
            Integer duplicateCheckInterval) {
        EweChatApplicationParams params = getConfig();
        return sendMpNewsMsg(params.getCorpId(), params.getCorpSecret(), params.getAgentId(), sendTo, file, title, author, url, content,
                digest, duplicateCheckInterval);

    }

    public static Result sendMpNewsMsg(String sendTo, File file, String title, String author, String url, String content, String digest) {
        return sendMpNewsMsg(sendTo, file, title, author, url, content, digest, null);

    }
}
