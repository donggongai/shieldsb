package com.ld.shieldsb.common.core.util.useragent;

/**
 * 
 * @author 浩
 *
 */
public enum BrowserType {
    IE11, IE10, IE9, IE8, IE7, IE6, Firefox, Safari, Chrome, Opera, Camino, Gecko
}
