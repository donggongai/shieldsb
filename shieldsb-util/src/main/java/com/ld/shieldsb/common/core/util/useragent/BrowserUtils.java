package com.ld.shieldsb.common.core.util.useragent;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ld.shieldsb.common.core.util.useragent.impl.ChromeBrowserHandler;
import com.ld.shieldsb.common.core.util.useragent.impl.DefaultBrowserHandler;
import com.ld.shieldsb.common.core.util.useragent.impl.FirefoxBrowserHandler;
import com.ld.shieldsb.common.core.util.useragent.impl.IEBrowserHandler;
import com.ld.shieldsb.common.core.util.useragent.impl.SafariBrowserHandler;
import com.ld.shieldsb.common.core.util.useragent.impl.SougouBrowserHandler;

/**
 * 判断浏览器工具
 * 
 * @ClassName BrowserUtils
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2019年7月19日 下午1:04:59
 *
 */
public class BrowserUtils {
    public final static String IE = "IE";
    public final static String CHROME = "Chrome";
    public final static String FIREFOX = "Firefox";
    public final static String SAFARI = "Safari";
    public final static String SOUGOU = "sougou";
    public final static String OPERA = "Opera";
    public final static String BAIDU = "baidubrowser";
    public final static String UC = "UCBrowser";
    public final static String QQ = "QQBrowser";
    public final static String MICRO_MESSENGER = "MicroMessenger"; // 微信内置浏览器
    public final static String WEIBO = "Weibo";
    public final static String MAXTHON = "Maxthon";
    public final static String GREEN = "GreenBrowser";
    public final static String SE360 = "360SE";
    public final static String OTHER = "其它";
    protected static final Map<String, BrowserHandler> BROWSER_TYPE_HANDLERS = new LinkedHashMap<>(); // 参数处理器,有序
    public static final String BROWSER_TYPE_DEFAULT_KEY = "defalult"; // 默认处理
    public static final BrowserHandler DEFALUT_HANDLER = new DefaultBrowserHandler(); // 默认处理
    static {
        BROWSER_TYPE_HANDLERS.put(SOUGOU, new SougouBrowserHandler()); // 搜狗浏览器,需要放到chrome前，否则被认为是chrome
        BROWSER_TYPE_HANDLERS.put(CHROME, new ChromeBrowserHandler()); // 谷歌
        BROWSER_TYPE_HANDLERS.put(FIREFOX, new FirefoxBrowserHandler()); // 火狐
        BROWSER_TYPE_HANDLERS.put(IE, new IEBrowserHandler()); // ie
        BROWSER_TYPE_HANDLERS.put(SAFARI, new SafariBrowserHandler()); // 苹果浏览器
        BROWSER_TYPE_HANDLERS.put(OPERA, DEFALUT_HANDLER); //
        BROWSER_TYPE_HANDLERS.put(BAIDU, DEFALUT_HANDLER); //
        BROWSER_TYPE_HANDLERS.put(UC, DEFALUT_HANDLER); //
        BROWSER_TYPE_HANDLERS.put(QQ, DEFALUT_HANDLER); //
        BROWSER_TYPE_HANDLERS.put(MICRO_MESSENGER, DEFALUT_HANDLER); // 微信内置浏览器
        BROWSER_TYPE_HANDLERS.put(WEIBO, DEFALUT_HANDLER); // 微信内置浏览器
        BROWSER_TYPE_HANDLERS.put(SE360, DEFALUT_HANDLER); // 360浏览器
        BROWSER_TYPE_HANDLERS.put(MAXTHON, DEFALUT_HANDLER); // 遨游
        BROWSER_TYPE_HANDLERS.put(GREEN, DEFALUT_HANDLER); //
        BROWSER_TYPE_HANDLERS.put(BROWSER_TYPE_DEFAULT_KEY, DEFALUT_HANDLER); // 默认，其他不符合条件的都走这个，需要最后一个验证
    }

    /**
     * 注册新的处理器，也可对旧的进行覆盖
     * 
     * @Title registerBrowserHandler
     * @author 吕凯
     * @date 2019年7月18日 下午2:04:11
     * @param paramNameprefix
     * @param defalutHandler
     *            void
     */
    public static void registerBrowserHandler(String browserType, BrowserHandler defalutHandler) {
        BROWSER_TYPE_HANDLERS.put(browserType, defalutHandler);
        // 保证默认处理为最后一个
        BROWSER_TYPE_HANDLERS.remove(BROWSER_TYPE_DEFAULT_KEY);
        BROWSER_TYPE_HANDLERS.put(BROWSER_TYPE_DEFAULT_KEY, DEFALUT_HANDLER); // 默认，其他不符合条件的都走这个，需要最后一个验证
    }

    // 判断是否是IE
    /**
     * 判断是否是ie浏览器
     * 
     * @Title isIE
     * @author 吕凯
     * @date 2019年7月19日 下午2:55:40
     * @param userAgent
     * @return boolean
     */
    public static boolean isIE(String userAgent) {
        userAgent = userAgent.toLowerCase();
        return (userAgent.indexOf("msie") > 0 || userAgent.indexOf("rv:11.0") > 0) ? true : false;
    }

    public static BrowserModel getBrowserModel(String userAgent) {
        BrowserModel browserModel = new BrowserModel();
        for (BrowserHandler handler : BROWSER_TYPE_HANDLERS.values()) {
            if (handler.checkBrowse(userAgent)) {
                browserModel = handler.deal(userAgent);
                break;
            }

        }
        return browserModel;
    }

    /**
     * 获取浏览器的类型
     * 
     * @Title checkBrowse
     * @author 吕凯
     * @date 2019年7月19日 下午2:55:14
     * @param userAgent
     * @return String
     */
    public static String checkBrowse(String userAgent) {
        BrowserModel browserModel = getBrowserModel(userAgent);
        return browserModel == null ? null : browserModel.getBrowserType();
    }

    /**
     * 在后面字符串中是否存在前面的字符
     * 
     * @Title matchIn
     * @author 吕凯
     * @date 2019年7月19日 下午2:52:19
     * @param regex
     * @param str
     * @return boolean
     */
    public static boolean matchIn(String regex, String str) {
        Pattern p = Pattern.compile(regex, Pattern.MULTILINE);
        Matcher m = p.matcher(str);
        return m.find();
    }

    /**
     * 获取浏览器版本号
     * 
     * @Title getBrowserVersion
     * @author 吕凯
     * @date 2019年7月19日 上午9:35:26
     * @param browserName
     * @param userAgent
     * @return String
     */
    public static String getBrowserVersion(String browserName, String userAgent) {
        String browserVersion = null;
        Pattern p = Pattern.compile(browserName + "/(.+)");
        Matcher m = p.matcher(userAgent);
        if (m.find() == true) {
            browserVersion = m.group(1).replaceAll("_", ".");
        }
        if (browserVersion != null && browserVersion.indexOf(" ") > 0) {// 形如"16.0.1 Gecko/20121011"
            browserVersion = com.ld.shieldsb.common.core.util.StringUtils.substringBefore(browserVersion, " ");
        }
        return browserVersion;
    }

}
