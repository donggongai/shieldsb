package com.ld.shieldsb.common.core.util.properties;

import java.io.UnsupportedEncodingException;

import org.apache.commons.configuration2.PropertiesConfiguration;

import com.ld.shieldsb.common.core.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 参数管理类
 * 
 * @ClassName SysconfigAction
 * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
 * @date 2014-6-21 下午2:37:40
 * 
 */
@Slf4j
public class ConfigPropertiesUtil extends ConfigAbstractUtil<PropertiesConfiguration> {

    public ConfigPropertiesUtil(String fileName) {
        super(fileName);
    }

    /**
     * 获取某个key的注释信息
     * 
     * @Title getComment
     * @author 吕凯
     * @date 2020年6月16日 上午8:37:26
     * @param configuration
     * @param key
     * @return
     * @see com.ld.shieldsb.common.core.util.properties.ConfigAbstractUtil#getComment(org.apache.commons.configuration2.FileBasedConfiguration,
     *      java.lang.String)
     */
    @Override
    protected String getComment(PropertiesConfiguration configuration, String key) {
        String comment = configuration.getLayout().getComment(key);
        if (comment != null) {
            try {
                comment = new String(comment.getBytes("iso-8859-1"));
            } catch (UnsupportedEncodingException e) {
                log.error("编码异常", e);
            }
            comment = comment.replace("#", "");
        }
        return comment;
    }

    /**
     * 保存注释
     * 
     * @Title saveComment
     * @author 吕凯
     * @date 2020年6月16日 上午8:37:52
     * @param key
     * @param comment
     * @param configuration
     * @see com.ld.shieldsb.common.core.util.properties.ConfigAbstractUtil#saveComment(java.lang.String, java.lang.String,
     *      org.apache.commons.configuration2.FileBasedConfiguration)
     */
    @Override
    protected void saveComment(String key, String comment, PropertiesConfiguration configuration) {
        if (StringUtils.isNotBlank(comment)) {
            String commentVal = comment;
            try {
                commentVal = new String(comment.getBytes(), "iso-8859-1");
            } catch (UnsupportedEncodingException e) {
                log.error("", e);
            }
            configuration.getLayout().setComment(key, commentVal);
        }

    }

}
