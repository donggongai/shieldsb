package com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard;

import lombok.Data;
import lombok.experimental.SuperBuilder;

/**
 * 一级标题,可为空
 * 
 * @ClassName TemplateCardMainTitle
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年9月13日 上午8:24:47
 *
 */
@Data
@SuperBuilder
public class TmplCardMainTitleModel {
    private String title; // 一级标题，建议不超过36个字
    private String desc; // 标题辅助信息，建议不超过44个字

}
