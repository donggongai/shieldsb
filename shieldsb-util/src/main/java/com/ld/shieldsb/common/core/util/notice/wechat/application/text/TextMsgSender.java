package com.ld.shieldsb.common.core.util.notice.wechat.application.text;

import java.util.HashMap;
import java.util.Map;

import com.ld.shieldsb.common.core.model.Result;
import com.ld.shieldsb.common.core.util.notice.wechat.EweChatTool;
import com.ld.shieldsb.common.core.util.notice.wechat.Type;
import com.ld.shieldsb.common.core.util.notice.wechat.application.EweChatApplicationParams;
import com.ld.shieldsb.common.core.util.notice.wechat.application.EwechatBasicMsgSender;

/**
 * 文本消息发送器
 * 
 * @ClassName TextMsgSender
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年9月14日 上午9:38:57
 *
 */
public class TextMsgSender extends EwechatBasicMsgSender {

    // ============================================= 文本消息 ==============================================
    /**
     * 调用配置文件中配置好的参数发送文本信息
     * 
     * @Title sendEnterpriseWeChatMsg
     * @author 吕凯
     * @date 2021年4月19日 下午3:14:33
     * @param sendTo
     * @param msg
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Result
     */
    public static Result sendTextMsg(String sendTo, String msg, Integer duplicateCheckInterval) {
        // 注意下面参数需要到企业微信中获取
        EweChatApplicationParams params = getConfig();
        return sendTextMsg(params.getCorpId(), params.getCorpSecret(), params.getAgentId(), sendTo, msg, duplicateCheckInterval);
    }

    public static Result sendTextMsg(String sendTo, String msg) {
        return sendTextMsg(sendTo, msg, null);
    }

    /**
     * 发送企业微信文本信息，参考https://work.weixin.qq.com/api/doc/#90000/90135/90236
     * 
     * @Title sendMsg
     * @author 吕凯
     * @date 2018年12月29日 下午3:27:01
     * @param corpid
     * @param corpsecret
     * @param agentid
     * @param sendTo
     *            发送给的用户，多个用户用|分割，@all为发送给所有用户
     * @param msg
     *            内容，支持换行、以及A标签(注意：换行符请用转义过的\n)
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Result
     */
    public static Result sendTextMsg(String corpid, String corpsecret, String agentid, String sendTo, String msg,
            Integer duplicateCheckInterval) {
        return sendMsg(corpid, corpsecret, agentid, parse2TextDataMap(agentid, sendTo, msg, duplicateCheckInterval));

    }

    public static Result sendTextMsg(String corpid, String corpsecret, String agentid, String sendTo, String msg) {
        return sendTextMsg(corpid, corpsecret, agentid, sendTo, msg, null);

    }

    /**
     * 转换为文本dataMap
     * 
     * @Title parse2TextDataMap
     * @author 吕凯
     * @date 2021年4月19日 下午4:12:53
     * @param agentid
     * @param sendTo
     * @param msg
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Map<String,Object>
     */
    public static Map<String, Object> parse2TextDataMap(String agentid, String sendTo, String msg, Integer duplicateCheckInterval) {
        Map<String, Object> dataMap = defaultDataMap(agentid, sendTo, duplicateCheckInterval);

        dataMap.put(EweChatTool.MSG_TYPE, Type.TEXT.value);
        Map<String, String> contentMap = new HashMap<>();
        contentMap.put(CONTENT, msg); // 消息内容，最长不超过2048个字节，超过将截断（支持id转译）
        dataMap.put(Type.TEXT.value, contentMap);
        return dataMap;
    }

    public static Map<String, Object> parse2TextDataMap(String agentid, String sendTo, String msg) {
        return parse2TextDataMap(agentid, sendTo, msg, null);
    }

}
