package com.ld.shieldsb.common.core.util.date;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.SimpleTimeZone;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 日期操作相关
 * 
 * @ClassName DateUtil
 * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
 * @date 2016年8月25日 下午3:02:07
 *
 */
@Slf4j
public class DateUtil {

    public static final int TIMEUNIT_MILLISECONDS = 0;
    public static final int TIMEUNIT_SECONDS = 1;
    public static final int TIMEUNIT_MINUTES = 2;
    public static final int TIMEUNIT_HOURS = 3;
    public static final int TIMEUNIT_DAYS = 4;

    public static final String DATE_FORMAT_PATTERN1 = "yyyy-MM-dd";
    public static final String DATE_FORMAT_PATTERN2 = "yyyy/MM/dd";
    public static final String DATE_FORMAT_PATTERN3 = "yyyyMMdd";
    public static final String DATE_TIME_FORMAT_PATTERN1 = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_TIME_FORMAT_PATTERN2 = "yyyy/MM/dd HH:mm:ss";
    public static final String DATE_TIMEMILL_FORMAT_PATTERN1 = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String DATE_TIMEMILL_FORMAT_PATTERN2 = "yyyy/MM/dd HH:mm:ss.SSS";
    public static final String DATE_MILL_FORMAT_PATTERN1 = "yyyy-MM-dd'T'HH:mm:ss.SSS Z"; // 毫秒
    public static final String DATE_MILL_FORMAT_PATTERN2 = "yyyy/MM/dd'T'HH:mm:ss.SSS Z"; // 毫秒

    // 判断日期格式的正则，yyyy-MM-dd HH:mm:ss或yyyy/MM/dd HH:mm:ss
    private static final String DATE_TIME_FORMAT_REGEX = "\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}|\\d{4}/\\d{2}/\\d{2} \\d{2}:\\d{2}:\\d{2}";
    // 判断日期格式的正则，yyyy-MM-dd HH:mm:ss.SSS或yyyy/MM/dd HH:mm:ss.SSS
    private static final String DATE_TIMEMILL_FORMAT_REGEX = "\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}\\.\\d{3}|\\d{4}/\\d{2}/\\d{2} \\d{2}:\\d{2}:\\d{2}\\.\\d{3}";

    // 2019-12-27T06:34:36.000Z
    private static final String DATE_MILL_FORMAT_REGEX = "\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}\\.\\d{3}Z|\\d{4}/\\d{2}/\\d{2}T\\d{2}:\\d{2}:\\d{2}\\.\\d{3}Z";

    // 判断日期格式的正则，yyyy-MM-dd或yyyy/MM/dd或yyyyMMdd
    private static final String DATE_FORMAT_REGEX = "\\d{4}-\\d{2}-\\d{2}|\\d{4}/\\d{2}/\\d{2}|\\d{4}\\d{2}\\d{2}";

    // 判断日期格式的正则，yyyy-MM或yyyy/MM
    private static final String MONTH_FORMAT_REGEX = "\\d{4}-\\d{2}|\\d{4}/\\d{2}";

    public static SimpleDateFormat getIeGmtSdf() {
        SimpleDateFormat ieGmtSdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.UK);
        ieGmtSdf.setTimeZone(new SimpleTimeZone(0, "GMT"));
        return ieGmtSdf;
    }

    public static SimpleDateFormat getGmtSdf() {
        SimpleDateFormat ieGmtSdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss z", Locale.UK);
        ieGmtSdf.setTimeZone(new SimpleTimeZone(0, "GMT"));
        return ieGmtSdf;
    }

    /**
     * 
     * 获取格林尼治时间
     * 
     * @Title getNowIeGMT
     * @param date
     *            日期
     * @return String
     */
    public static String getNowIeGMT(Date date) {
        return getIeGmtSdf().format(date);
    }

    /**
     * 
     * 获取格林尼治
     * 
     * @Title gmt2Date
     * @param gmtDate
     * @return Date
     */
    public static Date gmt2Date(String gmtDate) {
        try {
            if (gmtDate.matches("\\d+.*")) {
                return getGmtSdf().parse(gmtDate);
            } else if (gmtDate.matches("\\w+.*")) {
                return getIeGmtSdf().parse(gmtDate);
            }
        } catch (ParseException e) {
            log.error("dateStr:'" + gmtDate + "'", e);
        }
        return null;
    }

    /**
     * 
     * 获取当前日期yyyy-MM-dd
     * 
     * @Title getNowDate
     * @return String
     */
    public static String getNowDate() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    /**
     * 
     * 根据参数获取日期yyyy-MM-dd
     * 
     * @Title getDateString
     * @param date
     *            日期
     * @return String
     */
    public static String getDateString(Date date) {
        if (date == null) {
            return null;
        }
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    /**
     * 
     * 根据参数格式获取当前日期
     * 
     * @Title getNowDateTime
     * @param formatStr
     *            日期
     * @return String
     */
    public static String getNowDateTime(String formatStr) {
        if (formatStr == null) {
            return getNowDateTime();
        }
        return new SimpleDateFormat(formatStr).format(new Date());
    }

    /**
     * 
     * 获取当前时间yyyy-MM-dd HH:mm:ss
     * 
     * @Title getNowDateTime
     * @return String
     */
    public static String getNowDateTime() {
        return new SimpleDateFormat(DATE_TIME_FORMAT_PATTERN1).format(new Date());
    }

    /**
     * 
     * 根据参数获取yyyy-MM-dd HH:mm:ss格式的时间
     * 
     * @Title getDateTimeString
     * @param date
     *            日期
     * @return String
     */
    public static String getDateTimeString(Date date) {
        return new SimpleDateFormat(DATE_TIME_FORMAT_PATTERN1).format(date);
    }

    /**
     * 
     * 获取某天某个格式的日期
     * 
     * @Title getDateStr
     * @param date
     *            日期
     * @param format
     *            格式化类型
     * @return String
     */
    public static String getDateStr(Date date, String format) {
        return new SimpleDateFormat(format).format(date);
    }

    /**
     * 
     * 获取当前月份的第一天
     * 
     * @Title getMonthFirstDate
     * @return String
     */
    public static String getMonthFirstDate() {
        return new SimpleDateFormat("yyyy-MM-01 00:00:00").format(new Date());
    }

    /**
     * 
     * String类型转成Date，类型：yyyy-MM-dd
     * 
     * @Title str2date
     * @param dateStr
     * @return Date
     */
    public static Date str2Date(String dateStr) {
        return str2Date(dateStr, "yyyy-MM-dd");
    }

    /**
     * str转date（如果转换失败返回默认值）
     * 
     * @Title str2Date
     * @author 吕凯
     * @date 2016年10月11日 下午3:33:12
     * @param dateStr
     * @param format
     * @return Date
     */
    public static Date str2Date(String dateStr, String format) {
        return str2Date(dateStr, format, null);
    }

    /**
     * 字符串转日期，自动匹配格式，支持yyyy-MM-dd HH:mm:ss，yyyy-MM-dd，yyyy/MM/dd HH:mm:ss，yyyy/MM/dd等
     * 
     * @Title string2Date
     * @author 吕凯
     * @date 2020年8月11日 下午5:12:03
     * @param source
     * @return Date
     */
    public static Date string2Date(String source) {
        Date date = null;
        if (StringUtils.isBlank(source)) {
            return null;
        }
        source = source.trim();
        try {
            if (source.matches(DATE_FORMAT_REGEX)) { // 日期
                if (source.contains("-")) {
                    date = str2Date(source, DATE_FORMAT_PATTERN1);
                } else if (source.contains("/")) {
                    date = str2Date(source, DATE_FORMAT_PATTERN2);
                } else {
                    date = str2Date(source, DATE_FORMAT_PATTERN3);
                }
            } else if (source.matches(DATE_TIME_FORMAT_REGEX)) { // 时间
                if (source.contains("-")) {
                    date = str2Date(source, DATE_TIME_FORMAT_PATTERN1);
                } else if (source.contains("/")) {
                    date = str2Date(source, DATE_TIME_FORMAT_PATTERN2);
                }
            } else if (source.matches(DATE_MILL_FORMAT_REGEX)) { // 2017-11-18T07:12:06.000Z 格式
                source = source.replace("Z", " UTC");// UTC是本地时间
                if (source.contains("-")) {
                    date = str2Date(source, DATE_MILL_FORMAT_PATTERN1);
                } else if (source.contains("/")) {
                    date = str2Date(source, DATE_MILL_FORMAT_PATTERN2);
                }
            } else if (source.matches(DATE_TIMEMILL_FORMAT_REGEX)) { // 2017-11-18 07:12:06.000 格式
                if (source.contains("-")) {
                    date = str2Date(source, DATE_TIMEMILL_FORMAT_PATTERN1);
                } else if (source.contains("/")) {
                    date = str2Date(source, DATE_TIMEMILL_FORMAT_PATTERN2);
                }
            } else if (source.matches(MONTH_FORMAT_REGEX)) { // 年月
                if (source.contains("-")) {
                    date = str2Date(source + "-01", DATE_FORMAT_PATTERN1);
                } else if (source.contains("/")) {
                    date = str2Date(source + "/01", DATE_FORMAT_PATTERN2);
                }
            } else if (source.length() == 4) { // 年
                date = str2Date(source + "-01-01", DATE_FORMAT_PATTERN1);
            }

        } catch (Exception e) {
            log.error(String.format("parser %s to Date fail(日期转换失败)", source), e);
        }

        return date;

    }

    /**
     * 判断是否是日期格式，支持格式有：yyyy-MM-dd，yyyy/MM/dd，yyyy-MM-dd HH:mm:ss，yyyy/MM/dd HH:mm:ss，yyyy-MM-dd'T'HH:mm:ss.SSS Z，yyyy/MM/dd'T'HH:mm:ss.SSS Z
     * 
     * @Title isDate
     * @author 吕凯
     * @date 2021年2月19日 上午11:21:02
     * @param source
     * @return Boolean
     */
    public static Boolean isDate(String source) {
        Boolean result = false;
        if (StringUtils.isBlank(source)) {
            return result;
        }
        if (source.matches(DATE_FORMAT_REGEX) // 日期
                || source.matches(DATE_TIME_FORMAT_REGEX)// 时间
                || source.matches(DATE_MILL_FORMAT_REGEX)) { // 2017-11-18T07:12:06.000Z 格式
            result = true;
        }

        return result;
    }

    /**
     * 
     * String 转成dateTime
     * 
     * @Title str2dateTime
     * @param dateStr
     *            日期
     * @return Date
     */
    public static Date str2dateTime(String dateStr) {
        return str2dateTime(dateStr, null);
    }

    /**
     * 
     * String类型转成yyyy-MM-dd HH:mm:ss
     * 
     * @Title str2dateTime
     * @param dateStr
     *            日期
     * @param defaultDate
     *            默认日期
     * @return Date
     */
    public static Date str2dateTime(String dateStr, Date defaultDate) {
        return str2Date(dateStr, DATE_TIME_FORMAT_PATTERN1, defaultDate);
    }

    /**
     * 
     * str转date（如果转换失败返回默认值）
     * 
     * @Title str2dateTime
     * @author 吕凯
     * @date 2016年10月11日 下午3:32:23
     * @param dateStr
     * @param format
     * @param defaultDate
     * @return Date
     */
    public static Date str2Date(String dateStr, String format, Date defaultDate) {
        if (!StringUtils.isEmpty(dateStr)) {
            try {
                return new SimpleDateFormat(format).parse(dateStr);
            } catch (Exception e) {
                log.warn("日期转换错误，dateStr‘" + dateStr + "’，" + e.getMessage());
            }
        }
        return defaultDate;
    }

    public static Date getLongStringToDate(String dateStr) throws ParseException {
        Date retDate = new SimpleDateFormat(DATE_TIME_FORMAT_PATTERN1).parse(dateStr);
        return retDate;
    }

    /**
     * 
     * 是否是当前时间的下一天
     * 
     * @Title isNextDate
     * @param dateStr
     *            日期
     * @return
     * @throws ParseException
     *             boolean
     */
    public static boolean isNextDate(String dateStr) throws ParseException {
        if (dateStr == null || dateStr.trim().equals("")) {
            return false;
        }
        Date retDate = new SimpleDateFormat("yyyy-MM-dd").parse(dateStr);
        return retDate.getTime() > System.currentTimeMillis();
    }

    /**
     * 
     * 返回增加某天后的时间
     * 
     * @Title addDate
     * @param date
     * @param addNum
     * @param addUnit
     * @return Date
     */
    public static Date addDate(Date date, int addNum, int addUnit) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(addUnit, addNum);
        return c.getTime();
    }

    public static Timestamp getLongStringToTimestamp(String dateStr) {
        Timestamp time = Timestamp.valueOf(dateStr);
        return time;
    }

    /**
     * 
     * 判断2个日期月份是否相同
     * 
     * @Title isSameMonth
     * @param date1
     *            前一个月份
     * @param date2
     *            后一个月份
     * @return boolean
     */
    public static boolean isSameMonth(Date date1, Date date2) {
        Calendar calst = Calendar.getInstance();
        Calendar caled = Calendar.getInstance();
        calst.setTime(date1);
        caled.setTime(date2);
        return calst.get(Calendar.MONTH) == caled.get(Calendar.MONTH);
    }

    /**
     * 
     * 根据参数检查两个时间是否想同
     * 
     * @Title isSameTimeByParam
     * @author 于鹏
     * @date 2020年7月16日 上午9:11:55
     * @param date1
     * @param date2
     * @return boolean
     */
    public static boolean isSameTimeByParam(Date date1, Date date2, Integer par) {
        Calendar calst = Calendar.getInstance();
        Calendar caled = Calendar.getInstance();
        calst.setTime(date1);
        caled.setTime(date2);
        return calst.get(par) == caled.get(par);
    }

    /**
     * 检查2个日期是否相等(检查到秒)
     * 
     * @Title checkDateIsEqual
     * @author 吕凯
     * @date 2019年2月23日 下午12:05:08
     * @param date1
     * @param date2
     * @return boolean
     */
    public static boolean isSameDate(Date date1, Date date2) {
        if (date1 == null && date2 == null) {
            return true;
        } else if (date1 != null && date2 != null) {
            return DateUtil.getDateTimeString(date1).equals(DateUtil.getDateTimeString(date2));
        }
        return false;
    }

    /**
     * 
     * 根据两个时间差值返回对应的数字,设置保留数字，默认两位
     * 
     * @Title getDateDifference
     * @author 于鹏
     * @date 2020年7月16日 下午12:00:46
     * @param date1
     *            开始时间
     * @param date2
     *            结束时间
     * @param pre
     *            计算单位，时分秒，月日
     * @param digit
     *            返回数字的保留位数
     * @param strategy
     *            返回数字的策略 具体查看BigDecimal常量
     * @return double
     */
    public static double getDateDifference(Date date1, Date date2, Integer pre, Integer digit, Integer strategy) {
        double difference = 0;
        long time1 = date1.getTime();
        long time2 = date2.getTime();
        long timeD = time2 - time1;

        switch (pre) {
            case Calendar.SECOND:
                difference = timeD / 1000D;
                break;
            case Calendar.MINUTE:
                difference = timeD / 1000 / 60D;
                break;
            case Calendar.HOUR_OF_DAY:
                difference = timeD / 1000 / 60 / 60D;
                break;
            case Calendar.DAY_OF_YEAR:
                difference = timeD / 1000 / 60 / 60 / 24D;
                break;
            default:
                break;
        }
        BigDecimal b = new BigDecimal("" + difference);
        double f1 = b.setScale(digit == null ? 2 : digit, strategy == null ? BigDecimal.ROUND_HALF_UP : strategy).doubleValue();
        return f1;
    }

    /**
     * 
     * Date 转 LocalDateTime
     * 
     * @Title date2LocalDateTime
     * @author 于鹏
     * @date 2020年8月12日 上午9:10:43
     * @param date
     * @return LocalDateTime
     */
    public static LocalDateTime date2LocalDateTime(Date date) {
        /*
         * 为什么选择转为LocalDateTime而不是TemporalAdjuster LocalDateTime提供toLocalDate()，toLocalTime()等方法（具体见api）进行转化 反正方法非常的多撒，自己瞅呗
         */
        Instant instant = date.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zone);
        return localDateTime;
    }

    /**
     * 
     * 根据id获取后11位文字显示，string：XXX-XXXX-XXXX，如1231142225333=》311-4222-5333
     * 
     * @Title getDh
     * @param id
     *            时间戳
     * @return String
     */
    public static String getDh(long id) {
        String dh = "";
        String dh1 = id + "";
        if (dh1.length() == 13) {
            dh = dh1.substring(2, 5) + "-" + dh1.substring(5, 9) + "-" + dh1.substring(9, 13);
        } else {
            dh = dh1;
        }
        return dh;
    }

    /**
     * 
     * 得到两个日期相差的天数（只计算日期差不考虑小时数秒数等）
     * 
     * @Title daysBetween
     * @param early
     *            前一个天数
     * @param late
     *            后一个天数
     * @return int
     */
    public static final int daysBetween(Date early, Date late) {
        Calendar calst = Calendar.getInstance();
        Calendar caled = Calendar.getInstance();
        diffDates(calst, caled, early, late);
        // 得到两个日期相差的天数
        int days = ((int) (caled.getTime().getTime() / 1000) - (int) (calst.getTime().getTime() / 1000)) / 3600 / 24;
        return days;
    }

    /**
     * 
     * 得到两个日期相差的小时数
     * 
     * @Title hoursBetween
     * @param early
     * @param late
     * @return int
     */
    public static final int hoursBetween(Date early, Date late) {
        return minutesBetween(early, late) / 60;
    }

    /**
     * 
     * 得到两个日期相差的分钟数
     * 
     * @Title minutesBetween
     * @param early
     * @param late
     * @return int
     */
    public static final int minutesBetween(Date early, Date late) {
        int minutes = ((int) (late.getTime() / 1000) - (int) (early.getTime() / 1000)) / 60;
        return minutes;
    }

    /**
     * 
     * 得到两个日期相差的毫秒数
     * 
     * @Title timeBetween
     * @param early
     * @param late
     * @return long
     */
    public static final long timeBetween(Date early, Date late) {
        return late.getTime() - early.getTime();
    }

    /**
     * 判断2个日期类型是否相等，如果都为null也相等
     * 
     * @Title equal
     * @author 吕凯
     * @date 2019年10月15日 上午10:46:27
     * @param early
     * @param late
     * @return long
     */
    public static final boolean equals(Date date1, Date date2) {
        if (date1 == null && date2 == null) {
            return true;
        } else if (date1 == null || date2 == null) {
            return false;
        }
        return timeBetween(date1, date2) == 0;
    }

    private static void diffDates(Calendar calst, Calendar caled, Date early, Date late) {
        calst.setTime(early);
        caled.setTime(late);
        // 设置时间为0时
        calst.set(Calendar.HOUR_OF_DAY, 0);
        calst.set(Calendar.MINUTE, 0);
        calst.set(Calendar.SECOND, 0);
        caled.set(Calendar.HOUR_OF_DAY, 0);
        caled.set(Calendar.MINUTE, 0);
        caled.set(Calendar.SECOND, 0);
    }

    /**
     * 将毫秒数转换为时间表示1000(ms)==》1秒
     * 
     * @Title formatMill
     * @author 吕凯
     * @date 2013-12-17 下午2:29:03
     * @param time
     *            毫秒数
     * @return String
     */
    public static String millFormat(double time) {
        return millFormat(time, TIMEUNIT_MILLISECONDS);
    }

    /**
     * 将毫秒数转换为时间表示1000(ms)==》1秒
     * 
     * @Title formatMill
     * @author 吕凯
     * @date 2013-12-17 下午2:29:03
     * @param time
     *            毫秒数
     * @param timeunit
     *            计算到单位
     * @return String
     */

    public static String millFormat(double time, int timeunit) {
        String[] sizearr = { "毫秒", "秒", "分钟", "小时", "天" };
        int[] steps = { 1000, 60, 60, 24, 30 };
//        time = ArithUtil.div(time, 1, scale);
        String result = "";
        boolean minus = false;
        double sized = time;
        if (time < 0) {
            minus = true;
            sized = -sized;
        }
        for (int i = 0; i < steps.length; i++) {
            double rem = sized % steps[i];
            double value = sized / steps[i];
            if (rem != 0 || time == 0) {
                if (timeunit <= i) {
                    result = (int) rem + sizearr[i] + result;
                }
                if (value > 1) {
                    sized /= steps[i];
                } else {
                    break;
                }
            } else {
                if (value >= steps[i + 1]) {
                    sized /= steps[i];
                } else {
                    result = (int) (value) + sizearr[i + 1] + result;
                    break;
                }
            }
        }
        if (minus) {
            result += "前";
        }
        return result;
    }

    /**
     * 
     * 获取当前是第几周
     * 
     * @Title weekOfDate
     * @param d1
     * @return int
     */
    public static int weekOfDate(java.util.Date d1) {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(d1);
        return gc.get(Calendar.DAY_OF_WEEK);
    }

    /**
     * 
     * 获取当前天数
     * 
     * @Title getDayInYear
     * @param d1
     * @return int
     */
    public static int getDayInYear(java.util.Date d1) {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(d1);
        return gc.get(Calendar.DAY_OF_YEAR);
    }

    /**
     * 
     * 获取年
     * 
     * @Title getYear
     * @param date
     * @return int
     */
    public static int getYear(java.util.Date date) {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(date);
        return gc.get(Calendar.YEAR);
    }

    /**
     * 
     * 获取当前月份
     * 
     * @Title getMonthInYear
     * @param date
     * @return int
     */
    public static int getMonthInYear(java.util.Date date) {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(date);
        return gc.get(Calendar.MONTH);
    }

    /**
     * @Description:获取当前随机的一个时间点
     * @return
     * @author: 玄承勇
     * @date: 2017年2月10日 上午9:17:11
     */
    public static Date getNowRandomTime() {
        String dateStr = getNowDate();
        int hour = RandomUtils.nextInt(24);
        int minute = RandomUtils.nextInt(60);
        int second = RandomUtils.nextInt(60);
        return str2dateTime(dateStr + " " + hour + ":" + minute + ":" + second);

    }

    /*
     * 输入日期字符串，返回当月最后一天的Date
     */
    public static Date getMaxDateMonth(String month, String dateFmt) {
        try {
            Calendar calendar = Calendar.getInstance();
            Date nowDate = str2Date(month, dateFmt);
            calendar = Calendar.getInstance();
            calendar.setTime(nowDate);
            // 设置为当月最后一天
            calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            // 将小时至23
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            // 将分钟至59
            calendar.set(Calendar.MINUTE, 59);
            // 将秒至59
            calendar.set(Calendar.SECOND, 59);
            // 将毫秒至999
            calendar.set(Calendar.MILLISECOND, 999);
            return calendar.getTime();
        } catch (Exception e) {
            log.error("", e);
        }
        return null;
    }

    /**
     * 
     * 计算两个时间差,返回中文格式化后的字符串，xx天xx分xx秒
     * 
     * @Title getDatePoor
     * @author 刘金浩
     * @date 2019年11月5日 下午3:45:11
     * @param early
     * @param late
     * @return String
     */
    public static String formatTimeBetween(Date early, Date late) {
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = timeBetween(early, late);
        return millFormat(diff, TIMEUNIT_MINUTES);
    }

    /**
     * 判断日期是否为周末
     *
     * @author 张和祥 2020/7/13 16:05
     */
    public static boolean isWeekend(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY;
    }

    /**
     * 
     * 获取当前周的第一天
     * 
     * @Title getStartDayOfWeek
     * @author 武杨
     * @date 2020年9月2日 上午10:02:19
     * @return Date
     */
    public static String getStartDayOfWeek() {
        LocalDate today = LocalDate.now();
        return today.with(DayOfWeek.MONDAY).toString();
    }

    /**
     *
     * 判断某个时间是否包含在某个时间段内
     *
     * @param dateStr
     *            待比较的时间
     * @param startTimeStr
     *            时间范围的开始时间,eg("06:00")
     * @param endTimeStr
     *            时间范围的结束时间,eg("22:00")
     *
     */
    public static Boolean dateIsContain(String dateStr, String startTimeStr, String endTimeStr) {
        // 默认时间格式
        return dateIsContain("yyyy-MM-dd", dateStr, startTimeStr, endTimeStr);
    }

    /**
     *
     * 判断当前时间是否包含在某个时间段内
     *
     * @param startTimeStr
     *            时间范围的开始时间,eg("06:00")
     * @param endTimeStr
     *            时间范围的结束时间,eg("22:00")
     *
     */
    public static Boolean nowDateIsContain(String startTimeStr, String endTimeStr) {
        // 默认当前时间
        String dateStr = getDateString(new Date());
        return dateIsContain("yyyy-MM-dd", dateStr, startTimeStr, endTimeStr);
    }

    /**
     *
     * 判断当前时间是否包含在某个时间段内
     *
     * @param startTimeStr
     *            时间范围的开始时间,eg("06:00")
     * @param endTimeStr
     *            时间范围的结束时间,eg("22:00")
     *
     */
    public static Boolean nowDateIsContain(String format, String startTimeStr, String endTimeStr) {
        // 默认当前时间
        return dateIsContain(format, getNowDateTime(format), startTimeStr, endTimeStr);
    }

    /**
     *
     * 判断当前时间是否包含在某个时间段内
     * 
     * @param startTime
     *            时间范围的开始时间,eg("06:00")
     * @param endTime
     *            时间范围的结束时间,eg("22:00")
     *
     */
    public static Boolean nowDateIsContain(Date startTime, Date endTime) {
        // 默认当前时间
        return dateIsContain("yyyy-MM-dd", new Date(), startTime, endTime);
    }

    /**
     *
     * 判断某个时间是否包含在某个时间段内
     * 
     * @param format
     *            时间格式,eg("HH:mm")
     * @param startTimeStr
     *            时间范围的开始时间,eg("06:00")
     * @param endTimeStr
     *            时间范围的结束时间,eg("22:00")
     *
     */
    public static Boolean dateIsContain(String format, String dateStr, String startTimeStr, String endTimeStr) {
        // 设置日期格式
        Date date = str2Date(dateStr, format);
        Date beginTime = str2Date(startTimeStr, format);
        Date endTime = str2Date(endTimeStr, format);
        return dateIsContain(date, beginTime, endTime);
    }

    /**
     * 判断某个时间是否包含在某个时间段内
     * 
     * @param format
     *            时间格式,eg("HH:mm")
     * @param dateTime
     *            待比较的时间
     * @param startTime
     *            时间范围的开始时间
     * @param endTime
     *            时间范围的结束时间
     */
    public static boolean dateIsContain(String format, Date dateTime, Date startTime, Date endTime) {
        // 设置日期格式
        dateTime = str2Date(getDateStr(dateTime, format), format);
        startTime = str2Date(getDateStr(startTime, format), format);
        endTime = str2Date(getDateStr(endTime, format), format);
        return dateIsContain(dateTime, startTime, endTime);
    }

    /**
     * 判断某个时间是否包含在某个时间段内
     * 
     * @param dateTime
     *            待比较的时间
     * @param startTime
     *            时间范围的开始时间
     * @param endTime
     *            时间范围的结束时间
     * @return
     */
    public static boolean dateIsContain(Date dateTime, Date startTime, Date endTime) {
        if (dateTime.after(startTime) && dateTime.before(endTime)) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    public static void main(String[] args) {
        System.out.println(getNowRandomTime());
        System.out.println(millFormat(11151200, TIMEUNIT_SECONDS));
        String time = "2016-06-23T09:46:27.000Z";
        Date date = null;
        time = time.replace("Z", " UTC"); // UTC是本地时间
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
        try {
            date = formatter.parse(time);
            System.out.println(date);
            System.out.println(getStartDayOfWeek());
        } catch (ParseException e) {
            log.error("", e);
        }
    }
}
