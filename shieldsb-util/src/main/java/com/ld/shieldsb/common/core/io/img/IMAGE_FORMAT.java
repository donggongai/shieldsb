package com.ld.shieldsb.common.core.io.img;

/**
 * 图片格式
 * 
 * @ClassName IMAGE_FORMAT
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2017年5月17日 上午9:32:14
 *
 */
public enum IMAGE_FORMAT {
    // 小写
    BMP("bmp"), JPG("jpg"), WBMP("wbmp"), JPEG("jpeg"), PNG("png"), GIF("gif");

    private String value;

    IMAGE_FORMAT(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}