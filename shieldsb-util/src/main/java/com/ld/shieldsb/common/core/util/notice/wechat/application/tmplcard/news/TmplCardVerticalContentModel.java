package com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.news;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 卡片二级垂直内容，该字段可为空数组，但有数据的话需确认对应字段是否必填，列表长度不超过4
 * 
 * @ClassName TemplateCardHorizontalContentModel
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年9月13日 上午8:32:43
 *
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class TmplCardVerticalContentModel {
    private String title; // 卡片二级标题，建议不超过38个字
    private String desc; // 二级普通文本，建议不超过160个字

    public static TmplCardVerticalContentModel build() {
        return TmplCardVerticalContentModel.builder().build();
    }

    /**
     * 构造链接为url的对象
     * 
     * @Title buildUrl
     * @author 吕凯
     * @date 2021年9月13日 上午10:34:54
     * @param keyname
     * @param value
     * @param url
     * @return TmplCardHorizontalContentModel
     */
    public static TmplCardVerticalContentModel buildUrl(String title, String desc) {
        return TmplCardVerticalContentModel.builder().title(title).desc(desc).build();
    }

}
