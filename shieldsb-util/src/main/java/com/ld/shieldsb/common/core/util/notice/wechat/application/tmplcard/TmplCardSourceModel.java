package com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 卡片来源样式信息，不需要来源样式可不填写
 * 
 * @ClassName TemplateCardSource
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年9月13日 上午8:22:52
 *
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class TmplCardSourceModel {
    private String iconUrl; // 来源图片的url
    private String desc; // 来源图片的描述，建议不超过20个字

}
