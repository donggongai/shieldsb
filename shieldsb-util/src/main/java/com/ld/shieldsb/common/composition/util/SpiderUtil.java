package com.ld.shieldsb.common.composition.util;

import java.util.Map;

import com.ld.shieldsb.common.core.util.HtmlUnitUtil;
import com.ld.shieldsb.common.core.util.JsoupUtil;

/**
 * 蜘蛛工具类，获取远程url内容等
 * 
 * @ClassName SpiderUtil
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2018年11月17日 上午10:36:26
 *
 */
public class SpiderUtil {
    private SpiderUtil() {
        throw new IllegalStateException("该类不可初始化");
    }

    /**
     * 获取远程地址的内容
     * 
     * @Title getRemoteContent
     * @author 吕凯
     * @date 2018年11月17日 上午10:26:06
     * @param url
     * @return String
     */
    public static String getRemoteURLContent(String url) {
        return getRemoteURLContent(url, null, null);
    }

    public static String getRemoteURLContent(String url, Map<String, String> data) {
        return getRemoteURLContent(url, data, "get");
    }

    public static String getRemoteURLContent(String url, Map<String, String> data, String method) {
        String userAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36";
        int timeoutMillis = 5000; // 5秒超时
        return getRemoteURLContent(url, userAgent, timeoutMillis, data, method);
    }

    /**
     * 
     * 获取远程地址的内容
     * 
     * @Title getRemoteURLContent
     * @author 吕凯
     * @date 2018年11月17日 上午10:38:44
     * @param url
     * @param userAgent
     * @param timeoutMillis
     *            超时时间，单位毫秒
     * @return String
     */
    public static String getRemoteURLContent(String url, String userAgent, int timeoutMillis) {
        return JsoupUtil.getRemoteURLContent(url, userAgent, timeoutMillis);
    }

    public static String getRemoteURLContent(String url, String userAgent, int timeoutMillis, Map<String, String> data, String method) {
        return JsoupUtil.getRemoteURLContent(url, userAgent, timeoutMillis, data, method);
    }

    /**
     * 针对ajax获取页面的内容抓取
     * 
     * @Title getRemoteURLString4Ajax
     * @author 吕凯
     * @date 2018年11月20日 上午11:41:32
     * @param url
     * @return String
     */
    public static String getRemoteURLContent4Ajax(String url) {
        return HtmlUnitUtil.getRemoteURLContent(url);
    }

}
