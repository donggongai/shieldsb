package com.ld.shieldsb.common.core.util.sensitiveWord;

import java.util.HashMap;
import java.util.Map;

import com.ld.shieldsb.common.core.util.sensitiveWord.impl.DefaultSensitiveWordHandler;
import com.ld.shieldsb.common.core.util.sensitiveWord.impl.HtmlSensitiveWordHandler;
import com.ld.shieldsb.common.core.util.sensitiveWord.impl.TextSensitiveWordHandler;

/**
 * 
 * 敏感词处理工厂类
 * 
 * @ClassName SensitiveWordFactory
 * @author 刘金浩
 * @date 2019年8月30日 下午4:29:28
 *
 */
public class SensitiveWordFactory {

    protected static final Map<String, SensitiveWordHandler> SENSITIVE_WORD_HANDLERS = new HashMap<>(); // 敏感词处理器
    public static final String SENSITIVE_WORD_DEFAULT_KEY = "simple"; // 默认处理

    public static final String DEFAULT_REPLACECHAR = "*"; // 默认处理

    static {
        SensitiveWordHandler defalutHandler = new DefaultSensitiveWordHandler();
        SENSITIVE_WORD_HANDLERS.put(SENSITIVE_WORD_DEFAULT_KEY, defalutHandler); // 默认，其他不符合条件的都走这个
        SENSITIVE_WORD_HANDLERS.put("html", new HtmlSensitiveWordHandler()); // html标签中包含敏感词
        SENSITIVE_WORD_HANDLERS.put("text", new TextSensitiveWordHandler()); // 字符串中用特殊字符包含敏感词
    }

    /**
     * 
     * 注册新的敏感词处理器，也可对旧的进行覆盖
     * 
     * @Title registerSensitiveWordHandler
     * @author 刘金浩
     * @date 2019年8月30日 下午4:30:16
     * @param handlerType
     * @param defalutHandler
     *            void
     */
    public static void registerSensitiveWordHandler(String handlerType, SensitiveWordHandler defalutHandler) {
        SENSITIVE_WORD_HANDLERS.put(handlerType, defalutHandler);
    }

    public static SensitiveWordResult dealSensitiveWord(String value, String handlerMarchKey, String replaceChar) {
        SensitiveWordHandler sensitiveWordHandler = SENSITIVE_WORD_HANDLERS.get(handlerMarchKey);
        if (sensitiveWordHandler == null) {
            sensitiveWordHandler = SENSITIVE_WORD_HANDLERS.get(SENSITIVE_WORD_DEFAULT_KEY);
        }
        SensitiveWordResult result = sensitiveWordHandler.deal(value, replaceChar);
        return result;

    }

    public static SensitiveWordResult dealSensitiveWord(String value, String handlerMarchKey) {

        return dealSensitiveWord(value, handlerMarchKey, DEFAULT_REPLACECHAR);
    }

}
