package com.ld.shieldsb.common.core.util.notice.wechat.log;

public interface IEweChatLogService {
    /**
     * 保存日志
     * 
     * @Title saveLog
     * @author 吕凯
     * @date 2021年9月11日 上午10:38:10
     * @param EweChatLogModel
     *            参数对象
     * @return boolean
     */
    public boolean saveLog(EweChatLogModel logModel);

}
