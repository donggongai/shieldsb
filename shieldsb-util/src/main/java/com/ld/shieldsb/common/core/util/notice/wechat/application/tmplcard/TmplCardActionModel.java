package com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard;

import com.ld.shieldsb.common.core.util.notice.wechat.EweChatTool;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 跳转事件
 * 
 * @ClassName TmplCardActionModel
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年9月13日 上午8:38:17
 *
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class TmplCardActionModel {
    private Integer type; // 跳转事件类型，0或不填代表不是链接，1 代表跳转url，2 代表打开小程序。text_notice卡片模版中该字段取值范围为[1,2]
    private String title; // 整体事件时可为空，跳转链接样式的文案内容，建议不超过18个字
    private String url; // 跳转链接的url，type是1时必填

    private String appid; // 跳转链接的小程序的appid，必须是与当前应用关联的小程序，type是2时必填
    private String pagepath; // 跳转事件的小程序的pagepath，type是2时选填

    public static TmplCardActionModel build() {
        return TmplCardActionModel.builder().build();
    }

    /**
     * 构造跳转url格式的对象
     * 
     * @Title buildUrl
     * @author 吕凯
     * @date 2021年9月13日 上午10:34:17
     * @param title
     * @param url
     * @return TmplCardActionModel
     */
    public static TmplCardActionModel buildUrl(String title, String url) {
        return TmplCardActionModel.builder().type(EweChatTool.ACTION_TYPE_URL).title(title).url(url).build();
    }

    /**
     * 构造跳转小程序的对象
     * 
     * @Title buildApp
     * @author 吕凯
     * @date 2021年9月13日 上午10:34:32
     * @param title
     * @param appid
     * @param pagepath
     * @return TmplCardActionModel
     */
    public static TmplCardActionModel buildApp(String title, String appid, String pagepath) {
        return TmplCardActionModel.builder().type(EweChatTool.ACTION_TYPE_APP).title(title).appid(appid).pagepath(pagepath).build();
    }

    public static TmplCardActionModel build(Integer type, String title, String url, String appid, String pagepath) {
        return TmplCardActionModel.builder().type(type).title(title).url(url).appid(appid).pagepath(pagepath).build();
    }

}
