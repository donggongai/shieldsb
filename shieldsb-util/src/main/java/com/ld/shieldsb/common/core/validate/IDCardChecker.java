package com.ld.shieldsb.common.core.validate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ld.shieldsb.common.core.model.Result;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IDCardChecker {

    protected static final Map<String, String> AREAS_MAP = new LinkedHashMap<>();
    private static final Pattern PATTERN_ID = Pattern.compile("(^\\d{15}$)|(\\d{17}(?:\\d|x|X)$)");
    // 日期
    private static final Pattern PATTERN_DATE = Pattern.compile(
            "^((\\d{2}(([02468][048])|([13579][26]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\\s(((0?[0-9])|([1-2][0-3]))\\:([0-5]?[0-9])((\\s)|(\\:([0-5]?[0-9])))))?$");

    static {
        AREAS_MAP.put("11", "北京");
        AREAS_MAP.put("12", "天津");
        AREAS_MAP.put("13", "河北");
        AREAS_MAP.put("14", "山西");
        AREAS_MAP.put("15", "内蒙古");
        AREAS_MAP.put("21", "辽宁");
        AREAS_MAP.put("22", "吉林");
        AREAS_MAP.put("23", "黑龙江");
        AREAS_MAP.put("31", "上海");
        AREAS_MAP.put("32", "江苏");
        AREAS_MAP.put("33", "浙江");
        AREAS_MAP.put("34", "安徽");
        AREAS_MAP.put("35", "福建");
        AREAS_MAP.put("36", "江西");
        AREAS_MAP.put("37", "山东");
        AREAS_MAP.put("41", "河南");
        AREAS_MAP.put("42", "湖北");
        AREAS_MAP.put("43", "湖南");
        AREAS_MAP.put("44", "广东");
        AREAS_MAP.put("45", "广西");
        AREAS_MAP.put("46", "海南");
        AREAS_MAP.put("50", "重庆");
        AREAS_MAP.put("51", "四川");
        AREAS_MAP.put("52", "贵州");
        AREAS_MAP.put("53", "云南");
        AREAS_MAP.put("54", "西藏");
        AREAS_MAP.put("61", "陕西");
        AREAS_MAP.put("62", "甘肃");
        AREAS_MAP.put("63", "青海");
        AREAS_MAP.put("64", "宁夏");
        AREAS_MAP.put("65", "新疆");
        AREAS_MAP.put("71", "台湾");
        AREAS_MAP.put("81", "香港");
        AREAS_MAP.put("82", "澳门");
        AREAS_MAP.put("91", "国外");
    }

    /*********************************** 身份证验证开始 ****************************************/
    /*身份证号验证 
    /* 参考：http://www.cnblogs.com/lzrabbit/archive/2011/10/23/2221643.html
    根据〖中华人民共和国国家标准 GB 11643-1999〗中有关公民身份号码的规定，公民身份号码是特征组合码，由十七位数字本体码和一位数字校验码组成。排列顺序从左至右依次为：六位数字地址码，八位数字出生日期码，三位数字顺序码和一位数字校验码。
        地址码表示编码对象常住户口所在县(市、旗、区)的行政区划代码，按GB/T2260的规定执行。
        出生日期码（第七位至十四位） 表示编码对象出生的年、月、日，其中年份用四位数字表示，年、月、日之间不用分隔符。
        顺序码表示同一地址码所标识的区域范围内，对同年、月、日出生的人员编定的顺序号。顺序码的奇数分给男性，偶数分给女性。
        校验码是根据前面十七位数字码，按照ISO 7064:1983.MOD 11-2校验码计算出来的检验码。
        （1）十七位数字本体码加权求和公式 S = Sum(iDCardNo * wf), i = 0, ... , 16 ，先对前17位数字的权求和 iDCardNo:表示第i位置上的身份证号码数字值 Wi:表示第i位置上的加权因子 wf: 7 9 10 5 8 4 2 1 6 3 7 9 10 5 8 4 2
    　　（2）计算模 Y = mod(S, 11) （3）通过模得到对应的校验码 Y: 0 1 2 3 4 5 6 7 8 9 10 校验码: 1 0 X 9 8 7 6 5 4 3 2
    --------------------- 
    作者：qinxu0611 
    来源：CSDN 
    原文：https://blog.csdn.net/qinxu0611/article/details/86146182 
    版权声明：本文为博主原创文章，转载请附上博文链接！
    
    出生日期计算方法。
        15位的身份证编码首先把出生年扩展为4位，简单的就是增加一个19或18,这样就包含了所有1800-1999年出生的人;
        2000年后出生的肯定都是18位的了没有这个烦恼，至于1800年前出生的,那啥那时应该还没身份证号这个东东，⊙﹏⊙b汗...
    下面是正则表达式:
     出生日期1800-2099  (18|19|20)?\d{2}(0[1-9]|1[12])(0[1-9]|[12]\d|3[01])
     身份证正则表达式 /^\d{6}(18|19|20)?\d{2}(0[1-9]|1[12])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)$/i            
     15位校验规则 6位地址编码+6位出生日期+3位顺序号
     18位校验规则 6位地址编码+8位出生日期+3位顺序号+1位校验位
     
     校验位规则     公式:∑(ai×Wi)(mod 11)……………………………………(1)
                    公式(1)中： 
                    i----表示号码字符从由至左包括校验码在内的位置序号； 
                    ai----表示第i位置上的号码字符值； 
                    Wi----示第i位置上的加权因子，其数值依据公式Wi=2^(n-1）(mod 11)计算得出。
                    i 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1
                    Wi 7 9 10 5 8 4 2 1 6 3 7 9 10 5 8 4 2 1
    */

    public static Map<String, String> getAreasMap() {
        return AREAS_MAP;
    }

    /**
     * 功能：身份证的有效验证
     * 
     * @param idStr
     *            身份证号
     * @return 有效：返回"" 无效：返回String信息
     * @throws ParseException
     */
    public static Result validate(String idStr) {
        Result result = new Result();
        String errorInfo = "";// 记录错误信息
        if (idStr == null) {
            errorInfo = "身份证号码错误";
            result.setSuccess(false);
            result.setMessage(errorInfo);
            return result;
        }
        idStr = idStr.toLowerCase();
        String iDCardNo = ""; // 前17位
        // ================ 号码的长度 15位或18位 ================
        if (idStr.length() != 15 && idStr.length() != 18) {
            errorInfo = "身份证号码长度应该为15位或18位。";
            result.setSuccess(false);
            result.setMessage(errorInfo);
            return result;
        }
        if (idStr.length() == 18) {
            iDCardNo = idStr.substring(0, 17);
        } else if (idStr.length() == 15) {
            iDCardNo = idStr.substring(0, 6) + "19" + idStr.substring(6, 15);
        }

        // ================ 地区码时候有效 ================
        if (getAreaCode(iDCardNo.substring(0, 2)) == null) {
            errorInfo = "身份证地区编码错误。";
            result.setSuccess(false);
            result.setMessage(errorInfo);
            return result;
        }

        // ================ 数字 除最后一位可能为X外都为数字 ================

        Matcher mt = PATTERN_ID.matcher(idStr);
        if (!mt.find()) {
            errorInfo = "身份证15位号码都应为数字 ; 18位号码除最后一位外，都应为数字。";
            result.setSuccess(false);
            result.setMessage(errorInfo);
            return result;
        }
        // =======================(end)========================

        // ================ 出生年月是否有效 ================
        String strYear = iDCardNo.substring(6, 10);// 年份
        String strMonth = iDCardNo.substring(10, 12);// 月份
        String strDay = iDCardNo.substring(12, 14);// 月份
        if (isDate(strYear + "-" + strMonth + "-" + strDay) == false) {
            errorInfo = "身份证生日无效。";
            result.setSuccess(false);
            result.setMessage(errorInfo);
            return result;
        }
        GregorianCalendar gc = new GregorianCalendar();
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        try {
            if ((gc.get(Calendar.YEAR) - Integer.parseInt(strYear)) > 150
                    || (gc.getTime().getTime() - s.parse(strYear + "-" + strMonth + "-" + strDay).getTime()) < 0) {
                errorInfo = "身份证生日不在有效范围。";
                result.setSuccess(false);
                result.setMessage(errorInfo);
                return result;
            }
        } catch (NumberFormatException | java.text.ParseException e) {
            log.error("转换为日期错误", e);
        }
        if (Integer.parseInt(strMonth) > 12 || Integer.parseInt(strMonth) == 0) {
            errorInfo = "身份证月份无效";
            result.setSuccess(false);
            result.setMessage(errorInfo);
            return result;
        }
        if (Integer.parseInt(strDay) > 31 || Integer.parseInt(strDay) == 0) {
            errorInfo = "身份证日期无效";
            result.setSuccess(false);
            result.setMessage(errorInfo);
            return result;
        }

        // ================ 判断最后一位的值 ================
        int totalmulAiWi = 0;
        String[] Wi = { "7", "9", "10", "5", "8", "4", "2", "1", "6", "3", "7", "9", "10", "5", "8", "4", "2" };
        for (int i = 0; i < 17; i++) {
            totalmulAiWi = totalmulAiWi + Integer.parseInt(String.valueOf(iDCardNo.charAt(i))) * Integer.parseInt(Wi[i]);
        }
        int modValue = totalmulAiWi % 11;
        String[] valCodeArr = { "1", "0", "x", "9", "8", "7", "6", "5", "4", "3", "2" };
        String strVerifyCode = valCodeArr[modValue];
        iDCardNo = iDCardNo + strVerifyCode;

        if (idStr.length() == 18) {
            if (iDCardNo.equals(idStr) == false) {
                errorInfo = "身份证无效，不是合法的身份证号码";
                result.setSuccess(false);
                result.setMessage(errorInfo);
                return result;
            }
        } else {
            result.setSuccess(true);
            return result;
        }
        result.setSuccess(true);
        return result;
    }

    public static String getAreaCode(String key) {
        return AREAS_MAP.get(key);
    }

    /**
     * 功能：判断字符串是否为日期格式
     * 
     * @param str
     * @return
     */
    private static boolean isDate(String strDate) {
        Matcher m = PATTERN_DATE.matcher(strDate);
        if (m.matches()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param args
     * @throws ParseException
     */
    public static void main(String[] args) {
        // String IDCardNum="210102820826411";
        // String IDCardNum="210102198208264114";
        String IDCardNum = "370783197605229137";
        System.out.println(IDCardChecker.validate(IDCardNum));
        // System.out.println(cc.isDate("1996-02-29"));
    }

}
