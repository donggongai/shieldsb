package com.ld.shieldsb.common.core.util.properties;

import lombok.Data;

/**
 * 配置参数model
 * 
 * @ClassName PropertiesPojo
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2019年3月6日 下午3:24:41
 *
 */
@Data
public class PropertiesPojo {
    private String key;
    private String value;
    private String comment; // 注释

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof PropertiesPojo)) {
            return false;
        }
        PropertiesPojo other = (PropertiesPojo) o;
        if (!other.canEqual(this)) {
            return false;
        }
        // key相等就认为是相同的对象
        Object thisKey = getKey();
        Object otherKey = other.getKey();
        if (thisKey == null ? otherKey != null : !thisKey.equals(otherKey)) {
            return false;
        }
        return true;
    }

    protected boolean canEqual(Object other) {
        return other instanceof PropertiesPojo;
    }

    @Override
    public int hashCode() {
        int PRIME = 59;
        int result = 1;
        Object key = getKey();
        result = result * PRIME + (key == null ? 43 : key.hashCode());
        return result * PRIME + 79;
    }

}
