package com.ld.shieldsb.common.core.queue;

//自动恢复执行队列，示例类
public class TestRecoverRunningQueue extends RecoverRunningQueue {

    protected TestRecoverRunningQueue(String queueName) {
        super(queueName);
    }

    @Override
    protected void doing(String model) {
        System.out.println(model);
    }

    public static void main(String[] args) {
        TestRecoverRunningQueue testRecoverRunningQueue = new TestRecoverRunningQueue("testQueue");
//        testRecoverRunningQueue.put("wwwwww");
//        testRecoverRunningQueue.put("wwwww");
//        testRecoverRunningQueue.put("wwww");
//        testRecoverRunningQueue.put("www");
//        testRecoverRunningQueue.put("ww");
//        testRecoverRunningQueue.shutdown();
    }

}
