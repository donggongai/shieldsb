package com.ld.shieldsb.common.core.util.redis;

import com.ld.shieldsb.common.core.model.PropertiesModel;

/**
 * redis参数配置
 * 
 * @ClassName RedisParams
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2018年11月19日 上午8:53:34
 *
 */
public final class RedisParams {
    public static final String KEY_REDIS_USE = "shieldsb.redis.use";

    // Redis服务器IP
    public static final String ADDR = PropertiesModel.CONFIG.getString("shieldsb.redis.host");

    // Redis的端口号
    public static final int PORT = PropertiesModel.CONFIG.getInt("shieldsb.redis.port", 6379);

    // 访问密码
    public static final String PASSWORD = PropertiesModel.CONFIG.getString("shieldsb.redis.password");

    // 库下标
    public static final Integer DATABASE = PropertiesModel.CONFIG.getInt("shieldsb.redis.database", 0);

    // 可用连接实例的最大数目，默认值为8；
    // 如果赋值为-1，则表示不限制；如果pool已经分配了maxActive个jedis实例，则此时pool的状态为exhausted(耗尽)。
    public static final int MAX_ACTIVE = PropertiesModel.CONFIG.getInt("shieldsb.redis.maxActive", -1);

    // 控制一个pool最多有多少个状态为idle(空闲的)的jedis实例，默认值也是8。
    public static final int MAX_IDLE = PropertiesModel.CONFIG.getInt("shieldsb.redis.maxIdle", 8);

    // 等待可用连接的最大时间，单位毫秒，默认值为-1，表示永不超时。如果超过等待时间，则直接抛出JedisConnectionException；
    public static final int MAX_WAIT = PropertiesModel.CONFIG.getInt("shieldsb.redis.maxWait", -1);
    public static final int TIMEOUT = PropertiesModel.CONFIG.getInt("shieldsb.redis.timeout", 10000);

    // 在borrow一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
    public static final boolean TEST_ON_BORROW = PropertiesModel.CONFIG.getBoolean("shieldsb.redis.testOnBorrow", true);

    public static boolean canUse() {
        return PropertiesModel.CONFIG.getBoolean(KEY_REDIS_USE, false);
    }
}