package com.ld.shieldsb.common.core.util;

import java.util.regex.Pattern;

public class XSSutil {
    // script 标签
    public static final Pattern PATTERN_SCRIPT = Pattern.compile("<script>(.*?)</script>", Pattern.CASE_INSENSITIVE);
    // src形式的表达式
    public static final Pattern PATTERN_SRC = Pattern.compile("src[\r\n]*=[\r\n]*\\\'(.*?)\\\'",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL); // 单引号
    public static final Pattern PATTERN_SRC2 = Pattern.compile("src[\r\n]*=[\r\n]*\\\"(.*?)\\\"",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL); // 双引号
    // 单个的 </script> 标签
    public static final Pattern PATTERN_SCRIPT_1 = Pattern.compile("</script>", Pattern.CASE_INSENSITIVE);
    // 单个的<script ...> 标签
    public static final Pattern PATTERN_SCRIPT_2 = Pattern.compile("<script(.*?)>",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    // eval(...) 形式表达式
    public static final Pattern PATTERN_EVAL = Pattern.compile("eval\\((.*?)\\)",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    // e­xpression(...) 表达式
    public static final Pattern PATTERN_E_XPRESSION = Pattern.compile("e­xpression\\((.*?)\\)",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    // javascript: 表达式
    public static final Pattern PATTERN_JAVASCRIPT = Pattern.compile("javascript:", Pattern.CASE_INSENSITIVE);
    // vbscript:表达式
    public static final Pattern PATTERN_VBSCRIPT = Pattern.compile("vbscript:", Pattern.CASE_INSENSITIVE);
    // onload= 表达式
    public static final Pattern PATTERN_ONLOAD = Pattern.compile("onload(.*?)=",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

    public static String cleanXSS(String value) {
        if (value != null) {
            // 推荐使用ESAPI库来避免脚本攻击,value = ESAPI.encoder().canonicalize(value);
            // 避免空字符串
            value = value.replaceAll(" ", "");
            // 避免script 标签
            value = PATTERN_SCRIPT.matcher(value).replaceAll("");
            // 避免src形式的表达式
            value = PATTERN_SRC.matcher(value).replaceAll("");
            value = PATTERN_SRC2.matcher(value).replaceAll("");
            // 删除单个的 </script> 标签
            value = PATTERN_SCRIPT_1.matcher(value).replaceAll("");
            // 删除单个的<script ...> 标签
            value = PATTERN_SCRIPT_2.matcher(value).replaceAll("");
            // 避免 eval(...) 形式表达式
            value = PATTERN_EVAL.matcher(value).replaceAll("");
            // 避免 e­xpression(...) 表达式
            value = PATTERN_E_XPRESSION.matcher(value).replaceAll("");
            // 避免 javascript: 表达式
            value = PATTERN_JAVASCRIPT.matcher(value).replaceAll("");
            // 避免 vbscript:表达式
            value = PATTERN_VBSCRIPT.matcher(value).replaceAll("");
            // 避免 onload= 表达式
            value = PATTERN_ONLOAD.matcher(value).replaceAll("");
        }
        return value;
    }
}
