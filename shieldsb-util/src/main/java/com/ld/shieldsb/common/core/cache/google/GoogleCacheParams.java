package com.ld.shieldsb.common.core.cache.google;

import com.ld.shieldsb.common.core.model.PropertiesModel;

/**
 * 
 * GoogleCache参数配置
 * 
 * @ClassName GoogleCacheParams
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2019年7月5日 下午3:46:49
 *
 */
public class GoogleCacheParams {
    private static Boolean use = PropertiesModel.CONFIG.getBoolean("cache_google_use", true); // ehcache是否可用，默认可用

    private GoogleCacheParams() {
        throw new IllegalStateException("该类不可初始化");
    }

    /**
     * 
     * 缓存是否可用
     * 
     * @Title canUse
     * @return boolean
     */
    public static boolean canUse() {
        return use;
    }

    private static Integer maxImumSize = null; // 限定缓存元素个数

    // Guava Cache缓存过期后不一定会立马被清理，一般会在Cache整体被读取一定次数后清理。这中策略对性能是有好处的，
    // 如果想强制清理可以手动调用`Cache.cleanup()`或者使用`ScheduledExecutorService`来完成定期清理

    // expireAfterAccess: 当缓存项在指定的时间段内没有被读或写就会被回收。
    // expireAfterWrite：当缓存项在指定的时间段内没有更新就会被回收。
    // refreshAfterWrite：当缓存项上一次更新操作之后的多久会被刷新。
    private static String expireTypeExpireafteraccess = "expireAfterAccess";
    private static String expireTypeExpireafterwrite = "expireAfterWrite";
    private static String expireTypeRefreshafterwrite = "refreshAfterWrite";

    private static String expireType = expireTypeExpireafterwrite;

    public static Boolean getUse() {
        return use;
    }

    public static void setUse(Boolean use) {
        GoogleCacheParams.use = use;
    }

    public static Integer getMaxImumSize() {
        return maxImumSize;
    }

    public static void setMaxImumSize(Integer maxImumSize) {
        GoogleCacheParams.maxImumSize = maxImumSize;
    }

    public static String getExpireTypeExpireafteraccess() {
        return expireTypeExpireafteraccess;
    }

    public static void setExpireTypeExpireafteraccess(String expireTypeExpireafteraccess) {
        GoogleCacheParams.expireTypeExpireafteraccess = expireTypeExpireafteraccess;
    }

    public static String getExpireTypeExpireafterwrite() {
        return expireTypeExpireafterwrite;
    }

    public static void setExpireTypeExpireafterwrite(String expireTypeExpireafterwrite) {
        GoogleCacheParams.expireTypeExpireafterwrite = expireTypeExpireafterwrite;
    }

    public static String getExpireTypeRefreshafterwrite() {
        return expireTypeRefreshafterwrite;
    }

    public static void setExpireTypeRefreshafterwrite(String expireTypeRefreshafterwrite) {
        GoogleCacheParams.expireTypeRefreshafterwrite = expireTypeRefreshafterwrite;
    }

    public static String getExpireType() {
        return expireType;
    }

    public static void setExpireType(String expireType) {
        GoogleCacheParams.expireType = expireType;
    }

}
