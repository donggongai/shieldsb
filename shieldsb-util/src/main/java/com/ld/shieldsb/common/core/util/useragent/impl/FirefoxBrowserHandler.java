package com.ld.shieldsb.common.core.util.useragent.impl;

import com.ld.shieldsb.common.core.util.useragent.BrowserHandler;
import com.ld.shieldsb.common.core.util.useragent.BrowserModel;
import com.ld.shieldsb.common.core.util.useragent.BrowserUtils;

/**
 * 火狐浏览器的判断
 * 
 * @ClassName FirefoxBrowserHandler
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2019年7月19日 下午2:46:14
 *
 */
public class FirefoxBrowserHandler implements BrowserHandler {
    private static final String BROWSER_TYPE = BrowserUtils.FIREFOX;

    @Override
    public BrowserModel deal(String userAgent) {
        BrowserModel browserModel = new BrowserModel();
        if (checkBrowse(userAgent)) {
            browserModel.setBrowserType(BROWSER_TYPE);
            browserModel.setBrowserVersion(BrowserUtils.getBrowserVersion(BROWSER_TYPE, userAgent));
        }
        return browserModel;
    }

    @Override
    public boolean checkBrowse(String userAgent) {
        return BrowserUtils.matchIn(BROWSER_TYPE, userAgent);
    }

}
