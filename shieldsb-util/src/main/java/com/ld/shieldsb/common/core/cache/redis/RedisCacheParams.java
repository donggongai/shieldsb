package com.ld.shieldsb.common.core.cache.redis;

import com.ld.shieldsb.common.core.model.PropertiesModel;

/**
 * redis参数配置
 * 
 * @ClassName RedisParams
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2018年11月19日 上午8:53:34
 *
 */
public final class RedisCacheParams {
    public static final Boolean USE = PropertiesModel.CONFIG.getBoolean("cache_redis_use", false);

    private RedisCacheParams() {
        throw new IllegalStateException("该类不可初始化");
    }

    public static boolean canUse() {
        return USE;
    }
}