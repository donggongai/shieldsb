package com.ld.shieldsb.common.core.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * 用于分页，非dao中的分页，做其他功能处理,如敏感词模块
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2019年10月31日 上午9:46:08
 * 
 * @param <T>
 */
@Data
public class PageBean<T> {
    private Integer currentPage = 1;
    private Integer pageSize = 30;

    private Integer totalCount = 0;

    private boolean hasError = false;
    private String errMsg;

    private List<T> resultList = new ArrayList<>();

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        if (currentPage > 1) {
            this.currentPage = currentPage;
        }
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        if (pageSize > 0) {
            this.pageSize = pageSize;
        }
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        if (totalCount > 0) {
            this.totalCount = totalCount;
        }
    }

    public Integer getTotalPage() {
        return (totalCount - 1) / pageSize + 1;
    }

    public Integer getCurrentPoint() {
        int currentPoint = (currentPage - 1) * pageSize + 1;
        if (currentPoint < 1) {
            return 1;
        }
        return currentPoint;
    }

    public List<T> getResultList() {
        return resultList;
    }

    public void setResultList(List<T> resultList) {
        if (resultList != null) {
            this.resultList = resultList;
        }
    }
}
