package com.ld.shieldsb.common.core.util.notice.sms;

import com.ld.shieldsb.common.core.model.Result;

/**
 * 短信发送服务
 * 
 * @ClassName SMSUtil
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2017年8月16日 下午3:13:39
 *
 */
public interface SMSSend {
    public static final String ERR_MISSING_USERNAME = "用户名为空";
    public static final String ERR_MISSING_PASSWORD = "密码为空";
    public static final String ERR_MISSING_APIKEY = "APIKEY为空";
    public static final String ERR_MISSING_RECIPIENT = "收件人手机号码为空";
    public static final String ERR_MISSING_MESSAGE_CONTENT = "短信内容为空或编码不正确";
    public static final String ERR_ACCOUNT_IS_BLOCKED = "账号被禁用";
    public static final String ERR_UNRECOGNIZED_ENCODING = "编码未能识别";
    public static final String ERR_APIKEY_OR_PASSWORD_ERROR = "APIKEY或密码错误";
    public static final String ERR_UNAUTHORIZED_IP_ADDRESS = "未授权IP地址";
    public static final String ERR_ACCOUNT_BALANCE_IS_INSUFFICIENT = "余额不足";
    public static final String ERR_THROUGHPUT_RATE_EXCEEDED = "发送频率受限";
    public static final String ERR_INVALID_MD5_PASSWORD_LENGTH = "MD5密码长度非32位";

    /**
     * 
     * 发送短信
     * 
     * @Title sendSMS
     * @author 吕凯
     * @date 2020年6月10日 下午3:05:15
     * @param sendInfo
     *            发送对象
     * @param auth
     *            api验证对象
     * @return SMSResult
     */
    public Result sendSMS(SMSSendModel sendInfo, SMSSendAuth auth);

    /**
     * 
     * 发送短信
     * 
     * @Title sendSMS
     * @author 吕凯
     * @date 2017年8月17日 上午8:47:34
     * @param phoneNum
     *            手机号
     * @param message
     *            信息
     * @return SMSResult 返回结果，其中success字段表示是否发送成功
     */
    public Result sendSMS(String phoneNum, String message);

    /**
     * 
     * 发送短信
     * 
     * @Title sendSMS
     * @author 吕凯
     * @date 2017年8月17日 上午8:33:27
     * @param phoneNum
     *            手机号
     * @param message
     *            信息
     * @param signName
     *            签名，发送短信时的前缀，自动添加中括号包裹如：【绿盾征信】
     * @return SMSResult
     */
    public Result sendSMS(String phoneNum, String message, String signName);

}
