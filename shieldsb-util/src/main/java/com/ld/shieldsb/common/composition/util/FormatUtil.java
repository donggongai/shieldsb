package com.ld.shieldsb.common.composition.util;

import java.util.Date;

import com.ld.shieldsb.common.core.reflect.ClassUtil;
import com.ld.shieldsb.common.core.util.ArithUtil;
import com.ld.shieldsb.common.core.util.date.DateUtil;

/**
 * 聚合工具类：：格式化工具类，包括字节格式化，钱格式，时间格式化等与格式化相关的操作
 * 
 * @ClassName FormatUtil
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2018年11月16日 上午9:30:40
 *
 */
public class FormatUtil {
    private FormatUtil() {
        throw new IllegalStateException("该类不可初始化");
    }

    /**
     * 格式化字节数 按字节数的大小格式化为Kb、Mb等实际大小
     * 
     * @Title byteFormat
     * @author 吕凯
     * @date 2013-8-19 上午11:41:19
     * @param size
     * @return String
     */
    public static String byteFormat(long size) {
        return byteFormat(size, 2);
    }

    /**
     * 格式化字节数 按字节数的大小格式化为Kb、Mb等实际大小
     * 
     * @Title byteFormat
     * @author 吕凯
     * @date 2013-8-19 上午11:41:19
     * @param size
     *            原字节数
     * @param dec
     *            保留位数
     * @return String
     */
    public static String byteFormat(long size, int dec) {
        String[] sizearr = { " B", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB" };
        int pos = 0;
        double sized = (double) size;
        while (sized >= 1024) {
            sized /= 1024;
            pos++;
        }
        return ArithUtil.round(sized, dec) + "" + sizearr[pos];

    }

    /**
     * 格式化钱数 按字节数的大小格式化为万、百万、千万、亿等
     * 
     * @Title moneyFormat
     * @author 吕凯
     * @date 2013-8-19 上午11:41:19
     * @param money
     *            原钱数
     * @param dec
     *            保留位数
     * @return String
     */
    public static String moneyFormat(double money, int dec) {
        String[] sizearr = { " ", " 万", " 亿" };
        long[] posarr = { 10000, 10000 };
        int pos = 0;
        double sized = money;
        while (sized >= posarr[pos]) {
            sized /= posarr[pos];
            pos++;
            if (pos == posarr.length) {
                break;
            }
        }
        return ArithUtil.round(sized, dec) + "" + sizearr[pos];

    }

    /**
     * 格式化钱数 按字节数的大小格式化为万、百万、千万、亿等
     * 
     * @Title moneyFormat
     * @author 吕凯
     * @date 2014-9-2 上午11:18:08
     * @param size
     * @return String
     */
    public static String moneyFormat(double size) {
        return moneyFormat(size, 2);
    }

    /**
     * 格式化整数显示，位数不足时补0
     * 
     * @Title formatLength
     * @author 吕凯
     * @date 2017年7月13日 上午9:03:35
     * @param number
     * @param minLength
     *            最小长度 当数字少于该长度时前面增加0，当数字长度大于该长度时原样返回
     * @return String
     */
    public static String formatIntLength(Object number, int minLength) {
        String numberString = "" + number;
        String formatStr = "%0" + minLength + "d";
        String bh = String.format(formatStr, ClassUtil.obj2int(number));
        return ("" + parse2Int(numberString)).replace("" + parse2Int(numberString), bh);
    }

    /**
     * 
     * String转成Int,非法字符转换为0
     * 
     * @Title parseInt
     * @param a
     * @return Integer
     */
    public static Integer parse2Int(String a) {
        return ClassUtil.obj2int(a);
    }

    /**
     * 
     * String转成Double,非法字符转换为0
     * 
     * @Title parseInt
     * @param a
     * @return Integer
     */
    public static Double parse2Doulbe(String a) {
        return ClassUtil.obj2doulbe(a);
    }

    // ===============================日期时间相关 begin================================
    /**
     * 将毫秒数转换为时间表示1000(ms)==》1秒
     * 
     * @Title formatMill
     * @author 吕凯
     * @date 2013-12-17 下午2:29:03
     * @param time
     *            毫秒数
     * @return String
     */
    public static String millFormat(double time) {
        return DateUtil.millFormat(time);
    }

    /**
     * 将毫秒数转换为时间表示1000(ms)==》1秒
     * 
     * @Title formatMill
     * @author 吕凯
     * @date 2013-12-17 下午2:29:03
     * @param time
     *            毫秒数
     * @param timeunit
     *            计算到单位，参考DateUtil中的静态变量
     * @return String
     */
    public static String millFormat(double time, int timeunit) {
        return DateUtil.millFormat(time, timeunit);
    }

    /**
     * 
     * 根据参数获取yyyy-MM-dd HH:mm:ss格式的时间
     * 
     * @Title getDateTimeString
     * @param date
     *            日期
     * @return String
     */
    public static String getDateTimeString(Date date) {
        return DateUtil.getDateTimeString(date);
    }

    /**
     * 
     * 获取某天某个格式的日期
     * 
     * @Title getDateStr
     * @param date
     *            日期
     * @param format
     *            格式化类型
     * @return String
     */
    public static String getDateTimeString(Date date, String format) {
        return DateUtil.getDateStr(date, format);
    }
    // ===============================日期时间相关end================================

}
