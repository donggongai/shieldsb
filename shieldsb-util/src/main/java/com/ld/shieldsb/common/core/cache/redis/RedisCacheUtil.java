package com.ld.shieldsb.common.core.cache.redis;

public final class RedisCacheUtil {
    private static final RedisCache CACHE = new RedisCache();

    private RedisCacheUtil() {
        throw new IllegalStateException("该类不可初始化");
    }

    /**
     * 
     * 创建缓存
     * 
     * @Title createCache
     * @author 吕凯
     * @date 2021年4月27日 上午10:54:23
     * @param cacheName
     * @param duration
     *            缓存有效期，单位秒
     * @return void
     */
    public static RedisCacheResult createCache(String cacheName, int duration) { // 有线程安全问题，但是不影响最终效果，后期优化
        return CACHE.createCache(cacheName, duration);
    }

    /**
     * 
     * 在redis数据库中插入 key 和value 并且设置过期时间
     * 
     * @Title set
     * @author 吕凯
     * @date 2017年7月28日 上午10:33:20
     * @param key
     * @param value
     * @param exp
     *            过期时间 s
     * @return boolean
     */
    public static boolean putExp(String key, Object value, int exp) {
        return CACHE.putExp(key, value, exp);
    }

    /**
     * 
     * 防止缓存
     * 
     * @Title put
     * @author 吕凯
     * @date 2017年7月28日 上午11:01:20
     * @param cacheName
     * @param key
     * @param value
     * @return boolean
     */
    public static boolean put(String cacheName, String key, Object value) {
        return CACHE.put(cacheName, key, value);
    }

    /**
     * 根据key 去redis 中获取value
     * 
     * @Title get
     * @author 吕凯
     * @date 2017年7月28日 上午10:39:00
     * @param key
     * @return Object
     */
    public static <T> T get(String cacheName, String key) {
        return CACHE.get(cacheName, key);
    }

    /**
     * 获取缓存key
     * 
     * @Title getCacheKey
     * @author 吕凯
     * @date 2017年8月4日 下午1:47:32
     * @param cacheName
     * @param key
     * @return String
     */
    protected static String getCacheKey(String cacheName, String key) {
        return CACHE.getCacheKey(cacheName, key);
    }

    /**
     * 移除缓存
     * 
     * @Title remove
     * @author 吕凯
     * @date 2017年7月28日 上午11:05:58
     * @param cacheName
     * @param key
     * @return boolean
     */
    public static boolean remove(String cacheName, String key) {
        return CACHE.remove(cacheName, key);
    }

    public static boolean removeAll(String cacheName) {
        return CACHE.removeAll(cacheName);
    }

    public static boolean canUse() {
        return RedisCacheParams.canUse();
    }

    public static boolean close() {
        return CACHE.close();
    }

}