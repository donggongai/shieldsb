package com.ld.shieldsb.common.core.util.useragent;

// 函数式接口
public interface BrowserHandler {
    public BrowserModel deal(String userAgent);

    public boolean checkBrowse(String userAgent);

    // 静态方法
    public static boolean getBrowserType(String userAgent, String brosertype) {
        return userAgent.indexOf(brosertype) > 0 ? true : false;
    }

}
