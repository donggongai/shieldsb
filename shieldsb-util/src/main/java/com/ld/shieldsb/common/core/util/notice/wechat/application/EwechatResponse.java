package com.ld.shieldsb.common.core.util.notice.wechat.application;

import com.alibaba.fastjson.annotation.JSONField;

import lombok.Data;

/**
 * 响应结果
 * 
 * @ClassName EwechatResponse
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年9月14日 上午11:24:37
 *
 */
@Data
public class EwechatResponse {

    private Integer errcode; // 返回码
    private String errmsg; // 对返回码的文本描述内容
    private String invaliduser; // 不合法的userid，不区分大小写，统一转为小写，一般用|连接的字符串，但是更新任务卡片时发现返回的数组
    private String invalidparty; // 不合法的partyid
    private String invalidtag; // 不合法的标签id
    private String msgid; // 消息id，用于撤回应用消息

    @JSONField(name = "response_code")
    private String responseCode; // 仅消息类型为“按钮交互型”，“投票选择型”和“多项选择型”的模板卡片消息返回，应用可使用response_code调用更新模版卡片消息接口，24小时内有效，且只能使用一次

}
