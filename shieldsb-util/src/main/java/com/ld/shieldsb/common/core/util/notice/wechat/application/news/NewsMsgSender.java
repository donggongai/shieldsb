package com.ld.shieldsb.common.core.util.notice.wechat.application.news;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ld.shieldsb.common.core.model.Result;
import com.ld.shieldsb.common.core.util.StringUtils;
import com.ld.shieldsb.common.core.util.notice.wechat.EweChatTool;
import com.ld.shieldsb.common.core.util.notice.wechat.Type;
import com.ld.shieldsb.common.core.util.notice.wechat.application.EweChatApplicationParams;
import com.ld.shieldsb.common.core.util.notice.wechat.application.EwechatBasicMsgSender;

/**
 * 新闻
 * 
 * @ClassName NewsMsgSender
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年9月14日 上午9:42:23
 *
 */
public class NewsMsgSender extends EwechatBasicMsgSender {

    // ============================================= 图文消息 ==============================================
    /**
     * 转换图文dataMap，
     * 
     * @Title parse2NewsDataMap
     * @author 吕凯
     * @date 2021年4月19日 下午5:02:59
     * @param agentid
     * @param sendTo
     * @param title
     * @param msg
     * @param url
     * @param btnTxt
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Map<String,Object>
     */

    public static Map<String, Object> parse2NewsDataMap(String agentid, String sendTo, String title, String description, String url,
            String picurl, Integer duplicateCheckInterval) {
        /*{
            "touser" : "UserID1|UserID2|UserID3",
            "toparty" : "PartyID1 | PartyID2",
            "totag" : "TagID1 | TagID2",
            "msgtype" : "news",
           "agentid" : 1,
           "news" : {
               "articles" : [
               {
                   "title" : "中秋节礼品领取",
                   "description" : "今年中秋节公司有豪礼相送",
                   "url" : "URL",
                   "picurl" : "http://res.mail.qq.com/node/ww/wwopenmng/images/independent/doc/test_pic_msg1.png"
               }]
            },
            "enable_id_trans": 0,
            "enable_duplicate_check": 0,
            "duplicate_check_interval": 1800
         }*/

        Map<String, Object> dataMap = defaultDataMap(agentid, sendTo, duplicateCheckInterval);

        dataMap.put(EweChatTool.MSG_TYPE, Type.NEWS.value);

        Map<String, List<Map<String, String>>> dataNewsMap = new HashMap<>();
        List<Map<String, String>> newsArtiList = new ArrayList<>();
        Map<String, String> dataTextMap = new HashMap<>();
        dataTextMap.put(TITLE, title);
        if (StringUtils.isNotEmpty(description)) {
            dataTextMap.put("description", description);
        }
        dataTextMap.put("url", url); // 必须有
        dataTextMap.put("picurl", picurl);

        newsArtiList.add(dataTextMap);

        dataNewsMap.put("articles", newsArtiList);
        dataMap.put(Type.NEWS.value, dataNewsMap);
        return dataMap;
    }

    public static Map<String, Object> parse2NewsDataMap(String agentid, String sendTo, String title, String description, String url,
            String picurl) {
        return parse2NewsDataMap(agentid, sendTo, title, description, url, picurl, null);
    }

    public static Result sendNewsMsg(String corpid, String corpsecret, String agentid, String sendTo, String title, String description,
            String url, String picurl, Integer duplicateCheckInterval) {
        return sendMsg(corpid, corpsecret, agentid,
                parse2NewsDataMap(agentid, sendTo, title, description, url, picurl, duplicateCheckInterval));

    }

    public static Result sendNewsMsg(String corpid, String corpsecret, String agentid, String sendTo, String title, String description,
            String url, String picurl) {
        return sendNewsMsg(corpid, corpsecret, agentid, sendTo, title, description, url, picurl, null);

    }

    /**
     * 发送图文消息
     * 
     * @Title parse2NewsDataMap
     * @author 吕凯
     * @date 2021年4月19日 下午4:32:10
     * @param sendTo
     * @param title
     * @param msg
     * @param url
     * @param btnTxt
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Result
     */
    public static Result sendNewsMsg(String sendTo, String title, String description, String url, String picurl,
            Integer duplicateCheckInterval) {
        // 注意下面参数需要到企业微信中获取
        EweChatApplicationParams params = getConfig();
        Result flag = sendNewsMsg(params.getCorpId(), params.getCorpSecret(), params.getAgentId(), sendTo, title, description, url, picurl,
                duplicateCheckInterval);
        return flag;
    }

    public static Result sendNewsMsg(String sendTo, String title, String description, String url, String picurl) {
        return sendNewsMsg(sendTo, title, description, url, picurl, null);
    }
}
