package com.ld.shieldsb.common.core.util.notice.sms.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.apache.commons.lang.StringUtils;

import com.ld.shieldsb.common.core.model.PropertiesModel;
import com.ld.shieldsb.common.core.model.Result;
import com.ld.shieldsb.common.core.util.notice.sms.SMSSend;
import com.ld.shieldsb.common.core.util.notice.sms.SMSSendAuth;
import com.ld.shieldsb.common.core.util.notice.sms.SMSSendModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SMSSend5c implements SMSSend {
    public static final String PHONE_RETURN_SUCCESS = "success";

    /**
     * 
     * 发送短信
     * 
     * @Title sendSMS
     * @author 吕凯
     * @date 2017年8月17日 上午8:44:51
     * @param phoneNum
     *            手机号,只发一个号码：13800000001。发多个号码：13800000001,13800000002,...N 。使用半角逗号分隔
     * @param message
     * @param signName
     *            签名
     * @return
     * @see com.ld.util.sms.SMSSend#sendSMS(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public Result sendSMS(String phoneNum, String message, String signName) {

        SMSSendModel sendInfo = new SMSSendModel();
        sendInfo.setPhoneNum(phoneNum);
        sendInfo.setMessage(message);
        sendInfo.setSignName(signName);

        SMSSendAuth auth = getAuthFromProperties();
        return sendSMS(sendInfo, auth);
    }

    /**
     * 从配置文件中获取验证信息
     * 
     * @Title getAuthFromProperties
     * @author 吕凯
     * @date 2020年6月10日 下午3:35:50
     * @return SMSSendAuth
     */
    private SMSSendAuth getAuthFromProperties() {
        String apikey = PropertiesModel.CONFIG.getString("notice.sms.client-secret", "");
        String username = PropertiesModel.CONFIG.getString("notice.sms.client-username", ""); // 用户名
        String password = PropertiesModel.CONFIG.getString("notice.sms.client-password", ""); // 密码
        String password_md5 = PropertiesModel.CONFIG.getString("notice.sms.client-password-encrypted", ""); // md5密码
        SMSSendAuth auth = new SMSSendAuth();
        auth.setApiKey(apikey);
        auth.setApiUser(username);
        auth.setPassword(password);
        auth.setPasswordEncrypted(password_md5);
        return auth;
    }

    @Override
    public Result sendSMS(String phoneNum, String message) {
        return sendSMS(phoneNum, message, null);
    }

    @Override
    public Result sendSMS(SMSSendModel sendInfo, SMSSendAuth auth) {
        Result SMSResult = new Result();

        // 连接超时及读取超时设置
//        System.setProperty("sun.net.client.defaultConnectTimeout", "30000"); // 连接超时：30秒
//        System.setProperty("sun.net.client.defaultReadTimeout", "30000"); // 读取超时：30秒

        // 新建一个StringBuffer链接
        StringBuffer buffer = new StringBuffer();
        // String encode = "GBK";
        // 页面编码和短信内容编码为GBK。重要说明：如提交短信后收到乱码，请将GBK改为UTF-8测试。如本程序页面为编码格式为：ASCII/GB2312/GBK则该处为GBK。如本页面编码为UTF-8或需要支持繁体，阿拉伯文等Unicode，请将此处写为：UTF-8
        String encode = "UTF-8";
        String mobile = sendInfo.getPhoneNum(); // 手机号,只发一个号码：13800000001。发多个号码：13800000001,13800000002,...N 。使用半角逗号分隔。
        // apikey秘钥（请登录http://m.5c.com.cn短信平台-->账号管理-->我的信息中复制apikey）
        String apikey = auth.getApiKey();
        String username = auth.getApiUser(); // 用户名
        String password = auth.getPassword(); // 密码
        String password_md5 = auth.getPasswordEncrypted(); // md5密码
        String content = sendInfo.getMessage(); // 要发送的短信内容，注意：签名设置，网页验证码应用需要加添加【图形识别码】。
        try {
            if (!StringUtils.isEmpty(sendInfo.getSignName())) {
                content += "【" + sendInfo.getSignName() + "】";
            }
            String contentUrlEncode = URLEncoder.encode(content, encode); // 对短信内容做Urlencode编码操作。注意：如
            // 把发送链接存入buffer中，如连接超时，可能是您服务器不支持域名解析，请将下面连接中的：【m.5c.com.cn】修改为IP：【115.28.23.78】
            buffer.append("http://m.5c.com.cn/api/send/index.php?encode=" + encode);
            buffer.append("&apikey=").append(apikey);
            buffer.append("&username=").append(username);
            if (!StringUtils.isEmpty(password_md5)) {
                buffer.append("&password_md5=").append(password_md5);
            } else {
                buffer.append("&password=").append(password);
            }
            buffer.append("&mobile=").append(mobile);
            buffer.append("&content=").append(contentUrlEncode);
            // System.out.println(buffer); //调试功能，输入完整的请求URL地址
            // 把buffer链接存入新建的URL中
            URL url = new URL(buffer.toString());
            // 打开URL链接
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            // 使用POST方式发送
            connection.setRequestMethod("POST");
            // 使用长链接方式
            connection.setRequestProperty("Connection", "Keep-Alive");
            // 发送短信内容
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()))) {

                // 获取返回值
                String result = reader.readLine();
//            success:msgid  提交成功，发送状态请见 4.1
//            error:msgid  提交失败
//            error:Missing username  用户名为空
//            error:Missing password  密码为空
//            error:Missing apikey  APIKEY 为空
//            error:Missing recipient  收件人手机号码为空
//            error:Missing message content  短信内容为空或编码不正确
//            error:Account is blocked  帐号被禁用
//            error:Unrecognized encoding  编码未能识别
//            error:APIKEY or password error  APIKEY 或密码错误
//            error:Unauthorized IP address  未授权 IP 地址
//            error:Account balance is insufficient  余额不足
//            error:Throughput Rate Exceeded  发送频率受限
//            error:Invalid md5 password length  MD5 密码长度非 32 位
                // result内容，成功为success，错误为error，详见该文档起始注释
                SMSResult.setData(result); // 存入完整的返回信息
                if (!StringUtils.isEmpty(result)) { // 发送成功
                    if (result.contains(PHONE_RETURN_SUCCESS)) {
                        SMSResult.setSuccess(true);
                    } else if (result.contains(":")) { // 拆分
                        String returnMsg = result.split(":")[1];
                        if ("Missing username".equals(returnMsg)) {
                            returnMsg = ERR_MISSING_USERNAME;
                        } else if ("Missing password".equals(returnMsg)) {
                            returnMsg = ERR_MISSING_PASSWORD;
                        } else if ("Missing apikey".equals(returnMsg)) {
                            returnMsg = ERR_MISSING_APIKEY;
                        } else if ("Missing recipient".equals(returnMsg)) {
                            returnMsg = ERR_MISSING_RECIPIENT;
                        } else if ("Missing message content".equals(returnMsg)) {
                            returnMsg = ERR_MISSING_MESSAGE_CONTENT;
                        } else if ("Account is blocked".equals(returnMsg)) {
                            returnMsg = ERR_ACCOUNT_IS_BLOCKED;
                        } else if ("Unrecognized encoding".equals(returnMsg)) {
                            returnMsg = ERR_UNRECOGNIZED_ENCODING;
                        } else if ("APIKEY or password error".equals(returnMsg)) {
                            returnMsg = ERR_APIKEY_OR_PASSWORD_ERROR;
                        } else if ("Unauthorized IP address".equals(returnMsg)) {
                            returnMsg = ERR_UNAUTHORIZED_IP_ADDRESS;
                        } else if ("Account balance is insufficient".equals(returnMsg)) {
                            returnMsg = ERR_ACCOUNT_BALANCE_IS_INSUFFICIENT;
                        } else if ("Throughput Rate Exceeded".equals(returnMsg)) {
                            returnMsg = ERR_THROUGHPUT_RATE_EXCEEDED;
                        } else if ("Invalid md5 password length".equals(returnMsg)) {
                            returnMsg = ERR_INVALID_MD5_PASSWORD_LENGTH;
                        }
                        SMSResult.setMessage(returnMsg);
                    }
                } else {
                    SMSResult.setMessage("系统暂时无法发送验证码");
                    log.error("系统暂时无法发送验证码");
                }
            }

        } catch (Exception e) {
            log.error("", e);
        }
        return SMSResult;
    }

}
