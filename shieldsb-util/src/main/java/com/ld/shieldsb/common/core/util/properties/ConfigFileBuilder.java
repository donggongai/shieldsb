package com.ld.shieldsb.common.core.util.properties;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

import com.ld.shieldsb.common.core.io.FileUtils;
import com.ld.shieldsb.common.core.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ConfigFileBuilder {

    private static final Map<String, Class<? extends ConfigFile>> PARAMS_QUERY_HANDLERS = new HashMap<>(); // 参数处理器，key需要包含_
    public static final String PARAMS_QUERY_DEFAULT_KEY = "defalult"; // 默认处理
    static {
        PARAMS_QUERY_HANDLERS.put("properties", ConfigPropertiesUtil.class); // properties格式
        PARAMS_QUERY_HANDLERS.put("yml", ConfigYamlUtil.class); // yml格式
        PARAMS_QUERY_HANDLERS.put("yaml", ConfigYamlUtil.class); // yml格式
        PARAMS_QUERY_HANDLERS.put(PARAMS_QUERY_DEFAULT_KEY, ConfigPropertiesUtil.class); // 默认，其他不符合条件的都走这个
    }

    /**
     * 注册新的参数处理器，也可对旧的进行覆盖
     * 
     * @Title registerParamsQueryHandler
     * @author 吕凯
     * @date 2019年7月18日 下午2:04:11
     * @param paramNameprefix
     * @param defalutHandler
     *            void
     */
    public static void registerParamsQueryHandler(String paramNameprefix, Class<? extends ConfigFile> clses) {
        PARAMS_QUERY_HANDLERS.put(paramNameprefix, clses);
    }

    public static Map<String, Class<? extends ConfigFile>> getParamsQueryHandlers() {
        return PARAMS_QUERY_HANDLERS;
    }

    @SuppressWarnings("rawtypes")
    public static ConfigFile build(String fileName) {
        ConfigFile file = null;
        if (StringUtils.isNotBlank(fileName)) {
            String extension = FileUtils.getFileExtension(fileName);
            Class<? extends ConfigFile> fileClass = PARAMS_QUERY_HANDLERS.get(extension);
            if (fileClass == null) {
                log.warn("后缀为：" + extension + "的配置文件处理类未注册！fileName：为" + fileName + "，使用默认的"
                        + PARAMS_QUERY_HANDLERS.get(PARAMS_QUERY_DEFAULT_KEY) + "类进行处理！");
                fileClass = PARAMS_QUERY_HANDLERS.get(PARAMS_QUERY_DEFAULT_KEY);
            }
            Constructor con;
            // 通过构造器对象 newInstance 方法对对象进行初始化 有参数构造函数
            try {
                con = fileClass.getConstructor(String.class);
                file = (ConfigFile) con.newInstance(fileName);
            } catch (Exception e) {
                log.error("", e);
            }
        }
        return file;
    }

    public static void main(String[] args) {
//        System.out.println(ConfigFileBuilder.build("config.properties").getPropertys());
//        System.out.println(ConfigFileBuilder.build("config.properties").save("tt.my", "test"));
//        System.out.println(ConfigFileBuilder.build("config.properties").getPropertys());
//
//        System.out.println(ConfigFileBuilder.build("config.yml").getPropertys());
//        System.out.println(ConfigFileBuilder.build("config.yml").save("tt.my", "test"));
//        System.out.println(ConfigFileBuilder.build("config.yml").getPropertys());
    }

}
