package com.ld.shieldsb.common.core.util;

import java.io.IOException;
import java.net.MalformedURLException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * HtmlUnit工具类
 * 
 * @ClassName HtmlUnitUtil
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2018年11月20日 上午11:42:12
 *
 */
@Slf4j
public class HtmlUnitUtil {

    public static String getRemoteURLContent(String url) {
        Document doc;
        try {
            doc = getRemoteURLDocument(url);
            if (doc != null) {
                return doc.toString();
            }
        } catch (FailingHttpStatusCodeException | IOException e) {
            log.error("获取内容出错！", e);
        }
        return null;
    }

    public static Document getRemoteURLDocument(String url) throws FailingHttpStatusCodeException, MalformedURLException, IOException {
        WebClient webClient = new WebClient(BrowserVersion.CHROME); // 新建一个模拟谷歌Chrome浏览器的浏览器客户端对象
        webClient.getOptions().setJavaScriptEnabled(true); // 很重要，启用JS
        webClient.getOptions().setActiveXNative(false);
        webClient.getOptions().setCssEnabled(false); // 是否启用CSS, 因为不需要展现页面, 所以不需要启用
        webClient.getOptions().setThrowExceptionOnScriptError(true); // 当JS执行出错的时候是否抛出异常, 这里选择不需要
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false); // 当HTTP的状态非200时是否抛出异常, 这里选择不需要
        webClient.setAjaxController(new NicelyResynchronizingAjaxController());// 很重要，设置支持AJAX
        webClient.getOptions().setTimeout(10000);
        HtmlPage htmlPage = null;
        try {
            htmlPage = webClient.getPage(url);
            webClient.waitForBackgroundJavaScript(20000); // 异步JS执行需要耗时,所以这里线程要阻塞10秒,等待异步JS执行结束
            String htmlString = htmlPage.asXml(); // 直接将加载完成的页面转换成xml格式的字符串
            return Jsoup.parse(htmlString);
        } finally {
            webClient.close();
        }
    }

}
