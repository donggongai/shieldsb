package com.ld.shieldsb.common.core.util.useragent.impl;

import com.ld.shieldsb.common.core.util.useragent.BrowserHandler;
import com.ld.shieldsb.common.core.util.useragent.BrowserModel;
import com.ld.shieldsb.common.core.util.useragent.BrowserUtils;

/**
 * Safari浏览器处理
 * 
 * @ClassName SafariBrowserHandler
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2019年7月19日 下午2:47:19
 *
 */
public class SafariBrowserHandler implements BrowserHandler {
    private static final String BROWSER_TYPE = BrowserUtils.SAFARI;

    @Override
    public BrowserModel deal(String userAgent) {
        BrowserModel browserModel = new BrowserModel();
        if (checkBrowse(userAgent)) {
            browserModel.setBrowserType(BROWSER_TYPE);
            browserModel.setBrowserVersion(BrowserUtils.getBrowserVersion(BROWSER_TYPE, userAgent));
        }
        return browserModel;
    }

    @Override
    public boolean checkBrowse(String userAgent) {
        return userAgent.contains(" Safari") && !userAgent.contains(" Chrome") && userAgent.contains("Mozilla");
    }

}
