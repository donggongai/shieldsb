package com.ld.shieldsb.common.core.io.img;

import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import com.gif4j.GifDecoder;
import com.gif4j.GifEncoder;
import com.gif4j.GifImage;
import com.gif4j.GifTransformer;

/**
 * 
 * Gif格式图片操作工具类
 * 
 * @ClassName ImgUtil
 * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
 * @date 2016年8月25日 下午3:12:22
 *
 */
public class GifUtil {
    private GifUtil() {
        throw new IllegalStateException("该类不可初始化");
    }

    /**
     * 拉伸图片
     * 
     * @param src
     * @param dest
     * @param width
     * @param height
     * @throws IOException
     */
    public static void resize(File src, File dest, int width, int height) throws IOException {
        GifImage gifImage = GifDecoder.decode(src);// 创建一个GifImage对象.
        resize(dest, width, height, gifImage);
    }

    /**
     * 拉伸图片
     * 
     * @Title makeGif
     * @author 吕凯
     * @date 2017年5月16日 下午4:37:12
     * @param is
     * @param dest
     * @param width
     * @param height
     * @throws IOException
     *             void
     */
    public static void resize(InputStream is, File dest, int width, int height) throws IOException {
        GifImage gifImage = GifDecoder.decode(is);// 创建一个GifImage对象.
        resize(dest, width, height, gifImage);
    }

    /**
     * 
     * 拉伸图片
     * 
     * @Title resize
     * @author 吕凯
     * @date 2017年5月18日 上午10:53:29
     * @param dest
     * @param width
     * @param height
     * @param gifImage
     * @throws IOException
     *             void
     */
    private static void resize(File dest, int width, int height, GifImage gifImage) throws IOException {
        GifImage resizeIMG = GifTransformer.resize(gifImage, width, height, true); // 最后一个参数smooth
        GifEncoder.encode(resizeIMG, dest);
    }

    /**
     * 裁剪gif图片
     * 
     * @Title cutGif
     * @author 吕凯
     * @date 2017年5月18日 上午9:56:02
     * @param is
     *            文件输入流
     * @param dest
     *            输出文件
     * @param x
     *            裁剪开始x坐标
     * @param y
     *            裁剪开始y坐标
     * @param destWidth
     *            裁剪宽度
     * @param destHeight
     *            裁剪高度
     * @throws IOException
     *             void
     */
    public static void crop(InputStream is, File dest, int x, int y, int destWidth, int destHeight) throws IOException {
        GifImage gifImage = GifDecoder.decode(is);// 创建一个GifImage对象.
        crop(dest, x, y, destWidth, destHeight, gifImage);
    }

    /**
     * 裁剪gif图片
     * 
     * @Title cutGif
     * @author 吕凯
     * @date 2017年5月18日 上午9:56:02
     * @param src
     *            源文件
     * @param dest
     *            输出文件
     * @param x
     *            裁剪开始x坐标
     * @param y
     *            裁剪开始y坐标
     * @param destWidth
     *            裁剪宽度
     * @param destHeight
     *            裁剪高度
     * @throws IOException
     *             void
     */
    public static void crop(File src, File dest, int x, int y, int destWidth, int destHeight) throws IOException {
        GifImage gifImage = GifDecoder.decode(src);// 创建一个GifImage对象.
        crop(dest, x, y, destWidth, destHeight, gifImage);
    }

    /**
     * 裁剪图片
     * 
     * @Title crop
     * @author 吕凯
     * @date 2017年5月18日 上午10:52:59
     * @param dest
     * @param x
     * @param y
     * @param destWidth
     * @param destHeight
     * @param gifImage
     * @throws IOException
     *             void
     */
    private static void crop(File dest, int x, int y, int destWidth, int destHeight, GifImage gifImage) throws IOException {
        Rectangle rect = new Rectangle(x, y, destWidth, destHeight);
        GifImage resizeIMG = GifTransformer.crop(gifImage, rect);
        GifEncoder.encode(resizeIMG, dest);
    }

    /**
     * 
     * 等比例缩放
     * 
     * @Title scale
     * @author 吕凯
     * @date 2017年5月18日 上午10:46:43
     * @param src
     *            源文件
     * @param dest
     *            目标文件
     * @param xTimes
     *            x轴缩放倍数
     * @param yTimes
     *            y轴缩放倍数
     * @throws IOException
     *             void
     */
    public static void scale(File src, File dest, double xTimes, double yTimes) throws IOException {
        GifImage gifImage = GifDecoder.decode(src);// 创建一个GifImage对象.
        scale(dest, xTimes, yTimes, gifImage);
    }

    /**
     * 
     * 等比例缩放
     * 
     * @Title scale
     * @author 吕凯
     * @date 2017年5月18日 上午10:46:43
     * @param src
     *            源文件
     * @param dest
     *            目标文件
     * @param xTimes
     *            x轴缩放倍数
     * @param yTimes
     *            y轴缩放倍数
     * @throws IOException
     *             void
     */
    public static void scale(InputStream is, File dest, double xTimes, double yTimes) throws IOException {
        GifImage gifImage = GifDecoder.decode(is);// 创建一个GifImage对象.
        scale(dest, xTimes, yTimes, gifImage);
    }

    /**
     * 等比例缩放
     * 
     * @Title scale
     * @author 吕凯
     * @date 2017年5月18日 上午10:52:25
     * @param dest
     * @param xTimes
     * @param yTimes
     * @param gifImage
     * @throws IOException
     *             void
     */
    private static void scale(File dest, double xTimes, double yTimes, GifImage gifImage) throws IOException {
        GifImage scaleIMG = GifTransformer.scale(gifImage, xTimes, yTimes, true);
        GifEncoder.encode(scaleIMG, dest);
    }

}
