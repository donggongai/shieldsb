/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.ld.shieldsb.common.core.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

/**
 * 数据流工具类
 * 
 * @author ThinkGem
 */
@Slf4j

public class IOUtils extends org.apache.commons.io.IOUtils {
    private static final String FILE_NOT_EXIST = "错误信息:文件不存在";

    /**
     * 根据文件路径创建文件输入流处理 以字节为单位（非 unicode ）
     * 
     * @param path
     * @return
     */
    public static FileInputStream getFileInputStream(String filepath) {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(filepath);
        } catch (FileNotFoundException e) {
            log.error(FILE_NOT_EXIST, e);
        }
        return fileInputStream;
    }

    /**
     * 根据文件对象创建文件输入流处理 以字节为单位（非 unicode ）
     * 
     * @param path
     * @return
     */
    public static FileInputStream getFileInputStream(File file) {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            log.error(FILE_NOT_EXIST, e);
        }
        return fileInputStream;
    }

    /**
     * 根据文件对象创建文件输出流处理 以字节为单位（非 unicode ）
     * 
     * @param file
     * @param append
     *            true:文件以追加方式打开,false:则覆盖原文件的内容
     * @return
     */
    public static FileOutputStream getFileOutputStream(File file, boolean append) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(file, append);
        } catch (FileNotFoundException e) {
            log.error(FILE_NOT_EXIST, e);
        }
        return fileOutputStream;
    }

    /**
     * 根据文件路径创建文件输出流处理 以字节为单位（非 unicode ）
     * 
     * @param path
     * @param append
     *            true:文件以追加方式打开,false:则覆盖原文件的内容
     * @return
     */
    public static FileOutputStream getFileOutputStream(String filepath, boolean append) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(filepath, append);
        } catch (FileNotFoundException e) {
            log.error(FILE_NOT_EXIST, e);
        }
        return fileOutputStream;
    }

    /**
     * 读取resource文件并写入文件
     * 
     * @Title readResource2File
     * @author 吕凯
     * @date 2019年4月13日 上午9:56:20
     * @param resourceName
     * @param file
     *            void
     */
    public static void readResource2File(String resourceName, File file) {
        try {

            URL resourceURL = IOUtils.class.getClassLoader().getResource(resourceName);
            if (resourceURL != null) {
                List<String> lines = IOUtils.readLines(resourceURL.openStream(), "UTF-8"); // 去除缓存的写法
                try (FileOutputStream outStream = new FileOutputStream(file)) {
                    IOUtils.writeLines(lines, IOUtils.LINE_SEPARATOR, outStream, "UTF-8");
                }
            }
        } catch (IOException e) {
            log.error("", e);
        }
    }

    /**
     * 读取为流
     * 
     * @Title readAsInputStream
     * @author 吕凯
     * @date 2019年10月31日 下午6:05:45
     * @param resourceName
     * @param cache
     * @return InputStream
     */
    public static InputStream readAsInputStream(String resourceName, boolean cache) {
        try {

            if (cache) { // 读取一次后从缓存中取
                return IOUtils.class.getClassLoader().getResourceAsStream(resourceName);
            } else { // 不缓存的写法
                // 该方法直接读文件，所以在读文件频繁时会造成一定性能损耗；但能够确保获取的配置信息是最新的
                URL resourceURL = IOUtils.class.getClassLoader().getResource(resourceName);
                if (resourceURL != null) {
                    return resourceURL.openStream(); // 去除缓存的写法
                }
            }
        } catch (IOException e) {
            log.error("", e);
        }
        return null;
    }

    /**
     * 读取为流,读取一次后会从缓存中取
     * 
     * @Title readAsInputStream
     * @author 吕凯
     * @date 2019年10月31日 下午6:09:00
     * @param resourceName
     * @return InputStream
     */
    public static InputStream readAsInputStream(String resourceName) {
        return readAsInputStream(resourceName, true);
    }

    /**
     * 
     * 读取resource文件并写入文件
     * 
     * @Title readResource2File
     * @author 吕凯
     * @date 2019年4月13日 上午10:01:07
     * @param input
     * @param file
     *            void
     */
    public static void readResource2File(InputStream input, File file) {
        try (FileOutputStream outStream = new FileOutputStream(file)) {
            IOUtils.copy(input, outStream);
        } catch (Exception e) {
            log.error("", e);
        }
    }

}