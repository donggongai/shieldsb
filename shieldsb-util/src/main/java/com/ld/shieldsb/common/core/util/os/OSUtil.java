package com.ld.shieldsb.common.core.util.os;

/**
 * 系统信息工具类
 * 
 * @ClassName OSUtil
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2018年11月16日 上午9:06:43
 *
 */
public class OSUtil {

    private static final String OS = System.getProperty("os.name").toLowerCase();

    private static final OSUtil instance = new OSUtil();

    private OSType platform;

    private OSUtil() {
    }

    public static boolean isLinux() {
        return OS.indexOf("linux") >= 0;
    }

    public static boolean isMacOS() {
        return OS.indexOf("mac") >= 0 && OS.contains("os") && !OS.contains("x");
    }

    public static boolean isMacOSX() {
        return OS.indexOf("mac") >= 0 && OS.contains("os") && OS.contains("x");
    }

    public static boolean isWindows() {
        return OS.indexOf("windows") >= 0;
    }

    public static boolean isOS2() {
        return OS.indexOf("os/2") >= 0;
    }

    public static boolean isSolaris() {
        return OS.indexOf("solaris") >= 0;
    }

    public static boolean isSunOS() {
        return OS.indexOf("sunos") >= 0;
    }

    public static boolean isMPEiX() {
        return OS.indexOf("mpe/ix") >= 0;
    }

    public static boolean isHPUX() {
        return OS.indexOf("hp-ux") >= 0;
    }

    public static boolean isAix() {
        return OS.indexOf("aix") >= 0;
    }

    public static boolean isOS390() {
        return OS.indexOf("os/390") >= 0;
    }

    public static boolean isFreeBSD() {
        return OS.indexOf("freebsd") >= 0;
    }

    public static boolean isIrix() {
        return OS.indexOf("irix") >= 0;
    }

    public static boolean isDigitalUnix() {
        return OS.indexOf("digital") >= 0 && OS.indexOf("unix") > 0;
    }

    public static boolean isNetWare() {
        return OS.indexOf("netware") >= 0;
    }

    public static boolean isOSF1() {
        return OS.indexOf("osf1") >= 0;
    }

    public static boolean isOpenVMS() {
        return OS.indexOf("openvms") >= 0;
    }

    /**
     * 获取操作系统名字
     *
     * @return 操作系统名
     */
    public static OSType getOSType() {
        if (isAix()) {
            instance.platform = OSType.AIX;
        } else if (isDigitalUnix()) {
            instance.platform = OSType.Digital_Unix;
        } else if (isFreeBSD()) {
            instance.platform = OSType.FreeBSD;
        } else if (isHPUX()) {
            instance.platform = OSType.HP_UX;
        } else if (isIrix()) {
            instance.platform = OSType.Irix;
        } else if (isLinux()) {
            instance.platform = OSType.Linux;
        } else if (isMacOS()) {
            instance.platform = OSType.Mac_OS;
        } else if (isMacOSX()) {
            instance.platform = OSType.Mac_OS_X;
        } else if (isMPEiX()) {
            instance.platform = OSType.MPEiX;
        } else if (isNetWare()) {
            instance.platform = OSType.NetWare_411;
        } else if (isOpenVMS()) {
            instance.platform = OSType.OpenVMS;
        } else if (isOS2()) {
            instance.platform = OSType.OS2;
        } else if (isOS390()) {
            instance.platform = OSType.OS390;
        } else if (isOSF1()) {
            instance.platform = OSType.OSF1;
        } else if (isSolaris()) {
            instance.platform = OSType.Solaris;
        } else if (isSunOS()) {
            instance.platform = OSType.SunOS;
        } else if (isWindows()) {
            instance.platform = OSType.Windows;
        } else {
            instance.platform = OSType.Others;
        }
        return instance.platform;
    }

    public static String getOSName() {
        return OS;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(OSUtil.getOSType());
    }
}
