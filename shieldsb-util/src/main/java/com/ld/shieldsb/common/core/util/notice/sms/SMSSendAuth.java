package com.ld.shieldsb.common.core.util.notice.sms;

import lombok.Data;

/**
 * 短信服务验证对象
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2020年6月10日 下午3:03:37
 *
 */
@Data
public final class SMSSendAuth {
    private String apiKey; // apikey
    private String apiUser; // 用户名
    private String password; // 密码
    private String passwordEncrypted; // md5密码
}
