package com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.text;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 关键数据样式,可为空
 * 
 * @ClassName TemplateCardEmphasisTitle
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年9月13日 上午8:24:22
 *
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class TmplCardEmphasisContentModel {
    private String title; // 关键数据样式的数据内容，建议不超过14个字
    private String desc; // 关键数据样式的数据描述内容，建议不超过22个字

}
