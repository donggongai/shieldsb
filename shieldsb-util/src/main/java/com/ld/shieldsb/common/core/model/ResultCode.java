package com.ld.shieldsb.common.core.model;

public class ResultCode {
    private ResultCode() {
        // 工具类无需对象实例化
    }

    /**
     * 参数错误
     */
    public static final String ERROR_PARAM_NOT_VALID = "1001";

}
