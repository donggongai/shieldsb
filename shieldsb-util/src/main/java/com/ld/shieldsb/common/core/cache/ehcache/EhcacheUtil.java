package com.ld.shieldsb.common.core.cache.ehcache;

import java.io.Serializable;

import org.ehcache.Cache;
import org.ehcache.config.CacheConfiguration;

import com.ld.shieldsb.common.core.cache.CacheResult;
import com.ld.shieldsb.common.core.cache.dataloader.IDataLoader;

/**
 * 
 * Ehcache缓存相关，通过ehcache.xml配置
 * 
 * @ClassName EhcacheUtil
 * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
 * @date 2016年8月26日 下午2:00:15
 *
 */
public class EhcacheUtil {
    private static final EhcacheCache CACHE = new EhcacheCache();

    private EhcacheUtil() {
        throw new IllegalStateException("该类不可初始化");
    }

    /**
     * 创建缓存
     * 
     * @Title createCache
     * @author 吕凯
     * @date 2019年10月15日 上午11:51:50
     * @param cacheName
     * @param duration
     *            缓存有效期，单位秒
     * @return Cache<Object,Object>
     */
    public static CacheResult createCache(String cacheName, long duration) { // 有线程安全问题，但是不影响最终效果，后期优化
        return CACHE.createCache(cacheName, duration);
    }

    public static CacheResult createCache(String cacheName, long duration, long entries, long offheapSize, long diskSize,
            boolean persistent) {
        return CACHE.createCache(cacheName, duration, entries, offheapSize, diskSize, persistent);
    }

    public static CacheResult createCache(String cacheName, long duration, long entries) {
        return CACHE.createCache(cacheName, duration, entries);
    }

    public static CacheResult createCache(String cacheName, CacheConfiguration<String, Serializable> configuration) { // 有线程安全问题，但是不影响最终效果，后期优化
        return CACHE.createCache(cacheName, configuration);
    }

    public static CacheResult getCache(String cacheName) {
        return CACHE.getCache(cacheName);
    }

    public static void removeCache(String cacheName) {
        CACHE.removeCache(cacheName);
    }

    public static String getPersistencePath() {
        return CACHE.getFilePath();
    }

    /**
     * 创建缓存
     * 
     * @Title createCache
     * @author 吕凯
     * @date 2019年10月15日 上午11:50:22
     * @param cacheName
     * @return Cache
     */
    public static Cache<String, Serializable> createCache(String cacheName) {
        return CACHE.getOrAddCache(cacheName);
    }

    public static boolean put(String cacheName, String key, Object value) {
        return CACHE.put(cacheName, key, value);
    }

    public static <T> T get(String cacheName, String key) {
        return CACHE.get(cacheName, key);
    }

    public static void remove(String cacheName, String key) {
        CACHE.remove(cacheName, key);
    }

    public static void removeAll(String cacheName) {
        CACHE.removeAll(cacheName);
    }

    /**
     * 通过数据加载器加载
     * 
     * @Title get
     * @author 吕凯
     * @date 2019年7月2日 上午8:16:21
     * @param cacheName
     * @param key
     * @param dataLoader
     *            数据加载器实现类
     * @return T
     */
    public static <T> T get(String cacheName, String key, IDataLoader dataLoader) {
        return CACHE.get(cacheName, key, dataLoader);
    }

    /**
     * 通过数据加载器加载
     * 
     * @Title get
     * @author 吕凯
     * @date 2019年7月2日 上午8:17:20
     * @param cacheName
     * @param key
     * @param dataLoaderClass
     *            数据加载器实现类的类名
     * @return T
     */
    public static <T> T get(String cacheName, String key, Class<? extends IDataLoader> dataLoaderClass) {
        return CACHE.get(cacheName, key, dataLoaderClass);
    }

    public static void close() {
        CACHE.close();
    }

    /**
     * 
     * 缓存是否可用
     * 
     * @Title canUse
     * @return boolean
     */
    public static boolean canUse() {
        return CACHE.canUse();
    }

}
