package com.ld.shieldsb.common.core.cache.redis;

import com.ld.shieldsb.common.core.cache.CacheResult;

import lombok.Data;

@Data
public class RedisCacheResult implements CacheResult {
    private Boolean success;
}
