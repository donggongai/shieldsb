package com.ld.shieldsb.common.core.cache.ehcache;

import com.ld.shieldsb.common.core.model.PropertiesModel;

/**
 * Ehcache参数配置
 * 
 * @ClassName EhcacheParams
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2018年11月19日 上午8:54:07
 *
 */
public class EhcacheParams {
    private static Boolean use = PropertiesModel.CONFIG.getBoolean("cache_ehcache_use", true); // ehcache是否可用，默认可用

    private EhcacheParams() {
        throw new IllegalStateException("该类不可初始化");
    }

    /**
     * 
     * 缓存是否可用
     * 
     * @Title canUse
     * @return boolean
     */
    public static boolean canUse() {
        return use;
    }

}
