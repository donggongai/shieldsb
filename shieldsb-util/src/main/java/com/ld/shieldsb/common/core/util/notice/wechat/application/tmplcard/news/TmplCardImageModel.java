package com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.news;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 
 * 图片样式
 * 
 * @ClassName TmplCardImageModel
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年9月13日 上午11:58:37
 *
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class TmplCardImageModel {
    private String url; // 图片的url
    private Double aspectRatio; // 图片的宽高比，宽高比要小于2.25，大于1.3，不填该参数默认1.3

}
