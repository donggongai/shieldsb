package com.ld.shieldsb.common.core.model;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import com.ld.shieldsb.common.core.util.properties.ConfigFile;
import com.ld.shieldsb.common.core.util.properties.ConfigFileBuilder;
import com.ld.shieldsb.common.core.util.properties.ConfigPropertiesUtil;

/**
 * 配置文件类
 * 
 * @ClassName SysconfigAction
 * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
 * @date 2014-6-21 下午2:37:40
 * 
 */
public class PropertiesModel {
    private PropertiesModel() {
        // 私有类不允许构造
    }

    private static ServletContext context = null;

    // 项目配置文件后缀（只有1个，不支持更新）
    public static final ConfigFile APPLICATION;
    // 配置文件后缀（可能配置多个）
    public static final String PROPERTIES_SUFFIX;
    // config主要参数配置，放置到properties目录中
    public static final ConfigFile CONFIG;
    // project主要参数配置，放置到properties目录中
    public static final ConfigFile PROJECT;

    // 构造对象
    static {
        APPLICATION = ConfigFileBuilder.build("application.properties");
        // 配置文件后缀（可能配置多个）
        if (APPLICATION != null && APPLICATION.checkFileExist()) {
            PROPERTIES_SUFFIX = APPLICATION.getString("properties.suffix");
        } else {
            PROPERTIES_SUFFIX = "";
        }
        CONFIG = new ConfigPropertiesUtil(
                "".equals(PROPERTIES_SUFFIX) ? "config.properties" : "properties/config_" + PROPERTIES_SUFFIX + ".properties") {

            /**
             * 
             * 保存成功的回调函数
             * 
             * @Title saveCallback
             * @author 吕凯
             * @date 2017年12月20日 下午6:01:40 void
             */
            @Override
            public void saveCallback(String key, Object value) {
                refreshConfig2Context(key, value); // 写到上下文中实时更新
            }

            /**
             * 
             * 保存成功的回调函数
             * 
             * @Title saveCallback
             * @author 吕凯
             * @date 2017年12月20日 下午6:01:40 void
             */
            @Override
            public void saveCallback(Map<String, Object> proMap) {
                refreshConfig2Context(); // 写到上下文中实时更新

            }

        };

        // project主要参数配置，放置到properties目录中
        PROJECT = new ConfigPropertiesUtil(
                "".equals(PROPERTIES_SUFFIX) ? "project.properties" : "properties/project_" + PROPERTIES_SUFFIX + ".properties") {
            /**
             * 
             * 保存成功的回调函数
             * 
             * @Title saveCallback
             * @author 吕凯
             * @date 2017年12月20日 下午6:01:40 void
             */
            @Override
            public void saveCallback(String key, Object value) {
                refreshConfig2Context(key, value); // 写到上下文中实时更新
            }

            /**
             * 
             * 保存成功的回调函数
             * 
             * @Title saveCallback
             * @author 吕凯
             * @date 2017年12月20日 下午6:01:40 void
             */
            @Override
            public void saveCallback(Map<String, Object> proMap) {
                refreshProject2Context(); // 写到上下文中实时更新
            }

        };
    }

    /**
     * 刷新基本配置参数到上下文
     * 
     * @Title refreshProjectConfig
     * @author 吕凯
     * @date 2016年11月1日 下午4:22:04
     * @param context
     *            void
     */
    public static void refreshConfig2Context() {
        List<String> keys = CONFIG.getKeys();
        if (context != null) {
            keys.stream().forEach(key -> {
                String value = CONFIG.getString(key);
                context.setAttribute(key, value);
            });
        }
    }

    public static void refreshConfig2Context(String key, Object value) {
        if (context != null) {
            context.setAttribute(key, value);
        }
    }

    /**
     * 刷新项目配置参数到上下文
     * 
     * @Title refreshProjectConfig
     * @author 吕凯
     * @date 2016年11月1日 下午4:22:04
     * @param context
     *            void
     */
    public static void refreshProject2Context() {
        List<String> keys = PROJECT.getKeys();
        if (context != null) {
            keys.stream().forEach(key -> {
                String value = PROJECT.getString(key);
                context.setAttribute(key, value);
            });
        }
    }

    /**
     * 刷新配置项参数到上下文
     * 
     * @Title refreshApplication2Context
     * @author 吕凯
     * @date 2017年12月20日 下午5:54:38
     * @param context
     *            void
     */
    public static void refreshApplication2Context() {
        List<String> keys = APPLICATION.getKeys();
        if (context != null) {
            keys.stream().forEach(key -> {
                String value = APPLICATION.getString(key);
                context.setAttribute(key, value);
            });
        }
    }

    /**
     * 注册上下文变量
     * 
     * @Title registerContext
     * @author 吕凯
     * @date 2017年12月21日 上午8:35:23
     * @param contextParam
     *            void
     */
    public static void registerContext(ServletContext contextParam) {
        if (context == null) {
            context = contextParam;
        }
        refreshConfig2Context();
        refreshProject2Context();
        refreshApplication2Context();
    }

}
