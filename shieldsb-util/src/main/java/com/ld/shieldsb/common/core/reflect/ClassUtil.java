package com.ld.shieldsb.common.core.reflect;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Optional;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import com.ld.shieldsb.common.core.util.PathUtil;
import com.ld.shieldsb.common.core.util.StringUtils;
import com.ld.shieldsb.common.core.util.date.DateUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 数据转换操作相关
 * 
 * @ClassName ClassUtil
 * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
 * @date 2016年8月25日 下午2:59:02
 *
 */
@Slf4j
public class ClassUtil {
    private static final String ROOT_PATH = PathUtil.getClassRootPath();
    private static final String JAVA_CLASS_SUFFIX = ".class";
    private static final String DOT = ".";

    /**
     * 
     * 获取某个包下面的所有类名
     * 
     * @Title getClassList
     * @param pckgname
     * @param superClass
     * @return List<Class<? extends T>>
     */
    @SuppressWarnings("unchecked")
    public static final <T> List<Class<? extends T>> getClassList(String pkgName, Class<T> superClass) {
        List<Class<? extends T>> classes = new ArrayList<>();
        File directory = new File(ROOT_PATH + pkgName.replace('.', '/'));
        if (directory.exists()) {
            File[] files = directory.listFiles();
            for (int i = 0; i < files.length; i++) {
                String fileName = files[i].getName();
                if (fileName.endsWith(JAVA_CLASS_SUFFIX)) {
                    try {
                        Class<?> cl = Class.forName(pkgName + '.' + fileName.replace(JAVA_CLASS_SUFFIX, ""));
                        if (superClass.isAssignableFrom(cl)) {
                            classes.add((Class<T>) cl);
                        }
                    } catch (ClassNotFoundException e) {
                        log.error("", e);
                    }
                } else if (files[i].isDirectory()) {
                    classes.addAll(getClassList(pkgName + DOT + fileName, superClass));
                }
            }
        }
        return classes;
    }

    /**
     * 获取某个包下面的所有父类为superClass的类名（jar包下的也会获取）
     * 
     * @Title getClassList
     * @author 吕凯
     * @date 2016年10月10日 下午5:02:43
     * @param pkgName
     *            包名
     * @param superClass
     *            父类
     * @param isRecursive
     *            是否查询下级子目录
     * @return List<Class<?>>
     */
    public static <T> List<Class<? extends T>> getClassList(String pkgName, Class<T> superClass, boolean isRecursive) {
        return getClassList(pkgName, null, superClass, isRecursive);
    }

    /**
     * 获取某个包下面的所有父类为superClass的类名（jar包下的也会获取）
     * 
     * @Title getClassList
     * @author 吕凯
     * @date 2016年10月10日 下午5:02:43
     * @param pkgName
     *            包名
     * @param pkgFilterName
     *            筛选包名
     * @param superClass
     *            父类
     * @param isRecursive
     *            是否查询下级子目录
     * @return List<Class<?>>
     */
    public static <T> List<Class<? extends T>> getClassList(String pkgName, String pkgFilterName, Class<T> superClass,
            boolean isRecursive) {
        List<Class<? extends T>> classList = new ArrayList<>();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        ClassLoader superClsLoad = superClass.getClassLoader();
        if (superClsLoad != null && !superClsLoad.equals(loader)) {
            log.warn("类加载器不相同 父类的类加载器：" + superClsLoad + " 默认类加载器：" + loader);
        }
        try {
            // 按文件的形式去查找
            String strFile = pkgName.replaceAll("\\.", "/");
            Enumeration<URL> urls = loader.getResources(strFile);
            while (urls.hasMoreElements()) {
                URL url = urls.nextElement();
                if (url != null) {
                    String protocol = url.getProtocol();
                    String pkgPath = url.getPath();
                    pkgPath = URLDecoder.decode(pkgPath, "utf-8"); // 中文等特殊字符解码处理
                    log.warn("protocol:" + protocol + " path:" + pkgPath + " 父类：" + superClass.getName());
                    if ("file".equals(protocol)) {
                        // 本地自己可见的代码
                        findClassName(classList, pkgName, pkgFilterName, pkgPath, isRecursive, superClass);
                    } else if ("jar".equals(protocol)) {
                        // 引用第三方jar的代码
                        findClassName(classList, pkgName, pkgFilterName, url, isRecursive, superClass);
                    }
                }
            }
        } catch (IOException e) {
            log.error("", e);
        }

        return classList;
    }

    /**
     * 本地文件
     * 
     * @Title findClassName
     * @author 吕凯
     * @date 2016年10月11日 上午8:43:48
     * @param clazzList
     * @param pkgName
     *            包名
     * @param pkgFilterName
     *            筛选包名
     * @param pkgPath
     *            包文件路径
     * @param isRecursive
     *            是否查询下级子目录
     * @param superClass
     *            void
     */
    public static <T> void findClassName(List<Class<? extends T>> clazzList, String pkgName, String pkgFilterName, String pkgPath,
            boolean isRecursive, Class<T> superClass) {
        if (clazzList == null) {
            return;
        }
        File[] files = filterClassFiles(pkgPath, pkgFilterName);// 过滤出.class文件及文件夹
        if (files.length > 0) {
            for (File f : files) {
                String fileName = f.getName();
                if (f.isFile()) {
                    String filePath = f.getAbsolutePath();
                    if (StringUtils.isBlank(pkgFilterName) || filePath.contains(pkgFilterName)) {
                        // .class 文件的情况
                        String clazzName = getClassName(pkgName, fileName);
                        addClassName(clazzList, clazzName, superClass);
                    }
                } else if (isRecursive) { // 文件夹的情况 且需要继续查找该文件夹/包名下的类

                    String subPkgName = pkgName + DOT + fileName;
                    String subPkgPath = pkgPath + File.separator + fileName;
                    findClassName(clazzList, subPkgName, pkgFilterName, subPkgPath, true, superClass);
                }
            }
        }
    }

    /**
     * 第三方Jar类库的引用。<br/>
     * 
     * @throws IOException
     */
    public static <T> void findClassName(List<Class<? extends T>> clazzList, String pkgName, String pkgFilterName, URL url,
            boolean isRecursive, Class<T> superClass) throws IOException {
        JarURLConnection jarURLConnection = (JarURLConnection) url.openConnection();
        JarFile jarFile = jarURLConnection.getJarFile();
        log.warn("获取jar包中" + superClass.getName() + "的子类:" + pkgName + " jarFile:" + jarFile.getName());
        Enumeration<JarEntry> jarEntries = jarFile.entries();
        while (jarEntries.hasMoreElements()) {
            JarEntry jarEntry = jarEntries.nextElement();
            String jarEntryName = jarEntry.getName(); // 类似：com/dao/log/DBLogDao.class
            String clazzName = jarEntryName.replace("/", DOT);
            int endIndex = clazzName.lastIndexOf(DOT);
            String prefix = null;
            if (endIndex > 0) {
                String prefixName = clazzName.substring(0, endIndex);
                endIndex = prefixName.lastIndexOf(DOT);
                if (endIndex > 0) {
                    prefix = prefixName.substring(0, endIndex);
                }
            }
            // 如果过滤包为空或者jar包名称中包含过滤包关键字
            if ((StringUtils.isBlank(pkgFilterName) || jarEntryName.contains(pkgFilterName)) //
                    && prefix != null && jarEntryName.endsWith(JAVA_CLASS_SUFFIX)) {
                if (prefix.equals(pkgName)) {
                    log.info("jar entryName:" + jarEntryName);
                    addClassName(clazzList, clazzName, superClass);
                } else if (isRecursive && prefix.startsWith(pkgName)) {
                    // 遍历子包名：子类
                    log.info("jar entryName:" + jarEntryName + " isRecursive:" + isRecursive);
                    addClassName(clazzList, clazzName, superClass);
                }
            }
        }
    }

    private static File[] filterClassFiles(String pkgPath, String pkgFilterName) {
        if (pkgPath == null) {
            return new File[0];
        }
        // 接收 .class 文件 或 类文件夹
        return new File(pkgPath).listFiles(file -> (file.isFile() && file.getName().endsWith(JAVA_CLASS_SUFFIX)) || file.isDirectory());
    }

    private static String getClassName(String pkgName, String fileName) {
        int endIndex = fileName.lastIndexOf(DOT);
        String clazz = null;
        if (endIndex >= 0) {
            clazz = fileName.substring(0, endIndex);
        }
        String clazzName = null;
        if (clazz != null) {
            clazzName = pkgName + DOT + clazz;
        }
        return clazzName;
    }

    @SuppressWarnings("unchecked")
    private static <T> void addClassName(List<Class<? extends T>> clazzList, String clazzName, Class<T> superClass) {
        if (clazzList != null && clazzName != null) {
            clazzName = clazzName.replace(JAVA_CLASS_SUFFIX, "");
            Class<?> clazz = getClass(clazzName);
            // 不包含父类
            if (clazz != null && superClass.isAssignableFrom(clazz) && !superClass.equals(clazz)) {
                clazzList.add((Class<T>) clazz);
            }
        }
    }

    /**
     * 
     * Object转换成Int类型
     * 
     * @Title obj2int
     * @param obj
     *            Object
     * @return int
     */
    public static int obj2int(Object obj) {
        return obj2int(obj, 0);
    }

    /**
     * 
     * Object转换成Int类型
     * 
     * @Title obj2int
     * @param obj
     *            Object
     * @return int
     */
    public static Integer obj2int(Object obj, Integer defalutValue) {
        if (obj != null) {
            try {
                return Integer.parseInt(obj.toString());
            } catch (NumberFormatException e) {
                log.warn("", e);
            }
        }
        return defalutValue;
    }

    /**
     * 
     * Object转换成Long类型
     * 
     * @Title obj2long
     * @param obj
     *            Object
     * @return long
     */
    public static long obj2long(Object obj) {
        return obj2long(obj, 0L);
    }

    public static Long obj2long(Object obj, Long defalutValue) {
        if (obj != null) {
            try {
                return Long.parseLong("".equals(obj.toString()) ? "0" : obj.toString());
            } catch (NumberFormatException e) {
                log.warn("", e);
            }
        }
        return defalutValue;
    }

    /**
     * 
     * Object转换成Double类型
     * 
     * @Title obj2doulbe
     * @param obj
     *            Object
     * @return double
     */
    public static double obj2doulbe(Object obj) {
        return obj2doulbe(obj, 0D);
    }

    public static Double obj2doulbe(Object obj, Double defalutValue) {
        if (obj != null) {
            try {
                return Double.parseDouble(obj.toString());
            } catch (NumberFormatException e) {
                log.warn("", e);
            }
        }
        return defalutValue;
    }

    /**
     * Object转换成Float类型
     * 
     * @Title obj2float
     * @author 吕凯
     * @date 2021年5月7日 上午9:53:27
     * @param obj
     * @return float
     */
    public static float obj2float(Object obj) {
        return obj2float(obj, 0F);
    }

    public static Float obj2float(Object obj, Float defalutValue) {
        if (obj != null) {
            try {
                return Float.parseFloat(obj.toString());
            } catch (NumberFormatException e) {
                log.warn("", e);
            }
        }
        return defalutValue;
    }

    /**
     * Object转换成Boolean类型
     * 
     * @Title obj2boolean
     * @author 吕凯
     * @date 2021年5月7日 上午9:53:27
     * @param obj
     * @return float
     */
    public static boolean obj2boolean(Object obj) {
        return obj2boolean(obj, false);
    }

    public static Boolean obj2boolean(Object obj, Boolean defalutValue) {
        if (obj != null) {
            String v = obj.toString().toLowerCase();
            try {
                if (v.equalsIgnoreCase("true") || v.equalsIgnoreCase("false")) {
                    return Boolean.valueOf(v);
                }
            } catch (NumberFormatException e) {
                log.warn("", e);
            }
        }
        return defalutValue;
    }

    /**
     * Object转换成BigDecimal类型
     * 
     * @Title obj2BigDecimal
     * @author 吕凯
     * @date 2019年10月23日 下午6:00:41
     * @param obj
     * @return BigDecimal
     */
    public static BigDecimal obj2BigDecimal(Object obj) {
        return obj2BigDecimal(obj, 0D);
    }

    public static BigDecimal obj2BigDecimal(Object obj, Double defalutValue) {
        Double val = defalutValue;
        if (obj != null) {
            try {
                val = Double.parseDouble(obj.toString());
            } catch (NumberFormatException e) {
                log.warn("", e);
            }
        }
        if (val == null) {
            return null;
        }
        return new BigDecimal(Double.toString(val));
    }

    @SuppressWarnings("unchecked")
    public static <T> T obj2T(Object obj, Class<T> type) {
        if (obj == null) {
            return null;
        }
        Object model = null;
        if (type.equals(Integer.class) || type.equals(int.class)) {
            model = obj2int(obj, null);
        } else if (type.equals(Long.class) || type.equals(long.class)) {
            model = obj2long(obj, null);
        } else if (type.equals(Float.class) || type.equals(float.class)) {
            model = obj2float(obj, null);
        } else if (type.equals(Double.class) || type.equals(double.class)) {
            model = obj2doulbe(obj, null);
        } else if (type.equals(Boolean.class) || type.equals(boolean.class)) {
            model = obj2boolean(obj, null);
        } else if (type.equals(Date.class)) { // 日期
            model = parseDate(obj);

        } else if (type.equals(String.class)) {
            model = obj.toString();
        } else {
            model = obj;
        }
        return (T) model;
    }

    /**
     * 转换为日期
     * 
     * @Title parseDate
     * @author 吕凯
     * @date 2021年5月7日 上午9:36:59
     * @param obj
     *            待转换的值
     * @return Object
     */
    private static Object parseDate(Object obj) {
        Object model = null;
        // 如果model中字段是Date类型，而map中对应值是String时，需要把String转换为Date
        if (obj instanceof String) {
            String strValue = obj.toString();
            model = DateUtil.string2Date(strValue);

        } else if (obj instanceof Long) { // 毫秒数
            model = new Date((Long) obj);
        } else if (obj instanceof Date) {
            model = obj;
        }
        return model;
    }

    /**
     * 
     * Date转换成int
     * 
     * @Title date2int
     * @param dateStr
     *            date字符串
     * @return int
     */
    public static int date2int(String dateStr) {
        int dateInt;
        try {
            dateInt = Integer.parseInt(dateStr.replace("-", ""));
        } catch (Exception e) {
            log.warn("日期转换出错：'" + dateStr + "'");
            dateInt = 0;
        }
        return dateInt;
    }

    /**
     * 
     * Date转换成Long
     * 
     * @Title date2long
     * @param dateStr
     *            date字符串
     * @return long
     */
    public static long date2long(String dateStr) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = sdf.parse(dateStr);
            return date.getTime();
        } catch (ParseException e) {
            log.warn("日期转换出错：'" + dateStr + "'");
            return 0;
        }
    }

    /**
     * 获取类，只获取链接不初始化
     * 
     * @Title getClass
     * @author 吕凯
     * @date 2020年4月2日 上午9:16:08
     * @param className
     * @return Class<?>
     */
    public static Class<?> getClass(String className) {
        return getClass(className, false);
    }

    /**
     * 
     * 修改 类加载器修改
     * 
     * @Title getClass
     * @author 刘金浩
     * @date 2020年4月2日 上午9:02:50
     * @param className
     * @param initialize
     *            是否初始化（static执行）
     * @return Class<?>
     */
    public static Class<?> getClass(String className, boolean initialize) {
        Class<?> classType = null;
        try {
            if (initialize) {
                classType = Class.forName(className); // JVM会执行该类的静态代码段，动态加载和创建Class对象。
            } else {
                classType = Thread.currentThread().getContextClassLoader().loadClass(className);
            }
        } catch (Exception e) {
            log.warn(className + "加载出错！", e);
        } catch (Throwable t) {
            log.debug(className + "加载出错（Throwable）！", t); // 依赖其他类时可能出现java.lang.NoClassDefFoundError
        }
        return classType;
    }

    /**
     * 根据类名获取类实例
     * 
     * @Title getInstance
     * @author 吕凯
     * @date 2019年3月4日 下午5:22:29
     * @param className
     *            类完整名称，如com.ld.TestModel
     * @return Object
     */
    public static <T> T getInstance(String className) {
        return getInstance(className, null);
    }

    /**
     * 获取父类的子类实例，如果不是该父类的子类则不返回
     * 
     * @Title getInstance
     * @author 吕凯
     * @date 2019年3月4日 下午5:24:40
     * @param className
     *            类完整名称，如com.ld.TestModel
     * @param superClass
     *            父类
     * @return Object
     */
    @SuppressWarnings("unchecked")
    public static <T> T getInstance(String className, Class<?> superClass) {

        Object obj = null;
        try {
            Class<?> classType = getClass(className, false); // 默认不初始化
            if (superClass == null || superClass.isAssignableFrom(classType)) {
                obj = getInstance(classType);
            }
        } catch (Exception e) {
            log.error("", e);
        } catch (Throwable t) {
            log.debug(className + "加载出错（Throwable）！", t); // 依赖其他类时可能出现java.lang.NoClassDefFoundError
        }
        return (T) obj;
    }

    /**
     * 根据类获取实例
     * 
     * @Title getInstance
     * @author 吕凯
     * @date 2021年5月7日 上午11:04:38
     * @param classType
     * @return
     * @throws NoSuchMethodException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     *             Object
     */
    @SuppressWarnings("unchecked")
    public static Object getInstance(Class<?> classType)
            throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        Object obj;
        if (classType.isMemberClass()) { // 成员变量，即属性，内部类
            Class<?> declaredClass = classType.getDeclaringClass();
            Constructor<Object> dc = (Constructor<Object>) classType.getDeclaredConstructor(declaredClass);
            dc.setAccessible(true);
            obj = dc.newInstance(declaredClass.newInstance());

        } else { // 普通类直接反射
            obj = classType.newInstance();
        }
        return obj;
    }

    /**
     * 获取属性中对象类型，普通对象返回其自身类型，List，Set返回泛型类型
     * 
     * @Title getFieldPojoType
     * @author 吕凯
     * @date 2019年7月25日 下午4:31:39
     * @param field
     * @return Object
     */
    public static Class<?> getFieldPojoType(Field field) {
        Class<?> fieldType = field.getType();
        if (field.getGenericType() instanceof ParameterizedType) { // 带泛型的<>
            ParameterizedType pt = (ParameterizedType) field.getGenericType();
            // 判断具体类的类型
//            if (pt.getRawType().equals(List.class) || pt.getRawType().equals(Set.class)) {
            // 判断泛型类的类型
            fieldType = (Class<?>) pt.getActualTypeArguments()[0]; // 如果是map会返回2个
//            }
        }
        return fieldType;
    }

}
