package com.ld.shieldsb.common.core.util.useragent;

import lombok.Data;

@Data
public class BrowserModel {
    private String browserType; // 浏览器类型
    private String browserVersion; // 浏览器版本

    public BrowserModel() {
    }

    public BrowserModel(String browserType, String browserVersion) {
        this.browserType = browserType;
        this.browserVersion = browserVersion;
    }

}
