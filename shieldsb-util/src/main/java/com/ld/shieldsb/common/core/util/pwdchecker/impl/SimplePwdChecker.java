package com.ld.shieldsb.common.core.util.pwdchecker.impl;

import java.util.ArrayList;
import java.util.List;

import com.ld.shieldsb.common.core.collections.ListUtils;
import com.ld.shieldsb.common.core.model.Result;
import com.ld.shieldsb.common.core.util.pwdchecker.PwdChecker;

/**
 * 简单的密码校验器
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2020年9月7日 上午11:56:48
 *
 */
public class SimplePwdChecker implements PwdChecker {
    protected static final List<String> WEAK_PWD_PATTERN = new ArrayList<>(); // 弱口令
    // 初始化弱口令
    static {
        WEAK_PWD_PATTERN.add("^(.)\\1+$"); // 相同字符
        // 纯字母
        WEAK_PWD_PATTERN.add("admin");
        WEAK_PWD_PATTERN.add("manager");
        WEAK_PWD_PATTERN.add("tomcat");
        WEAK_PWD_PATTERN.add("jboss");
        WEAK_PWD_PATTERN.add("weblogic");
        // 纯数字
        WEAK_PWD_PATTERN.add("123456");
        WEAK_PWD_PATTERN.add("12345678");
        WEAK_PWD_PATTERN.add("1314520");
        WEAK_PWD_PATTERN.add("1314521");
        WEAK_PWD_PATTERN.add("123123");
        WEAK_PWD_PATTERN.add("112233");
        WEAK_PWD_PATTERN.add("5201314");
        WEAK_PWD_PATTERN.add("7758521");
        WEAK_PWD_PATTERN.add("1123581321");
        // 数字+字母
        WEAK_PWD_PATTERN.add("admin123");
        WEAK_PWD_PATTERN.add("123456abc");
        WEAK_PWD_PATTERN.add("123qwe");
        WEAK_PWD_PATTERN.add("a123123");
        WEAK_PWD_PATTERN.add("root123");
        WEAK_PWD_PATTERN.add("woaini1314");
    }

    public static List<String> getWeakPwdPattern() {
        return WEAK_PWD_PATTERN;
    }

    @Override
    public Result checkIsSafe(String newpwd) {
        Result result = new Result();
        result.setSuccess(true);
        if (newpwd == null) {
            result.setSuccess(false);
            result.setMessage("密码不能为空！");
            return result;
        }
        if (newpwd.length() < 8) { // 不少于8位
            result.setSuccess(false);
            result.setMessage("密码长度不能少于8位！");
            return result;
        }
        if (ListUtils.isNotEmpty(WEAK_PWD_PATTERN)) {
            if (WEAK_PWD_PATTERN.stream().anyMatch(str -> newpwd.matches(str))) {
                result.setSuccess(false);
            }
        }
        return result;
    }

}
