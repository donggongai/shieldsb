package com.ld.shieldsb.common.core.encryptor;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ZipUtils {
    private ZipUtils() {
        throw new IllegalStateException("该类不可初始化");
    }

    /**
     * 
     * 使用gzip进行压缩
     */
    public static String gzip(String primStr) {
        return EncodeUtils.encodeBase642Str(gzip2Byte(primStr));
    }

    public static byte[] gzip2Byte(String primStr) {
        if (primStr == null || primStr.length() == 0) {
            return new byte[0];
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try (GZIPOutputStream gzip = new GZIPOutputStream(out)) {
            gzip.write(primStr.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            log.error("", e);
        }

        return out.toByteArray();
    }

    /**
     *
     * <p>
     * Description:使用gzip进行解压缩
     * </p>
     * 
     * @param compressedStr
     * @return
     */
    public static String ungzip(String compressedStr) {
        if (compressedStr == null) {
            return null;
        }

        String decompressed = null;
        byte[] compressed = EncodeUtils.decodeBase64(compressedStr);
        try (ByteArrayInputStream in = new ByteArrayInputStream(compressed);
                GZIPInputStream ginzip = new GZIPInputStream(in);
                ByteArrayOutputStream out = new ByteArrayOutputStream();) {

            byte[] buffer = new byte[1024];
            int offset = -1;
            while ((offset = ginzip.read(buffer)) != -1) {
                out.write(buffer, 0, offset);
            }
            decompressed = out.toString("UTF-8");
        } catch (IOException e) {
            log.error("", e);
        }

        return decompressed;
    }

    /**
     * 使用zip进行压缩
     * 
     * @param str
     *            压缩前的文本
     * @return 返回压缩后的文本
     */
    public static final String zip(String str) {
        if (str == null) {
            return null;
        }
        byte[] compressed;

        String compressedStr = null;
        try (ByteArrayOutputStream out = new ByteArrayOutputStream(); ZipOutputStream zout = new ZipOutputStream(out);) {
            zout.putNextEntry(new ZipEntry("0"));
            zout.write(str.getBytes(StandardCharsets.UTF_8));
            zout.closeEntry();
            compressed = out.toByteArray();
            compressedStr = EncodeUtils.encodeBase642Str(compressed);
        } catch (IOException e) {
            compressed = null;
        }
        return compressedStr;
    }

    /**
     * 使用zip进行解压缩
     * 
     * @param compressed
     *            压缩后的文本
     * @return 解压后的字符串
     */
    public static final String unzip(String compressedStr) {
        if (compressedStr == null) {
            return null;
        }
        String decompressed = null;
        byte[] compressed = Base64.getDecoder().decode(compressedStr);
        try (ByteArrayOutputStream out = new ByteArrayOutputStream();
                ByteArrayInputStream in = new ByteArrayInputStream(compressed);
                ZipInputStream zin = new ZipInputStream(in);) {

            zin.getNextEntry();
            byte[] buffer = new byte[1024];
            int offset = -1;
            while ((offset = zin.read(buffer)) != -1) {
                out.write(buffer, 0, offset);
            }
            decompressed = out.toString("UTF-8");
        } catch (IOException e) {
            decompressed = null;
        }
        return decompressed;
    }

}