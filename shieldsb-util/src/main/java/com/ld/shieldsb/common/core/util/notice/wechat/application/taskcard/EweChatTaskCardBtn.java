package com.ld.shieldsb.common.core.util.notice.wechat.application.taskcard;

import lombok.Data;

@Data
public class EweChatTaskCardBtn {
    private String key;
    private String name;
    private String color;
    private Boolean isBold;

}
