package com.ld.shieldsb.common.core.cache;

import com.ld.shieldsb.common.core.cache.dataloader.DataLoaderFactory;
import com.ld.shieldsb.common.core.cache.dataloader.IDataLoader;
import com.ld.shieldsb.common.core.exception.ShieldSbException;

/**
 * 缓存接口适配器
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月27日 上午9:24:03
 *
 */
public abstract class CacheAbstractAdaptar implements Cache {
    protected String defaultGroup = "defaultCache";

    protected boolean open = true; // 是否开启

    @Override
    public boolean open() {
        open = true;
        return true;
    }

    @Override
    public boolean close() {
        open = false;
        return true;
    }

    @Override
    public boolean canUse() {
        return open;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T get(String cacheName, String key, Class<? extends IDataLoader> dataLoaderClass) {
        Object value = get(cacheName, key);
        if (value == null) {
            try {
                IDataLoader dataLoader = DataLoaderFactory.getInstance().getService(dataLoaderClass); // 使用数据加载器加载
                value = dataLoader.load(key);
                put(cacheName, key, value);
            } catch (Exception e) {
                throw new ShieldSbException(e); // 捕获未知异常
            }
        }
        return value != null ? (T) value : null;
    }

    /**
     * 放置缓存到默认分组
     * 
     * @Title put
     * @author 吕凯
     * @date 2021年4月28日 下午12:08:41
     * @param key
     * @param value
     * @return boolean
     */
    @Override
    public boolean put(String key, Object value) {
        return put(defaultGroup, key, value);
    }

    /**
     * 获取默认分组的缓存
     * 
     * @Title get
     * @author 吕凯
     * @date 2021年4月28日 下午12:04:22
     * @param <T>
     * @param key
     * @return T
     */
    @Override
    public <T> T get(String key) {
        return get(defaultGroup, key);
    }

    /**
     * 获取默认分组的通过数据加载器加载的缓存
     * 
     * @Title get
     * @author 吕凯
     * @date 2021年4月28日 下午12:03:58
     * @param <T>
     * @param key
     * @param dataLoaderClass
     * @return T
     */
    @Override
    public <T> T get(String key, Class<? extends IDataLoader> dataLoaderClass) {
        return get(defaultGroup, key, dataLoaderClass);
    }

    /**
     * 获取默认分组的经过处理的key，部分缓存容器的可以可能需要处理
     * 
     * @Title getCacheKey
     * @author 吕凯
     * @date 2021年4月28日 下午12:03:39
     * @param key
     * @return String
     */
    @Override
    public String getCacheKey(String key) {
        return getCacheKey(defaultGroup, key);
    }

    /**
     * 移除默认分组中的缓存
     * 
     * @Title remove
     * @author 吕凯
     * @date 2021年4月28日 下午12:02:53
     * @param key
     * @return boolean
     */
    @Override
    public boolean remove(String key) {
        return remove(defaultGroup, key);
    }

    /**
     * 移除默认分组下的所有缓存
     * 
     * @Title removeAll
     * @author 吕凯
     * @date 2021年4月28日 下午12:13:48
     * @return
     * @see com.ld.shieldsb.common.core.cache.Cache#removeAll()
     */
    public boolean removeAll() {
        return removeAll(defaultGroup);
    }

}
