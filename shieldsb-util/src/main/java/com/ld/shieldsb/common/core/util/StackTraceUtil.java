package com.ld.shieldsb.common.core.util;

import java.util.ArrayList;
import java.util.List;

//获取调用类
public class StackTraceUtil {

    // 返回字符串
    public static String getTrace() {
        StackTraceElement stack[] = Thread.currentThread().getStackTrace();
        StringBuilder msg = new StringBuilder("called by {");
        for (StackTraceElement ste : stack) {
            String callName = ste.getClassName();
            if (callName.startsWith("com.") && !callName.equals(StackTraceUtil.class.getName())) {
//                String calssName = ste.getClassName().substring(ste.getClassName().lastIndexOf(".") + 1, ste.getClassName().length());
//                msg += " at " + ste.getClassName() + "." + ste.getMethodName() + "(" + calssName + ".java:" + ste.getLineNumber() + ")\n";
//                msg += "\n " + ste;
                msg.append("\n ").append(ste);
            }
        }
//        msg += "\n}";
        msg.append("\n}");
        return msg.toString();
    }

    // 返回集合
    public static List<StackTraceElement> getTraceEleList() {
        List<StackTraceElement> eleList = new ArrayList<StackTraceElement>();
        StackTraceElement stack[] = Thread.currentThread().getStackTrace();
        for (StackTraceElement ste : stack) {
            String callName = ste.getClassName();
            if (callName.startsWith("com.") && !callName.equals(StackTraceUtil.class.getName())) {
                eleList.add(ste);
            }
        }
        return eleList;
    }

    /**
     * 获取最后一个调用的堆栈信息
     * 
     * @Title getLastTraceEle
     * @author 吕凯
     * @date 2019年3月30日 上午10:17:19
     * @return StackTraceElement
     */
    public static StackTraceElement getLastTraceEle() {
        return getTraceEle(0);
    }

    /**
     * 
     * 获取倒数第二个调用的堆栈信息，一般用于工具类中打印真实的调用信息，否则最后一个一定为工具类
     * 
     * @Title getSecond2LastTraceEle
     * @author 吕凯
     * @date 2021年3月10日 上午8:41:14
     * @return StackTraceElement
     */
    public static StackTraceElement getSecond2LastTraceEle() {
        return getTraceEle(1);
    }

    /**
     * 获取指定下标的调用的堆栈信息
     * 
     * @Title getTraceEle
     * @author 吕凯
     * @date 2021年3月10日 上午8:39:16
     * @param index
     * @return StackTraceElement
     */
    public static StackTraceElement getTraceEle(int index) {
        List<StackTraceElement> eleList = getTraceEleList();
        if (eleList != null && eleList.size() > index) {
            return eleList.get(index);
        }
        return null;
    }

    public static void main(String[] args) {
        System.out.println(getTrace() + "==\n" + StackTraceUtil.class.getName());
    }

}
