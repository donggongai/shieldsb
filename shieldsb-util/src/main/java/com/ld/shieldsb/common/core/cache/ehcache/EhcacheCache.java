package com.ld.shieldsb.common.core.cache.ehcache;

import java.io.Serializable;
import java.net.URL;
import java.time.Duration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.Status;
import org.ehcache.config.CacheConfiguration;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.MemoryUnit;
import org.ehcache.xml.XmlConfiguration;

import com.ld.shieldsb.common.core.cache.CacheAbstractAdaptar;
import com.ld.shieldsb.common.core.cache.dataloader.IDataLoader;
import com.ld.shieldsb.common.core.util.PathUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * Ehcache缓存相关，通过ehcache.xml配置
 * 
 * @ClassName EhcacheUtil
 * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
 * @date 2016年8月26日 下午2:00:15
 *
 */
@Slf4j
public class EhcacheCache extends CacheAbstractAdaptar {
    public static final String CACHE_TYPE = "ehcache";

    private static final String CACEH_NAME_DEFAULT = "defaultCache";
    protected final Map<String, EhcacheCacheResult> cacheMap = new ConcurrentHashMap<>();

    private CacheManager cacheManager = null;
    private boolean useXml = false;
    private String filePath = null;

    public String getFilePath() {
        return filePath;
    }

    CacheConfiguration<String, Serializable> defaultConfiguration = CacheConfigurationBuilder.newCacheConfigurationBuilder(
            // 缓存数据K和V的数值类型
            // 在ehcache3.3中必须指定缓存键值类型,如果使用中类型与配置的不同,会报类转换异常
            String.class, Serializable.class,
            // 设置缓存堆容纳元素个数(JVM内存空间)超出个数后会存到offheap中
            ResourcePoolsBuilder.heap(10000L)
                    // 设置堆外储存大小(内存存储) 超出offheap的大小会淘汰规则被淘汰
                    .offheap(100L, MemoryUnit.MB)
                    // 配置磁盘持久化储存(硬盘存储)用来持久化到磁盘,这里设置为false不启用
                    .disk(500L, MemoryUnit.MB, false))
    // no expiry : 永不过期
    // time-to-live ：创建后一段时间过期
    // time-to-idle ： 访问后一段时间过期
    // 存活时间是duration秒，过期后自动清除
//                    .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(
//                            // 设置缓存过期时间
//                            Duration.ofSeconds(duration)))
            .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(
                    // 设置被访问后过期时间(同时设置和TTL和TTI之后会被覆盖,这里TTI生效,之前版本xml配置后是两个配置了都会生效)
                    Duration.ofSeconds(300))
            // 缓存淘汰策略 默认策略是LRU（最近最少使用）。你可以设置为FIFO（先进先出）或是LFU（较少使用）。
            ).build();

    public EhcacheCache() {
        init();
    }

    @Override
    public void init() {
        URL url = getClass().getResource("/ehcache.xml");
        if (url != null) {
            useXml = true;
            // 配置默认缓存属性
            cacheManager = CacheManagerBuilder.newCacheManager(new XmlConfiguration(getClass().getResource("/ehcache.xml")));// class根目录下的ehcache.xml
            cacheManager.init();
            log.warn("使用/ehcache.xml配置！");
        } else {
            filePath = PathUtil.getTmpDir();
            cacheManager = CacheManagerBuilder.newCacheManagerBuilder()
                    // 硬盘持久化地址
                    .with(CacheManagerBuilder.persistence(filePath))
                    // 设置一个默认缓存配置
                    .withCache(CACEH_NAME_DEFAULT, defaultConfiguration)
                    // 创建之后立即初始化
                    .build(true);
            log.warn("未检测到/ehcache.xml文件，使用程序配置配置！");
        }
    }

    @Override
    public EhcacheCacheResult createCache(String cacheName, long duration) {
        return createCache(cacheName, duration, 10000);
    }

    public EhcacheCacheResult createCache(String cacheName, long duration, long entries, long offheapSize, long diskSize,
            boolean persistent) {
        CacheConfiguration<String, Serializable> configuration = CacheConfigurationBuilder.newCacheConfigurationBuilder(
                // 缓存数据K和V的数值类型
                // 在ehcache3.3中必须指定缓存键值类型,如果使用中类型与配置的不同,会报类转换异常
                String.class, Serializable.class,
                // 设置缓存堆容纳元素个数(JVM内存空间)超出个数后会存到offheap中
                ResourcePoolsBuilder.heap(entries)
                        // 设置堆外储存大小(内存存储) 超出offheap的大小会淘汰规则被淘汰
                        .offheap(offheapSize, MemoryUnit.MB)
                        // 配置磁盘持久化储存(硬盘存储)用来持久化到磁盘,这里设置为false不启用
                        .disk(diskSize, MemoryUnit.MB, persistent))
//              no expiry : 永不过期
//              time-to-live ：创建后一段时间过期
//              time-to-idle ： 访问后一段时间过期
        // 存活时间是duration秒，过期后自动清除
//                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(
//                        // 设置缓存过期时间
//                        Duration.ofSeconds(duration)))
                .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(
                        // 设置被访问后过期时间(同时设置和TTL和TTI之后会被覆盖,这里TTI生效,之前版本xml配置后是两个配置了都会生效)
                        Duration.ofSeconds(duration))
                // 缓存淘汰策略 默认策略是LRU（最近最少使用）。你可以设置为FIFO（先进先出）或是LFU（较少使用）。
                ).build();
        return createCache(cacheName, configuration);
    }

    public EhcacheCacheResult createCache(String cacheName, long duration, long entries) {
        return createCache(cacheName, duration, entries, 20L, 100L, false);
    }

    public EhcacheCacheResult createCache(String cacheName, CacheConfiguration<String, Serializable> configuration) {
        EhcacheCacheResult cacheResult = new EhcacheCacheResult();
        cacheResult.setCache(cacheManager.createCache(cacheName, configuration));
        cacheMap.put(cacheName, cacheResult);
        return cacheResult;
    }

    /**
     * 获取缓存对象，不存在时创建
     * 
     * @Title getOrAddCache
     * @author 吕凯
     * @date 2019年10月15日 上午11:49:28
     * @param cacheName
     * @return Cache
     */
    public Cache<String, Serializable> getOrAddCache(String cacheName) {
        Cache<String, Serializable> cache = null;
        if (canUse()) {
            cache = cacheManager.getCache(cacheName, String.class, Serializable.class);
            if (cache == null) {
                synchronized (cacheManager) {
                    cache = cacheManager.getCache(cacheName, String.class, Serializable.class);
                    if (cache == null) {
                        if (useXml) {
                            log.warn("没有在xml中找到alias为 {} 的缓存，将使用默认缓存defaultCache", cacheName);
                            cache = getXmlSetCache(cacheName);
                        } else {
                            log.warn("没有在xml中找到alias为 {} 的缓存，将使用默认参数创建缓存", cacheName);
                            cache = createCache(cacheName, defaultConfiguration).getCache();
                        }
                        log.debug("Cache [" + cacheName + "] started.");
                    }
                }
            }
        }
        return cache;
    }

    /**
     * 获取xml配置的cache，没有会使用默认配置创建
     * 
     * @Title getXmlSetCache
     * @author 吕凯
     * @date 2021年5月6日 上午8:34:53
     * @param cacheName
     * @return Cache<String,Serializable>
     */
    private Cache<String, Serializable> getXmlSetCache(String cacheName) {
        Cache<String, Serializable> cache;
        cache = cacheManager.getCache(CACEH_NAME_DEFAULT, String.class, Serializable.class);
        if (cache == null) {
            log.warn("defaultCache未配置，将使用默认参数创建缓存 {}", cacheName);
            cache = createCache(cacheName, defaultConfiguration).getCache();
        }
        return cache;
    }

    /**
     * 获取缓存对象，不存在时返回null
     * 
     * @Title getCache
     * @author 吕凯
     * @date 2019年10月15日 上午11:49:47
     * @param cacheName
     * @return Cache
     */
    @Override
    public EhcacheCacheResult getCache(String cacheName) {
        return cacheMap.computeIfAbsent(cacheName, key -> {
            Cache<String, Serializable> cache = null;
            if (canUse()) {
                cache = cacheManager.getCache(cacheName, String.class, Serializable.class);
            }
            EhcacheCacheResult result = null;
            if (cache != null) {
                result = new EhcacheCacheResult();
                result.setCache(cache);
            }
            return result;
        });
    }

    /**
     * 移除缓存
     * 
     * @Title removeCache
     * @author 吕凯
     * @date 2021年4月27日 下午3:58:15
     * @param cacheName
     *            void
     */
    public void removeCache(String cacheName) {
        if (canUse()) {
            cacheManager.removeCache(cacheName);
        }
    }

    @Override
    public boolean put(String cacheName, String key, Object value) {
        Cache<String, Serializable> cache = getOrAddCache(cacheName);
        if (cache == null) {
            if (cacheManager == null) {
                log.error("cacheManager is null ");
            } else {
                log.error("cache is null status:" + cacheManager.getStatus());
            }
            return false;
        } else {
            cache.put(key, (Serializable) value);
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T get(String cacheName, String key) {
        Object element = getOrAddCache(cacheName).get(key);
        return element != null ? (T) element : null;
    }

    @Override
    public boolean remove(String cacheName, String key) {
        getOrAddCache(cacheName).remove(key);
        return true;
    }

    @Override
    public boolean removeAll(String cacheName) {
        getOrAddCache(cacheName).clear();
        return true;
    }

    /**
     * 通过数据加载器加载
     * 
     * @Title get
     * @author 吕凯
     * @date 2019年7月2日 上午8:16:21
     * @param cacheName
     * @param key
     * @param dataLoader
     *            数据加载器实现类
     * @return T
     */
    @SuppressWarnings("unchecked")
    public <T> T get(String cacheName, String key, IDataLoader dataLoader) {
        Object data = get(cacheName, key);
        if (data == null) {
            data = dataLoader.load(key);
            put(cacheName, key, data);
        }
        return (T) data;
    }

    @Override
    public boolean close() {
        cacheMap.clear();
        if (canUse()) {
            cacheManager.close();
        }
        return true;
    }

    /**
     * 
     * 缓存是否可用
     * 
     * @Title canUse
     * @return boolean
     */
    @Override
    public boolean canUse() {
        boolean flag = false;
        if (EhcacheParams.canUse() && cacheManager != null && (cacheManager.getStatus().equals(Status.AVAILABLE))) {
            flag = true;
        }
        return flag;
    }

    @Override
    public String getCacheKey(String cacheName, String key) {
        return key;
    }

}
