package com.ld.shieldsb.common.core.util;

import com.github.zafarkhaja.semver.Version;

/**
 * 版本号判断工具,参考https://github.com/gorkem/java-semver
 * 
 * @ClassName VersionUtil
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2018年12月10日 上午9:58:06
 *
 */
public class VersionUtil {
    public static final int V_BIG = 1;
    public static final int V_SMALL = -1;
    public static final int V_EQUAL = 0;

    /**
     * 版本号1是否比版本号2大，结果1大-1小，0相等<br>
     * 语言版本号可能包含5部分主版本号.次版本号.修订号-预发布版本号+构建号 <br>
     * 1.0.0-alpha < 1.0.0-alpha.1 < 1.0.0-alpha.beta < 1.0.0-beta < 1.0.0-beta.2 < 1.0.0-beta.11 < <br>
     * 1.0.0-rc.1 < 1.0.0 <br>
     * <br>
     * 1.0.0-alpha < 1.0.0-alpha.1 < 1.0.0-beta.2 < 1.0.0-beta.11 < 1.0.0-rc.1 < 1.0.0-rc.1+build.1 < 1.0.0 < 1.0.0+0.3.7 < 1.3.7+build <br>
     * < 1.3.7+build.2.b8f12d7 < 1.3.7+build.11.e0f985a.
     * 
     * @Title isBigVersion
     * @author 吕凯
     * @date 2018年12月10日 上午9:59:59
     * @param version1
     * @param version2
     * @return boolean
     */
    public static int compareVersion(String version1, String version2) {
        //
        if (version1 != null && version2 != null) {
            Version v1 = Version.valueOf(version1);
            Version v2 = Version.valueOf(version2);
            return compareVersion(v1, v2);
        } else if (version1 == null && version2 == null) {
            return V_EQUAL;
        } else if (version2 == null) {
            return V_BIG;
        }
        return V_SMALL;
    }

    /**
     * 版本号1是否比版本号2大，结果1大-1小，0相等<br>
     * 语言版本号可能包含5部分主版本号.次版本号.修订号-预发布版本号+构建号 <br>
     * 1.0.0-alpha < 1.0.0-alpha.1 < 1.0.0-alpha.beta < 1.0.0-beta < 1.0.0-beta.2 < 1.0.0-beta.11 < <br>
     * 1.0.0-rc.1 < 1.0.0 <br>
     * <br>
     * 1.0.0-alpha < 1.0.0-alpha.1 < 1.0.0-beta.2 < 1.0.0-beta.11 < 1.0.0-rc.1 < 1.0.0-rc.1+build.1 < 1.0.0 < 1.0.0+0.3.7 < 1.3.7+build <br>
     * < 1.3.7+build.2.b8f12d7 < 1.3.7+build.11.e0f985a.
     * 
     * @Title compareVersion
     * @author 吕凯
     * @date 2021年9月1日 上午9:06:46
     * @param version1
     * @param version2
     * @return int
     */
    public static int compareVersion(Version version1, Version version2) {
        //
        if (version1 != null && version2 != null) {
//            int compV = v1.compareTo(v2); //不比较构建号
            int compV = Version.BUILD_AWARE_ORDER.compare(version1, version2);
            if (compV > V_BIG) {
                compV = V_BIG;
            } else if (compV < V_SMALL) {
                compV = V_SMALL;
            }
            return compV;
        } else if (version1 == null && version2 == null) {
            return V_EQUAL;
        } else if (version2 == null) {
            return V_BIG;
        }
        return V_SMALL;
    }

    /**
     * 判断version1是否比version2小
     * 
     * @Title lessThan
     * @author 吕凯
     * @date 2018年12月10日 上午11:34:11
     * @param version1
     * @param version2
     * @return boolean
     */
    public static boolean lessThan(String version1, String version2) {
        return compareVersion(version1, version2) == V_SMALL;
    }

    public static boolean lessThan(Version version1, Version version2) {
        return compareVersion(version1, version2) == V_SMALL;
    }

}
