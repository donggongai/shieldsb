package com.ld.shieldsb.common.core.util.notice.wechat.application;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.ImmutableList;
import com.ld.shieldsb.common.core.model.Result;
import com.ld.shieldsb.common.core.util.notice.wechat.application.file.FileMsgSender;
import com.ld.shieldsb.common.core.util.notice.wechat.application.markdown.MarkdownMsgSender;
import com.ld.shieldsb.common.core.util.notice.wechat.application.mpnews.MpNewsMsgSender;
import com.ld.shieldsb.common.core.util.notice.wechat.application.news.NewsMsgSender;
import com.ld.shieldsb.common.core.util.notice.wechat.application.taskcard.EweChatTaskCardBtn;
import com.ld.shieldsb.common.core.util.notice.wechat.application.taskcard.TaskCardMsgSender;
import com.ld.shieldsb.common.core.util.notice.wechat.application.text.TextMsgSender;
import com.ld.shieldsb.common.core.util.notice.wechat.application.textcard.TextCardMsgSender;
import com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.TemplateCardBasicModel;
import com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.TemplateCardMsgSender;
import com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.button.TemplateCardButtonModel;
import com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.news.TemplateCardNewsModel;
import com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.text.TemplateCardTextModel;
import com.ld.shieldsb.common.core.util.notice.wechat.application.workbench.WorkbenchMsgSender;

/**
 * 企业微信应用API工具类，需要创建应用才能使用，应用支持推送文本、图片、视频、文件、图文等类型。<br/>
 * 参考文档 <br/>
 * https://work.weixin.qq.com/api/doc/90000/90135/90664 <br/>
 * https://work.weixin.qq.com/api/doc/90000/90003/90487 <br/>
 * https://work.weixin.qq.com/api/doc/90000/90135/90236 <br/>
 * https://work.weixin.qq.com/api/doc/90000/90139/90312 访问频率限制
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月15日 下午5:17:03
 *
 */
public class EweChatApplicationUtil {

    /**
     * 获取访问token
     * 
     * @Title getAccessToken
     * @author 吕凯
     * @date 2021年4月21日 上午10:12:05
     * @param corpid
     * @param corpsecret
     * @return Result
     */
    public static Result getAccessToken(String corpid, String corpsecret, String agentId) {
        return EwechatBasicMsgSender.getAccessToken(corpid, corpsecret, agentId);
    }

    /**
     * 获取配置参数
     * 
     * @Title getConfig
     * @author 吕凯
     * @date 2021年4月19日 下午5:14:07
     * @return EweChatApplicationParams
     */
    public static EweChatApplicationParams getConfig() {
        return EwechatBasicMsgSender.getConfig();
    }

    public static Result sendMsg(String corpid, String corpsecret, String agentid, Map<String, Object> data) {
        return EwechatBasicMsgSender.sendMsg(corpid, corpsecret, agentid, data);

    }

    /**
     * 撤回消息
     * 
     * @Title recallMsg
     * @author 吕凯
     * @date 2021年9月14日 下午2:04:23
     * @param corpid
     * @param corpsecret
     * @param agentid
     * @param msgid
     * @return Result
     */
    public static Result recallMsg(String corpid, String corpsecret, String agentid, String msgid) {
        return EwechatBasicMsgSender.recallMsg(corpid, corpsecret, agentid, msgid);
    }

    public static Result recallMsg(String msgid) {
        return EwechatBasicMsgSender.recallMsg(msgid);
    }

    // ============================================= 文本消息 ==============================================
    /**
     * 调用配置文件中配置好的参数发送文本信息
     * 
     * @Title sendEnterpriseWeChatMsg
     * @author 吕凯
     * @date 2021年4月19日 下午3:14:33
     * @param sendTo
     * @param msg
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Result
     */
    public static Result sendTextMsg(String sendTo, String msg, Integer duplicateCheckInterval) {
        return TextMsgSender.sendTextMsg(sendTo, msg, duplicateCheckInterval);
    }

    public static Result sendTextMsg(String sendTo, String msg) {
        return sendTextMsg(sendTo, msg, null);
    }

    /**
     * 发送企业微信文本信息，参考https://work.weixin.qq.com/api/doc/#90000/90135/90236
     * 
     * @Title sendMsg
     * @author 吕凯
     * @date 2018年12月29日 下午3:27:01
     * @param corpid
     * @param corpsecret
     * @param agentid
     * @param sendTo
     *            发送给的用户，多个用户用|分割，@all为发送给所有用户
     * @param msg
     *            内容，支持换行、以及A标签(注意：换行符请用转义过的\n)
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Result
     */
    public static Result sendTextMsg(String corpid, String corpsecret, String agentid, String sendTo, String msg,
            Integer duplicateCheckInterval) {
        return TextMsgSender.sendTextMsg(corpid, corpsecret, agentid, sendTo, msg, duplicateCheckInterval);

    }

    public static Result sendTextMsg(String corpid, String corpsecret, String agentid, String sendTo, String msg) {
        return sendTextMsg(corpid, corpsecret, agentid, sendTo, msg, null);

    }

    // ============================================= 文本卡片 ==============================================

    public static Result sendTextCardMsg(String corpid, String corpsecret, String agentid, String sendTo, String title, String msg,
            String url, String btnTxt) {
        return TextCardMsgSender.sendTextCardMsg(corpid, corpsecret, agentid, sendTo, title, msg, url, btnTxt);

    }

    public static Result sendTextCardMsg(String corpid, String corpsecret, String agentid, String sendTo, String title, String msg,
            String url, String btnTxt, Integer duplicateCheckInterval) {
        return TextCardMsgSender.sendTextCardMsg(corpid, corpsecret, agentid, sendTo, title, msg, url, btnTxt, duplicateCheckInterval);

    }

    /**
     * 发送文本卡片消息
     * 
     * @Title parse2NewsDataMap
     * @author 吕凯
     * @date 2021年4月19日 下午4:32:10
     * @param sendTo
     * @param title
     * @param msg
     * @param url
     * @param btnTxt
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Result
     */
    public static Result sendTextCardMsg(String sendTo, String title, String msg, String url, String btnTxt,
            Integer duplicateCheckInterval) {
        return TextCardMsgSender.sendTextCardMsg(sendTo, title, msg, url, duplicateCheckInterval);
    }

    public static Result sendTextCardMsg(String sendTo, String title, String msg, String url, String btnTxt) {
        return sendTextCardMsg(sendTo, title, msg, url, btnTxt, null);
    }

    public static Result sendTextCardMsg(String sendTo, String title, String msg, String url, Integer duplicateCheckInterval) {
        return sendTextCardMsg(sendTo, title, msg, url, null, duplicateCheckInterval);
    }

    public static Result sendTextCardMsg(String sendTo, String title, String msg, String url) {
        return sendTextCardMsg(sendTo, title, msg, url, null, null);
    }

    // ============================================= 图文消息 ==============================================

    public static Result sendNewsMsg(String corpid, String corpsecret, String agentid, String sendTo, String title, String description,
            String url, String picurl, Integer duplicateCheckInterval) {
        return NewsMsgSender.sendNewsMsg(corpid, corpsecret, agentid, sendTo, title, description, url, picurl, duplicateCheckInterval);
    }

    public static Result sendNewsMsg(String corpid, String corpsecret, String agentid, String sendTo, String title, String description,
            String url, String picurl) {
        return NewsMsgSender.sendNewsMsg(corpid, corpsecret, agentid, sendTo, title, description, url, picurl);

    }

    /**
     * 发送图文消息
     * 
     * @Title parse2NewsDataMap
     * @author 吕凯
     * @date 2021年4月19日 下午4:32:10
     * @param sendTo
     * @param title
     * @param msg
     * @param url
     * @param btnTxt
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Result
     */
    public static Result sendNewsMsg(String sendTo, String title, String description, String url, String picurl,
            Integer duplicateCheckInterval) {
        return NewsMsgSender.sendNewsMsg(sendTo, title, description, url, picurl, duplicateCheckInterval);
    }

    public static Result sendNewsMsg(String sendTo, String title, String description, String url, String picurl) {
        return NewsMsgSender.sendNewsMsg(sendTo, title, description, url, picurl);
    }

    // ============================================= 图文消息2 ==============================================

    public static Result sendMpNewsMsg(String corpid, String corpsecret, String agentid, String sendTo, String title, String thumbMediaId,
            String author, String url, String content, String digest, Integer duplicateCheckInterval) {
        return MpNewsMsgSender.sendMpNewsMsg(corpid, corpsecret, agentid, sendTo, title, thumbMediaId, author, url, content, digest,
                duplicateCheckInterval);
    }

    public static Result sendMpNewsMsg(String corpid, String corpsecret, String agentid, String sendTo, String title, String thumbMediaId,
            String author, String url, String content, String digest) {
        return MpNewsMsgSender.sendMpNewsMsg(corpid, corpsecret, agentid, sendTo, title, thumbMediaId, author, url, content, digest);
    }

    /**
     * 发送存储在企业微信的图文消息
     * 
     * @Title parse2NewsDataMap
     * @author 吕凯
     * @date 2021年4月19日 下午4:32:10
     * @param sendTo
     * @param title
     * @param msg
     * @param url
     * @param btnTxt
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Result
     */
    public static Result sendMpNewsMsg(String sendTo, String title, String thumbMediaId, String author, String url, String content,
            String digest, Integer duplicateCheckInterval) {
        return MpNewsMsgSender.sendMpNewsMsg(sendTo, title, thumbMediaId, author, url, content, digest, duplicateCheckInterval);
    }

    public static Result sendMpNewsMsg(String sendTo, String title, String thumbMediaId, String author, String url, String content,
            String digest) {
        return MpNewsMsgSender.sendMpNewsMsg(sendTo, title, thumbMediaId, author, url, content, digest);
    }

    /**
     * 发送存储在企业微信的图文消息
     * 
     * @Title sendMpNewsMsg
     * @author 吕凯
     * @date 2021年4月21日 上午10:48:30
     * @param corpid
     * @param corpsecret
     * @param agentid
     * @param sendTo
     *            发送给
     * @param file
     *            文件
     * @param title
     *            必填，标题，不超过128个字节，超过会自动截断（支持id转译）
     * @param author
     *            非必填，图文消息的作者，不超过64个字节
     * @param url
     *            非必填，图文消息点击“阅读原文”之后的页面链接
     * @param content
     *            必填，图文消息的内容，支持html标签，不超过666 K个字节（支持id转译）
     * @param digest
     *            非必填，图文消息的描述，不超过512个字节，超过会自动截断（支持id转译）
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Result
     */
    public static final Result sendMpNewsMsg(String corpid, String corpsecret, String agentid, String sendTo, File file, String title,
            String author, String url, String content, String digest, Integer duplicateCheckInterval) {

        return MpNewsMsgSender.sendMpNewsMsg(corpid, corpsecret, agentid, sendTo, file, title, author, url, content, digest,
                duplicateCheckInterval);
    }

    public static final Result sendMpNewsMsg(String corpid, String corpsecret, String agentid, String sendTo, File file, String title,
            String author, String url, String content, String digest) {
        return sendMpNewsMsg(corpid, corpsecret, agentid, sendTo, file, title, author, url, content, digest, null);
    }

    /**
     * 
     * 发送存储在企业微信的图文消息
     * 
     * @Title sendMpNewsMsg
     * @author 吕凯
     * @date 2021年4月21日 上午10:51:07
     * @param sendTo
     *            发送给
     * @param file
     *            文件
     * @param title
     *            必填，标题，不超过128个字节，超过会自动截断（支持id转译）
     * @param author
     *            非必填，图文消息的作者，不超过64个字节
     * @param url
     *            非必填，图文消息点击“阅读原文”之后的页面链接
     * @param content
     *            必填，图文消息的内容，支持html标签，不超过666 K个字节（支持id转译）
     * @param digest
     *            非必填，图文消息的描述，不超过512个字节，超过会自动截断（支持id转译）
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Result
     */
    public static Result sendMpNewsMsg(String sendTo, File file, String title, String author, String url, String content, String digest,
            Integer duplicateCheckInterval) {
        return MpNewsMsgSender.sendMpNewsMsg(sendTo, file, title, author, url, content, digest, duplicateCheckInterval);
    }

    public static Result sendMpNewsMsg(String sendTo, File file, String title, String author, String url, String content, String digest) {
        return sendMpNewsMsg(sendTo, file, title, author, url, content, digest, null);
    }

    // ============================================= Markdown消息 ==============================================

    public static Result sendMarkdownMsg(String corpid, String corpsecret, String agentid, String sendTo, String content) {
        return MarkdownMsgSender.sendMarkdownMsg(corpid, corpsecret, agentid, sendTo, content);
    }

    public static Result sendMarkdownMsg(String corpid, String corpsecret, String agentid, String sendTo, String content,
            Integer duplicateCheckInterval) {
        return MarkdownMsgSender.sendMarkdownMsg(corpid, corpsecret, agentid, sendTo, content, duplicateCheckInterval);
    }

    /**
     * 发送markdown消息
     * 
     * @Title sendMarkdownMsg
     * @author 吕凯
     * @date 2021年4月20日 上午8:39:08
     * @param sendTo
     * @param content
     * @return Result
     */
    public static Result sendMarkdownMsg(String sendTo, String content) {
        return sendMarkdownMsg(sendTo, content, null);
    }

    public static Result sendMarkdownMsg(String sendTo, String content, Integer duplicateCheckInterval) {
        return MarkdownMsgSender.sendMarkdownMsg(sendTo, content, duplicateCheckInterval);
    }
    // ============================================= 任务卡片消息 ==============================================

    public static Result sendTaskCardMsg(String corpid, String corpsecret, String agentid, String sendTo, String taskId, String title,
            String description, String url, List<EweChatTaskCardBtn> btns, Integer duplicateCheckInterval) {
        return TaskCardMsgSender.sendTaskCardMsg(corpid, corpsecret, agentid, sendTo, taskId, title, description, url, btns,
                duplicateCheckInterval);
    }

    public static Result sendTaskCardMsg(String corpid, String corpsecret, String agentid, String sendTo, String taskId, String title,
            String description, String url, List<EweChatTaskCardBtn> btns) {
        return sendTaskCardMsg(corpid, corpsecret, agentid, sendTo, taskId, title, description, url, btns, null);
    }

    /**
     * 
     * 发送任务卡片消息
     * 
     * @Title sendTaskCardMsg
     * @author 吕凯
     * @date 2021年4月20日 下午6:04:09
     * @param sendTo
     * @param taskId
     * @param title
     * @param description
     * @param url
     * @param btns
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Result
     */
    public static Result sendTaskCardMsg(String sendTo, String taskId, String title, String description, String url,
            List<EweChatTaskCardBtn> btns, Integer duplicateCheckInterval) {
        return TaskCardMsgSender.sendTaskCardMsg(sendTo, taskId, title, description, url, btns, duplicateCheckInterval);
    }

    public static Result sendTaskCardMsg(String sendTo, String taskId, String title, String description, String url,
            List<EweChatTaskCardBtn> btns) {
        return sendTaskCardMsg(sendTo, taskId, title, description, url, btns, null);
    }

    public static Result sendTaskCardMsgAudit(String sendTo, String taskId, String title, String description, String url,
            Integer duplicateCheckInterval) {
        return sendTaskCardMsgAudit(sendTo, taskId, title, description, url, ImmutableList.of("通过", "退回"), duplicateCheckInterval);
    }

    public static Result sendTaskCardMsgAudit(String sendTo, String taskId, String title, String description, String url) {
        return sendTaskCardMsgAudit(sendTo, taskId, title, description, url, ImmutableList.of("通过", "退回"), null);
    }

    public static Result sendTaskCardMsgAudit(String sendTo, String taskId, String title, String description, String url,
            List<String> btnNames, Integer duplicateCheckInterval) {
        return TaskCardMsgSender.sendTaskCardMsgAudit(sendTo, taskId, title, description, url, btnNames, duplicateCheckInterval);
    }

    public static Result sendTaskCardMsgAudit(String sendTo, String taskId, String title, String description, String url,
            List<String> btnNames) {
        return sendTaskCardMsgAudit(sendTo, taskId, title, description, url, btnNames, null);
    }

    public static Result sendTaskCardMsgConfirm(String sendTo, String taskId, String title, String description, String url,
            Integer duplicateCheckInterval) {
        return sendTaskCardMsgConfirm(sendTo, taskId, title, description, url, "确认", duplicateCheckInterval);
    }

    public static Result sendTaskCardMsgConfirm(String sendTo, String taskId, String title, String description, String url) {
        return sendTaskCardMsgConfirm(sendTo, taskId, title, description, url, "确认", null);
    }

    /**
     * 发送确认任务卡片
     * 
     * @Title sendTaskCardMsgConfirm
     * @author 吕凯
     * @date 2021年4月21日 下午4:31:10
     * @param sendTo
     * @param taskId
     *            必须有，任务id，同一个应用发送的任务卡片消息的任务id不能重复，只能由数字、字母和“_-@”组成，最长支持128字节
     * @param title
     *            标题
     * @param description
     *            表述
     * @param url
     * @param btnNames
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Result
     */
    public static Result sendTaskCardMsgConfirm(String sendTo, String taskId, String title, String description, String url, String btnName,
            Integer duplicateCheckInterval) {
        return TaskCardMsgSender.sendTaskCardMsgConfirm(sendTo, taskId, title, description, url, btnName, duplicateCheckInterval);
    }

    public static Result sendTaskCardMsgConfirm(String sendTo, String taskId, String title, String description, String url,
            String btnName) {
        return sendTaskCardMsgConfirm(sendTo, taskId, title, description, url, btnName, null);
    }

    /**
     * 更新任务卡片
     * 
     * @Title updateTaskcard
     * @author 吕凯
     * @date 2021年4月21日 上午9:21:19
     * @param corpid
     * @param corpsecret
     * @param agentid
     * @param sendTo
     *            发送给，多个用户用逗号分隔
     * @param taskId
     * @param replaceName
     * @return Result
     */
    public static Result updateTaskcard(String corpid, String corpsecret, String agentid, String sendTo, String taskId,
            String replaceName) {
        return TaskCardMsgSender.updateTaskcard(corpid, corpsecret, agentid, sendTo, taskId, replaceName);
    }

    /**
     * 更新任务卡片状态
     * 
     * @Title updateTaskcard
     * @author 吕凯
     * @date 2021年4月21日 上午9:24:52
     * @param sendTo
     * @param taskId
     * @param replaceName
     * @return Result
     */
    public static Result updateTaskcard(String sendTo, String taskId, String replaceName) {
        // 注意下面参数需要到企业微信中获取
        return TaskCardMsgSender.updateTaskcard(sendTo, taskId, replaceName);
    }

    // ===============================================上传文件============================================
    /**
     * 上传文件
     * 
     * @Title uploadFile
     * @author 吕凯
     * @date 2021年4月16日 下午3:06:19
     * @param key
     *            key
     * @param file
     *            文件对象
     * @return JSONObject
     */
    public static final JSONObject uploadFile(String accessToken, File file) {
        return TaskCardMsgSender.uploadFile(accessToken, file);
    }

    /**
     * 上传文件
     * 
     * @Title uploadFile
     * @author 吕凯
     * @date 2021年4月16日 下午3:06:59
     * @param file
     * @return JSONObject
     */
    public static final JSONObject uploadFile(File file) {
        return TaskCardMsgSender.uploadFile(file);
    }

    /**
     * 上传文件，通过流
     * 
     * @Title uploadFile
     * @author 吕凯
     * @date 2021年4月19日 下午2:40:14
     * @param key
     *            密钥
     * @param inputStream
     * @param fileName
     *            文件名称
     * @return JSONObject
     */
    public static final JSONObject uploadFile(String accessToken, InputStream inputStream, String fileName) {
        return TaskCardMsgSender.uploadFile(accessToken, inputStream, fileName);
    }

    public static final JSONObject uploadFile(InputStream inputStream, String fileName) {
        return TaskCardMsgSender.uploadFile(inputStream, fileName);
    }

    // ===============================================图片消息============================================

    /**
     * 发送文件信息，通过文件对象
     * 
     * @Title sendFileMsg
     * @author 吕凯
     * @date 2021年4月19日 下午2:41:01
     * @param key
     *            密钥
     * @param file
     *            文件
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return JSONObject
     */
    public static final Result sendFileMsg(String corpid, String corpsecret, String agentid, String sendTo, File file,
            Integer duplicateCheckInterval) {
        return FileMsgSender.sendFileMsg(corpid, corpsecret, agentid, sendTo, file, duplicateCheckInterval);
    }

    public static final Result sendFileMsg(String corpid, String corpsecret, String agentid, String sendTo, File file) {
        return sendFileMsg(corpid, corpsecret, agentid, sendTo, file, null);
    }

    public static Result sendFileMsg(String sendTo, File file, Integer duplicateCheckInterval) {
        return FileMsgSender.sendFileMsg(sendTo, file, duplicateCheckInterval);
    }

    public static Result sendFileMsg(String sendTo, File file) {
        return sendFileMsg(sendTo, file, null);
    }

    /**
     * 发送文件通过流
     * 
     * @Title sendFileMsg
     * @author 吕凯
     * @date 2021年4月21日 上午10:39:22
     * @param corpid
     * @param corpsecret
     * @param agentid
     * @param sendTo
     * @param inputStream
     * @param fileName
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Result
     */
    public static final Result sendFileMsg(String corpid, String corpsecret, String agentid, String sendTo, InputStream inputStream,
            String fileName, Integer duplicateCheckInterval) {
        return FileMsgSender.sendFileMsg(corpid, corpsecret, agentid, sendTo, inputStream, fileName, duplicateCheckInterval);
    }

    public static final Result sendFileMsg(String corpid, String corpsecret, String agentid, String sendTo, InputStream inputStream,
            String fileName) {
        return sendFileMsg(corpid, corpsecret, agentid, sendTo, inputStream, fileName, null);
    }

    public static Result sendFileMsg(String sendTo, InputStream inputStream, String fileName, Integer duplicateCheckInterval) {
        return FileMsgSender.sendFileMsg(sendTo, inputStream, fileName, duplicateCheckInterval);
    }

    public static Result sendFileMsg(String sendTo, InputStream inputStream, String fileName) {
        return sendFileMsg(sendTo, inputStream, fileName, null);
    }

    // ============================================= 设置工作台模板 ===============================================
    // 参考https://work.weixin.qq.com/api/doc/90000/90135/92535
    public static Result setWorkbenchTemplate(String corpid, String corpsecret, String agentid, Map<String, Object> data) {
        return WorkbenchMsgSender.setWorkbenchTemplate(corpid, corpsecret, agentid, data);
    }

    public static Result setWorkbenchTemplate(Map<String, Object> data) {
        return WorkbenchMsgSender.setWorkbenchTemplate(data);
    }

    // ============================================= 模板卡片-文本消息 ==============================================

    public static Result sendTemplateCardMsg(String corpid, String corpsecret, String agentid, String sendTo, TemplateCardBasicModel model,
            Integer duplicateCheckInterval) {
        return TemplateCardMsgSender.sendTemplateCardMsg(corpid, corpsecret, agentid, sendTo, model, duplicateCheckInterval);
    }

    public static Result sendTemplateCardTextMsg(String corpid, String corpsecret, String agentid, String sendTo,
            TemplateCardTextModel model, Integer duplicateCheckInterval) {
        return sendTemplateCardMsg(corpid, corpsecret, agentid, sendTo, model, duplicateCheckInterval);
    }

    public static Result sendTemplateCardTextMsg(String sendTo, TemplateCardTextModel model) {
        return sendTemplateCardTextMsg(sendTo, model, null);
    }

    public static Result sendTemplateCardTextMsg(String sendTo, TemplateCardTextModel model, Integer duplicateCheckInterval) {
        return TemplateCardMsgSender.sendTemplateCardTextMsg(sendTo, model, duplicateCheckInterval);
    }

    /**
     * 发送文本类型的通知模板
     * 
     * @Title sendTemplateCardTextMsg
     * @author 吕凯
     * @date 2021年9月13日 上午9:43:56
     * @param corpid
     * @param corpsecret
     * @param agentid
     * @param sendTo
     * @param title
     *            一级标题，建议不超过36个字
     * @param description
     *            二级普通文本，建议不超过160个字
     * @param url，点击后跳转链接
     * @param duplicateCheckInterval
     * @return Result
     */
    public static Result sendTemplateCardTextMsg(String corpid, String corpsecret, String agentid, String sendTo, String title,
            String description, String url, Integer duplicateCheckInterval) {
        return TemplateCardMsgSender.sendTemplateCardTextMsg(corpid, corpsecret, agentid, sendTo, title, description, url,
                duplicateCheckInterval);
    }

    public static Result sendTemplateCardTextMsg(String corpid, String corpsecret, String agentid, String sendTo, String title,
            String description, String url) {
        return sendTemplateCardTextMsg(corpid, corpsecret, agentid, sendTo, title, description, url, null);
    }

    /**
     * 
     * 发送模板任务卡片消息
     * 
     * @Title sendTaskCardMsg
     * @author 吕凯
     * @date 2021年4月20日 下午6:04:09
     * @param sendTo
     * @param taskId
     * @param title
     * @param description
     * @param url
     * @param btns
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Result
     */
    public static Result sendTemplateCardTextMsg(String sendTo, String title, String description, String url,
            Integer duplicateCheckInterval) {
        return TemplateCardMsgSender.sendTemplateCardTextMsg(sendTo, title, description, url, duplicateCheckInterval);
    }

    public static Result sendTemplateCardTextMsg(String sendTo, String title, String description, String url) {
        return sendTemplateCardTextMsg(sendTo, title, description, url, null);
    }

    // ========================================== 模板卡片：：图文展示型 ====================================
    /**
     * 发送图文展示型的通知
     * 
     * @Title sendTemplateCardNewsMsg
     * @author 吕凯
     * @date 2021年9月13日 上午9:48:33
     * @param corpid
     * @param corpsecret
     * @param agentid
     * @param sendTo
     * @param model
     * @param duplicateCheckInterval
     * @return Result
     */

    public static Result sendTemplateCardNewsMsg(String corpid, String corpsecret, String agentid, String sendTo,
            TemplateCardNewsModel model, Integer duplicateCheckInterval) {
        return TemplateCardMsgSender.sendTemplateCardNewsMsg(corpid, corpsecret, agentid, sendTo, model, duplicateCheckInterval);
    }

    public static Result sendTemplateCardNewsMsg(String sendTo, TemplateCardNewsModel model) {
        return sendTemplateCardNewsMsg(sendTo, model, null);
    }

    public static Result sendTemplateCardNewsMsg(String sendTo, TemplateCardNewsModel model, Integer duplicateCheckInterval) {
        return TemplateCardMsgSender.sendTemplateCardNewsMsg(sendTo, model, duplicateCheckInterval);
    }

    /**
     * 发送图文展示型通知模板
     * 
     * @Title sendTemplateCardNewsMsg
     * @author 吕凯
     * @date 2021年9月13日 下午3:03:45
     * @param corpid
     * @param corpsecret
     * @param agentid
     * @param sendTo
     * @param title
     * @param imagePath
     * @param url
     * @param duplicateCheckInterval
     * @return Result
     */
    public static Result sendTemplateCardNewsMsg(String corpid, String corpsecret, String agentid, String sendTo, String title,
            String imagePath, String url, Integer duplicateCheckInterval) {
        return TemplateCardMsgSender.sendTemplateCardNewsMsg(corpid, corpsecret, agentid, sendTo, title, imagePath, url,
                duplicateCheckInterval);
    }

    public static Result sendTemplateCardNewsMsg(String corpid, String corpsecret, String agentid, String sendTo, String title,
            String description, String url) {
        return sendTemplateCardNewsMsg(corpid, corpsecret, agentid, sendTo, title, description, url, null);
    }

    /**
     * 
     * 发送模板任务卡片消息
     * 
     * @Title sendTemplateCardNewsMsg
     * @author 吕凯
     * @date 2021年4月20日 下午6:04:09
     * @param sendTo
     * @param taskId
     * @param title
     * @param imagePath
     *            图片路径
     * @param url
     * @param btns
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Result
     */
    public static Result sendTemplateCardNewsMsg(String sendTo, String title, String imagePath, String url,
            Integer duplicateCheckInterval) {
        return TemplateCardMsgSender.sendTemplateCardNewsMsg(sendTo, title, imagePath, url, duplicateCheckInterval);
    }

    public static Result sendTemplateCardNewsMsg(String sendTo, String title, String imagePath, String url) {
        return sendTemplateCardNewsMsg(sendTo, title, imagePath, url, null);
    }

    // ========================================== 模板卡片：：按钮交互型 ====================================
    /**
     * 发送按钮交互型的通知
     * 
     * @Title sendTemplateCardTextMsg
     * @author 吕凯
     * @date 2021年9月13日 上午9:48:33
     * @param corpid
     * @param corpsecret
     * @param agentid
     * @param sendTo
     * @param model
     * @param duplicateCheckInterval
     * @return Result
     */

    public static Result sendTemplateCardButtonMsg(String corpid, String corpsecret, String agentid, String sendTo,
            TemplateCardButtonModel model, Integer duplicateCheckInterval) {
        return TemplateCardMsgSender.sendTemplateCardButtonMsg(corpid, corpsecret, agentid, sendTo, model, duplicateCheckInterval);
    }

    public static Result sendTemplateCardButtonMsg(String sendTo, TemplateCardButtonModel model) {
        return sendTemplateCardButtonMsg(sendTo, model, null);
    }

    public static Result sendTemplateCardButtonMsg(String sendTo, TemplateCardButtonModel model, Integer duplicateCheckInterval) {
        return TemplateCardMsgSender.sendTemplateCardButtonMsg(sendTo, model, duplicateCheckInterval);
    }

    /**
     * 发送图文展示型通知模板
     * 
     * @Title sendTemplateCardNewsMsg
     * @author 吕凯
     * @date 2021年9月13日 下午3:03:45
     * @param corpid
     * @param corpsecret
     * @param agentid
     * @param sendTo
     * @param title
     * @param imagePath
     * @param url
     * @param duplicateCheckInterval
     * @return Result
     */
    public static Result sendTemplateCardButtonMsg(String corpid, String corpsecret, String agentid, String sendTo, String taskId,
            String title, String content, String url, String btnName, Integer duplicateCheckInterval) {
        return TemplateCardMsgSender.sendTemplateCardButtonMsg(sendTo, taskId, title, content, url, btnName, duplicateCheckInterval);
    }

    public static Result sendTemplateCardButtonMsg(String corpid, String corpsecret, String agentid, String sendTo, String taskId,
            String title, String content, String url, String btnName) {
        return sendTemplateCardButtonMsg(corpid, corpsecret, agentid, sendTo, taskId, title, content, url, btnName, null);
    }

    /**
     * 
     * 发送模板卡片-按钮交互消息
     * 
     * @Title sendTemplateCardButtonMsg
     * @author 吕凯
     * @date 2021年4月20日 下午6:04:09
     * @param sendTo
     * @param taskId
     * @param title
     * @param imagePath
     *            图片路径
     * @param url
     * @param btns
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Result
     */
    public static Result sendTemplateCardButtonMsg(String sendTo, String taskId, String title, String content, String url, String btnName,
            Integer duplicateCheckInterval) {
        return TemplateCardMsgSender.sendTemplateCardButtonMsg(sendTo, taskId, title, content, url, btnName, duplicateCheckInterval);
    }

    public static Result sendTemplateCardButtonMsg(String sendTo, String taskId, String title, String content, String url, String btnName) {
        return sendTemplateCardButtonMsg(sendTo, taskId, title, content, url, btnName, null);
    }
    // ================================== 更新按钮文字 ===================================

    /**
     * 
     * 更新模板卡片（仅原卡片为 按钮交互型、投票选择型、多项选择型的卡片可以调用本接口更新）,可回调的卡片可以将按钮更新为不可点击状态，并且自定义文案
     * 
     * @Title updateTemplateCardButton
     * @author 吕凯
     * @date 2021年9月14日 上午9:02:04
     * @param corpid
     * @param corpsecret
     * @param agentid
     * @param sendTo
     *            发送给，多个用户用逗号分隔,全部用@all
     * @param responseCode
     *            响应码
     * @param replaceName
     *            替换按钮名称
     * @return Result
     */
    public static Result updateTemplateCardButton(String corpid, String corpsecret, String agentid, String sendTo, String responseCode,
            String replaceName) {
        return TemplateCardMsgSender.updateTemplateCardButton(corpid, corpsecret, agentid, sendTo, responseCode, replaceName);
    }

    /**
     * 更新任务卡片状态
     * 
     * @Title updateTaskcard
     * @author 吕凯
     * @date 2021年4月21日 上午9:24:52
     * @param sendTo
     * @param taskId
     * @param replaceName
     * @return Result
     */
    public static Result updateTemplateCardButton(String sendTo, String taskId, String replaceName) {
        return TemplateCardMsgSender.updateTemplateCardButton(sendTo, taskId, replaceName);
    }
    // ========================== 替换模板卡片 =========================

}
