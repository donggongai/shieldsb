package com.ld.shieldsb.common.core.cache.google;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.ld.shieldsb.common.core.cache.CacheAbstractAdaptar;
import com.ld.shieldsb.common.core.cache.CacheConfigUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * google Guava 缓存
 * 
 * @ClassName GoogleCache
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2019年7月5日 下午3:47:07
 *
 */
@Slf4j
public class GoogleCache extends CacheAbstractAdaptar {
    public static final String CACHE_TYPE = "google_guava";
    protected static final Map<String, GoogleCacheResult<Object, Object>> cacheMap = new ConcurrentHashMap<>();

    public GoogleCache() {
        init();
    }

    @Override
    public void init() {
        // 准备做点啥
    }

    @Override
    public GoogleCacheResult<Object, Object> getCache(String cacheName) { // 有线程安全问题，但是不影响最终效果，后期优化
        return cacheMap.get(cacheName);
    }

    @Override
    public GoogleCacheResult<Object, Object> createCache(String cacheName, long duration) { // 有线程安全问题，但是不影响最终效果，后期优化
        return createCache(cacheName, duration, GoogleCacheParams.getExpireType());
    }

    public GoogleCacheResult<Object, Object> createCache(String cacheName, long duration, String expireType) { // 有线程安全问题，但是不影响最终效果，后期优化
        GoogleCacheResult<Object, Object> cacheResult = cacheMap.get(cacheName);
        if (cacheResult == null) {
            synchronized (cacheMap) {
                cacheResult = cacheMap.get(cacheName);
                if (cacheResult == null) {
                    cacheResult = new GoogleCacheResult<>();
                    CacheBuilder<Object, Object> builder = CacheBuilder.newBuilder();
                    if (GoogleCacheParams.getMaxImumSize() != null) {
                        builder.maximumSize(GoogleCacheParams.getMaxImumSize());
                    }
                    if (GoogleCacheParams.getExpireTypeExpireafteraccess().equals(expireType)) {
                        builder.expireAfterAccess(duration, TimeUnit.SECONDS);
                    } else if (GoogleCacheParams.getExpireTypeExpireafterwrite().equals(expireType)) {
                        builder.expireAfterWrite(duration, TimeUnit.SECONDS);
                    } else if (GoogleCacheParams.getExpireTypeRefreshafterwrite().equals(expireType)) {
                        builder.refreshAfterWrite(duration, TimeUnit.SECONDS);
                    }
                    Cache<Object, Object> cache = builder.build();// 缓存有效期为duration秒
                    cache.cleanUp();
                    cacheResult.setCache(cache);
                    cacheMap.put(cacheName, cacheResult);
                }
            }
        }
        return cacheResult;
    }

    @Override
    public boolean put(String cacheName, String key, Object value) {
        GoogleCacheResult<Object, Object> cacheResult = getCache(cacheName);
        if (cacheResult == null || cacheResult.getCache() == null) {
            log.error(CacheConfigUtil.CACHE_NOT_EXISTS + cacheName);
            return false;
        } else {
            cacheResult.getCache().put(key, value);
        }
        return true;
    }

    public boolean put(Cache<Object, Object> cache, String key, Object value) {
        if (cache == null) {
            log.error(CacheConfigUtil.CACHE_NOT_EXISTS);
            return false;
        } else {
            cache.put(key, value);
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T get(String cacheName, String key) {
        Object value = Optional.ofNullable(getCache(cacheName)) //
                .map(GoogleCacheResult::getCache) //
                .map(cache -> cache.getIfPresent(key)) //
                .orElse(null); // 默认值
        return value != null ? (T) value : null;
    }

    @Override
    public String getCacheKey(String cacheName, String key) {
        return key;
    }

    @Override
    public boolean remove(String cacheName, String key) {
        Cache<Object, Object> cache = getImplCache(cacheName);
        if (cache == null) {
            log.error(CacheConfigUtil.CACHE_NOT_EXISTS + cacheName);
            return false;
        } else {
            cache.invalidate(key);
        }
        return true;
    }

    @Override
    public boolean removeAll(String cacheName) {
        Cache<Object, Object> cache = getImplCache(cacheName);
        if (cache == null) {
            log.error(CacheConfigUtil.CACHE_NOT_EXISTS + cacheName);
            return false;
        } else {
            cache.invalidateAll();
        }
        return true;
    }

    /**
     * 获取实现层的cache对象
     * 
     * @Title getImplCache
     * @author 吕凯
     * @date 2021年4月28日 上午10:10:42
     * @param cacheName
     * @return Cache<Object,Object>
     */
    private Cache<Object, Object> getImplCache(String cacheName) {
        return Optional.ofNullable(getCache(cacheName)).map(GoogleCacheResult::getCache).orElse(null); // 默认值;
    }

    @Override
    public boolean close() {
        cacheMap.clear();
        return true;
    }

    /**
     * 
     * 缓存是否可用
     * 
     * @Title canUse
     * @return boolean
     */
    @Override
    public boolean canUse() {
        return GoogleCacheParams.canUse();
    }

}
