/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.ld.shieldsb.common.core.util;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.commons.lang3.RandomStringUtils;

import com.ld.shieldsb.common.core.encryptor.EncodeUtils;
import com.ld.shieldsb.common.core.reflect.ClassUtil;

/**
 * 字符串工具类, 继承org.apache.commons.lang3.StringUtils类
 * 
 * @author ThinkGem
 * @version 2018-1-6
 */
public class StringUtils extends org.apache.commons.lang3.StringUtils {

    private static final char SEPARATOR = '_';
    private static final String CHARSET_NAME = "UTF-8";
    private static final Pattern PATTERN_LINE = Pattern.compile("_(\\w)");
    private static final Pattern HTML_TAG = Pattern.compile("<([a-zA-Z]+)[^<>]*>"); // html标签

    /**
     * 字符串转换为字节数组
     * 
     * @param str
     * @return
     */
    public static byte[] getBytes(String str) {
        if (str != null) {
            try {
                return str.getBytes(CHARSET_NAME);
            } catch (UnsupportedEncodingException e) {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * 字节数组转换为字符串
     * 
     * @param str
     * @return
     */
    public static String toString(byte[] bytes) {
        try {
            return new String(bytes, CHARSET_NAME);
        } catch (UnsupportedEncodingException e) {
            return EMPTY;
        }
    }

    /**
     * 数组中是否包含字符串
     * 
     * @param str
     *            验证字符串
     * @param strs
     *            字符串组
     * @return 包含返回true
     */
    public static boolean inStrArr(String str, String... strs) {
        boolean contains = false;
        if (str != null && strs != null) {
            contains = Stream.of(strs).anyMatch(s -> str.equals(trim(s)));
        }
        return contains;
    }

    /**
     * 数组中是否包含字符串（忽略大小写）
     * 
     * @param str
     *            验证字符串
     * @param strs
     *            字符串组
     * @return 包含返回true
     */
    public static boolean inStrArrIgnoreCase(String str, String... strs) {
        boolean contains = false;
        if (str != null && strs != null) {
            contains = Stream.of(strs).anyMatch(s -> str.equalsIgnoreCase(trim(s)));
        }
        return contains;
    }

    public static void main(String[] args) {
//        System.out.println(inStrArr("a", "test", "ab"));
        String ss = "2<3,特殊是3>21<a style='aaa' css='' name=''>sss</a>&nbsp;&nbsp;&nbsp;&nbsp;<sCript>script</script>123\n\r\n110&112" + "";
        System.out.println("1==" + ss);
        System.out.println("2==" + stripHtml(ss));
        System.out.println("2-0==" + JsoupUtil.clean(ss));
        System.out.println("2-1==" + JsoupUtil.cleanAllHtml(ss));
        System.out.println("2-2==" + StringUtils.escapeHtml(JsoupUtil.cleanAllHtml(ss)));
        System.out.println("3==" + toCamelCase("hello_word"));
//        System.out.println(toSimpleHtml(ss));
//        System.out.println(getRandomStr(5));
    }

    /**
     * 移除字符串中的html标签（html特殊字符转换为正常显示的代码，如 & gt 显示为>）
     * 
     * @Title stripHtml
     * @author 吕凯
     * @date 2014-6-20 上午11:16:54
     * @param htmlStr
     * @return String
     */
    public static String stripHtml(String htmlStr) {
        // String regEx = "<.+?>"; //范围太大
        if (htmlStr == null) {
            return null;
        }
        htmlStr = JsoupUtil.stripHtml(htmlStr);
        htmlStr = JsoupUtil.stripHtml(htmlStr); // 可能存在转义字符，二次移除
        return htmlStr.trim(); // 返回文本字符串
    }

    /**
     * 替换为简单HTML，去掉样式及属性，保留回车。
     * 
     * @param html
     * @return
     */
    public static String toSimpleHtml(String html) {
        if (html == null) {
            return "";
        }
        return html.replaceAll("<([a-z]+?)\\s+?.*?>", "<$1>");
    }

    /**
     * 对txt进行HTML编码，并将\n转换为&gt;br/&lt;、\t转换为&nbsp; &nbsp;
     * 
     * @param txt
     * @return
     */
    public static String toHtml(String txt) {
        if (txt == null) {
            return "";
        }
        return replace(replace(EncodeUtils.encodeHtml(trim(txt)), "\n", "<br/>"), "\t", "&nbsp; &nbsp; ");
    }

    /**
     * 判断字符串中是否包含html标签
     * 
     * @title hasHtmlTag
     * @author 张和祥
     * @date 2016年3月2日
     * @param htmlStr
     * @return
     */
    public static boolean hasHtmlTag(String htmlStr) {
        if (htmlStr != null && htmlStr.length() > 0) {
            String regEx_html = "</?[A-z]+.*?>"; // 定义HTML标签的正则表达式

            Pattern p_html = Pattern.compile(regEx_html, Pattern.CASE_INSENSITIVE);
            Matcher m_html = p_html.matcher(htmlStr);
            if (m_html.find()) {
                return true;
            }
        }
        return false;
    }

    /**
     * 首字母大写
     */
    public static String cap(String str) {
        return capitalize(str);
    }

    /**
     * 首字母小写
     */
    public static String uncap(String str) {
        return uncapitalize(str);
    }

    /**
     * 驼峰命名法工具
     * 
     * @return toCamelCase("hello_world") == "helloWorld" toCapCamelCase("hello_world") == "HelloWorld" toUnderlineStr("helloWorld") =
     *         "hello_world"
     */
    public static String toCamelCase(String str) {
        if (str == null) {
            str = "";
        }
        str = str.replaceAll("__", "_");
//        str = str.toLowerCase();
        Matcher matcher = PATTERN_LINE.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    /**
     * 驼峰命名法工具
     * 
     * @return toCamelCase("hello_world") == "helloWorld" toCapCamelCase("hello_world") == "HelloWorld" toUnderlineStr("helloWorld") =
     *         "hello_world"
     */
    public static String toCapCamelCase(String s) {
        if (s == null) {
            return null;
        }
        s = toCamelCase(s);
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    /**
     * 驼峰命名法工具
     * 
     * @return toCamelCase("hello_world") == "helloWorld" toCapCamelCase("hello_world") == "HelloWorld" toUnderlineStr("helloWorld") =
     *         "hello_world"
     */
    public static String toUnderlineStr(String s) {
        if (s == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        boolean upperCase = false;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);

            boolean nextUpperCase = true;

            if (i < (s.length() - 1)) {
                nextUpperCase = Character.isUpperCase(s.charAt(i + 1));
            }

            if ((i > 0) && Character.isUpperCase(c)) {
                if (!upperCase || !nextUpperCase) {
                    sb.append(SEPARATOR);
                }
                upperCase = true;
            } else {
                upperCase = false;
            }

            sb.append(Character.toLowerCase(c));
        }

        return sb.toString();
    }

    /**
     * 生成指定长度的字母和数字的随机组合字符串
     * 
     * @param count
     * @return
     */
    public static String getRandomStr(int count) {
        return RandomStringUtils.randomAlphanumeric(count);
    }

    /**
     * 生成随机数字字符串
     * 
     * @param count
     * @return
     */
    public static String getRandomNum(int count) {
        return RandomStringUtils.randomNumeric(count);
    }

    /**
     * 
     * HTML字符转义，保存数据库用
     * 
     * @Title escapeHtml
     * @param str
     * @return String
     */
    public static String escapeHtml(String str) {
        if (str == null) {
            return null;
        } else {
            return str.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;").replace("'", "&apos;").replace("\"", "&quot;");
        }
    }

    /**
     * 
     * 常用字符串分隔方法
     * 
     * @Title getStrArr
     * @param source
     * @return String[]
     */
    public static String[] getStrArr(String source) {
        if (source == null) {
            return null;
        }
        return source.split("，| | |,|、|;|；");
    }

    /**
     * 按字节长度截取字符串(支持截取带HTML代 码样式的字符串)
     * 
     * @author 吕凯
     * 
     * @version 1.0
     * 
     * @param param
     *            将要截取的字符串参数
     * @param length
     *            截取的字节长度
     * @param end
     *            字符串末尾补上的字符串
     * @return 返回截取后的字符串
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public static String subStringHTML(String param, int length, String end) {
        StringBuffer result = new StringBuffer();
        int n = 0;
        char temp;
        boolean isCode = false; // 是不是HTML代码
        boolean isHTML = false; // 是不是HTML特殊字符,如&nbsp;
        for (int i = 0; i < param.length(); i++) {
            temp = param.charAt(i);
            if (temp == '<') {
                isCode = true;
            } else if (temp == '&') {
                isHTML = true;
            } else if (temp == '>' && isCode) {
                n = n - 1;
                isCode = false;
            } else if (temp == ';' && isHTML) {
                isHTML = false;
            }

            if (!isCode && !isHTML) {
                n = n + 1;
                // UNICODE码字符占两个字节
                if ((temp + "").getBytes().length > 1) {
                    n = n + 1;
                }
            }

            result.append(temp);
            if (n >= length) {
                result.append(end);
                break;
            }
        }
        // 取出截取字符串中的HTML标记
        String temp_result = result.toString().replaceAll("(>)[^<>]*(<?)", "$1$2");
        // 去掉不需要结束标记的HTML标记
        temp_result = temp_result.replaceAll(
                "</?(AREA|BASE|BASEFONT|BODY|BR|COL|COLGROUP|DD|DT|FRAME|HEAD|HR|HTML|IMG|INPUT|ISINDEX|LI|LINK|META|OPTION|P|PARAM|TBODY|TD|TFOOT|TH|THEAD|TR|area|base|basefont|body|br|col|colgroup|dd|dt|frame|head|hr|html|img|input|isindex|li|link|meta|option|p|param|tbody|td|tfoot|th|thead|tr)[^<>]*/?>",
                "");
        // 去掉成对的HTML标记
        temp_result = temp_result.replaceAll("<([a-zA-Z]+)[^<>]*>(.*?)</\\1>", "$2");
        // 用正则表达式取出标记

        Matcher m = HTML_TAG.matcher(temp_result);

        List endHTML = new ArrayList();

        while (m.find()) {
            endHTML.add(m.group(1));
        }
        // 补全不成对的HTML标记
        for (int i = endHTML.size() - 1; i >= 0; i--) {
            result.append("</");
            result.append(endHTML.get(i));
            result.append(">");
        }

        return result.toString();
    }

    /**
     * 按html标签截字，html标签不处理原样返回
     * 
     * @Title subStringHTML
     * @author 吕凯
     * @date 2014-6-20 上午10:56:50
     * @param param
     *            需要截取的字符串
     * @param length
     *            汉字长度，英文按半个字算
     * @return String
     */
    public static String subStringHTML(String param, int chlength) {
        return subStringHTML(param, chlength * 2, "...");
    }

    /**
     * 替换xml特殊字符， 过滤非法字符 HJX
     * 
     * @Title formatXML
     * @author 吕凯
     * @date 2017年5月5日 上午9:08:39
     * @param s
     *            过滤的字符串
     * @return String
     */
    public static String formatXML(String s) {
        // 第一类字符: 不允许出现在xml中的字符，这些字符不在xml的定义范围之内
        //
        // 对于第一类字符，我们可以通过W3C的XML文档来查看都有哪些字符不被允许出现在xml文档中。
        // XML允许的字符范围是“#x9 | #xA | #xD | [#x20-#xD7FF] | [#xE000-#xFFFD] | [#x10000-#x10FFFF]”。因此我们可以把这个范围之外的字符过滤掉。
        // 需要过滤的字符的范围为：
        // 0x00-0x08
        // 0x0b-0x0c
        // 0x0e-0x1f
        String reg = "[\\x00-\\x08\\x0b-\\x0c\\x0e-\\x1f]";// 过滤掉非法字符
        if (s == null)
            return "";
        else {
            // 第二类字符: xml自身要使用的字符，如果内容中有这些字符则需被替换成别的字符
            // 对于第二类字符一共有5个，如下：
            // 字符 HTML字符 字符编码
            // 和(and)& &amp; &#38;
            // 单引号 ’ &apos; &#39;
            // 双引号 ” &quot; &#34;
            // 大于号 > &gt; &#62;
            // 小于号 < &lt; &#60;
            s = s.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\"", "&quot;")
                    .replaceAll("'", "&apos;").replaceAll(reg, "");
            return s;
        }

    }

    /**
     * 移除字符串中的html标签
     * 
     * @Title stripScriptTag
     * @author 吕凯
     * @date 2016-12-28 上午11:16:54
     * @param htmlStr
     * @return String
     */
    public static String stripScriptTag(String htmlStr) {
        if (htmlStr == null) {
            htmlStr = "";
        }
        if (htmlStr.length() > 0) {
            String regEx_script = "<script[^>]*?>[\\s\\S]*?<\\/script>"; // 定义script的正则表达式
            Pattern p_script = Pattern.compile(regEx_script, Pattern.CASE_INSENSITIVE);
            Matcher m_script = p_script.matcher(htmlStr);
            htmlStr = m_script.replaceAll(""); // 过滤script标签

        }

        return htmlStr.trim(); // 返回文本字符串
    }

    /**
     * 
     * 移除字符串中的iframe标签
     * 
     * @Title stripIframeTag
     * @author 吕凯
     * @date 2016年12月28日 下午2:04:54
     * @param htmlStr
     * @return String
     */
    public static String stripIframeTag(String htmlStr) {
        if (htmlStr == null) {
            htmlStr = "";
        }
        if (htmlStr.length() > 0) {
            String regEx_script = "<iframe[^>]*?>[\\s\\S]*?<\\/iframe>"; // 定义iframe的正则表达式
            Pattern p_script = Pattern.compile(regEx_script, Pattern.CASE_INSENSITIVE);
            Matcher m_script = p_script.matcher(htmlStr);
            htmlStr = m_script.replaceAll(""); // 过滤标签
        }

        return htmlStr.trim(); // 返回文本字符串
    }

    /**
     * 移除xss注入标签
     * 
     * @Title stripXSSTag
     * @author 吕凯
     * @date 2016年12月28日 下午2:07:48
     * @param htmlStr
     * @return String
     */
    public static String stripXSSTag(String htmlStr) {
        htmlStr = stripScriptTag(htmlStr);
        htmlStr = stripIframeTag(htmlStr);
        return htmlStr.trim(); // 返回文本字符串
    }

    /**
     * 移除所有空格
     * 
     * @Title removeBlank
     * @author 吕凯
     * @date 2016年2月23日 上午11:25:17
     * @param htmlStr
     * @return String
     */
    public static String stripBlank(String htmlStr) {
        if (htmlStr == null) {
            htmlStr = "";
        }
        if (htmlStr.length() > 0) {
            htmlStr = htmlStr.replaceAll("[\\s\\u00A0]+", "");
        }
        return htmlStr.trim(); // 返回文本字符串
    }

    /**
     * 去除科学计数法
     * 
     * @Title number2String
     * @author 鞠晓彬
     * @date 2017年11月9日 下午4:47:35
     * @param param
     * @return String
     */
    public static String number2String(Object param) {
        BigDecimal bd = null;
        if (param instanceof Number) {
            bd = new BigDecimal(param + "");
        }
        if (bd != null) {
            return bd.toPlainString();
        }
        return "";
    }

    /**
     * 屏蔽身份证号码
     * 
     * @author lv
     * @param idNo
     *            身份证号
     * @param maskAfter
     *            屏蔽位数
     * @return
     */
    public static String getIdCardMark(String idNo, int maskAfter) {
        StringBuilder result = new StringBuilder("");
        final int idlength = 8;
        final int leaveLast = 4;
        if (idNo != null) {
            result.append(idNo);
            if (idNo.length() > idlength) {
                result.append(idNo.substring(0, maskAfter));
                int endIndex = idNo.length() - leaveLast;
                if (endIndex < (maskAfter + leaveLast)) {
                    endIndex = idNo.length();
                }
                for (int i = maskAfter; i < endIndex; i++) {
                    result.append("*");
                }
                result.append(idNo.substring(endIndex, idNo.length()));
            }
        }
        return result.toString();
    }

    public static String getIdCardMark(String idNo) {
        return getIdCardMark(idNo, 6);
    }

    /**
     * 兼容原来 按汉字数计
     * 
     * @Title substring
     * @param source
     * @param number
     *            长度以汉字长度为准 1个英文或数字算半个字符
     * @return String
     */
    public static String substring(String source, int number) {
        return substring(source, number * 2, true);
    }

    /**
     * 
     * 按字节截取字符串
     * 
     * @Title substring
     * @param content
     *            文字
     * @param size
     * @param suf
     * @return String
     */
    public static String substring(String content, int size, boolean suf) {
        if (content == null) {
            return "";
        }
        StringBuffer newStrSb = new StringBuffer();
        int totalCount = 0;
        for (int i = 0; i < content.length(); i++) {
            char c = content.charAt(i);
            if (isSingleByte(c)) {
                totalCount++;
            } else {
                totalCount += 2;
            }
            if (totalCount <= size) {
                newStrSb.append(String.valueOf(c));
            } else {
                if (suf) {
                    newStrSb.append("...");
                }
                break;
            }
        }
        return newStrSb.toString();
    }

    /**
     * 
     * 返回字节数
     * 
     * @Title getByteLen
     * @param content
     * @return int
     */
    public static int getByteLen(String content) {
        int totalCount = 0;
        if (content != null) {
            for (int i = 0; i < content.length(); i++) {
                char c = content.charAt(i);
                if (isSingleByte(c)) {
                    totalCount++;
                } else {
                    totalCount += 2;
                }
            }
        }
        return totalCount;
    }

    /**
     * 是否为单字节字符
     * 
     * @Title isSingleByte
     * @author 吕凯
     * @date 2013-8-1 下午2:28:00
     * @param c
     * @return boolean
     */
    public static boolean isSingleByte(char c) {
        if ((c >= 0x0001 && c <= 0x007e) || (0xff60 <= c && c <= 0xff9f)) {
            return true;
        }
        return false;
    }

    /**
     * 
     * /** 按指定的规则替换选定的字符串
     * 
     * @param tragetString
     * @param replaceIt
     * @param replacement
     * @return
     */
    public static String replaceString(String tragetString, String replaceIt, String replacement) {
        return replaceString(tragetString, replaceIt, replacement, false, false);
    }

    /**
     * 按指定的规则替换选定的字符串
     * 
     * @param tragetString
     *            目标字符
     * @param replaceIt
     *            查询字符
     * @param replacement
     *            替换的字符
     * @param toLowerCase
     *            大小写匹配: true - 不区分大小写, flase - 严格匹配大小写
     * @param prefixMode
     *            是否只替换第一次出现的前缀 : true - 只替换前缀, false - 替换所有字符
     * @return
     */
    public static String replaceString(String tragetString, String replaceIt, String replacement, boolean toLowerCase, boolean prefixMode) {
        if (tragetString == null || replaceIt == null || replacement == null) {
            return tragetString;
        }

        StringBuffer buf = new StringBuffer();
        int legthOfSource = tragetString.length();
        int legthOfReplaceIt = replaceIt.length();
        int postionStart = 0;
        int currentPos;// 每次找到的要替换的字符串的位置

        String tmpStr;
        String tmpReplace;
        if (toLowerCase) {
            tmpStr = tragetString.toLowerCase();
            tmpReplace = replaceIt.toLowerCase();
        } else {
            tmpStr = tragetString;
            tmpReplace = replaceIt;
        }

        while ((currentPos = (tmpStr.indexOf(tmpReplace, postionStart))) >= 0) {// 当能找到
            buf.append(tragetString.substring(postionStart, currentPos));// 替换的是真实字符串
            buf.append(replacement);// 替换
            if (prefixMode) {
                buf.append(tragetString.substring(tragetString.indexOf(replaceIt) + replaceIt.length()));
                return buf.toString();
            }
            postionStart = currentPos + legthOfReplaceIt;// 位置移动
        }

        if (postionStart < legthOfSource) {// 如果source中已没有需要替换的字符串存在且没有到尾部，就要将剩下的并入结果
            buf.append(tragetString.substring(postionStart));
        }

        return buf.toString();

    }

    /**
     * 对一个原始字符串内，指定两个字符串中间的区域文字进行替换操作，将中间的这段文字替换为另一段
     * <p>
     * 如:要将字符串1234567890内的3456789替换为abc，最终得到字符串12abc0,则这样写
     * </p>
     * 
     * <pre>
     * subStringReplace("1234567890", "34", "789", "abc");
     * </pre>
     * 
     * @param text
     *            原始字符串
     * @param startString
     *            要替换的这段区域的文字的开始字符串，替换后会包含这段一块被替换
     * @param endString
     *            要替换的这段区域的文字的结束，替换后会包含这段一块被替换
     * @param replaceNewString
     *            这是要替换成新的字符串
     * @return 返回替换好的。若是替换时，找不到替换的
     */
    public static String subStringReplace(String text, String startString, String endString, String replaceNewString) {
        int start = text.indexOf(startString);
        if (start == -1) {
            return text;
        }
        int end = text.substring(start, text.length()).indexOf(endString);
        if (end == -1) {
            return text;
        }
        end = end + start;

        // 如果开始、结束两个字符串都找到了，才进行替换操作
        String stringStart = text.substring(0, start);
        String stringEnd = text.substring(end + endString.length(), text.length());
        return stringStart + replaceNewString + stringEnd;
    }

    /**
     * 替换所有的空格
     * 
     * @param source
     * @return String
     */
    public static String replaceAllSpace(String source) {
        return replaceString(source, " ", "", false, false);
    }

    public static boolean isEmpty(Object object) {
        if (object == null) {
            return true;
        } else if (object.equals("")) {
            return true;
        } else if (object.equals("null")) {
            return true;
        }
        return false;
    }

    public static boolean isNotEmpty(Object object) {
        if (object != null && !object.equals("") && !object.equals("null")) {
            return true;
        }
        return false;
    }

    /**
     * 从shiledCore中复制过来
     * 
     * @Title removeHTMLTag
     * @author 刘金浩
     * @date 2019--29 上午11:16:54
     * @param htmlStr
     * @return String
     */
    public static String removeHTMLTag(String htmlStr) {
        if (htmlStr == null) {
            htmlStr = "";
        }
        if (htmlStr.length() > 0) {
            String regEx_script = "<script[^>]*?>[\\s\\S]*?<\\/script>"; // 定义script的正则表达式
            String regEx_style = "<style[^>]*?>[\\s\\S]*?<\\/style>"; // 定义style的正则表达式
            String regEx_html = "</?[A-z]+.*?>"; // 定义HTML标签的正则表达式
            String regEx_commit = "<[^>]+>"; // 定义HTML标签的正则表达式
            Pattern p_script = Pattern.compile(regEx_script, Pattern.CASE_INSENSITIVE);
            Matcher m_script = p_script.matcher(htmlStr);
            htmlStr = m_script.replaceAll(""); // 过滤script标签

            Pattern p_style = Pattern.compile(regEx_style, Pattern.CASE_INSENSITIVE);
            Matcher m_style = p_style.matcher(htmlStr);
            htmlStr = m_style.replaceAll(""); // 过滤style标签

            Pattern p_html = Pattern.compile(regEx_html, Pattern.CASE_INSENSITIVE);
            Matcher m_html = p_html.matcher(htmlStr);
            htmlStr = m_html.replaceAll(""); // 过滤html标签

            Pattern p_commit = Pattern.compile(regEx_commit, Pattern.CASE_INSENSITIVE);
            Matcher m_commitl = p_commit.matcher(htmlStr);
            htmlStr = m_commitl.replaceAll(""); // 过滤html标签
            htmlStr = htmlStr.replace("&nbsp;", "");
        }

        return htmlStr.trim(); // 返回文本字符串
    }

    /**
     * 
     * 去除HTML但是除了添加的标签excludeStr
     * 
     * @Title removeHTMLTagExclude
     * @author 武杨
     * @date 2021年6月4日 下午4:19:53
     * @param htmlStr
     * @param excludeStr
     *            排除的标签
     * @return String
     */
    public static String removeHTMLTagExclude(String htmlStr, String... excludeStr) {
        htmlStr = Optional.ofNullable(htmlStr).orElse("");
        StringBuffer exclStr = new StringBuffer();
        if (excludeStr.length > 0) {
            for (String str : excludeStr) {
                exclStr.append("(?!" + str + ")");
            }
        }
        if (htmlStr.length() > 0) {
            String regEx_html = "</?" + exclStr.toString() + "[A-z]+.*?>"; // 定义HTML标签的正则表达式

            Pattern p_html = Pattern.compile(regEx_html, Pattern.CASE_INSENSITIVE);
            Matcher m_html = p_html.matcher(htmlStr);
            htmlStr = m_html.replaceAll(""); // 过滤html标签

        }

        return htmlStr.trim(); // 返回文本字符串
    }

    /**
     * 从给定的字符串中截取想要的指定字符
     * 
     * @param sourceString
     *            源字符串，要切割的字符串
     * @param startString
     *            匹配的开始点字符
     *            <ul>
     *            <li>若为null或者""表示从头开始匹配</li>
     *            <li>若是没找到开始点字符串，默认为从最开始匹配</li>
     *            </ul>
     * @param endString
     *            匹配的结束点字符
     *            <ul>
     *            <li>若为null或者""表示匹配到末尾</li>
     *            <li>若是没找到结束点字符串，默认为匹配到最末尾</li>
     *            </ul>
     * @param matchType
     *            此项是针对结尾的匹配,可传入：
     *            <ul>
     *            <li>1:开始匹配找到的第一个，结束匹配找到的最后一个。</li>
     *            <li>2:开始匹配找到的第一个，结束匹配：找到的开始位置往后的第一个。</li>
     *            <li>3.开始匹配找到的最后一个，结束匹配找到的最后一个。</li>
     *            <li>4:开始匹配找到的最后一个，结束匹配：找到的开始位置往后的第一个。</li>
     *            </ul>
     * @return 截取的字符串,若是传入了但是没找到开始或者结束字符则返回null
     */
    public static String subString(String sourceString, String startString, String endString, int matchType) {
        // 开始点
        int start = 0;
        if (!(startString == null || startString.length() == 0)) {
            if (matchType == 1 || matchType == 2) {
                start = sourceString.indexOf(startString);
            } else {
                start = sourceString.lastIndexOf(startString);
            }

            if (start < 0) {
                // 没有找到，则定为0，从最开始处截取
                start = 0;
            } else {
                // 不截取传入的字符，从其后开始截取
                start = start + startString.length();
            }
        }

        // 结束点
        int end = 0;
        if (!(endString == null || endString.length() == 0)) {
            if (matchType == 1 || matchType == 3) {
                end = sourceString.lastIndexOf(endString);
                if (end < 0) {
                    // 没有找到，则定为－1，方法返回null
                    end = -1;
                }
            } else {
                String xnx3_string;
                if (start > -1) {
                    xnx3_string = sourceString.substring(start);
                } else {
                    xnx3_string = sourceString;
                }

                end = xnx3_string.indexOf(endString);
                if (end < 0) {
                    end = 0;
                }
                end = end + start;
            }
        } else {
            end = sourceString.length();
        }

        if (start == -1 || end == -1) {
            return null;
        } else {
            return sourceString.substring(start, end);
        }
    }

    /**
     * 
     * 字符格式补全
     * 
     * @Title formatNumLength
     * @author 刘金浩
     * @date 2022年4月13日 下午4:40:54
     * @param number
     * @param length
     * @return String
     */
    public static String formatNumLength(Object number, int length) {
        String numberString = "" + number;
        String bh = String.format("%0" + length + "d", ClassUtil.obj2int(number));
        return ("" + parseInt(numberString)).replace("" + parseInt(numberString), bh);
    }

    public static Integer parseInt(String a) {
        String regEx = "[^0-9]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(a);
        if (m.replaceAll("").trim() == "") {
            return 0;
        }
        try {
            return Integer.parseInt(m.replaceAll("").trim());
        } catch (Exception e) {
            return 0;
        }

    }
//	/**
//	 * 测试代码
//	 * @param args
//	 */
//	public static void main(String[] args) {
//		String html = "<p style=\"text-align: left; padding-bottom: 0px; text-transform: none; background-color: rgb(255,255,255); text-indent: 0px; margin: 0px; padding-left: 0px; letter-spacing: normal; padding-right: 0px; font: 14px/25px 宋体, tahoma, arial, sans-serif; white-space: normal; color: rgb(0,0,0); word-spacing: 0px; padding-top: 0px; -webkit-text-stroke-width: 0px\">__eol__	科技日报讯&nbsp;（通讯员辛志向&nbsp;记者王建高）日前，青岛高新区国家级生态工业示范园区通过了由国家环保部及科技部、商务部三部门联合组织的国家正式验收。</p>__eol__<p style=\"text-align: left; padding-bottom: 0px; text-transform: none; background-color: rgb(255,255,255); text-indent: 0px; margin: 0px; padding-left: 0px; letter-spacing: normal; padding-right: 0px; font: 14px/25px 宋体, tahoma, arial, sans-serif; white-space: normal; color: rgb(0,0,0); word-spacing: 0px; padding-top: 0px; -webkit-text-stroke-width: 0px\">__eol__	&nbsp;&nbsp;&nbsp;&nbsp;青岛市委常委、红岛经济区工委书记、管委主任陈飞介绍，建立国家生态工业示范园区是青岛高新区深化和拓展生态创建成果，加快结构调整，推进转型发展，进而辐射带动红岛经济区生态建设的重要抓手。</p>__eol__<p style=\"text-align: left; padding-bottom: 0px; text-transform: none; background-color: rgb(255,255,255); text-indent: 0px; margin: 0px; padding-left: 0px; letter-spacing: normal; padding-right: 0px; font: 14px/25px 宋体, tahoma, arial, sans-serif; white-space: normal; color: rgb(0,0,0); word-spacing: 0px; padding-top: 0px; -webkit-text-stroke-width: 0px\">__eol__	&nbsp;&nbsp;&nbsp;&nbsp;自2007年经国家三部委批准创建以来，青岛高新区高度重视，强化措施，始终按照创建工作要求，坚持生态、生活、生产&ldquo;三生共融&rdquo;理念，以生态环境的优化带动经济的发展。创建过程中，坚持&ldquo;科学性规划，高水平建设，生态化发展&rdquo;的原则，实施&ldquo;国际化、高端化、低碳化&rdquo;三大战略，逐步建立起适应循环经济发展的产业体系和政策体系，形成了完善的生态工业园建设框架和循环经济发展机制，实现了经济高速发展与环境持续改善的良性循环。随着发展环境和核心竞争力的不断提升，通过了ISO14001环境管理体系认证，获得了中国最具投资潜力经济园区、国家科技兴贸创新基地、山东省海外高层次人才创新创业基地等称号，在国家级高新区综合评价中连续多年跻身前列，已经成为山东半岛乃至环渤海区域快速成长、极具创新活力和竞争力的蓝色经济特色高科技产业园区。</p>";
//		html += "<div style=\"text-indent: 32pt\">__eol__	<span style=\"line-height: 20px; font-size: 20px\"><span style=\"font-family: 宋体\">我市积极支持和引导企业、高校院所&ldquo;走出去&rdquo;开展国际科技合作，在国外集聚和利用各种创新资源，增强我市企业和科研院所竞争实力。</span></span></div>__eol__<div align=\"left\" style=\"text-align: left; text-indent: 32pt; text-autospace: \">__eol__	<span style=\"line-height: 20px; font-size: 20px\"><span style=\"font-family: 宋体\">一是在国外设立研发中心。通过合资、并购、控股等方式，在国外设立研发中心和产业园，传播中国文化，提升青岛声誉。如，软控在斯洛伐克设立的研发中心成为我国与中东欧国家合作的成功样板。南车青岛四方积极筹建与泰国等高铁联合研发中心，将成为我国高端成套装备出口的重要研发基地。<span style=\"color: black\">2013</span><span style=\"color: black\">年，我市企业与大学科研机构与国外共建7家联合研发中心。 </span></span></span></div>__eol__<div style=\"text-indent: 32pt\">__eol__	<span style=\"line-height: 20px; font-size: 20px\"><span style=\"font-family: 宋体\"><span style=\"color: black\">二是利用科技资源和渠道助推中小科技企业&ldquo;走出去&rdquo;开发新兴市场。</span><span style=\"color: black\">利用尼日利亚总统访华契机，积极向尼方政府代表团推介我市现代农业技术和先进制造业企业。引导我市科技型企业输出技术，拓展西非共同体市场，推动企业转型升级，拓宽&ldquo;走出去&rdquo;渠道</span><span style=\"color: black\">。</span></span></span></div>";
//		html += "<p style='TEXT-ALIGN: center' class=\"sdfasf\"><strong>蓝海技术加以</strong></p><p style='TEXT-ALIGN: center'><em>蓝海技术加以</em></p><p style='TEXT-ALIGN: center'><span style='TEXT-DECORATION: underline'>蓝海技术加以</span></p><p style='TEXT-ALIGN: center'>蓝海技术加以</p><p></p>";
//		html = html.replaceAll("__eol__", "");
//		System.out.println(replaceMobileHtml(html));
//	}

}
