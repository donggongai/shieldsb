package com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.button;

import java.util.ArrayList;
import java.util.List;

import com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.TemplateCardBasicModel;
import com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.TmplCardActionModel;
import com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.TmplCardHorizontalContentModel;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class TemplateCardButtonModel extends TemplateCardBasicModel {

    private String subTitleText; // 二级普通文本，必填，建议不超过160个字
    private List<TmplCardHorizontalContentModel> horizontalContentList; // 二级标题+文本列表，该字段可为空数组，但有数据的话需确认对应字段是否必填，列表长度不超过6

    private String taskId; // 必填，任务id，同一个应用任务id不能重复，只能由数字、字母和“_-@”组成，最长128字节
    // 默认值
    @Builder.Default
    private List<TmplCardBtn> buttonList = new ArrayList<>(); // 按钮列表，该字段可为空数组，但有数据的话需确认对应字段是否必填，列表长度不超过6

    private TmplCardActionModel cardAction; // 整体卡片的点击跳转事件，text_notice必填本字段

    /*{
        "touser" : "UserID1|UserID2|UserID3",
        "toparty" : "PartyID1 | PartyID2",
        "totag" : "TagID1 | TagID2",
        "msgtype" : "template_card",
        "agentid" : 1,
        "template_card" : {
            "card_type" : "button_interaction",
            "source" : {
                "icon_url": "图片的url",
                "desc": "企业微信"
            },
            "main_title" : {
                "title" : "欢迎使用企业微信",
                "desc" : "您的好友正在邀请您加入企业微信"
            },
            "sub_title_text" : "下载企业微信还能抢红包！",
            "horizontal_content_list" : [
                {
                    "keyname": "邀请人",
                    "value": "张三"
                },
                {
                    "type": 1,
                    "keyname": "企业微信官网",
                    "value": "点击访问",
                    "url": "https://work.weixin.qq.com"
                },
                {
                    "type": 2,
                    "keyname": "企业微信下载",
                    "value": "企业微信.apk",
                    "media_id": "文件的media_id"
                }
            ],
            "card_action": {
                "type": 2,
                "url": "https://work.weixin.qq.com",
                "appid": "小程序的appid",
                "pagepath": "/index.html"
            },
            "task_id": "task_id",
            "button_list": [
                {
                    "text": "按钮1",
                    "style": 1,
                    "key": "button_key_1"
                },
                {
                    "text": "按钮2",
                    "style": 2,
                    "key": "button_key_2"
                }
            ]
        },
        "enable_id_trans": 0,
        "enable_duplicate_check": 0,
        "duplicate_check_interval": 1800
    }*/

}
