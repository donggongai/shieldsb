package com.ld.shieldsb.common.core.util.notice.wechat;

/**
 * 企业微信小工具类
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月19日 下午3:29:00
 *
 */
public class EweChatTool {
    public static final String MSG_TYPE = "msgtype"; // 消息类型
    public static final String SEND_TO_ALL = "@all"; // 发送给所有人

    public static final String ERRCODE = "errcode"; // 企业微信错误编码
    public static final String ERRMSG = "errmsg"; // 企业微信错误信息
    public static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36"; // 企业微信未发送通知的url，%s为占位符

    public static final Integer APPLICATION = 0; // 企业微信的应用
    public static final Integer WEBHOOK = 1; // 企业微信的web钩子

    public static final Integer ERROR = 0; // 执行失败
    public static final Integer SUCCESS = 1; // 执行成功

    public static final Integer ACTION_TYPE_URL = 1; // 1 代表跳转url，
    public static final Integer ACTION_TYPE_APP = 2; // 2 代表打开小程序

    public static final Integer LINK_TYPE_URL = 1; // 1 代表跳转url，
    public static final Integer LINK_TYPE_DOWNLOAD = 2; // 2 代表下载

    public static final Integer RECALL = 1; // 已撤回
    public static final Integer NOT_RECALL = 0; // 未撤回

    // 模板卡片通知的下级类型
    public static final String TEMPL_CARD_TYPE_TEXT = "text_notice"; // 文本通知
    public static final String TEMPL_CARD_TYPE_NEWS = "news_notice"; // 图文展示型
    public static final String TEMPL_CARD_TYPE_BUTTON = "button_interaction"; // 按钮交互型
    public static final String TEMPL_CARD_TYPE_VOTE = "vote_interaction"; // 投票选择型
    public static final String TEMPL_CARD_TYPE_MULTIPLE = "multiple_interaction"; // 多项选择型
}
