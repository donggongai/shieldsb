package com.ld.shieldsb.common.core.io.img;

import lombok.Builder;
import lombok.Data;

/**
 * 水印设置信息
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年5月6日 上午9:52:36
 *
 */
@Data
@Builder
public class WaterMaskSettings {
    @Builder.Default
    private float opacity = 1; // 水印的透明度
    private Integer distanceY; // 水印的上下间距，绘制时距x轴坐标
    private Integer distanceX; // 水印的左右间距，绘制时距y轴坐标

    private Integer width;
    private Integer height;

    private Integer minHeight; // 添加水印的最小高度，只有超过这个高度时才添加水印

}
