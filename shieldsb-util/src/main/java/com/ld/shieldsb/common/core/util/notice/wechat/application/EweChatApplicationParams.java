package com.ld.shieldsb.common.core.util.notice.wechat.application;

import lombok.Builder;
import lombok.Data;

/**
 * webhook(机器人api)消息参数，主要为文本消息使用
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月19日 下午2:53:21
 *
 */
@Builder
@Data
public class EweChatApplicationParams {
    private String corpId;
    private String corpSecret;
    private String agentId; // 企业应用的id，整型。企业内部开发，可在应用的设置页面查看；第三方服务商，可通过接口 获取企业授权信息 获取该参数值

}
