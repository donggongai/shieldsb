package com.ld.shieldsb.common.core.encryptor;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;

/**
 * SHA-1不可逆加密工具类 <br/>
 * SHA（安全散列算法）同样是作为加密 Hash 函数家族中的一员。除了它生成的 Hash 安全性比 MD5 更强之外其它都与 MD5 非常类似。然而，它们生成的 Hash 并不总是唯一的，这意味着输入两个不同的值所获的 Hash
 * 却是相同的。通常这种情况的发生我们称之为“碰撞”。 不过，SHA 碰撞的几率小于 MD5。你甚至无需担心碰撞的发生，因为这种情况非常罕见。<br/>
 * 
 * 在 Java 中有提供有4种 SHA 算法的实现，相对 MD5(128 bit hash) 它提供了以下长度的 Hash：<br/>
 * 
 * SHA-1 (简单实现 – 160 bits Hash) <br/>
 * SHA-256 (强于 SHA-1 – 256 bits Hash) <br/>
 * SHA-384 (强于 SHA-256 – 384 bits Hash) <br/>
 * SHA-512 (强于 SHA-384 – 512 bits Hash)<br/>
 * * 通常越长的 Hash 越难破解，这是核心思想。
 * 
 * @author ThinkGem
 */
public class Sha1Encryptor {

    private static final String SHA1 = "SHA-1";

    private Sha1Encryptor() {
        throw new IllegalStateException("该类不可初始化");
    }

    /**
     * 生成随机的Byte[]作为salt密钥.
     * 
     * @param numBytes
     *            byte数组的大小
     */
    public static byte[] getSalt(int numBytes) {
        return DigestUtils.getSalt(numBytes);
    }

    /**
     * 对输入字符串进行sha1散列.
     * 
     * @param input
     *            字符串
     */
    public static String sha1(String input) {
        return sha1(input, 1);
    }

    /**
     * 对输入字符串进行sha1散列.
     * 
     * @Title sha1
     * @author 吕凯
     * @date 2019年1月26日 上午11:33:51
     * @param data
     *            原字符
     * @param iterations
     *            迭代次数
     * @return
     * @throws NoSuchAlgorithmException
     *             String
     */
    public static final String sha1(String input, int iterations) {
        return EncodeUtils.encodeHex(DigestUtils.digest(input.getBytes(StandardCharsets.UTF_8), SHA1, null, iterations));
    }

    /**
     * 对输入字符串进行sha1散列.
     */
    public static byte[] sha1(byte[] input) {
        return DigestUtils.digest(input, SHA1, null, 1);
    }

    /**
     * 对输入字符串进行sha1散列.
     */
    public static byte[] sha1(byte[] input, byte[] salt) {
        return DigestUtils.digest(input, SHA1, salt, 1);
    }

    /**
     * 
     * 对输入字符串进行sha1散列.
     * 
     * @Title sha1
     * @author 吕凯
     * @date 2018年12月5日 下午2:46:22
     * @param input
     * @param iterations
     * @return byte[]
     */
    public static byte[] sha1(byte[] input, int iterations) {
        return DigestUtils.digest(input, SHA1, null, iterations);
    }

    /**
     * 对输入字符串进行sha1散列.
     */
    public static byte[] sha1(byte[] input, byte[] salt, int iterations) {
        return DigestUtils.digest(input, SHA1, salt, iterations);
    }

    /**
     * 对文件进行sha1散列.
     */
    public static byte[] sha1(InputStream input) throws IOException {
        return DigestUtils.digest(input, SHA1);
    }

    /**
     * 获取文件的sha1码(转换为16进制字符串)
     * 
     * @Title sha1
     * @author 吕凯
     * @date 2019年1月26日 上午11:28:48
     * @param file
     * @return
     * @throws IOException
     *             String
     */
    public static String sha1(File file) throws IOException {
        String result = null;
        try (InputStream fis = new FileInputStream(file)) {
            byte[] buffer = sha1(fis);
            result = EncodeUtils.encodeHex(buffer);
        }
        return result;
    }

}
