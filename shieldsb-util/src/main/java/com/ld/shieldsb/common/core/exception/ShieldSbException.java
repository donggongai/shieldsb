package com.ld.shieldsb.common.core.exception;

/**
 * shieldsb的通用异常，为了便于捕捉一般不建议使用
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月26日 上午8:36:53
 *
 */
public class ShieldSbException extends RuntimeException {

    /**
     * @Fields serialVersionUID
     */
    private static final long serialVersionUID = 478924930132001421L;

    // 用详细信息指定一个异常
    public ShieldSbException(String message) {
        super(message);
    }

    // 用指定的详细信息和原因构造一个新的异常
    public ShieldSbException(String message, Throwable cause) {
        super(message, cause);
    }

    // 用指定原因构造一个新的异常
    public ShieldSbException(Throwable cause) {
        super(cause);
    }

}
