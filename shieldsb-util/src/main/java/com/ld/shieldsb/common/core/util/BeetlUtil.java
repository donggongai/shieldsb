package com.ld.shieldsb.common.core.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.resource.ClasspathResourceLoader;
import org.beetl.core.resource.StringTemplateResourceLoader;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BeetlUtil {
    protected static ClasspathResourceLoader classResourceLoader = null;
    protected static Configuration config = null;
    protected static GroupTemplate groupTemplateClass = null;
    protected static GroupTemplate groupTemplateStr = null;

    private static StringTemplateResourceLoader stringResourceLoader = null; // 字符串模板

    static {
        try {
            classResourceLoader = new ClasspathResourceLoader("templates"); // class路径下寻找，不以/开头，传入模板文件路径，传出解析后的内容
            stringResourceLoader = new StringTemplateResourceLoader(); // 字符串模板，传入字符串，传出解析后的内容
            config = Configuration.defaultConfiguration(); // 可能抛异常报错，所以放到静态语句块中
            groupTemplateClass = new GroupTemplate(classResourceLoader, config);
            groupTemplateStr = new GroupTemplate(stringResourceLoader, config);
        } catch (IOException e) {
            log.error("", e);
        }

    }

    /**
     * 设置全局变量
     * 
     * @Title setSharedVars
     * @author 吕凯
     * @date 2018年12月7日 上午11:38:05
     * @param key
     * @param value
     *            void
     */
    public static void setSharedVars(String key, Object value) {
        Map<String, Object> shared = groupTemplateClass.getSharedVars();
        if (shared == null) {
            shared = new HashMap<>();
        }
        shared.put(key, value);
        groupTemplateClass.setSharedVars(shared);
    }

    /**
     * 设置全局变量
     * 
     * @Title setSharedVars
     * @author 吕凯
     * @date 2018年12月7日 上午11:38:20
     * @param shared
     *            void
     */
    public static void setSharedVars(Map<String, Object> shared) {
        Map<String, Object> sharedExist = groupTemplateClass.getSharedVars();
        if (sharedExist == null) {
            sharedExist = new HashMap<>();
        }
        sharedExist.putAll(shared);
        groupTemplateClass.getSharedVars().putAll(sharedExist);
    }

    /**
     * 普通处理，返回文本
     * 
     * @Title render
     * @author 吕凯
     * @date 2018年4月13日 上午9:51:32
     * @param templatePath
     * @param model
     * @return String
     */
    public static String render(String templatePath, Map<String, Object> model) {
        Template t;
        try {
            t = groupTemplateClass.getTemplate(templatePath);
            t.binding(model);
            return t.render();
        } catch (Exception e) {
            log.error("", e);
        }
        return "";
    }

    /**
     * 
     * 字符串模板
     * 
     * @Title renderFromStr
     * @author 刘金浩
     * @date 2019年11月6日 下午2:54:49
     * @param templateStr
     * @param model
     * @return String
     */
    public static String renderFromStr(String templateStr, Map<String, Object> model) {
        Template t;
        try {
            t = groupTemplateStr.getTemplate(templateStr);
            t.binding(model);
            return t.render();
        } catch (Exception e) {
            log.error("", e);
        }
        return "";
    }

}
