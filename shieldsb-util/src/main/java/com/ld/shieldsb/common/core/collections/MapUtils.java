package com.ld.shieldsb.common.core.collections;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.commons.beanutils.converters.DateConverter;
import org.apache.commons.beanutils.converters.DoubleConverter;
import org.apache.commons.beanutils.converters.IntegerConverter;
import org.apache.commons.beanutils.converters.LongConverter;
import org.apache.commons.beanutils.converters.ShortConverter;

import com.ld.shieldsb.common.core.util.StringUtils;

/**
 * Map工具类，实现 Map <-> Bean 互相转换
 * 
 * @ClassName MapUtils
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2018年11月15日 下午6:35:25
 *
 */
public class MapUtils extends org.apache.commons.collections.MapUtils {

    static {
        // 处理时间格式(map转obj时用)
        DateConverter dateConverter = new DateConverter();
        // 设置日期格式
        dateConverter.setPatterns(new String[] { "yyyy/MM/dd", "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss" });
        // 注册格式
        ConvertUtils.register(dateConverter, Date.class);
        // beanUtil工具类转换时null不转换为0
        ConvertUtils.register(new LongConverter(null), Long.class);
        ConvertUtils.register(new ShortConverter(null), Short.class);
        ConvertUtils.register(new IntegerConverter(null), Integer.class);
        ConvertUtils.register(new DoubleConverter(null), Double.class);
        ConvertUtils.register(new BigDecimalConverter(null), BigDecimal.class);
    }

    public static <K, V> HashMap<K, V> newHashMap() {
        return new HashMap<>();
    }

    public static <K, V> HashMap<K, V> newHashMap(int initialCapacity) {
        return new HashMap<>(initialCapacity);
    }

    public static <K, V> HashMap<K, V> newHashMap(Map<? extends K, ? extends V> map) {
        return new HashMap<>(map);
    }

    public static <K, V> LinkedHashMap<K, V> newLinkedHashMap() {
        return new LinkedHashMap<>();
    }

    public static <K, V> LinkedHashMap<K, V> newLinkedHashMap(Map<? extends K, ? extends V> map) {
        return new LinkedHashMap<>(map);
    }

    public static <K, V> ConcurrentMap<K, V> newConcurrentMap() {
        return new ConcurrentHashMap<>();
    }

    @SuppressWarnings("rawtypes")
    public static <K extends Comparable, V> TreeMap<K, V> newTreeMap() {
        return new TreeMap<>();
    }

    public static <K, V> TreeMap<K, V> newTreeMap(SortedMap<K, ? extends V> map) {
        return new TreeMap<>(map);
    }

    public static <C, K extends C, V> TreeMap<K, V> newTreeMap(Comparator<C> comparator) {
        return new TreeMap<>(comparator);
    }

    public static <K extends Enum<K>, V> EnumMap<K, V> newEnumMap(Class<K> type) {
        return new EnumMap<>((type));
    }

    public static <K extends Enum<K>, V> EnumMap<K, V> newEnumMap(Map<K, ? extends V> map) {
        return new EnumMap<>(map);
    }

    public static <K, V> IdentityHashMap<K, V> newIdentityHashMap() {
        return new IdentityHashMap<>();
    }

    /**
     * List<Map<String, V>转换为List<T>
     * 
     * @param clazz
     * @param list
     * @throws InstantiationException
     */
    public static <T, V> List<T> toObjectList(Class<T> clazz, List<HashMap<String, V>> list)
            throws IllegalAccessException, InvocationTargetException, InstantiationException {
        List<T> retList = new ArrayList<>();
        if (list != null && !list.isEmpty()) {
            for (HashMap<String, V> m : list) {
                retList.add(toObject(clazz, m));
            }
        }
        return retList;
    }

    /**
     * 将Map转换为Object
     * 
     * @param clazz
     *            目标对象的类
     * @param map
     *            待转换Map
     * @throws InstantiationException
     */
    public static <T, V> T toObject(Class<T> clazz, Map<String, V> map)
            throws IllegalAccessException, InvocationTargetException, InstantiationException {
        return toObject(clazz, map, false);
    }

    /**
     * 将Map转换为Object
     * 
     * @param clazz
     *            目标对象的类
     * @param map
     *            待转换Map
     * @param toCamelCase
     *            是否去掉下划线
     */
    public static <T, V> T toObject(Class<T> clazz, Map<String, V> map, boolean toCamelCase)
            throws InstantiationException, IllegalAccessException, InvocationTargetException {
        T object = clazz.newInstance();
        return toObject(object, map, toCamelCase);
    }

    /**
     * 将Map转换为Object
     * 
     * @param clazz
     *            目标对象的类
     * @param map
     *            待转换Map
     */
    public static <T, V> T toObject(T object, Map<String, V> map) throws IllegalAccessException, InvocationTargetException {
        return toObject(object, map, false);
    }

    /**
     * 将Map转换为Object
     * 
     * @param object
     *            目标对象的类
     * @param map
     *            待转换Map
     * @param toCamelCase
     *            是否采用驼峰命名法转换
     */
    public static <T, V> T toObject(T object, Map<String, V> map, boolean toCamelCase)
            throws IllegalAccessException, InvocationTargetException {
        if (toCamelCase) {
            map = toCamelCaseMap(map);
        }

        BeanUtils.populate(object, map);
        return object;
    }

    /**
     * 对象转Map
     * 
     * @param object
     *            目标对象
     * @return 转换出来的值都是String
     */
    public static Map<String, String> toMap(Object object) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        return BeanUtils.describe(object);
    }

    /**
     * 对象转Map
     * 
     * @param object
     *            目标对象
     * @return 转换出来的值类型是原类型
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    public static Map<String, Object> toNavMap(Object object)
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        return PropertyUtils.describe(object);
    }

    /**
     * 转换为Collection<Map<K, V>>
     * 
     * @param collection
     *            待转换对象集合
     * @return 转换后的Collection<Map<K, V>>
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     */
    public static <T> Collection<Map<String, String>> toMapList(Collection<T> collection)
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        List<Map<String, String>> retList = new ArrayList<>();
        if (collection != null && !collection.isEmpty()) {
            for (Object object : collection) {
                Map<String, String> map = toMap(object);
                retList.add(map);
            }
        }
        return retList;
    }

    /**
     * 转换为Collection,同时为字段做驼峰转换<Map<K, V>>
     * 
     * @param collection
     *            待转换对象集合
     * @return 转换后的Collection<Map<K, V>>
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     */
    public static <T> Collection<Map<String, String>> toMapListForFlat(Collection<T> collection)
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        List<Map<String, String>> retList = new ArrayList<>();
        if (collection != null && !collection.isEmpty()) {
            for (Object object : collection) {
                Map<String, String> map = toMapForFlat(object);
                retList.add(map);
            }
        }
        return retList;
    }

    /**
     * 转换成Map并提供字段命名驼峰转平行
     * 
     * @param clazz
     *            目标对象所在类
     * @param object
     *            目标对象
     * @param map
     *            待转换Map
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    public static Map<String, String> toMapForFlat(Object object)
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        Map<String, String> map = toMap(object);
        return toUnderlineStringMap(map);
    }

    /**
     * 将Map的Keys去下划线<br>
     * (例:branch_no -> branchNo )<br>
     * 
     * @param map
     *            待转换Map
     * @return
     */
    public static <V> Map<String, V> toCamelCaseMap(Map<String, V> map) {
        Map<String, V> newMap = new HashMap<>();
        for (Map.Entry<String, V> entry : map.entrySet()) {
            safeAddToMap(newMap, StringUtils.toCamelCase(entry.getKey()), entry.getValue());
        }
        return newMap;
    }

    /**
     * 将Map的Keys转译成下划线格式的<br>
     * (例:branchNo -> branch_no)<br>
     * 
     * @param map
     *            待转换Map
     * @return
     */
    public static <V> Map<String, V> toUnderlineStringMap(Map<String, V> map) {
        Map<String, V> newMap = new HashMap<>();
        for (Map.Entry<String, V> entry : map.entrySet()) {
            newMap.put(StringUtils.toUnderlineStr(entry.getKey()), entry.getValue());
        }
        return newMap;
    }

    /**
     * 获取map的副本
     * 
     * @Title getCopyMap
     * @author 吕凯
     * @date 2019年2月23日 下午1:42:10
     * @param map
     * @return Map<String,V>
     */
    public static <V> Map<String, V> getCopyMap(Map<String, V> map) {
        return newHashMap(map);
    }

    /**
     * 获取linkedHashMap的副本
     * 
     * @Title getCopyLinkedHashMap
     * @author 吕凯
     * @date 2019年2月23日 下午1:42:57
     * @param map
     * @return Map<String,V>
     */
    public static <V> Map<String, V> getCopyLinkedHashMap(Map<String, V> map) {
        return newLinkedHashMap(map);
    }

    /**
     * 
     * 获取valueList
     * 
     * @Title getValueList
     * @author 吕凯
     * @date 2019年3月29日 上午11:54:08
     * @param map
     * @return List<V>
     */
    public static <V> List<V> getValueList(Map<String, V> map) {
        if (map == null) {
            return Collections.emptyList();
        }
        return new ArrayList<>(map.values());
    }

}
