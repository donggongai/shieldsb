package com.ld.shieldsb.common.core.util.notice.sms;

import com.ld.shieldsb.common.core.model.PropertiesModel;
import com.ld.shieldsb.common.core.model.Result;

/**
 * 短信工具类
 * 
 * @ClassName SMSUtil
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2017年8月16日 下午3:13:39
 *
 */
public final class SMSUtil {
    private static final SMSSend SMSSender = getSMSSend();

    public static SMSSend getSMSSend() {
        String usersystemType = getUserSystemType();
        return SMSSendFactory.getInstance().getService(usersystemType);
    }

    private static String getUserSystemType() {
        String usersystemType = PropertiesModel.CONFIG.getString("smssend_type", SMSSendFactory.TYPE_DEFAULT);
        return usersystemType;
    }

    private SMSUtil() {
        super();
    }

    /**
     * 
     * 发送短信
     * 
     * @Title sendSMS
     * @author 吕凯
     * @date 2017年8月17日 上午10:22:27
     * @param infoType
     *            发送类型，具体跟业务相关，需大于-1
     * @param phoneNum
     *            手机号
     * @param message
     *            消息
     * @param signName
     *            签名
     * @return SMSResult
     */
    public static Result sendSMS(int infoType, String phoneNum, String message, String signName) {
        return sendSMS(infoType, phoneNum, message, signName, true);
    }

    /**
     * 发送短信
     * 
     * @Title sendSMS
     * @author 吕凯
     * @date 2017年8月17日 上午10:21:22
     * @param infoType
     *            发送类型，具体跟业务相关，需大于-1
     * @param phoneNum
     *            手机号
     * @param message
     *            消息
     * @param signName
     *            签名
     * @param save
     *            是否保存发送记录，默认保存
     * @return SMSResult
     */
    public static Result sendSMS(int infoType, String phoneNum, String message, String signName, boolean save) {
        Result smsResult = SMSSender.sendSMS(phoneNum, message, signName);
        if (save && infoType > -1) {
            // 记录
        }
        return smsResult;
    }

    public static void main(String[] args) {
        System.out.println(sendSMS(-1, "15253640441", "短信收到了吗？", "绿盾征信"));
    }
}
