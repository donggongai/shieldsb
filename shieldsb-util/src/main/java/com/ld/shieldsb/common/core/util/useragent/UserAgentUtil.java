package com.ld.shieldsb.common.core.util.useragent;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

/**
 * 根据 user agent string 判断用户的平台、浏览器，【该类判断方法可能过时，可以在具体项目中实现子类覆盖 】<br>
 * ********************************************************************<br>
 * 
 * 台式机<br>
 * 
 * Linux Ubuntu Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.2.2pre) Gecko/20100225 Ubuntu/9.10 (karmic) Namoroka/3.6.2pre<br>
 * Linux Mandriva 2008.1 Mozilla/5.0 (X11; U; Linux i686; en-US; * rv:1.9.0.1) Gecko/2008072403 Mandriva/3.0.1-1mdv2008.1 (2008.1)
 * Firefox/3.0.1 Linux suSE 10.1 Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.3) Gecko/20060425 SUSE/1.5.0.3-7 Firefox/1.5.0.31<br>
 * Windows XP SP3 Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US;rv:1.9.1) Gecko/20090624 Firefox/3.5 (.NET CLR 3.5.30729) <br>
 * Windows Vista Mozilla/5.0 (Windows; U; Windows NT 6.1; nl;rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13 Mozilla/5.0 (Windows; U; Windows NT
 * 6.0; en-US; rv:1.9.2.6) Gecko/20100625 Firefox/3.6.6 (.NET CLR 3.5.30729) <br>
 * windows 2000 Mozilla/5.0 (Windows; U; Windows NT 5.0; en-GB;rv:1.8.1b2) Gecko/20060821 Firefox/2.0b2 <br>
 * Windows 7 Mozilla/5.0 (Windows NT 6.1; WOW64; rv:14.0)Gecko/20100101 Firefox/14.0.1<br>
 * Windows Server 2008 Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US;rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5 (.NET CLR 3.5.30729) <br>
 * iMac OSX 10.7.4 Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:13.0) Gecko/20100101 Firefox/13.0.1<br>
 * Mac OS X Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US;rv:1.9.2.9) Gecko/20100824 Firefox/3.6.9 <br>
 * --------------------------------------------------------------------<br>
 * 
 * 手持设备<br>
 * 
 * iPad Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b
 * Safari/531.21.10 -------------------------------------------------------------- ----------------
 * -------------------------------------------------------------------- iPad 2 Mozilla/5.0 (iPad; CPU OS 5_1 like Mac OS X; en-us)
 * AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9B176 Safari/7534.48.3 ----------------
 * -------------------------------------------------------------- --------------------------------------------------------------------
 * iPhone 4 Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_0 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobile/8A293
 * Safari/6531.22.7 -------------------------------------------------------------- ----------------
 * -------------------------------------------------------------------- iPhone 5 Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X)
 * AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3 ----------------
 * -------------------------------------------------------------- --------------------------------------------------------------------
 * Android Mozilla/5.0 (Linux; U; Android 2.2; en-us; Nexus One Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile
 * Safari/533.1 ******** **********************************************************************
 * ********************************************************************
 * 
 * @author Defonds
 */
public class UserAgentUtil {
    protected static final Map<String, String> BROWERS_MAP = new HashMap<>();
    static final Pattern IPGONE_OS = Pattern.compile("iPhone OS (.+?) ");
    static final Pattern IPAD_OS = Pattern.compile("iPad; (U; )?CPU OS (.+?) ");
    static final Pattern MAC_OS = Pattern.compile("Mac OS X (.+?) ");
    static final Pattern ANDROID_OS = Pattern.compile("Android \\d+(\\.\\d+)*");

    static {
    }

    private UserAgentUtil() {
        // 工具类无需对象实例化
    }

    /**
     * 用途：根据客户端 User Agent Strings 判断其浏览器、操作平台 if 判断的先后次序： 根据设备的用户使用量降序排列，这样对于大多数用户来说可以少判断几次即可拿到结果： >>操作系统:Windows > 苹果 > 安卓 > Linux > ...
     * >>Browser:Chrome > FF > IE > ...
     * 
     * @param userAgentStr
     * @return
     */
    public static UserAgentModel getUserAgent(String userAgent) {
        if (StringUtils.isBlank(userAgent)) {
            return null;
        }

        if (userAgent.contains("Windows")) {// 主流应用靠前
            /**
             * ****************** 台式机 Windows 系列 ****************** Windows NT 6.2 - Windows 8 Windows NT 6.1 - Windows 7 Windows NT 6.0 -
             * Windows Vista Windows NT 5.2 - Windows Server 2003; Windows XP x64 Edition Windows NT 5.1 - Windows XP Windows NT 5.01 -
             * Windows 2000, Service Pack 1 (SP1) Windows NT 5.0 - Windows 2000 Windows NT 4.0 - Microsoft Windows NT 4.0 Windows 98; Win 9x
             * 4.90 - Windows Millennium Edition (Windows Me) Windows 98 - Windows 98 Windows 95 - Windows 95 Windows CE - Windows CE
             * 判断依据:http://msdn.microsoft .com/en-us/library/ms537503(v=vs.85).aspx
             */
            if (userAgent.contains("Windows NT 10.")) {// Windows 10
                return judgeBrowser(userAgent, "Windows", "10", null);// 判断浏览器
            } else if (userAgent.contains("Windows NT 6.3")) {// Windows 8.1
                return judgeBrowser(userAgent, "Windows", "8.1", null);// 判断浏览器
            } else if (userAgent.contains("Windows NT 6.2")) {// Windows 8
                return judgeBrowser(userAgent, "Windows", "8", null);// 判断浏览器
            } else if (userAgent.contains("Windows NT 6.1")) {// Windows 7
                return judgeBrowser(userAgent, "Windows", "7", null);
            } else if (userAgent.contains("Windows NT 6.0")) {// Windows Vista
                return judgeBrowser(userAgent, "Windows", "Vista", null);
            } else if (userAgent.contains("Windows NT 5.2")) {// Windows XP x64
                                                              // Edition
                return judgeBrowser(userAgent, "Windows", "XP", "x64 Edition");
            } else if (userAgent.contains("Windows NT 5.1")) {// Windows XP
                return judgeBrowser(userAgent, "Windows", "XP", null);
            } else if (userAgent.contains("Windows NT 5.01")) {// Windows 2000,
                                                               // Service Pack 1
                                                               // (SP1)
                return judgeBrowser(userAgent, "Windows", "2000", "SP1");
            } else if (userAgent.contains("Windows NT 5.0")) {// Windows 2000
                return judgeBrowser(userAgent, "Windows", "2000", null);
            } else if (userAgent.contains("Windows NT 4.0")) {// Microsoft
                                                              // Windows NT 4.0
                return judgeBrowser(userAgent, "Windows", "NT 4.0", null);
            } else if (userAgent.contains("Windows 98; Win 9x 4.90")) {// Windows
                                                                       // Millennium
                                                                       // Edition
                                                                       // (Windows
                                                                       // Me)
                return judgeBrowser(userAgent, "Windows", "ME", null);
            } else if (userAgent.contains("Windows 98")) {// Windows 98
                return judgeBrowser(userAgent, "Windows", "98", null);
            } else if (userAgent.contains("Windows 95")) {// Windows 95
                return judgeBrowser(userAgent, "Windows", "95", null);
            } else if (userAgent.contains("Windows CE")) {// Windows CE
                return judgeBrowser(userAgent, "Windows", "CE", null);
            }
        } else if (userAgent.contains("Mac OS X")) {
            /**
             * ******** 苹果系列 ******** <br>
             * iPod - Mozilla/5.0 (iPod; U; CPU iPhone OS 4_3_1 like Mac OS X; zh-cn) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2
             * Mobile/8G4 Safari/6533.18.5<br>
             * iPad - Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4
             * Mobile/7B334b Safari/531.21.10 <br>
             * iPad2 - Mozilla/5.0 (iPad; CPU OS 5_1 like Mac OS X; en-us) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9B176
             * Safari/7534.48.3 <br>
             * iPhone 4 - Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_0 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko)
             * Version/4.0.5 Mobile/8A293 Safari/6531.22.7 <br>
             * iPhone 5 - Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1
             * Mobile/9A334 Safari/7534.48.3 <br>
             * 判断依据:http://www.useragentstring.com/pages/Safari/ <br>
             * 参考:http://stackoverflow .com/questions/7825873/what-is-the-ios-5-0-user-agent-string <br>
             * 参考: http ://stackoverflow.com/questions/3105555/what-is-the-iphone-4 -user-agent
             */
            if (userAgent.contains("iPod")) { // iPod,ua中包含iPhone OS
                return judgeBrowser(userAgent, "iPod", null, null);// 判断浏览器
            } else if (userAgent.contains("iPhone OS")) { // 苹果手机
                String platformSeries = null;

                Matcher m = IPGONE_OS.matcher(userAgent);
                if (m.find() == true) {
                    platformSeries = m.group(1).replaceAll("_", ".");
                }
                return judgeBrowser(userAgent, "iPhone", platformSeries, null);// 判断浏览器
            } else if (userAgent.contains("iPad")) { // iPad
                String platformSeries = null;

                Matcher m = IPAD_OS.matcher(userAgent);
                if (m.find() == true) {
                    platformSeries = m.group(2).replaceAll("_", ".");
                }
                return judgeBrowser(userAgent, "iPad", platformSeries, null);// 判断浏览器
            } else {
                String platformSeries = null;

                Matcher m = MAC_OS.matcher(userAgent);
                if (m.find() == true) {
                    platformSeries = m.group(1).replaceAll("_", ".");
                }
                return judgeBrowser(userAgent, "Mac OS X", platformSeries, null);// 判断浏览器
            }
        } else if (userAgent.contains("Linux")) {
            if (userAgent.contains("Android")) {
                String platformSeries = null;
                Matcher m = ANDROID_OS.matcher(userAgent);
                if (m.find() == true) {
                    platformSeries = m.group().replaceAll("Android ", "");
                }
                return judgeBrowser(userAgent, "Android", platformSeries, null);
            } else if (userAgent.contains("Ubuntu")) {
                return judgeBrowser(userAgent, "Ubuntu", null, null);
            } else {
                return judgeBrowser(userAgent, "Linux", null, null);
            }
        } else if (userAgent.contains("Baiduspider")) {
            return judgeBrowser(userAgent, "Baiduspider", null, null);
        }
        return null;
    }

    /**
     * 用途：根据客户端 User Agent Strings 判断其浏览器 if 判断的先后次序： 根据浏览器的用户使用量降序排列，这样对于大多数用户来说可以少判断几次即可拿到结果： >>Browser:Chrome > FF > IE > ...
     * 
     * @param userAgent
     *            :user agent
     * @param platformType
     *            :平台
     * @param platformSeries
     *            :系列
     * @param platformVersion
     *            :版本
     * @return
     */
    private static UserAgentModel judgeBrowser(String userAgent, String platformType, String platformSeries, String platformVersion) {
        BrowserModel browserModel = BrowserUtils.getBrowserModel(userAgent);
        if (browserModel != null) {
            return new UserAgentModel(browserModel.getBrowserType(), browserModel.getBrowserVersion(), platformType, platformSeries,
                    platformVersion);
        } else {// 暂时支持以上.其它浏览器,待续...
            return new UserAgentModel(null, null, platformType, platformSeries, platformVersion);
        }
    }

    public static void main(String[] args) {
        Map<String, String> uaMap = new LinkedHashMap<>();
        String ua = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36";
        uaMap.put("mac os", ua);
        // iPhone 4.3.2 系统：
        ua = "Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_3_2 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8H7 Safari/6533.18.5";
        uaMap.put("iPhone 4.3.2 系统", ua);
        // iPone 5.1 系统：
        ua = "Mozilla/5.0 (iPhone; U; CPU iPhone OS 5_1 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8H7 Safari/6533.18.5";
        uaMap.put("iPone 5.1 系统", ua);
        // iPone 5.1.1 系统：
        ua = "Mozilla/5.0 (iPhone; U; CPU iPhone OS 5_1_1 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8H7 Safari/6533.18.5";
        uaMap.put("iPone 5.1.1 系统", ua);
        uaMap.put("iPod",
                "Mozilla/5.0 (iPod; U; CPU iPhone OS 4_3_1 like Mac OS X; zh-cn) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8G4 Safari/6533.18.5");
        uaMap.put("iPad",
                "Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.10");
        uaMap.put("iPad2",
                "Mozilla/5.0 (iPad; CPU OS 5_1 like Mac OS X; en-us) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9B176 Safari/7534.48.3");
        uaMap.put("iPhone 4",
                "Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_0 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobile/8A293 Safari/6531.22.7");
        uaMap.put("iPhone 5",
                "Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3");
        uaMap.put("Firefox 68", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:68.0) Gecko/20100101 Firefox/68.0");
        uaMap.put("sougou -ie11", "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0; SE 2.X MetaSr 1.0) like Gecko");
        uaMap.put("sougou -ie8",
                "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 10.0; WOW64; Trident/7.0; .NET4.0C; .NET4.0E; InfoPath.3; .NET CLR 2.0.50727; .NET CLR 3.0.30729; .NET CLR 3.5.30729)");
        uaMap.put("sougou",
                "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 SE 2.X MetaSr 1.0");
        ua = "Mozilla/5.0 (Linux; Android 5.1.1; Nexus 6 Build/LYZ28E) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Mobile Safari/537.3636";
        uaMap.put("Andorid", ua);

        ua = " Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0";
        uaMap.put("Ubuntu", ua);

        ua = " Mozilla/5.0 (compatible; Baiduspider-render/2.0; +http://www.baidu.com/search/spider.html)";
        uaMap.put("Baiduspider", ua);

        for (Map.Entry<String, String> entry : uaMap.entrySet()) {
            System.out.println(entry.getKey() + "==" + getUserAgent(entry.getValue()));
        }
    }
}
