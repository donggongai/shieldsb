package com.ld.shieldsb.common.core.io.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;

import lombok.extern.slf4j.Slf4j;

/**
 * 合并处理Runnable
 *
 * @author yjmyzz@126.com
 */
@Slf4j
public class MergeRunnable implements Runnable {
    private long startPos;
    private String mergeFileName;
    private File partFile;

    /**
     * 合并文件，
     * 
     * @Title 构造函数
     * 
     * @Description
     * @author 吕凯
     * @date 2018年3月31日 下午1:21:26
     * @param startPos
     *            开始位置
     * @param mergeFileName
     *            合并到的文件
     * @param partFile
     *            需要合并的文件
     */
    public MergeRunnable(long startPos, String mergeFileName, File partFile) {
        this.startPos = startPos;
        this.mergeFileName = mergeFileName;
        this.partFile = partFile;
    }

    public void run() {
        try (RandomAccessFile rFile = new RandomAccessFile(mergeFileName, "rw"); FileInputStream fs = new FileInputStream(partFile);) {
            rFile.seek(startPos);

            byte[] b = new byte[fs.available()];
//            fs.read(b);
            while (fs.read(b) > 0) {
                rFile.write(b);
            }
        } catch (IOException e) {
            log.error("", e);
        }
    }
}