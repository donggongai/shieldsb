package com.ld.shieldsb.common.core.cache.dataloader;

/**
 * IDataLoader.
 * <p>
 * Example:
 * 
 * <pre>
 * List&lt;Blog&gt; blogList = EhCacheKit.handle(&quot;blog&quot;, &quot;blogList&quot;, new IDataLoader() {
 *     public Object load() {
 *         return dao.find(&quot;select * from blog&quot;);
 *     }
 * });
 * </pre>
 */
public interface IDataLoader {
    // 根据key加载数据，需要自己写具体的实现类
    public Object load(Object key);
}