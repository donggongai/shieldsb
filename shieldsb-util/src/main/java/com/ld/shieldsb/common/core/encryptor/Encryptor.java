package com.ld.shieldsb.common.core.encryptor;

/**
 * 加密器接口，有加密解密方法
 * 
 * @ClassName Encryptor
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2018年12月5日 下午2:51:45
 *
 */
public interface Encryptor {

    /**
     * 加密字符串
     *
     * @param value
     *            The clear text attribute
     * @return The encrypted attribute, or null
     */
    String encrypt(String value);

    /**
     * 解密字符串
     *
     * @param value
     *            The encrypted attribute in Base64 encoding
     * @return The clear text attribute, or null
     */
    String decrypt(String value);

    /**
     * 设置密钥
     *
     * @param key
     *            The encryption key
     */
    void setKey(String key);

    /**
     * 验证加密解密字符是否匹配
     * 
     * @Title match
     * @author 吕凯
     * @date 2018年12月5日 下午3:25:31
     * @param plaintext
     * @param hashed
     * @param salt
     * @return boolean
     */
    boolean match(String plaintext, String hashed, String salt) throws Exception;

}
