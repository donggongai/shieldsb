package com.ld.shieldsb.common.core.util;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;

import org.apache.commons.lang.StringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 获取路径相关
 * 
 * @ClassName PathUtil
 * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
 * @date 2016年8月25日 下午3:32:26
 *
 */
@Slf4j
public class PathUtil {
    public static final String PREFERENCE_FILE_DIR = ".shieldsb";

    public static String getClassRootPath() {
        return getClassRootPath(false);
    }

    /**
     * 
     * 获取class路径绝对路径（class目录），如：xxx\target\classes
     * 
     * @Title getProjectAbsolutePath
     * @return String
     */
    public static String getClassRootPath(boolean checkJar) {
        URL url = PathUtil.class.getResource("/");
        if (url == null) {
            log.warn("获取项目绝对路径出错，返回空");
            return "";
        }
        String path = url.getPath();
        try {
            path = URLDecoder.decode(path, "utf-8"); // 中文等特殊字符解码处理
        } catch (UnsupportedEncodingException e) {
            log.error("#获取项目绝对路径出错：转码失败！", e);
        }
        if (new File(path).exists()) {
            return path;
        }
        if (checkJar) { // 检查jar包，如果为jar包则返回user.dir工作目录
            return getUserDir();
        }
        return "";
    }

    /**
     * 获取传入类的所在工程路径; 如果不加“/” 获取当前类的加载目录 D:\git\daotie\daotie\target\classes\my
     * 
     * @Title getThisClassAbsolutePath
     * @author 吕凯
     * @date 2019年1月18日 下午12:16:41
     * @return String
     */
    public static String getThisClassPath(Class<?> cls) {
        URL url = cls.getResource("");
        String path = url.getPath();
        try {
            path = URLDecoder.decode(path, "utf-8"); // 中文等特殊字符解码处理
        } catch (UnsupportedEncodingException e) {
            log.error("#获取项目绝对路径出错：转码失败！", e);
        }
        if (new File(path).exists()) {
            return path;
        }
        return "";
    }

    /**
     * 获取java的class路径
     * 
     * @Title getClassPath
     * @author 吕凯
     * @date 2019年1月18日 下午12:10:20
     * @return String
     */
    public static String getClassPath() {
        return getSystemProperty("java.class.path");
    }

    /**
     * 
     * 获取项目路径 ,到项目执行路径，如：/home/ubuntu/tomcat/work-tomcat-8.5.34-8800/bin，Eclipse下启动tomcat时得到的是Eclipse的根目录
     * 
     * @Title getProjectPath
     * @author 吕凯
     * @date 2019年1月18日 下午12:04:46
     * @return String
     */
    public static String getProjectPath() {
        return getSystemProperty("user.dir");
    }

    public static String getSystemProperty(String property) {
        try {
            return System.getProperty(property);
        } catch (SecurityException ex) {
            // we are not allowed to look at this property
            log.warn("Caught a SecurityException reading the system property '" + property
                    + "'; the SystemUtils property value will default to null.");
            return null;
        }
    }

    public static String getUserHome() {
        return getSystemProperty("user.home");
    }

    public static String getUserDir() {
        return getSystemProperty("user.dir");
    }

    public static String getTmpDir() {
        return getSystemProperty("java.io.tmpdir");
    }

    /**
     * 获取类的真实路径
     * 
     * @Title getClassPackageDir
     * @author 于国帅
     * @date 2018年9月12日 下午5:40:30
     * @param clazz
     * @return String
     */
    public static String getClassPackageDir(Class<?> clazz) {
        if (clazz != null) {
            String classPackageName = clazz.getPackage().toString().replaceFirst("package ", "");
            return StringUtils.replace(classPackageName, ".", File.separator);
        }
        return null;
    }

    public static String getClassPackage(Class<?> clazz) {
        if (clazz != null) {
            return clazz.getPackage().toString().replaceFirst("package ", "");
        }
        return null;
    }

    public static String getPackageDir(String packageName) {
        if (packageName != null) {
            return StringUtils.replace(packageName, ".", File.separator);
        }
        return null;
    }

    /**
     * 获取shieldsb配置文件目录
     * 
     * @Title getShieldsbPreferenceDir
     * @author 吕凯
     * @date 2018年12月21日 下午2:10:03
     * @return String
     */
    public static File getShieldsbPreferenceDir() {
        String parDir = getUserHome();
        File prefDirFile = new File(parDir, PREFERENCE_FILE_DIR);
        if (!prefDirFile.exists()) {
            prefDirFile.mkdirs();
        }
        return prefDirFile;
    }

}
