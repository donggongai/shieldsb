package com.ld.shieldsb.common.core.cache.dataloader;

import com.ld.shieldsb.common.core.ServiceFactory;

/**
 * 数据加载器的享元工厂
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月27日 上午9:42:46
 *
 */
public class DataLoaderFactory extends ServiceFactory<IDataLoader> {

    public static DataLoaderFactory getInstance() {
        return (DataLoaderFactory) getInstance(DataLoaderFactory.class);
    }

    public IDataLoader getService(Class<? extends IDataLoader> dataLoaderClass) {
        return super.getService(dataLoaderClass.getName());
    }

}
