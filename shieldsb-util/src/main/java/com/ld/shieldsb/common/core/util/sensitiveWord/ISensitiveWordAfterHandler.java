package com.ld.shieldsb.common.core.util.sensitiveWord;

/**
 * 
 * 敏感词后置处理接口
 * 
 * @ClassName ISensitiveWordAfterHandler
 * @author 刘金浩
 * @date 2021年11月12日 上午9:30:43
 *
 */
public interface ISensitiveWordAfterHandler {

    public <T> SensitiveWordResult handler(SensitiveWordResult result, T modelBean);

}
