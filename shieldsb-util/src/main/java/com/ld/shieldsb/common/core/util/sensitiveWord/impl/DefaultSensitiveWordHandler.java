package com.ld.shieldsb.common.core.util.sensitiveWord.impl;

import com.ld.shieldsb.common.core.util.SensitiveWordUtil;
import com.ld.shieldsb.common.core.util.sensitiveWord.SensitiveWordHandler;
import com.ld.shieldsb.common.core.util.sensitiveWord.SensitiveWordResult;

public class DefaultSensitiveWordHandler implements SensitiveWordHandler {

    public SensitiveWordResult deal(String value, String replaceChar) {
        SensitiveWordResult result = new SensitiveWordResult();
        boolean contains = SensitiveWordUtil.contains(value);
        if (contains) {
            String sensitiveWord = SensitiveWordUtil.getSensitiveWordStr(value.toString());
            result.setSensitiveWordStr(sensitiveWord);
            result.setFilteredContent(SensitiveWordUtil.replaceSensitiveWord(value, replaceChar));
        }
        result.setOriginalContent(value);
        result.setContains(contains);
        return result;
    }
}
