package com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard;

import com.ld.shieldsb.common.core.util.notice.wechat.EweChatTool;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 二级标题+文本列表数组的内容
 * 
 * @ClassName TemplateCardHorizontalContentModel
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年9月13日 上午8:32:43
 *
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class TmplCardHorizontalContentModel {
    private Integer type; // 链接类型，0或不填代表不是链接，1 代表跳转url，2 代表下载附件
    private String keyname; // 二级标题，建议不超过5个字
    private String value; // 二级文本，如果horizontal_content_list.type是2，该字段代表文件名称（要包含文件类型），建议不超过30个字

    private String url; // 链接跳转的url，horizontal_content_list.type是1时必填
    private String mediaId; // 附件的media_id，horizontal_content_list.type是2时必填

    public static TmplCardHorizontalContentModel build() {
        return TmplCardHorizontalContentModel.builder().build();
    }

    /**
     * 构造链接为url的对象
     * 
     * @Title buildUrl
     * @author 吕凯
     * @date 2021年9月13日 上午10:34:54
     * @param keyname
     * @param value
     * @param url
     * @return TmplCardHorizontalContentModel
     */
    public static TmplCardHorizontalContentModel buildUrl(String keyname, String value, String url) {
        return TmplCardHorizontalContentModel.builder().type(EweChatTool.ACTION_TYPE_URL).keyname(keyname).value(value).url(url).build();
    }

    /**
     * 构造链接为下载的对象
     * 
     * @Title buildDownload
     * @author 吕凯
     * @date 2021年9月13日 上午10:35:05
     * @param keyname
     * @param value
     * @param mediaId
     * @return TmplCardHorizontalContentModel
     */
    public static TmplCardHorizontalContentModel buildDownload(String keyname, String value, String mediaId) {
        return TmplCardHorizontalContentModel.builder().type(EweChatTool.LINK_TYPE_DOWNLOAD).keyname(keyname).value(value).mediaId(mediaId)
                .build();
    }

    /**
     * 构造普通文本对象
     * 
     * @Title build
     * @author 吕凯
     * @date 2021年9月13日 上午10:35:17
     * @param keyname
     * @param value
     * @return TmplCardHorizontalContentModel
     */
    public static TmplCardHorizontalContentModel build(String keyname, String value) {
        return TmplCardHorizontalContentModel.builder().keyname(keyname).value(value).build();
    }

    public static TmplCardHorizontalContentModel build(Integer type, String keyname, String value, String url, String mediaId) {
        return TmplCardHorizontalContentModel.builder().type(type).keyname(keyname).value(value).url(url).mediaId(mediaId).build();
    }

}
