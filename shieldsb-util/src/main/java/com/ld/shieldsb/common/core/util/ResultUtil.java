package com.ld.shieldsb.common.core.util;

import com.ld.shieldsb.common.core.model.Result;

public class ResultUtil {
    public static Result success(String msg) {
        return success(msg, null);
    }

    public static Result success(String msg, Object object) {
        Result result = new Result();
        result.setSuccess(true);
        result.setMessage(msg);
        result.setData(object);
        return result;
    }

    public static Result error(String msg) {
        return error(msg, null);
    }

    public static Result error(String msg, Object object) {
        Result result = new Result();
        result.setSuccess(false);
        result.setMessage(msg);
        result.setData(object);
        return result;
    }

    /**
     * 返回结果1
     * 
     * @Title result
     * @author 吕凯
     * @date 2018年6月19日 上午10:15:35
     * @param result
     * @return Result
     */
    public static Result result(Result result) {
        if (result == null) {
            result = new Result();
            result.setSuccess(false);
        }
        return result;
    }

}
