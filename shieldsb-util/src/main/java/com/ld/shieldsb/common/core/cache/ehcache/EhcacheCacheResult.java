package com.ld.shieldsb.common.core.cache.ehcache;

import java.io.Serializable;

import org.ehcache.Cache;

import com.ld.shieldsb.common.core.cache.CacheResult;

import lombok.Data;

@Data
public class EhcacheCacheResult implements CacheResult {
    private Cache<String, Serializable> cache;
}
