package com.ld.shieldsb.common.core.cache;

import com.ld.shieldsb.common.core.model.PropertiesModel;

/**
 * 缓存参数工具类，在config.properties中配置shielsb.cache=xx,xx可选值有google_guava、ehcache、redis，也可自己实现 <br>
 * <b> 使用说明：</b><br>
 * <ul>
 * <li>在config.properties中配置shielsb.cache=xx,xx可选值有google_guava、ehcache、redis，也可自己实现</li>
 * <li>配置缓存的过期时间，有两级缓存，默认不使用二级缓存。在config.properties中配置cache_1_expseconds=30设置1级缓存的过期时间（通用设置），<br>
 * 也可以直接指定调用缓存类型的过期时间:cache_1_expseconds_google_guava_first=30 <br>
 * 格式：<b>cache_缓存级别(1、2)_expseconds_缓存类型（google_guava、redis）_缓存域（自定义）</b>，即google的first一级缓存超时时间30秒，first为自定义名称，必须指定，用于区分不同的缓存域<br>
 * <b>除ehcache外如果缓存时间不设置，默认1级缓存1天，2级缓存2天过期，ehcache需要配置ehcache.xml文件</b></li>
 * <li>调用 CacheUtil.put 和 CacheUtil.get 设置、获取缓存</li>
 * </ul>
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月27日 上午9:15:15
 *
 */
public class CacheConfigUtil {
    public static final String FIRST = "first";
    public static final String FIRST_LEVEL2 = "l2first";
    public static final String SECOND = "second";
    public static final String SECOND_LEVEL2 = "l2second";
    public static final String THIRD = "third";
    public static final String THIRD_LEVEL2 = "l2third";
    public static final String FOURTH = "fourth";
    public static final String FOURTH_LEVEL2 = "l2fourth";
    public static final String FIFTH = "fifth";
    public static final String FIFTH_LEVEL2 = "l2fifth";
    public static final String SIXTH = "sixth";
    public static final String SIXTH_LEVEL2 = "l2sixth";

    public static final String CACHE_NOT_EXISTS = "缓存不存在";

    private CacheConfigUtil() {
        throw new IllegalStateException("该类不可初始化");
    }

    /**
     * 默认过时时间,单位秒，1天
     */
    public static final int DEFAULT_EXP_SECONDS = 86400;

    /**
     * 获取缓存超时时间（秒）
     * 
     * @Title getCacheExpTimes
     * @author 吕凯
     * @date 2019年7月6日 上午11:27:28
     * @param cacheName
     *            缓存名称（自定义名称）
     * @param cacheType
     *            缓存类型，redis、ehcache等
     * @param level
     *            缓存级别
     * @param defaultseconds
     *            默认值
     * @return Integer
     */
    public static Integer getCacheExpTimes(String cacheName, String cacheType, int level, int defaultseconds) {
        // cache_1_expseconds_google_guava_first=30 即google的first一级缓存超时时间30秒，first为自定义名称
        // cache_2_expseconds_google_guava_first=300 即google的first一级缓存超时时间300秒
        // cache_1_expseconds_google_guava_second=30 即google的second一级缓存超时时间30秒
        // cache_1_expseconds_redis_first=30 即redis的first一级缓存超时时间30秒
        // cache_1_expseconds=30 即默认一级缓存超时时间30秒
        Integer cacheExpTimes = PropertiesModel.CONFIG.getInt("cache_" + level + "_expseconds_" + cacheType + "_" + cacheName, -1); // 优先找级别高的
        if (cacheExpTimes == -1) {
            return PropertiesModel.CONFIG.getInt("cache_" + level + "_expseconds", defaultseconds); // 没有则找默认
        }
        return cacheExpTimes;
    }

}
