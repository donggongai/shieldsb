package com.ld.shieldsb.common.core.util.notice.wechat.application.textcard;

import java.util.HashMap;
import java.util.Map;

import com.ld.shieldsb.common.core.model.Result;
import com.ld.shieldsb.common.core.util.StringUtils;
import com.ld.shieldsb.common.core.util.notice.wechat.EweChatTool;
import com.ld.shieldsb.common.core.util.notice.wechat.Type;
import com.ld.shieldsb.common.core.util.notice.wechat.application.EweChatApplicationParams;
import com.ld.shieldsb.common.core.util.notice.wechat.application.EwechatBasicMsgSender;

/**
 * 文本卡片消息发送器
 * 
 * @ClassName TextCardMsgSender
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年9月14日 上午9:38:41
 *
 */
public class TextCardMsgSender extends EwechatBasicMsgSender {

    // ============================================= 文本卡片 ==============================================
    /**
     * 转换文本卡片dataMap， 特殊说明：<br>
     * 卡片消息的展现形式非常灵活，支持使用br标签或者\n来进行换行处理，也支持使用div标签来使用不同的字体颜色，<br>
     * 目前内置了3种文字颜色：灰色(gray)、高亮(highlight)、默认黑色(normal)，将其作为div标签的class属性即可，具体用法请参考上面的示例。
     * 
     * @Title parse2TextCardDataMap
     * @author 吕凯
     * @date 2021年4月19日 下午5:02:59
     * @param agentid
     * @param sendTo
     * @param title
     * @param msg
     * @param url
     * @param btnTxt
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Map<String,Object>
     */

    public static Map<String, Object> parse2TextCardDataMap(String agentid, String sendTo, String title, String msg, String url,
            String btnTxt, Integer duplicateCheckInterval) {
        /*{
            "touser" : "UserID1|UserID2|UserID3",
            "toparty" : "PartyID1 | PartyID2",
            "totag" : "TagID1 | TagID2",
            "msgtype" : "textcard",
            "agentid" : 1,
            "textcard" : {
                "title" : "领奖通知",
                "description" : "<div class=\"gray\">2016年9月26日</div> <div class=\"normal\">恭喜你抽中iPhone 7一台，领奖码：xxxx</div><div class=\"highlight\">请于2016年10月10日前联系行政同事领取</div>",
                "url" : "URL",
                "btntxt":"更多"
            },
            "enable_id_trans": 0,
            "enable_duplicate_check": 0,
            "duplicate_check_interval": 1800
         }*/

        Map<String, Object> dataMap = defaultDataMap(agentid, sendTo, duplicateCheckInterval);

        dataMap.put(EweChatTool.MSG_TYPE, Type.TEXTCARD.value);

        Map<String, String> contentMap = new HashMap<>();
        contentMap.put(TITLE, title); // 标题，不超过128个字节，超过会自动截断（支持id转译）
        contentMap.put("description", msg); // 描述，不超过512个字节，超过会自动截断（支持id转译）
        contentMap.put("url", url); // 点击后跳转的链接。最长2048字节，请确保包含了协议头(http/https)
        if (StringUtils.isNotEmpty(btnTxt)) {
            contentMap.put("btntxt", btnTxt); // 按钮文字。 默认为“详情”， 不超过4个文字，超过自动截断。
        }
        dataMap.put(Type.TEXTCARD.value, contentMap);

        return dataMap;
    }

    public static Map<String, Object> parse2TextCardDataMap(String agentid, String sendTo, String title, String msg, String url,
            String btnTxt) {
        return parse2TextCardDataMap(agentid, sendTo, title, msg, url, btnTxt, null);
    }

    public static Result sendTextCardMsg(String corpid, String corpsecret, String agentid, String sendTo, String title, String msg,
            String url, String btnTxt) {
        return sendTextCardMsg(corpid, corpsecret, agentid, sendTo, title, msg, url, btnTxt, null);

    }

    public static Result sendTextCardMsg(String corpid, String corpsecret, String agentid, String sendTo, String title, String msg,
            String url, String btnTxt, Integer duplicateCheckInterval) {
        return sendMsg(corpid, corpsecret, agentid,
                parse2TextCardDataMap(agentid, sendTo, title, msg, url, btnTxt, duplicateCheckInterval));

    }

    /**
     * 发送文本卡片消息
     * 
     * @Title parse2NewsDataMap
     * @author 吕凯
     * @date 2021年4月19日 下午4:32:10
     * @param sendTo
     * @param title
     * @param msg
     * @param url
     * @param btnTxt
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Result
     */
    public static Result sendTextCardMsg(String sendTo, String title, String msg, String url, String btnTxt,
            Integer duplicateCheckInterval) {
        // 注意下面参数需要到企业微信中获取
        EweChatApplicationParams params = getConfig();
        Result flag = sendTextCardMsg(params.getCorpId(), params.getCorpSecret(), params.getAgentId(), sendTo, title, msg, url, btnTxt,
                duplicateCheckInterval);
        return flag;
    }

    public static Result sendTextCardMsg(String sendTo, String title, String msg, String url, String btnTxt) {
        return sendTextCardMsg(sendTo, title, msg, url, btnTxt, null);
    }

    public static Result sendTextCardMsg(String sendTo, String title, String msg, String url, Integer duplicateCheckInterval) {
        Result flag = sendTextCardMsg(sendTo, title, msg, url, null, duplicateCheckInterval);
        return flag;
    }

    public static Result sendTextCardMsg(String sendTo, String title, String msg, String url) {
        Result flag = sendTextCardMsg(sendTo, title, msg, url, null, null);
        return flag;
    }

}
