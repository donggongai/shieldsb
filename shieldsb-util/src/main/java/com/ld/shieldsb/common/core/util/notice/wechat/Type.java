package com.ld.shieldsb.common.core.util.notice.wechat;

public enum Type {
    /*  自定义机器人支持文本（text）、图片（image）、语音（voice）、视频（video）、文件（file）、文本卡片（textcard）、图文（news）、图文（mpnews）、markdown（markdown）、小程序（miniprogram_notice）、任务卡片消息（interactive_taskcard）仅企业微信3.1.6及以上版本支持消息类型。
                            支持id转译，将userid/部门id转成对应的用户名/部门名，目前仅文本/文本卡片/图文/图文（mpnews）这四种消息类型的部分字段支持。
                            仅第三方应用需要用到，企业自建应用可以忽略。具体支持的范围和语法如下：
        1.支持的消息类型和对应的字段
                    消息类型    支持字段
                    文本（text）    content
                    文本卡片（textcard）  title、description
                    图文（news）    title、description
                    图文（mpnews）  title、digest、content
                    任务卡片（taskcard）  title、description
                    小程序通知（miniprogram_notice）   title、description、content_item.value    
                    
        2.id转译模版语法
    $departmentName=DEPARTMENT_ID$
    $userName=USERID$
                其中 DEPARTMENT_ID 是数字类型的部门id，USERID 是成员帐号。
                譬如，
                将$departmentName=1$替换成部门id为“1”对应的部门名，如“企业微信产品部”；
                将$userName=lisi007$替换成userid为“lisi007”对应的用户名，如“李四”；
                                 */

    /*文本*/TEXT("text"), IMAGE("image"), VOICE("voice"), VIDEO("video"), /*文件*/FILE("file"), TEXTCARD("textcard"), /*图文*/ NEWS(
            "news"), MPNEWS("mpnews"), MARKDOWN("markdown"), /*小程序*/MINIPROGRAM_NOTICE(
                    "miniprogram_notice"), /*任务卡片，仅企业微信3.1.6及以上版本支持*/INTERACTIVE_TASKCARD(
                            "interactive_taskcard"), /*模板卡片，投票选择型和多项选择型卡片仅企业微信3.1.12及以上版本支持
                                                     文本通知型、图文展示型和按钮交互型三种卡片仅企业微信3.1.6及以上版本支持（但附件下载功能仍需更新至3.1.12）*/TEMPLATE_CARD(
                                    "template_card"), RECALL("recall");

    public final String value;

    Type(final String val) {
        this.value = val;
    }

    public String getValue() {
        return this.value;
    }

}