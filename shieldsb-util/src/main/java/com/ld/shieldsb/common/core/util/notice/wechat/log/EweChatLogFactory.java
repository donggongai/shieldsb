package com.ld.shieldsb.common.core.util.notice.wechat.log;

public class EweChatLogFactory {

    private EweChatLogFactory() {
        // 工具类，不提供构造方法
    }

    private static IEweChatLogService service;

    public static void setService(IEweChatLogService service) {
        EweChatLogFactory.service = service;
    }

    public static IEweChatLogService getService() {
        return service;
    }

    public static boolean saveLog(EweChatLogModel logModel) {
        IEweChatLogService service = getService();
        if (service != null) {

            return service.saveLog(logModel);
        }
        return false;
    }

}
