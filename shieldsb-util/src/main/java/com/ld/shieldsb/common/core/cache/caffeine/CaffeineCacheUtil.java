package com.ld.shieldsb.common.core.cache.caffeine;

import com.github.benmanes.caffeine.cache.Cache;
import com.ld.shieldsb.common.core.cache.CacheResult;
import com.ld.shieldsb.common.core.cache.dataloader.IDataLoader;

public class CaffeineCacheUtil {
    private static final CaffeineCache CACHE = new CaffeineCache();

    private CaffeineCacheUtil() {
        throw new IllegalStateException("该类不可初始化");
    }

    public static CaffeineCacheResult<Object, Object> getCache(String cacheName) { // 有线程安全问题，但是不影响最终效果，后期优化
        return CACHE.getCache(cacheName);
    }

    /**
     * 创建缓存
     * 
     * @Title createCache
     * @author 吕凯
     * @date 2019年10月15日 上午11:51:50
     * @param cacheName
     * @param duration
     *            缓存有效期，单位秒
     * @return Cache<Object,Object>
     */
    public static CacheResult createCache(String cacheName, long duration) { // 有线程安全问题，但是不影响最终效果，后期优化
        return CACHE.createCache(cacheName, duration);
    }

    public static boolean put(String cacheName, String key, Object value) {
        return CACHE.put(cacheName, key, value);
    }

    public static boolean put(Cache<Object, Object> cache, String key, Object value) {
        return CACHE.put(cache, key, value);
    }

    public static <T> T get(String cacheName, String key) {
        return CACHE.get(cacheName, key);
    }

    public static <T> T get(String cacheName, String key, Class<? extends IDataLoader> dataLoaderClass) {
        return CACHE.get(cacheName, key, dataLoaderClass);
    }

    public static boolean remove(String cacheName, String key) {
        return CACHE.remove(cacheName, key);
    }

    public static boolean removeAll(String cacheName) {
        return CACHE.removeAll(cacheName);
    }

}
