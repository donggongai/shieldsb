package com.ld.shieldsb.common.core.util.useragent.impl;

import java.util.ArrayList;
import java.util.List;

import com.ld.shieldsb.common.core.util.useragent.BrowserHandler;
import com.ld.shieldsb.common.core.util.useragent.BrowserModel;
import com.ld.shieldsb.common.core.util.useragent.BrowserUtils;

public class DefaultBrowserHandler implements BrowserHandler {
    private static final List<String> BROWS_LIST = new ArrayList<>();

    static {
        BROWS_LIST.add(BrowserUtils.OPERA);
        BROWS_LIST.add(BrowserUtils.BAIDU);
        BROWS_LIST.add(BrowserUtils.UC);
        BROWS_LIST.add(BrowserUtils.QQ);
        BROWS_LIST.add(BrowserUtils.MICRO_MESSENGER);
        BROWS_LIST.add(BrowserUtils.WEIBO);
        BROWS_LIST.add(BrowserUtils.SE360);
        BROWS_LIST.add(BrowserUtils.MAXTHON);
        BROWS_LIST.add(BrowserUtils.GREEN);
    }

    @Override
    public BrowserModel deal(String userAgent) {
        BrowserModel browserModel = new BrowserModel();
        for (String browser : BROWS_LIST) {
            if (BrowserUtils.matchIn(browser, userAgent)) {
                browserModel.setBrowserType(browser);
                browserModel.setBrowserVersion(BrowserUtils.getBrowserVersion(browser, userAgent));
                break;
            }
        }
        if (browserModel.getBrowserType() == null) {
            browserModel.setBrowserType(BrowserUtils.OTHER);
        }
        return browserModel;
    }

    @Override
    public boolean checkBrowse(String userAgent) {
        return true; // 默认都走这个
    }

}
