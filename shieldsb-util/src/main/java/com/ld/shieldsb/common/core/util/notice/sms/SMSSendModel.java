package com.ld.shieldsb.common.core.util.notice.sms;

import lombok.Data;

/**
 * 发送短信对象实体类
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2020年6月10日 下午3:03:37
 *
 */
@Data
public final class SMSSendModel {

    private String phoneNum; // 手机号,只发一个号码：13800000001。发多个号码：13800000001,13800000002,...N 。使用半角逗号分隔
    private String message; // 发送的信息
    private String signName; // 签名

}
