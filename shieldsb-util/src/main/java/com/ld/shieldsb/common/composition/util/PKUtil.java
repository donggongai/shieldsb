package com.ld.shieldsb.common.composition.util;

import com.ld.shieldsb.common.core.util.SnowflakeIdWorker;

/**
 * 主键工具类
 * 
 * @ClassName PkUtils
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2018年11月16日 下午4:58:41
 *
 */
public class PKUtil {
    public static final SnowflakeIdWorker ID_WORKER = new SnowflakeIdWorker(0, 0);

    public static final long nextId() {
        return ID_WORKER.nextId();
    }

    private PKUtil() {
        throw new IllegalStateException("该类不可初始化");
    }

}
