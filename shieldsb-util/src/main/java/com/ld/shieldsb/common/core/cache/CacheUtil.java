package com.ld.shieldsb.common.core.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import com.ld.shieldsb.common.core.cache.caffeine.CaffeineCache;
import com.ld.shieldsb.common.core.cache.ehcache.EhcacheCache;
import com.ld.shieldsb.common.core.cache.google.GoogleCache;
import com.ld.shieldsb.common.core.cache.redis.RedisCache;
import com.ld.shieldsb.common.core.model.PropertiesModel;
import com.ld.shieldsb.common.core.util.StackTraceUtil;
import com.ld.shieldsb.common.core.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 缓存工具类，在config.properties中配置shielsb.cache=xx,xx可选值有google_guava、ehcache、redis，也可自己实现 <br>
 * <b> 使用说明：</b><br>
 * <ul>
 * <li>在config.properties中配置shielsb.cache=xx,xx可选值有google_guava、ehcache、redis，也可自己实现</li>
 * <li>配置缓存的过期时间，有两级缓存，默认不使用二级缓存。在config.properties中配置cache_1_expseconds=30设置1级缓存的过期时间（通用设置），<br>
 * 也可以直接指定调用缓存类型的过期时间:cache_1_expseconds_google_guava_first=30 <br>
 * 格式：<b>cache_缓存级别(1、2)_expseconds_缓存类型（google_guava、redis）_缓存域（自定义）</b>，即google的first一级缓存超时时间30秒，first为自定义名称，必须指定，用于区分不同的缓存域<br>
 * <b>除ehcache外如果缓存时间不设置，默认1级缓存1天，2级缓存2天过期，ehcache需要配置ehcache.xml文件</b></li>
 * <li>调用 CacheUtil.put 和 CacheUtil.get 设置、获取缓存</li>
 * </ul>
 * 
 * @ClassName CacheUtil
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2017年7月28日 上午11:18:30
 *
 */
@Slf4j
public class CacheUtil {
    private static final String CANNOT_USE = "缓存设置为不可用！";
    protected static final Map<String, Class<? extends Cache>> CACHE_MAP = new ConcurrentHashMap<>();
    protected static final Map<String, Function<Cache, ?>> CACHE_INITFN_MAP = new ConcurrentHashMap<>();

    private static volatile Boolean canUse = false;
    private static final String KEY_FIRST_LEVEL_EXP_SECONDS = "cache_%s_firstLevelExpseconds"; // 1级缓存前缀
    private static final String KEY_SECOND_LEVEL_EXP_SECONDS = "cache_%s_secondLevelExpseconds"; // 2级缓存前缀

    static {
        // 注意工厂类不要一开始就注册对象，否则可能缺少必要的jar包不能实例化报错，应该注册类
        // 其他自定义的缓存可通过此类扩展
        registerCache(GoogleCache.CACHE_TYPE, GoogleCache.class, CacheUtil::initGoogleCache); // google_guava
        registerCache(EhcacheCache.CACHE_TYPE, EhcacheCache.class, CacheUtil::initEhcacheCache); // ehcache
        registerCache(RedisCache.CACHE_TYPE, RedisCache.class, CacheUtil::initRedisCache); // redis
        registerCache(CaffeineCache.CACHE_TYPE, CaffeineCache.class, CacheUtil::initCaffeineCache); // Caffeine
    }

    private CacheUtil() {
        throw new IllegalStateException("该类不可初始化");
    }

    public static void registerCache(String cacheType, Class<? extends Cache> cacheClass) {
        registerCache(cacheType, cacheClass, null);
    }

    public static void registerCache(String cacheType, Class<? extends Cache> cacheClass, Function<Cache, ?> initFn) {
        // 注意工厂类不要一开始就注册对象，否则可能缺少必要的jar包不能实例化报错，应该注册类
        // 其他自定义的缓存可通过此类扩展
        CACHE_MAP.put(cacheType, cacheClass);

        // 初始化方法
        if (initFn != null) {
            CACHE_INITFN_MAP.put(cacheType, initFn);
        }
    }

    public static final Cache CACHE = getCacheInstance();

    public static Cache getCacheInstance() {
        Cache cache = null;
        String cacheType = PropertiesModel.CONFIG.getString("shielsb.cache");
        if (StringUtils.isEmpty(cacheType)) {
            log.warn(StackTraceUtil.getTrace());
            log.error("未配置shielsb.cache属性，不能使用CacheUtil获取缓存实例！");
            return cache;
        }
        Class<? extends Cache> clazz = CACHE_MAP.get(cacheType);
        if (clazz == null) {
            log.error("CacheUtil中未注册" + cacheType + "对应的类！");
        } else {
            log.error("CacheUtil中使用" + cacheType + "作为缓存！" + clazz.getName());
            try {
                cache = clazz.newInstance();
                canUse = true;
                Function<Cache, ?> function = CACHE_INITFN_MAP.get(cacheType);
                if (function != null) {
                    function.apply(cache);
                }
            } catch (InstantiationException | IllegalAccessException e) {
                log.error("", e);
            }
        }
        return cache; // 配置文件制定
    }

    /**
     * 初始化GoogleCache缓存
     * 
     * @Title initGoogleCache
     * @author 吕凯
     * @date 2021年4月28日 上午11:14:00
     * @param cache
     * @return boolean
     */
    public static boolean initGoogleCache(Cache cache) {
        String cacheType = GoogleCache.CACHE_TYPE;
        createCache(cache, cacheType);
        return true;
    }

    private static void createCache(Cache cache, String cacheType) {
        /**
         * 默认过时时间,1天
         */
        // 默认过期时间（一级缓存）
        int firstLevelExpSeconds = PropertiesModel.CONFIG.getInt(String.format(KEY_FIRST_LEVEL_EXP_SECONDS, cacheType), 86400); // 1天
        // 默认过期时间（二级缓存）
        int secondLevelExpSeconds = PropertiesModel.CONFIG.getInt(String.format(KEY_SECOND_LEVEL_EXP_SECONDS, cacheType),
                firstLevelExpSeconds * 2); // 默认为1级缓存的2倍
        // 默认定义了6个缓存器（每个有2级缓存），过期时间从配置文件中读取
        cache.createCache(CacheConfigUtil.FIRST,
                CacheConfigUtil.getCacheExpTimes(CacheConfigUtil.FIRST, cacheType, 1, firstLevelExpSeconds));
        cache.createCache(CacheConfigUtil.FIRST_LEVEL2,
                CacheConfigUtil.getCacheExpTimes(CacheConfigUtil.FIRST, cacheType, 2, secondLevelExpSeconds));
        cache.createCache(CacheConfigUtil.SECOND,
                CacheConfigUtil.getCacheExpTimes(CacheConfigUtil.SECOND, cacheType, 1, firstLevelExpSeconds));
        cache.createCache(CacheConfigUtil.SECOND_LEVEL2,
                CacheConfigUtil.getCacheExpTimes(CacheConfigUtil.SECOND, cacheType, 2, secondLevelExpSeconds));
        cache.createCache(CacheConfigUtil.THIRD,
                CacheConfigUtil.getCacheExpTimes(CacheConfigUtil.THIRD, cacheType, 1, firstLevelExpSeconds));
        cache.createCache(CacheConfigUtil.THIRD_LEVEL2,
                CacheConfigUtil.getCacheExpTimes(CacheConfigUtil.THIRD, cacheType, 2, secondLevelExpSeconds));
        cache.createCache(CacheConfigUtil.FOURTH,
                CacheConfigUtil.getCacheExpTimes(CacheConfigUtil.FOURTH, cacheType, 1, firstLevelExpSeconds));
        cache.createCache(CacheConfigUtil.FOURTH_LEVEL2,
                CacheConfigUtil.getCacheExpTimes(CacheConfigUtil.FOURTH, cacheType, 2, secondLevelExpSeconds));
        cache.createCache(CacheConfigUtil.FIFTH,
                CacheConfigUtil.getCacheExpTimes(CacheConfigUtil.FIFTH, cacheType, 1, firstLevelExpSeconds));
        cache.createCache(CacheConfigUtil.FIFTH_LEVEL2,
                CacheConfigUtil.getCacheExpTimes(CacheConfigUtil.FIFTH, cacheType, 2, secondLevelExpSeconds));
        cache.createCache(CacheConfigUtil.SIXTH,
                CacheConfigUtil.getCacheExpTimes(CacheConfigUtil.SIXTH, cacheType, 1, firstLevelExpSeconds));
        cache.createCache(CacheConfigUtil.SIXTH_LEVEL2,
                CacheConfigUtil.getCacheExpTimes(CacheConfigUtil.SIXTH, cacheType, 2, secondLevelExpSeconds));
    }

    public static boolean initEhcacheCache(Cache cache) {
        return true;
    }

    public static boolean initRedisCache(Cache cache) {
        String cacheType = RedisCache.CACHE_TYPE;
        // 默认过期时间（一级缓存）
        int firstLevelExpSeconds = PropertiesModel.CONFIG.getInt(String.format(KEY_FIRST_LEVEL_EXP_SECONDS, cacheType), 86400); // 1天
        // 默认过期时间（二级缓存）
        int secondLevelExpSeconds = PropertiesModel.CONFIG.getInt(String.format(KEY_SECOND_LEVEL_EXP_SECONDS, cacheType),
                firstLevelExpSeconds * 2); // 默认为1级缓存的2倍
        // 默认定义了6个缓存器（每个有2级缓存）的过期时间,过期时间从配置文件中读取
        RedisCache.addExpTime(CacheConfigUtil.FIRST,
                CacheConfigUtil.getCacheExpTimes(CacheConfigUtil.FIRST, cacheType, 1, firstLevelExpSeconds));
        RedisCache.addExpTime(CacheConfigUtil.FIRST_LEVEL2,
                CacheConfigUtil.getCacheExpTimes(CacheConfigUtil.FIRST, cacheType, 2, secondLevelExpSeconds));
        RedisCache.addExpTime(CacheConfigUtil.SECOND,
                CacheConfigUtil.getCacheExpTimes(CacheConfigUtil.SECOND, cacheType, 1, firstLevelExpSeconds));
        RedisCache.addExpTime(CacheConfigUtil.SECOND_LEVEL2,
                CacheConfigUtil.getCacheExpTimes(CacheConfigUtil.SECOND, cacheType, 2, secondLevelExpSeconds));
        RedisCache.addExpTime(CacheConfigUtil.THIRD,
                CacheConfigUtil.getCacheExpTimes(CacheConfigUtil.THIRD, cacheType, 1, firstLevelExpSeconds));
        RedisCache.addExpTime(CacheConfigUtil.THIRD_LEVEL2,
                CacheConfigUtil.getCacheExpTimes(CacheConfigUtil.THIRD, cacheType, 2, secondLevelExpSeconds));
        RedisCache.addExpTime(CacheConfigUtil.FOURTH,
                CacheConfigUtil.getCacheExpTimes(CacheConfigUtil.FOURTH, cacheType, 1, firstLevelExpSeconds));
        RedisCache.addExpTime(CacheConfigUtil.FOURTH_LEVEL2,
                CacheConfigUtil.getCacheExpTimes(CacheConfigUtil.FOURTH, cacheType, 2, secondLevelExpSeconds));
        RedisCache.addExpTime(CacheConfigUtil.FIFTH,
                CacheConfigUtil.getCacheExpTimes(CacheConfigUtil.FIFTH, cacheType, 1, firstLevelExpSeconds));
        RedisCache.addExpTime(CacheConfigUtil.FIFTH_LEVEL2,
                CacheConfigUtil.getCacheExpTimes(CacheConfigUtil.FIFTH, cacheType, 2, secondLevelExpSeconds));
        RedisCache.addExpTime(CacheConfigUtil.SIXTH,
                CacheConfigUtil.getCacheExpTimes(CacheConfigUtil.SIXTH, cacheType, 1, firstLevelExpSeconds));
        RedisCache.addExpTime(CacheConfigUtil.SIXTH_LEVEL2,
                CacheConfigUtil.getCacheExpTimes(CacheConfigUtil.SIXTH, cacheType, 2, secondLevelExpSeconds));
        return true;
    }

    /**
     * 初始化Caffeine缓存
     * 
     * @Title initCaffeineCache
     * @author 吕凯
     * @date 2021年4月28日 上午11:13:38
     * @param cache
     * @return boolean
     */
    public static boolean initCaffeineCache(Cache cache) {
        String cacheType = CaffeineCache.CACHE_TYPE;
        createCache(cache, cacheType);
        return true;
    }

    /**
     * 获取一级缓存
     * 
     * @Title getFirstLevel
     * @author 吕凯
     * @date 2017年7月28日 上午11:26:06
     * @param cacheName
     *            缓存名称
     * @param key
     * @return T
     */
    public static <T> T getFirstLevel(String cacheName, String key) {
        if (canUse()) {
            return CACHE.get(cacheName, key + "");
        }
        log.warn(CANNOT_USE);
        return null;
    }

    /**
     * 获取二级缓存
     * 
     * @Title getSecondLevel
     * @author 吕凯
     * @date 2017年7月28日 上午11:25:56
     * @param cacheName
     * @param key
     * @return T
     */
    public static <T> T getSecondLevel(String cacheName, String key) {
        if (canUse()) {
            return CACHE.get("l2" + cacheName, key + "");
        }
        log.warn(CANNOT_USE);
        return null;
    }

    /**
     * 放置缓存(默认不启用二级缓存)
     * 
     * @Title put
     * @author 吕凯
     * @date 2017年7月28日 上午11:25:45
     * @param cacheName
     * @param key
     * @param value
     *            void
     */
    public static void put(String cacheName, String key, Object value) {
        put(cacheName, key, value, false);
    }

    /**
     * 放置缓存(可控制是否使用二级缓存)
     * 
     * @Title put
     * @author 吕凯
     * @date 2019年7月6日 上午10:52:59
     * @param cacheName
     * @param key
     * @param value
     * @param useSecondLevel
     *            void
     */
    public static void put(String cacheName, String key, Object value, boolean useSecondLevel) {
        if (canUse()) {
            CACHE.put(cacheName, key, value);
            if (useSecondLevel) {
                CACHE.put("l2" + cacheName, key, value); // 2级缓存，注意2及缓存需要比以及缓存时间长
            }
        } else {

            log.warn(CANNOT_USE);
        }
    }

    /**
     * 移除缓存
     * 
     * @Title remove
     * @author 吕凯
     * @date 2017年7月28日 上午11:29:09
     * @param uriKey
     * @param cacheName
     *            void
     */
    public static void remove(String uriKey, String cacheName) {
        CACHE.remove(cacheName, uriKey);
        CACHE.remove("l2" + cacheName, uriKey);
    }

    /**
     * 移除所有缓存
     * 
     * @Title remove
     * @author 吕凯
     * @date 2017年7月28日 上午11:29:09
     * @param cacheName
     *            void
     */
    public static void removeAll(String cacheName) {
        CACHE.removeAll(cacheName);
    }

    /**
     * 
     * 缓存是否可用
     * 
     * @Title canUse
     * @return boolean
     */
    public static boolean canUse() {
        return canUse;
    }

    public static void setCanUse(Boolean canUse) {
        CacheUtil.canUse = canUse;
    }

    public static void close() {
        CACHE.close();
    }

}
