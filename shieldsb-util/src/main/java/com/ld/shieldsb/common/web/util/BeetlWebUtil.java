package com.ld.shieldsb.common.web.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.beetl.ext.web.WebRender;

import com.ld.shieldsb.common.core.util.BeetlUtil;

public class BeetlWebUtil extends BeetlUtil {

    /**
     * web返回，内容直接写入到response中
     * 
     * @Title render
     * @author 吕凯
     * @date 2018年4月13日 上午9:51:14
     * @param templatePath
     * @param request
     * @param response
     *            void
     */
    public static void render(String templatePath, HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/html;charset=UTF-8");
        WebRender render = new WebRender(groupTemplateClass);
        render.render(templatePath, request, response);
    }

}
