package com.ld.shieldsb.common.core.model;

import lombok.Data;

/**
 * 数据库字段描述
 * 
 * @ClassName Description
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2019年7月25日 下午2:12:24
 *
 */
@Data
public class Description {
    // 原始名称
    private String name;
    // 数据库名称
    private String dbName;
    // 展示名称
    private String showName;

}
