package com.ld.shieldsb.common.core.io.file;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;

import lombok.extern.slf4j.Slf4j;

/**
 * 分割处理Runnable
 *
 * @author yjmyzz@126.com
 */
@Slf4j
public class SplitRunnable implements Runnable {
    private int byteSize;
    private String partFileName;
    private File originFile;
    private int startPos;

    public SplitRunnable(int byteSize, int startPos, String partFileName, File originFile) {
        this.startPos = startPos;
        this.byteSize = byteSize;
        this.partFileName = partFileName;
        this.originFile = originFile;
    }

    public void run() {
        try (RandomAccessFile rFile = new RandomAccessFile(originFile, "r"); OutputStream os = new FileOutputStream(partFileName);) {
            byte[] b = new byte[byteSize];
            rFile.seek(startPos);// 移动指针到每“段”开头
            int s = rFile.read(b);

            os.write(b, 0, s);
            os.flush();
        } catch (IOException e) {
            log.error("", e);
        }
    }
}
