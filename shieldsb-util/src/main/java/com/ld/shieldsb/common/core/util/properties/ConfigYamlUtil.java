package com.ld.shieldsb.common.core.util.properties;

import org.apache.commons.configuration2.YAMLConfiguration;

/**
 * Yaml格式参数管理工具，需要注意截止到目前yaml文件修改不能保存注释，也不能获取注释，留待后期扩展
 * 
 * @ClassName ConfigYamlUtil
 * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
 * @date 2014-6-21 下午2:37:40
 * 
 */
public class ConfigYamlUtil extends ConfigAbstractUtil<YAMLConfiguration> {
    public ConfigYamlUtil(String fileName) {
        super(fileName);
    }

    @Override
    protected String getComment(YAMLConfiguration configuration, String key) {
        return null;
    }

    @Override
    protected void saveComment(String key, String comment, YAMLConfiguration configuration) {

    }

}
