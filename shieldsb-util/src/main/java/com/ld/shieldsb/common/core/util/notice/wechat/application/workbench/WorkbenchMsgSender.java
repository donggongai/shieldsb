package com.ld.shieldsb.common.core.util.notice.wechat.application.workbench;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.ld.shieldsb.common.core.model.Result;
import com.ld.shieldsb.common.core.util.JsoupUtil;
import com.ld.shieldsb.common.core.util.StringUtils;
import com.ld.shieldsb.common.core.util.notice.wechat.EweChatTool;
import com.ld.shieldsb.common.core.util.notice.wechat.application.EweChatApplicationParams;
import com.ld.shieldsb.common.core.util.notice.wechat.application.EwechatBasicMsgSender;
import com.ld.shieldsb.common.core.util.notice.wechat.log.EweChatLogFactory;
import com.ld.shieldsb.common.core.util.notice.wechat.log.EweChatLogModel;

/**
 * 
 * 工作台设置
 * 
 * @ClassName MarkdownMsgSender
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年9月14日 上午9:52:49
 *
 */
public class WorkbenchMsgSender extends EwechatBasicMsgSender {
    // ============================================= 设置工作台模板 ===============================================
    // 参考https://work.weixin.qq.com/api/doc/90000/90135/92535
    public static Result setWorkbenchTemplate(String corpid, String corpsecret, String agentid, Map<String, Object> data) {
        Result result = getAccessToken(corpid, corpsecret, agentid);
        if (result.getSuccess()) { // 0是成功
            String accessToken = result.getMessage();
            String sendUrl = String.format(URL_SET_WORKBENCH_TEMPLATE, accessToken);
            data.put(AGENTID, agentid);
            if (data.get("replace_user_data") == null) {
                data.put("replace_user_data", true); // 是否覆盖用户工作台的数据。设置为true的时候，会覆盖企业所有用户当前设置的数据。若设置为false,则不会覆盖用户当前设置的所有数据。默认为false
            }

            EweChatLogModel.EweChatLogModelBuilder<?, ?> builder = EweChatLogModel.builder();
            builder.type(EweChatTool.APPLICATION).corpid(corpid).corpsecret(corpsecret).agentid(agentid).token(accessToken)
                    .msgType("setWorkbenchTemplate").data(data);

            JSONObject jsonResult = JsoupUtil.sendJson(data, sendUrl, EweChatTool.USER_AGENT, 10000);
            if (jsonResult != null) {
                result.setData(jsonResult);
                Integer errcode = jsonResult.getInteger(EweChatTool.ERRCODE);
                String errmsg = jsonResult.getString(EweChatTool.ERRMSG); // 信息
                String invaliduser = jsonResult.getString(INVALID_USER); // 未发送成功的用户

                String responseCode = jsonResult.getString(RESPONSE_CODE); // 响应码
                String msgId = jsonResult.getString(MSGID); // 响应码

                builder.errcode(errcode).errmsg(errmsg).responseCode(responseCode).msgid(msgId).invaliduser(invaliduser);

                result.setMessage(errmsg);
                if (errcode == 0) { // 0是成功
                    result.setSuccess(true);
                    builder.success(EweChatTool.SUCCESS);
                } else {
                    result.setSuccess(false);
                    builder.success(EweChatTool.ERROR);
                }
                if (StringUtils.isNotEmpty(invaliduser)) {
                    result.setSuccess(false);
                    result.setMessage("发送失败的用户：" + invaliduser);
                }
                log.debug(jsonResult + "");
            }

            EweChatLogFactory.saveLog(builder.build()); // 调用保存日志的方法，没有实现需要具体项目自己实现
        }
        return result;

    }

    public static Result setWorkbenchTemplate(Map<String, Object> data) {
        EweChatApplicationParams params = getConfig();
        return setWorkbenchTemplate(params.getCorpId(), params.getCorpSecret(), params.getAgentId(), data);

    }
}
