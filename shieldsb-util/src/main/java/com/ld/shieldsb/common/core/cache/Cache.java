package com.ld.shieldsb.common.core.cache;

import com.ld.shieldsb.common.core.cache.dataloader.IDataLoader;

/**
 * 缓存接口
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2020年8月25日 上午11:55:27
 *
 */
public interface Cache {

    public void init(); // 初始化

    /**
     * 创建缓存
     * 
     * @Title createCache
     * @author 吕凯
     * @date 2021年4月28日 下午12:02:23
     * @param cacheName
     *            缓存名称
     * @param duration
     *            过期时间
     * @return CacheResult
     */
    public CacheResult createCache(String cacheName, long duration);

    /**
     * 获取缓存
     * 
     * @Title getCache
     * @author 吕凯
     * @date 2021年4月28日 下午12:02:13
     * @param cacheName
     * @return CacheResult
     */
    public CacheResult getCache(String cacheName);

    /**
     * 放置缓存到默认分组
     * 
     * @Title put
     * @author 吕凯
     * @date 2021年4月28日 下午12:08:41
     * @param key
     * @param value
     * @return boolean
     */
    public boolean put(String key, Object value);

    /**
     * 放置缓存到某个分组（缓存库）
     * 
     * @Title put
     * @author 吕凯
     * @date 2021年4月28日 下午12:08:19
     * @param groupName
     * @param key
     * @param value
     * @return boolean
     */
    public boolean put(String groupName, String key, Object value); // 带分组

    /**
     * 获取默认分组的缓存
     * 
     * @Title get
     * @author 吕凯
     * @date 2021年4月28日 下午12:04:22
     * @param <T>
     * @param key
     * @return T
     */
    public <T> T get(String key);

    public <T> T get(String groupName, String key);

    /**
     * 获取默认分组的通过数据加载器加载的缓存
     * 
     * @Title get
     * @author 吕凯
     * @date 2021年4月28日 下午12:03:58
     * @param <T>
     * @param key
     * @param dataLoaderClass
     * @return T
     */
    public <T> T get(String key, Class<? extends IDataLoader> dataLoaderClass);

    public <T> T get(String groupName, String key, Class<? extends IDataLoader> dataLoaderClass);

    /**
     * 获取默认分组的经过处理的key，部分缓存容器的可以可能需要处理
     * 
     * @Title getCacheKey
     * @author 吕凯
     * @date 2021年4月28日 下午12:03:39
     * @param key
     * @return String
     */
    public String getCacheKey(String key);

    /**
     * 获取经过处理的key，部分缓存容器的可以可能需要处理
     * 
     * @Title getCacheKey
     * @author 吕凯
     * @date 2021年4月28日 下午12:07:27
     * @param groupName
     * @param key
     * @return String
     */
    public String getCacheKey(String groupName, String key);

    /**
     * 移除默认分组中的缓存
     * 
     * @Title remove
     * @author 吕凯
     * @date 2021年4月28日 下午12:02:53
     * @param key
     * @return boolean
     */
    public boolean remove(String key);

    public boolean remove(String groupName, String key);

    /**
     * 移除默认分组中的所有数据
     * 
     * @Title removeAll
     * @author 吕凯
     * @date 2021年4月28日 下午12:03:22
     * @return boolean
     */
    public boolean removeAll();

    public boolean removeAll(String groupName);

    public boolean open(); // 开启缓存

    public boolean close(); // 关闭

    public boolean canUse(); // 是否可使用

}
