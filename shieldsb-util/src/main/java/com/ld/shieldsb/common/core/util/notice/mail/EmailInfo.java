package com.ld.shieldsb.common.core.util.notice.mail;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.mail.EmailAttachment;

import lombok.Data;

/**
 * 邮件相关信息
 * 
 */
@Data
public class EmailInfo {

    // 收件人
    private List<String> toAddress;
    // 抄送人地址
    private List<String> ccAddress;
    // 密送人
    private List<String> bccAddress;
    // 附件信息
    private List<EmailAttachment> attachments;
    // 发送方显示名称
    private String fromName;
    // 邮件主题
    private String subject;
    // 邮件的文本内容
    private String content;

    public List<String> getToAddress() {
        return toAddress;
    }

    /**
     * 添加收件人
     * 
     * @Title addToAddress
     * @author 吕凯
     * @date 2019年1月10日 上午9:20:10
     * @param toAddress
     *            void
     */
    public void addToAddress(String toAddress) {
        if (this.toAddress == null) {
            this.toAddress = new ArrayList<>();
        }
        this.toAddress.add(toAddress);
    }

    public void addToAddress(List<String> toAddress) {
        if (this.toAddress == null) {
            this.toAddress = new ArrayList<>();
        }
        this.toAddress.addAll(toAddress);
    }

    /**
     * 添加抄送
     * 
     * @Title addCcAddress
     * @author 吕凯
     * @date 2019年1月10日 上午9:20:21
     * @param ccAddress
     *            void
     */
    public void addCcAddress(String ccAddress) {
        if (this.ccAddress == null) {
            this.ccAddress = new ArrayList<>();
        }
        this.ccAddress.add(ccAddress);
    }

    public void addCcAddress(List<String> ccAddress) {
        if (this.ccAddress == null) {
            this.ccAddress = new ArrayList<>();
        }
        this.ccAddress.addAll(ccAddress);
    }

    /**
     * 添加密送
     * 
     * @Title addBccAddress
     * @author 吕凯
     * @date 2019年1月10日 上午9:19:55
     * @param bccAddress
     *            void
     */
    public void addBccAddress(String bccAddress) {
        if (this.bccAddress == null) {
            this.bccAddress = new ArrayList<>();
        }
        this.bccAddress.add(bccAddress);
    }

    public void addBccAddress(List<String> bccAddress) {
        if (this.bccAddress == null) {
            this.bccAddress = new ArrayList<>();
        }
        this.bccAddress.addAll(ccAddress);
    }

    /**
     * 添加附件
     * 
     * @Title addAttachments
     * @author 吕凯
     * @date 2019年1月10日 上午9:24:28
     * @param attachments
     *            void
     */
    public void addAttachments(EmailAttachment attachments) {
        if (this.attachments == null) {
            this.attachments = new ArrayList<>();
        }
        this.attachments.add(attachments);
    }

    public void addAttachments(List<EmailAttachment> attachments) {
        if (this.attachments == null) {
            this.attachments = new ArrayList<>();
        }
        this.attachments.addAll(attachments);
    }

}