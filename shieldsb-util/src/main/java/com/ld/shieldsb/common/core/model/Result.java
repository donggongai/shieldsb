package com.ld.shieldsb.common.core.model;

import lombok.Data;

@Data
public class Result {
    /**
     * 结果
     */
    private Boolean success = false;
    /**
     * 结果代码
     */
    private String code = "";
    /**
     * 返回信息
     */
    private String message = "";
    /**
     * 附加对象
     */
    private Object data;

    public Result() {
        super();
    }

    public Result(Boolean success, String code, String message) {
        super();
        this.success = success;
        this.code = code;
        this.message = message;
    }

    public void copyTo(Result to) {
        if (to == null) {
            to = new Result();
        }
        to.setSuccess(this.getSuccess());
        to.setMessage(this.getMessage());
        to.setCode(this.getCode());
        to.setData(this.getData());
    }

    public boolean getSuccess() {
        return success != null && success; // 与运算
    }

    /**
     * 设置失败的信息
     * 
     * @Title setErrorMessage
     * @author 吕凯
     * @date 2022年1月11日 下午3:28:07
     * @param message
     *            void
     */
    public void setErrorMessage(String message) {
        this.success = false;
        this.message = message;
    }

    /**
     * 设置成功的信息
     * 
     * @Title setSuccessMessage
     * @author 吕凯
     * @date 2022年1月11日 下午3:27:55
     * @param message
     *            void
     */
    public void setSuccessMessage(String message) {
        this.success = true;
        this.message = message;
    }
}
