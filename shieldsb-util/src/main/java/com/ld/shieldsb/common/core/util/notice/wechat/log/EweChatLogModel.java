package com.ld.shieldsb.common.core.util.notice.wechat.log;

import java.util.Date;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 企业微信日志
 * 
 * @ClassName EweChatLogModel
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年9月11日 上午10:46:12
 *
 */
@SuperBuilder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EweChatLogModel {
    private Long id;

    private Integer type; // 0应用发送1群机器人

    private String corpid;
    private String corpsecret; // 应用时未应用私钥，机器人时为机器人私钥
    private String agentid;
    private String token;

    private String msgType; // 消息类型
    private String sendTo; // 发送给谁，即接收人
    private String title;
    private String content;
    private Integer success; // 是否成功
    private Map<String, Object> data; // 参数

    private Integer errcode; // 错误编码
    private String errmsg; // 信息
    private String invaliduser; // 未发送成功的用户
    private String responseCode; // 响应码
    private String msgid; // 消息id

    private Integer isRecall; // 1撤回0未撤回
    private Date recallTime; // 撤回时间

}
