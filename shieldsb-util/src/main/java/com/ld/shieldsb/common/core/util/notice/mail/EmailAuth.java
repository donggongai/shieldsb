package com.ld.shieldsb.common.core.util.notice.mail;

import lombok.Data;

/**
 * 邮件服务验证对象
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2020年6月10日 下午3:03:37
 *
 */
@Data
public final class EmailAuth {
    String charset = "UTF-8"; // 编码
    String mailHostName; // 服务器
    Integer mailHostPort = 25; // 服务器 端口，默认25，加密为465或587
    String mailUserName; // 发送人用户名
    String mailPassWord; //// 发送人密码
    String fromName; // 发件人显示名称
    String fromEmail;// 发件人邮箱地址
}
