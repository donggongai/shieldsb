package com.ld.shieldsb.common.core.cache.redis;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.ld.shieldsb.common.core.cache.CacheAbstractAdaptar;
import com.ld.shieldsb.common.core.cache.CacheConfigUtil;
import com.ld.shieldsb.common.core.cache.CacheResult;
import com.ld.shieldsb.common.core.util.redis.RedisUtil;

import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;

@Slf4j
public final class RedisCache extends CacheAbstractAdaptar {
    public static final String CACHE_TYPE = "redis";

    private static final Map<String, Integer> EXP_SECONDS_MAP = new HashMap<>(); // 过期时间（秒）

    public RedisCache() {
        init();
    }

    @Override
    public void init() {
        // 准备做点啥
    }

    /**
     * 创建缓存
     * 
     * @Title createCache
     * @author 吕凯
     * @date 2021年4月27日 上午10:51:26
     * @param cacheName
     * @param exp
     * @return boolean
     */
    @Override
    public RedisCacheResult createCache(String cacheName, long exp) {
        RedisCacheResult cacheResult = new RedisCacheResult();
        Integer expInt = Integer.valueOf((int) exp);
        RedisCache.addExpTime(cacheName, expInt);
        cacheResult.setSuccess(true);
        return cacheResult;
    }

    @Override
    public CacheResult getCache(String cacheName) {
        return null;
    }

    // 增加过期时间
    public static void addExpTime(String cacheName, int duration) {
        EXP_SECONDS_MAP.put(cacheName, duration);
    }

    /**
     * 
     * 在redis数据库中插入 key 和value 并且设置过期时间
     * 
     * @Title set
     * @author 吕凯
     * @date 2017年7月28日 上午10:33:20
     * @param key
     * @param value
     * @param exp
     *            过期时间 ，-1不过期
     * @return boolean
     */
    public boolean putExp(String key, Object value, int exp) {
        Jedis jedis = null;
        // 将key 和value 转换成 json 对象
        String jKey = key;
        String jValue = JSON.toJSONString(value, SerializerFeature.WriteClassName);
        // 操作是否成功
        if (StringUtils.isEmpty(jKey)) {
            log.warn("key is empty");
            return false;
        }
        try {
            // 获取客户端对象
            jedis = RedisUtil.getJedis();
            if (jedis != null) {
                // 执行插入
                if (exp != -1) {
                    jedis.setex(jKey, exp, jValue);
                } else {
                    jedis.set(jKey, jValue);
                }
            }
        } catch (Exception e) {
            log.error("client can't connect server", e);
            return false;
        } finally {
            if (jedis != null) {
                // 返还连接池
                RedisUtil.returnResource(jedis);
            }
        }
        return true;
    }

    /**
     * 
     * 防止缓存
     * 
     * @Title put
     * @author 吕凯
     * @date 2017年7月28日 上午11:01:20
     * @param cacheName
     * @param key
     * @param value
     * @return boolean
     */
    @Override
    public boolean put(String cacheName, String key, Object value) {
        Integer exp = EXP_SECONDS_MAP.get(cacheName);
        if (exp == null) { // 未取到，取默认值
            exp = CacheConfigUtil.DEFAULT_EXP_SECONDS;
        }
        return putExp(getCacheKey(cacheName, key), value, exp);
    }

    /**
     * 根据key 去redis 中获取value
     * 
     * @Title get
     * @author 吕凯
     * @date 2017年7月28日 上午10:39:00
     * @param key
     * @return Object
     */
    @SuppressWarnings("unchecked")
    @Override
    public <T> T get(String cacheName, String key) {
        Jedis jedis = null;
        // 将key 和value 转换成 json 对象
        String jKey = getCacheKey(cacheName, key);
        T jValue = null;
        // key 不能为空
        if (StringUtils.isEmpty(jKey)) {
            log.warn("key is empty");
            return null;
        }
        try {
            // 获取客户端对象
            jedis = RedisUtil.getJedis();
            if (jedis != null) {
                // 执行查询
                String value = jedis.get(jKey);
                // 判断值是否非空
                if (!StringUtils.isEmpty(value)) {
                    jValue = (T) JSON.parse(value);
                }
            }
        } catch (Exception e) {
            log.error("client can't connect server", e);
        } finally {
            if (jedis != null) {
                // 释放jedis 对象
                RedisUtil.returnResource(jedis);
            }

        }
        return jValue;
    }

    /**
     * 获取缓存key，可以用/分隔成不同的命名空间，如a/b 存储时会分隔为cacheName:a:b
     * 
     * @Title getCacheKey
     * @author 吕凯
     * @date 2017年8月4日 下午1:47:32
     * @param cacheName
     * @param key
     * @return String
     */
    @Override
    public String getCacheKey(String cacheName, String key) {
        if (key != null) {
            if (key.length() > 2) {
                key = key.replace("/", ":"); // 拆成不同的命名空间
            }
            if (key.startsWith(":")) {
                key = key.substring(1);
            }
            if (key.endsWith(":")) {
                key = key.substring(0, key.length() - 1);
            }
        }
        return cacheName + ":" + key;
    }

    /**
     * 移除缓存
     * 
     * @Title remove
     * @author 吕凯
     * @date 2017年7月28日 上午11:05:58
     * @param cacheName
     * @param key
     * @return boolean
     */
    @Override
    public boolean remove(String cacheName, String key) {
        Jedis jedis = null;
        // 获取客户端对象
        boolean flag = false;
        try {
            jedis = RedisUtil.getJedis();
            if (jedis != null) {
                String jKey = getCacheKey(cacheName, key);
                flag = jedis.del(jKey) > 0;
            }
        } catch (Exception e) {
            log.error("", e);
        } finally {
            if (jedis != null) {
                RedisUtil.returnResource(jedis);
            }
        }
        return flag;
    }

    @Override
    public boolean canUse() {
        return RedisCacheParams.canUse();
    }

    @Override
    public boolean removeAll(String cacheName) {
        // 暂不支持全部清空
        return false;
    }

    @Override
    public boolean close() {
        EXP_SECONDS_MAP.clear();
        RedisUtil.close();
        return false;
    }

}