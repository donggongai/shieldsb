package com.ld.shieldsb.common.core.cache.google;

import com.google.common.cache.Cache;
import com.ld.shieldsb.common.core.cache.CacheResult;

import lombok.Data;

@Data
public class GoogleCacheResult<K, V> implements CacheResult {
    private Cache<K, V> cache;
}
