package com.ld.shieldsb.common.core.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializeFilter;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.serializer.SimplePropertyPreFilter;
import com.ld.shieldsb.common.core.collections.ListUtils;

public class FastJsonUtil {
    /**
     * 过滤只返回指定的字段,filterFieldsJson(listObj,Student.class,"name","grade","school");
     * 
     * @Title filterFieldJson
     * @author 吕凯
     * @date 2020年11月30日 下午7:07:55
     * @param source
     * @param clazz
     * @param args
     *            每个属性一个字符串，也可以将所有的属性用英文逗号拼接为一个字符串
     * @return String
     */
    public static String filterFieldJsonStr(Object source, Class<?> clazz, String... includes) {
        return filterFieldJsonStr(source, clazz, includes, null);
    }

    public static String filterFieldJsonStr(Object source, Class<?> clazz, String[] args, String[] excludes) {
        String[] dealArgs = dealArgs(args);
        SimplePropertyPreFilter filter = new SimplePropertyPreFilter(clazz, dealArgs);
        if (excludes != null && excludes.length > 0) {
            String[] dealExcludess = dealArgs(excludes);
            filter.getExcludes().addAll(Arrays.asList(dealExcludess)); // 排除字段
        }
        return JSON.toJSONString(source, filter, SerializerFeature.WriteMapNullValue);
    }

    /**
     * 处理参数
     * 
     * @Title dealArgs
     * @author 吕凯
     * @date 2020年12月1日 上午10:56:04
     * @param args
     * @return String[]
     */
    private static String[] dealArgs(String... args) {
        List<String> argList = new ArrayList<>();
        if (args.length > 0) {
            for (String val : args) {

                if (val.contains(",")) {
                    String[] argArrs = args[0].split(",");
                    argList.addAll(Arrays.asList(argArrs));
                }
            }
        }
        String[] dealArgs = args;
        if (ListUtils.isNotEmpty(argList)) {
            dealArgs = argList.toArray(new String[0]);
        }
        return dealArgs;
    }

    public static String filterFieldJsonStr(Object source, SerializeFilter[] filters) {
        return JSON.toJSONString(source, filters, SerializerFeature.WriteMapNullValue);
    }

    /**
     * 过滤对象字段并返回对象
     * 
     * @Title filterFieldJson
     * @author 吕凯
     * @date 2020年12月1日 上午8:36:58
     * @param source
     * @param clazz
     * @param args
     * @return Object
     */
    public static Object filterFieldJson(Object source, Class<?> clazz, String... args) {
        String jsonOutput = filterFieldJsonStr(source, clazz, args);
        return JSON.parse(jsonOutput);
    }

    public static FilterFieldBuilder builder() {
        return new FilterFieldBuilder();
    }

    public static class FilterFieldBuilder {
        List<SerializeFilter> filterList = new ArrayList<>();

        public FilterFieldBuilder addFilter(Class<?> clazz, String... args) {
            return addFilter(clazz, args, null);
        }

        public FilterFieldBuilder addFilter(Class<?> clazz, String[] includes, String[] excludes) {
            String[] dealArgs = dealArgs(includes);
            SimplePropertyPreFilter filter = new SimplePropertyPreFilter(clazz, dealArgs);
            if (excludes != null && excludes.length > 0) {
                String[] dealExcludess = dealArgs(excludes);
                filter.getExcludes().addAll(Arrays.asList(dealExcludess)); // 排除字段
            }
            filterList.add(filter);
            return this;
        }

        /**
         * 过滤
         * 
         * @Title filter
         * @author 吕凯
         * @date 2020年12月1日 上午10:51:21
         * @param source
         * @return String
         */
        public String filter(Object source) {
            return FastJsonUtil.filterFieldJsonStr(source, filterList.toArray(new SerializeFilter[0]));
        }
    }

}
