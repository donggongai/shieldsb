package com.ld.shieldsb.common.core.util.notice.wechat;

import com.alibaba.fastjson.JSONObject;
import com.ld.shieldsb.common.core.cache.google.GoogleCacheUtil;
import com.ld.shieldsb.common.core.model.Result;
import com.ld.shieldsb.common.core.util.JsoupUtil;
import com.ld.shieldsb.common.core.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * 企业微信token工具类<br/>
 * 参考文档 <br/>
 * https://open.work.weixin.qq.com/api/doc/90000/90135/91039 <br/>
 * https://open.work.weixin.qq.com/api/doc/90001/90143/90605 <br/>
 * https://work.weixin.qq.com/api/doc/90000/90139/90312 访问频率限制
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年5月18日 下午3:42:05
 *
 */
@Slf4j
public class EweChatTokenUtil {

    public static final String EWCHAT_APP_TOKEN = "EWCHAT_APP_TOKEN";

    // access_token的有效期通过返回的expires_in来传达，正常情况下为7200秒（2小时）有效期内重复获取返回相同结果，过期后获取会返回新的access_token。
    public static final String URL_GETTOKEN = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=%s&corpsecret=%s"; // 企业微信获取token的url，%s为占位符，

    static {
        GoogleCacheUtil.createCache(EWCHAT_APP_TOKEN, 7000L); // 创建缓存,使用了google缓存，不需要额外引入jar包，缓存7000秒
    }

    private static boolean putCache(String key, Object value) {
        return GoogleCacheUtil.put(EWCHAT_APP_TOKEN, key, value);
    }

    private static <T> T getCache(String key) {
        return GoogleCacheUtil.get(EWCHAT_APP_TOKEN, key);
    }

    /**
     * 
     * 获取访问token
     * 
     * @Title getAccessToken
     * @author 吕凯
     * @date 2021年5月18日 下午3:54:22
     * @param corpid
     *            企业id
     * @param corpsecret
     * @param agentId
     *            应用id
     * @return Result
     */
    public static Result getAccessToken(String corpid, String corpsecret, String agentId) {
        Result result = new Result();
        result.setSuccess(false);
        String cacheKey = corpid + "_" + agentId;
        String accessToken = getCache(cacheKey); // 从缓存中取
        if (StringUtils.isEmpty(accessToken)) {
            result.setMessage("连接异常！");
            String getTokenUrl = String.format(URL_GETTOKEN, corpid, corpsecret);
            JSONObject jsonObj = JsoupUtil.getRemoteURLJson(getTokenUrl);
            log.warn("获取企业微信应用的token");
            if (jsonObj != null) {
                Integer errcode = jsonObj.getInteger(EweChatTool.ERRCODE);
                if (errcode == 0) { // 0是成功
                    accessToken = (String) jsonObj.get("access_token");
                    result.setSuccess(true);
                    result.setMessage(accessToken);
                    result.setData(accessToken);

                    putCache(cacheKey, accessToken); // 放置到缓存中
                } else {
                    String errmsg = jsonObj.getString(EweChatTool.ERRMSG); // 信息
                    result.setSuccess(false);
                    result.setMessage(errmsg);
                    /**
                     * {"errcode":41002,"errmsg":"corpid missing, hint: [1566888599_12_0c7cbc4ed7886d21f534b85b7aafebb3], from ip:
                     * 122.4.233.133, more info at https://open.work.weixin.qq.com/devtool/query?e=41002"}
                     */
                }
            }
        } else {
            result.setSuccess(true);
            result.setMessage(accessToken);
        }

        return result;
    }

}
