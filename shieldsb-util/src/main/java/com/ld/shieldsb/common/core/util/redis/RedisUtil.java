package com.ld.shieldsb.common.core.util.redis;

import com.ld.shieldsb.common.core.util.StringUtils;

import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

@Slf4j
public final class RedisUtil {

    private static JedisPool jedisPool = null;

    /**
     * 初始化Redis连接池
     */
    static {
        init();
    }

    /**
     * 初始化
     * 
     * @Title init
     * @author 吕凯
     * @date 2021年4月28日 下午1:49:48 void
     */
    public static void init() {
        close(); // 先关闭
        try {
            if (canUse()) {
                JedisPoolConfig config = new JedisPoolConfig();
                config.setMaxTotal(RedisParams.MAX_ACTIVE);
                config.setMaxIdle(RedisParams.MAX_IDLE);
                config.setMaxWaitMillis(RedisParams.MAX_WAIT);
                config.setTestOnBorrow(RedisParams.TEST_ON_BORROW);

                jedisPool = new JedisPool(config, RedisParams.ADDR, RedisParams.PORT, RedisParams.TIMEOUT,
                        StringUtils.isNotEmpty(RedisParams.PASSWORD) ? RedisParams.PASSWORD : null, RedisParams.DATABASE);
            } else {
                log.warn("redis未开启，请检查配置文件的参数：" + RedisParams.KEY_REDIS_USE);
            }
        } catch (Exception e) {
            log.error("", e);
        }
    }

    /**
     * 获取Jedis实例
     * 
     * @return
     */
    public static synchronized Jedis getJedis() {
        try {
            if (canUse() && jedisPool != null) {
                Jedis resource = jedisPool.getResource();
                return resource;
            } else {
                return null;
            }
        } catch (Exception e) {
            log.error("", e);
            return null;
        }
    }

    /**
     * 释放jedis资源
     * 
     * @param jedis
     */
    public static void returnResource(final Jedis jedis) {
        if (canUse() && jedis != null && jedis.isConnected()) {
            jedis.close();
        }
    }

    public static void close() {
        if (jedisPool != null) {
            jedisPool.close();
        }
    }

    public static boolean canUse() {
        return RedisParams.canUse();
    }
}