package com.ld.shieldsb.common.core.util.sensitiveWord;

import java.util.Map;
import java.util.Set;

import lombok.Data;

/**
 * 
 * ，处理结果
 * 
 * @ClassName SensitiveWordResult
 * @author 刘金浩
 * @date 2019年8月30日 下午4:29:03
 *
 */
@Data
public class SensitiveWordResult {

    public boolean contains = false; // 是否包含敏感词
    public String originalContent;// 原始字符串
    public String filteredContent;// 处理后的字符串，屏蔽敏感词后的文本内容
    public String sensitiveWordStr; // 屏蔽的敏感词串,eg:色魔,法轮功,GCD

    public Set<String> sensitiveWords; // 所有的敏感词
    public Map<String, String> replaceWords; // 替换的敏感词，替换前=替换后
    public Set<String> sensitiveWordsActive; // 排除替换后的敏感词
}
