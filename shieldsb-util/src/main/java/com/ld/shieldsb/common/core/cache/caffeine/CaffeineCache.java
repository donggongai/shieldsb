package com.ld.shieldsb.common.core.cache.caffeine;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.RemovalListener;
import com.ld.shieldsb.common.core.cache.CacheAbstractAdaptar;
import com.ld.shieldsb.common.core.cache.CacheConfigUtil;
import com.ld.shieldsb.common.core.cache.google.GoogleCacheParams;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * Caffeine 缓存
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2020年8月25日 上午11:24:03
 *
 */
@Slf4j
public class CaffeineCache extends CacheAbstractAdaptar {
    public static final String CACHE_TYPE = "caffeine";
    protected static final Map<String, CaffeineCacheResult<Object, Object>> cacheMap = new ConcurrentHashMap<>();

    public CaffeineCache() {
        init();
    }

    @Override
    public void init() {
        // 准备做点啥
    }

    @Override
    public CaffeineCacheResult<Object, Object> getCache(String cacheName) { // 有线程安全问题，但是不影响最终效果，后期优化
        return cacheMap.get(cacheName);
    }

    @Override
    public CaffeineCacheResult<Object, Object> createCache(String cacheName, long duration) { // 有线程安全问题，但是不影响最终效果，后期优化
        return createCache(cacheName, duration, CaffeineCacheParams.getExpireType());
    }

    public CaffeineCacheResult<Object, Object> createCache(String cacheName, long duration, String expireType) { // 有线程安全问题，但是不影响最终效果，后期优化
        CaffeineCacheResult<Object, Object> result = cacheMap.get(cacheName);
        if (result == null) {
            synchronized (cacheMap) {
                result = cacheMap.get(cacheName);
                if (result == null) {
                    result = new CaffeineCacheResult<>();
                    Caffeine<Object, Object> builder = Caffeine.newBuilder();
                    if (CaffeineCacheParams.getMaxImumSize() != null) {
                        builder.maximumSize(CaffeineCacheParams.getMaxImumSize()); // 最大数据个数
                    }
                    if (CaffeineCacheParams.getExpireTypeExpireafteraccess().equals(expireType)) {
                        builder.expireAfterAccess(duration, TimeUnit.SECONDS);
                    } else if (CaffeineCacheParams.getExpireTypeExpireafterwrite().equals(expireType)) {
                        builder.expireAfterWrite(duration, TimeUnit.SECONDS);
                    } else if (CaffeineCacheParams.getExpireTypeRefreshafterwrite().equals(expireType)) {
                        builder.refreshAfterWrite(duration, TimeUnit.SECONDS);
                    }
                    // 剔除监听
                    builder.removalListener((RemovalListener<String, String>) (key, value, cause) -> log
                            .debug("删除缓存 key:" + key + ", value:" + value + ", 删除原因:" + cause.toString()));

                    Cache<Object, Object> cache = builder.build();// 缓存有效期为duration秒
                    cache.cleanUp();
                    result.setCache(cache);
                    cacheMap.put(cacheName, result);
                }
            }
        }
        return result;
    }

    @Override
    public boolean put(String cacheName, String key, Object value) {
        CaffeineCacheResult<Object, Object> cacheResult = getCache(cacheName);
        if (cacheResult == null || cacheResult.getCache() == null) {
            log.error(CacheConfigUtil.CACHE_NOT_EXISTS + cacheName);
            return false;
        } else {
            cacheResult.getCache().put(key, value);
        }
        return true;
    }

    public boolean put(Cache<Object, Object> cache, String key, Object value) {
        if (cache == null) {
            log.error(CacheConfigUtil.CACHE_NOT_EXISTS);
            return false;
        } else {
            cache.put(key, value);
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T get(String cacheName, String key) {

        Object value = Optional.ofNullable(getCache(cacheName)) //
                .map(CaffeineCacheResult::getCache) //
                .map(cache -> cache.getIfPresent(key)) //
                .orElse(null); // 默认值
        return value != null ? (T) value : null;
    }

    @Override
    public String getCacheKey(String cacheName, String key) {
        return key;
    }

    @Override
    public boolean remove(String cacheName, String key) {
        Cache<Object, Object> cache = getImplCache(cacheName);
        if (cache == null) {
            log.error(CacheConfigUtil.CACHE_NOT_EXISTS + cacheName);
            return false;
        } else {
            cache.invalidate(key);
        }
        return true;
    }

    @Override
    public boolean removeAll(String cacheName) {
        Cache<Object, Object> cache = getImplCache(cacheName);
        if (cache == null) {
            log.error(CacheConfigUtil.CACHE_NOT_EXISTS + cacheName);
            return false;
        } else {
            cache.invalidateAll();
        }
        return true;
    }

    /**
     * 获取实现层的cache对象
     * 
     * @Title getImplCache
     * @author 吕凯
     * @date 2021年4月28日 上午10:10:42
     * @param cacheName
     * @return Cache<Object,Object>
     */
    private Cache<Object, Object> getImplCache(String cacheName) {
        Cache<Object, Object> cache = Optional.ofNullable(getCache(cacheName)).map(CaffeineCacheResult::getCache).orElse(null); // 默认值;
        return cache;
    }

    @Override
    public boolean close() {
        super.close();
        cacheMap.clear();
        return true;
    }

    /**
     * 
     * 缓存是否可用
     * 
     * @Title canUse
     * @return boolean
     */
    @Override
    public boolean canUse() {
        return GoogleCacheParams.canUse();
    }

}
