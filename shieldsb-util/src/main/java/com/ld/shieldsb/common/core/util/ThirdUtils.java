package com.ld.shieldsb.common.core.util;

import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;

/**
 * 第三方工具类描述
 * 
 * @ClassName ThirdUtils
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2018年12月5日 下午2:10:04
 *
 */

public class ThirdUtils {

    // byte操作工具类
    final byte[] init_parm = { (byte) 0xcd, (byte) 0x91, (byte) 0xa7, (byte) 0xc5 };
    final byte[] byteUtil = ByteUtils.fromHexString("7f4c0e7461398b8005a93e967737e58c4f0aecb657a97cde6583d2ab5d9f2dad22c9");
    final String ss = ByteUtils.toBinaryString(init_parm);

}
