package com.ld.shieldsb.common.core.util;

/**
 * 简单路径匹配器
 * 
 * @ClassName SimplePathMatcher
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2019年2月25日 下午4:24:05
 *
 */
public class SimplePathMatcher {
    public boolean matches(String pattern, String source) {
        pattern = pattern.replace("*", ".*"); // 通配符转为正则
        return source.matches(pattern);
    }
}