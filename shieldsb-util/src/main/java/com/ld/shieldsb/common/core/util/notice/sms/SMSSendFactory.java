package com.ld.shieldsb.common.core.util.notice.sms;

import com.ld.shieldsb.common.core.ServiceFactory;
import com.ld.shieldsb.common.core.util.notice.sms.impl.SMSSend5c;

public class SMSSendFactory extends ServiceFactory<SMSSend> {

    public static final String TYPE_DEFAULT = "smssend_5c";

    static {
        // 注意工厂类不要一开始就注册对象，否则可能缺少必要的jar包不能实例化报错，应该注册类
        getInstance().registerServiceClass(TYPE_DEFAULT, SMSSend5c.class);
    }

    public static ServiceFactory<SMSSend> getInstance() {
        return getInstance(SMSSendFactory.class);
    }

}
