package com.ld.shieldsb.common.core.util.notice.wechat.application.file;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.ld.shieldsb.common.core.model.Result;
import com.ld.shieldsb.common.core.util.notice.wechat.EweChatTool;
import com.ld.shieldsb.common.core.util.notice.wechat.Type;
import com.ld.shieldsb.common.core.util.notice.wechat.application.EweChatApplicationParams;
import com.ld.shieldsb.common.core.util.notice.wechat.application.EwechatBasicMsgSender;

/**
 * 文件格式
 * 
 * @ClassName MarkdownMsgSender
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年9月14日 上午9:52:49
 *
 */
public class FileMsgSender extends EwechatBasicMsgSender {
    // ============================================= 文件消息 ==============================================
    public static Map<String, Object> parse2FileDataMap(String agentid, String sendTo, String mediaId, Integer duplicateCheckInterval) {
        /*    {
        "touser" : "UserID1|UserID2|UserID3",
        "toparty" : "PartyID1|PartyID2",
        "totag" : "TagID1 | TagID2",
        "msgtype" : "image",
        "agentid" : 1,
        "image" : {
            "media_id" : "MEDIA_ID"
        },
        "safe":0,
        "enable_duplicate_check": 0,
        "duplicate_check_interval": 1800
        }*/

        Map<String, Object> dataMap = defaultDataMap(agentid, sendTo, duplicateCheckInterval);

        dataMap.put(EweChatTool.MSG_TYPE, Type.IMAGE.value); // 貌似图片也支持其他文件

        Map<String, String> contentMap = new HashMap<>();
        contentMap.put(UPLOAD_RESULT_FILE_ID, mediaId); // 图片媒体文件id，通过上传方法获取
        dataMap.put(Type.IMAGE.value, contentMap);

        return dataMap;
    }

    public static Map<String, Object> parse2FileDataMap(String agentid, String sendTo, String mediaId) {
        return parse2FileDataMap(agentid, sendTo, mediaId, null);
    }

    /**
     * 发送文件信息，通过文件对象
     * 
     * @Title sendFileMsg
     * @author 吕凯
     * @date 2021年4月19日 下午2:41:01
     * @param key
     *            密钥
     * @param file
     *            文件
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return JSONObject
     */
    public static final Result sendFileMsg(String corpid, String corpsecret, String agentid, String sendTo, File file,
            Integer duplicateCheckInterval) {
        Result result = getAccessToken(corpid, corpsecret, agentid);
        if (result.getSuccess()) { // 获取成功
            String accessToken = result.getMessage();
            JSONObject uploadResult = uploadFile(accessToken, file);
            if (uploadResult != null) {
                Integer errCode = uploadResult.getInteger(EweChatTool.ERRCODE);
                if (Integer.valueOf(0).equals(errCode)) {
                    Map<String, Object> dataMap = parse2FileDataMap(agentid, sendTo, uploadResult.getString(UPLOAD_RESULT_FILE_ID),
                            duplicateCheckInterval); // map转换
                    return sendMsg(corpid, corpsecret, agentid, dataMap);
                }
            }
        }
        return result;
    }

    public static final Result sendFileMsg(String corpid, String corpsecret, String agentid, String sendTo, File file) {
        return sendFileMsg(corpid, corpsecret, agentid, sendTo, file, null);
    }

    public static Result sendFileMsg(String sendTo, File file, Integer duplicateCheckInterval) {
        EweChatApplicationParams params = getConfig();
        return sendFileMsg(params.getCorpId(), params.getCorpSecret(), params.getAgentId(), sendTo, file, duplicateCheckInterval);
    }

    public static Result sendFileMsg(String sendTo, File file) {
        return sendFileMsg(sendTo, file, null);

    }

    /**
     * 发送文件通过流
     * 
     * @Title sendFileMsg
     * @author 吕凯
     * @date 2021年4月21日 上午10:39:22
     * @param corpid
     * @param corpsecret
     * @param agentid
     * @param sendTo
     * @param inputStream
     * @param fileName
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Result
     */
    public static final Result sendFileMsg(String corpid, String corpsecret, String agentid, String sendTo, InputStream inputStream,
            String fileName, Integer duplicateCheckInterval) {
        Result result = getAccessToken(corpid, corpsecret, agentid);
        if (result.getSuccess()) { // 获取成功
            String accessToken = result.getMessage();
            JSONObject uploadResult = uploadFile(accessToken, inputStream, fileName);
            if (uploadResult != null) {
                Integer errCode = uploadResult.getInteger(EweChatTool.ERRCODE);
                if (Integer.valueOf(0).equals(errCode)) {
                    Map<String, Object> dataMap = parse2FileDataMap(agentid, sendTo, uploadResult.getString(UPLOAD_RESULT_FILE_ID),
                            duplicateCheckInterval); // map转换
                    return sendMsg(corpid, corpsecret, agentid, dataMap);
                }
            }
        }
        return result;
    }

    public static final Result sendFileMsg(String corpid, String corpsecret, String agentid, String sendTo, InputStream inputStream,
            String fileName) {
        return sendFileMsg(corpid, corpsecret, agentid, sendTo, inputStream, fileName, null);
    }

    public static Result sendFileMsg(String sendTo, InputStream inputStream, String fileName, Integer duplicateCheckInterval) {
        EweChatApplicationParams params = getConfig();
        return sendFileMsg(params.getCorpId(), params.getCorpSecret(), params.getAgentId(), sendTo, inputStream, fileName,
                duplicateCheckInterval);

    }

    public static Result sendFileMsg(String sendTo, InputStream inputStream, String fileName) {
        return sendFileMsg(sendTo, inputStream, fileName, null);

    }
}
