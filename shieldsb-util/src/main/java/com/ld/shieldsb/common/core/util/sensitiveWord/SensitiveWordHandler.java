package com.ld.shieldsb.common.core.util.sensitiveWord;

/**
 * 
 * 处理敏感词接口
 * 
 * @ClassName SensitiveWordHandler
 * @author 刘金浩
 * @date 2019年8月30日 下午4:28:27
 *
 */
@FunctionalInterface
public interface SensitiveWordHandler {

    /**
     * 处理敏感词
     * 
     * @Title deal
     * @author 刘金浩
     * @date 2019年7月26日 上午8:03:23
     * @param value
     * @param replaceStr
     * 
     */
    public SensitiveWordResult deal(String value, String replaceStr);
}
