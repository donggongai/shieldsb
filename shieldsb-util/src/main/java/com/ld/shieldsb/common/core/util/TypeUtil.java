package com.ld.shieldsb.common.core.util;

import com.github.zafarkhaja.semver.Version;

public class TypeUtil {
    private TypeUtil() {
        // 工具类无需对象实例化
    }

    public static final String NEED_JAVA8 = "需要Java8以上";

    protected static Version javaVersion = null;
    protected static String javaVersionStr = null;
    private static final Version java8Version = Version.valueOf("1.8.0");
    protected static boolean geJdk8 = false;

    static {
        javaVersionStr = System.getProperty("java.version"); // 比如：1.8.0_291 11.0.12
        javaVersion = Version.valueOf(javaVersionStr);
        geJdk8 = VersionUtil.compareVersion(java8Version, javaVersion) <= 0;
    }

    public static boolean isGeJdk8() {
        return geJdk8;
    }

    public static Version getJavaVersion() {
        return javaVersion;
    }

    /**
     * 获取java版本字符串
     * 
     * @Title getJavaVersionStr
     * @author 吕凯
     * @date 2021年9月1日 上午8:53:08
     * @return String
     */
    public static String getJavaVersionStr() {
        return javaVersionStr;
    }

}
