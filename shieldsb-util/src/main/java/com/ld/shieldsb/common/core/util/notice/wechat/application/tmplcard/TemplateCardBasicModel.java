package com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 基类
 * 
 * @ClassName TemplateCardBasicModel
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年9月13日 下午1:56:11
 *
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class TemplateCardBasicModel {
    private String cardType; // 必填，模板卡片类型，可以参考EwechatTool中类型
    private TmplCardSourceModel source; // 卡片来源样式信息，不需要来源样式可不填写
    private TmplCardMainTitleModel mainTitle; // 一级标题，必填

}
