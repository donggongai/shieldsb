package com.ld.shieldsb.common.core.util.pwdchecker;

import com.ld.shieldsb.common.core.model.Result;

/**
 * 密码校验器
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2020年9月7日 下午12:04:10
 *
 */
public interface PwdChecker {
    public Result checkIsSafe(String newpwd);

}
