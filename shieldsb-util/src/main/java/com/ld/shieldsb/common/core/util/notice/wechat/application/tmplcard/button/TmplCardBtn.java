package com.ld.shieldsb.common.core.util.notice.wechat.application.tmplcard.button;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 按钮
 * 
 * @ClassName TmplCardBtn
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年9月13日 下午3:24:23
 *
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class TmplCardBtn {
    private String text; // 按钮存在必填，按钮文案，建议不超过10个字
    private String style; // 按钮样式，目前可填1~4，不填或错填默认1
    private String key; // 按钮key值，用户点击后，会产生回调事件将本参数作为EventKey返回，回调事件会带上该key值，最长支持1024字节，同一模板卡片中不可重复

}
