package com.ld.shieldsb.common.core.util.notice.wechat.application.markdown;

import java.util.HashMap;
import java.util.Map;

import com.ld.shieldsb.common.core.model.Result;
import com.ld.shieldsb.common.core.util.notice.wechat.EweChatTool;
import com.ld.shieldsb.common.core.util.notice.wechat.Type;
import com.ld.shieldsb.common.core.util.notice.wechat.application.EweChatApplicationParams;
import com.ld.shieldsb.common.core.util.notice.wechat.application.EwechatBasicMsgSender;

/**
 * markdown格式
 * 
 * @ClassName MarkdownMsgSender
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年9月14日 上午9:52:49
 *
 */
public class MarkdownMsgSender extends EwechatBasicMsgSender {
    // ============================================= Markdown消息 ==============================================
    /**
     * 
     * 转换markdown格式dataMap，目前仅支持markdown语法的子集https://work.weixin.qq.com/api/doc/90000/90135/90236#%E6%94%AF%E6%8C%81%E7%9A%84markdown%E8%AF%AD%E6%B3%95，<br>
     * 微工作台（原企业号）不支持展示markdown消息
     * 
     * @Title parse2MarkdownDataMap
     * @author 吕凯
     * @date 2021年4月20日 上午8:39:24
     * @param agentid
     * @param sendTo
     * @param content
     * @param duplicateCheckInterval
     *            重复信息检查时间间隔，单位秒，不检查设置为-1，默认600秒，最大4小时
     * @return Map<String,Object>
     */
    public static Map<String, Object> parse2MarkdownDataMap(String agentid, String sendTo, String content, Integer duplicateCheckInterval) {
        /*    {
        "touser" : "UserID1|UserID2|UserID3", //成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为@all，则向关注该企业应用的全部成员发送
        "toparty" : "PartyID1|PartyID2", //部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
        "totag" : "TagID1 | TagID2", //标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
        "msgtype": "markdown",
        "agentid" : 1,
        "markdown": {
            "content": "您的会议室已经预定，稍后会同步到`邮箱` 
                >**事项详情** 
                >事　项：<font color=\"info\">开会</font> 
                >组织者：@miglioguan 
                >参与者：@miglioguan、@kunliu、@jamdeezhou、@kanexiong、@kisonwang 
                > 
                >会议室：<font color=\"info\">广州TIT 1楼 301</font> 
                >日　期：<font color=\"warning\">2018年5月18日</font> 
                >时　间：<font color=\"comment\">上午9:00-11:00</font> 
                > 
                >请准时参加会议。 
                > 
                >如需修改会议信息，请点击：[修改会议信息](https://work.weixin.qq.com)"
        },
        "enable_duplicate_check": 0,
        "duplicate_check_interval": 1800
        }*/

        Map<String, Object> dataMap = defaultDataMap(agentid, sendTo, duplicateCheckInterval);

        dataMap.put(EweChatTool.MSG_TYPE, Type.MARKDOWN.value);

        Map<String, String> dataContentMap = new HashMap<>();
        dataContentMap.put(CONTENT, content); // 必须有，markdown内容，最长不超过2048个字节，必须是utf8编码
        dataMap.put(Type.MARKDOWN.value, dataContentMap);

        return dataMap;
    }

    public static Map<String, Object> parse2MarkdownDataMap(String agentid, String sendTo, String content) {
        return parse2MarkdownDataMap(agentid, sendTo, content, null);
    }

    public static Result sendMarkdownMsg(String corpid, String corpsecret, String agentid, String sendTo, String content) {
        return sendMarkdownMsg(corpid, corpsecret, agentid, sendTo, content, null);

    }

    public static Result sendMarkdownMsg(String corpid, String corpsecret, String agentid, String sendTo, String content,
            Integer duplicateCheckInterval) {
        return sendMsg(corpid, corpsecret, agentid, parse2MarkdownDataMap(agentid, sendTo, content, duplicateCheckInterval));

    }

    /**
     * 发送markdown消息
     * 
     * @Title sendMarkdownMsg
     * @author 吕凯
     * @date 2021年4月20日 上午8:39:08
     * @param sendTo
     * @param content
     * @return Result
     */
    public static Result sendMarkdownMsg(String sendTo, String content) {
        return sendMarkdownMsg(sendTo, content, null);
    }

    public static Result sendMarkdownMsg(String sendTo, String content, Integer duplicateCheckInterval) {
        // 注意下面参数需要到企业微信中获取
        EweChatApplicationParams params = getConfig();
        Result flag = sendMarkdownMsg(params.getCorpId(), params.getCorpSecret(), params.getAgentId(), sendTo, content,
                duplicateCheckInterval);
        return flag;
    }
}
