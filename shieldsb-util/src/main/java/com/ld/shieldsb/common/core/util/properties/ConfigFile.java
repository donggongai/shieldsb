package com.ld.shieldsb.common.core.util.properties;

import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.ld.shieldsb.common.core.model.Result;

/**
 * 参数管理类
 * 
 * @ClassName SysconfigAction
 * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
 * @date 2014-6-21 下午2:37:40
 * 
 */
public interface ConfigFile {

    /**
     * 获取所有的属性
     * 
     * @Title getPropertyKeys
     * @author 吕凯
     * @date 2016年11月1日 下午3:55:58
     * @return Set<String>
     */
    public List<PropertiesPojo> getPropertys();

    /**
     * 获取属性
     * 
     * @Title getProperty
     * @author 吕凯
     * @date 2019年2月19日 上午11:11:25
     * @param key
     * @return PropertiesPojo
     */
    public PropertiesPojo getProperty(String key);

    /**
     * 获取注释
     * 
     * @Title getComment
     * @author 吕凯
     * @date 2019年2月19日 上午11:27:39
     * @param key
     * @return String
     */
    public String getComment(String key);

    /**
     * 获取所有key
     * 
     * @Title getKeys
     * @author 吕凯
     * @date 2017年12月20日 下午5:52:11
     * @return List<String>
     */
    public List<String> getKeys();

    /**
     * 
     * 根据前缀获取Properties对象,返回的Properties将前缀去除
     * 
     * @Title getProperties
     * @author 吕凯
     * @date 2018年4月26日 下午6:15:53
     * @param prefix
     * @return Properties
     */
    public Properties getProperties(String prefix);

    /**
     * 
     * 根据前缀获取Properties对象
     * 
     * @Title getProperties
     * @author 吕凯
     * @date 2018年4月26日 下午6:16:07
     * @param prefix
     * @param removePrefix,是否移除前缀
     * @return Properties
     */
    public Properties getProperties(String prefix, boolean removePrefix);

    public String getString(String key);

    public List<Object> getList(String key);

    /**
     * 获取字符型的list
     * 
     * @Title getStringList
     * @author 吕凯
     * @date 2019年1月15日 上午8:55:27
     * @param key
     * @return List<String>
     */
    public List<String> getStringList(String key);

    /**
     * 返回属性值
     */
    public String getString(String key, String defaultValue);

    public int getInt(String key, int defaultValue);

    public boolean getBoolean(String key, boolean defaultValue);

    public long getLong(String key, long defaultValue);

    public double getDouble(String key, double defaultValue);

    /**
     * 写入properties信息
     */
    public Result save(String key, Object value);

    /**
     * 保存配置信息
     * 
     * @Title save
     * @author 吕凯
     * @date 2019年2月19日 下午12:04:22
     * @param key
     * @param value
     * @param comment
     *            void
     */
    public Result save(String key, Object value, String comment);

    /**
     * 写入properties信息
     */
    public Result save(Map<String, Object> proMap);

    /**
     * 保存属性
     * 
     * @Title save
     * @author 吕凯
     * @date 2019年2月19日 下午12:10:43
     * @param list
     *            void
     */
    public Result save(List<PropertiesPojo> list);

    /**
     * 加载其他文件
     * 
     * @Title load
     * @author 吕凯
     * @date 2019年10月30日 上午9:33:17
     * @param fileName
     *            可以为url，file或文件路径，inputstream
     * @return Result
     */
    public Result load(Object fileName);

    /**
     * 获取配置文件的Protocol
     * 
     * @Title getProtocol
     * @author 吕凯
     * @date 2019年10月30日 上午10:07:22
     * @return String
     */
    public String getProtocol();

    public URL getURL();

    public String getPath();

    /**
     * 
     * 保存成功的回调函数
     * 
     * @Title saveCallback
     * @author 吕凯
     * @date 2017年12月20日 下午6:01:40 void
     */
    public void saveCallback(String key, Object value);

    /**
     * 
     * 保存成功的回调函数
     * 
     * @Title saveCallback
     * @author 吕凯
     * @date 2017年12月20日 下午6:01:40 void
     */
    public void saveCallback(Map<String, Object> proMap);

    /**
     * 获取文件路径
     * 
     * @Title getFilePath
     * @author 吕凯
     * @date 2018年11月15日 下午2:08:20
     * @return String
     */
    public String getFilePath();

    /**
     * 返回List分隔符
     * 
     * @Title getListDelimiter
     * @author 吕凯
     * @date 2019年3月6日 下午3:05:35
     * @return char
     */
    public char getListDelimiter();

    /**
     * 判断配置文件是否存在
     * 
     * @Title checkFileExist
     * @author 吕凯
     * @date 2021年12月16日 上午10:01:48
     * @return boolean
     */
    public boolean checkFileExist();

}
