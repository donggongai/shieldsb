package com.ld.shieldsb.common.core.util.os;

/**
 * 系统类型
 * 
 * @ClassName OSType
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2018年11月16日 上午9:07:35
 *
 */
public enum OSType {

    Any("any"), Linux("Linux"), Mac_OS("Mac OS"), Mac_OS_X("Mac OS X"), Windows("Windows"), OS2("OS/2"), Solaris("Solaris"), SunOS(
            "SunOS"), MPEiX("MPE/iX"), HP_UX("HP-UX"), AIX("AIX"), OS390("OS/390"), FreeBSD("FreeBSD"), Irix(
                    "Irix"), Digital_Unix("Digital Unix"), NetWare_411("NetWare"), OSF1("OSF1"), OpenVMS("OpenVMS"), Others("Others");

    private OSType(String desc) {
        this.description = desc;
    }

    public String toString() {
        return description;
    }

    private String description;
}
