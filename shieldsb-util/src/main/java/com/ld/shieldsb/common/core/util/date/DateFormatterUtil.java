package com.ld.shieldsb.common.core.util.date;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ld.shieldsb.common.core.exception.ConvertException;
import com.ld.shieldsb.common.core.util.StringUtils;

/**
 * 字符串转日期工具类
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月25日 上午8:46:37
 *
 */
public class DateFormatterUtil {

    // 判断日期格式的正则，yyyy-MM-dd HH:mm:ss或yyyy/MM/dd HH:mm:ss，以及后面带毫秒数和时区的
    private static final String FORMAT_DATETIME_REGEX = "\\d{4}(.)\\d{1,2}(.)\\d{1,2}(\\s|日|号)\\d{1,2}(:|时|点)\\d{1,2}(:|分)\\d{1,2}(秒|\\.(.+))?";
    private static final Pattern FORMAT_DATETIME_PATTERN = Pattern.compile(FORMAT_DATETIME_REGEX);
    // 判断日期格式的正则，yyyy-MM-dd HH:mm或yyyy/MM/dd HH:mm
    private static final String FORMAT_MINUTE_REGEX = "\\d{4}(.)\\d{1,2}(.)\\d{1,2}(\\s|日|号)\\d{1,2}(:|时|点)\\d{1,2}(分)?";
    private static final Pattern FORMAT_MINUTE_PATTERN = Pattern.compile(FORMAT_MINUTE_REGEX);
    // 判断日期格式的正则，yyyy-MM-dd或yyyy/MM/dd或yyyy年MM月dd日
    private static final String FORMAT_DATE_REGEX = "\\d{4}(.)\\d{1,2}(.)\\d{1,2}(日|号)?";
    private static final Pattern FORMAT_DATE_PATTERN = Pattern.compile(FORMAT_DATE_REGEX);

    public static Date convert(String source) {
        if (StringUtils.isBlank(source)) {
            return null;
        }
        source = source.trim();
        try {
            SimpleDateFormat formatter = null;
            if (source.matches(FORMAT_DATETIME_REGEX)) { // 时间，到秒
                formatter = getFormatDatetime(source);
            } else if (source.matches(FORMAT_MINUTE_REGEX)) { // 到分
                formatter = getFormatTimeMinute(source);
            } else if (source.matches(FORMAT_DATE_REGEX)) { // 日期
                formatter = getFormatDate(source);
            }
            if (formatter != null) {
                Date dtDate = formatter.parse(source);
                return dtDate;
            }
        } catch (Exception e) {
            throw new ConvertException(String.format("parser %s to Date fail(日期转换失败)", source), e);
        }

        throw new ConvertException(String.format("parser %s to Date fail(日期转换失败)", source));

    }

    /**
     * 获取时间格式化对象
     * 
     * @Title getFormatDatetime
     * @author 吕凯
     * @date 2021年4月25日 上午10:07:30
     * @param source
     * @return SimpleDateFormat
     */
    private static SimpleDateFormat getFormatDatetime(String source) {
        SimpleDateFormat formatter;
        Matcher matcher = FORMAT_DATETIME_PATTERN.matcher(source);
        matcher.find();// 匹配字符串,匹配到的字符串可以在任何位置
        StringBuilder sb = new StringBuilder("yyyy");
        sb.append(matcher.group(1));
        sb.append("MM");
        sb.append(matcher.group(2));
        sb.append("dd");
        sb.append(matcher.group(3));
        sb.append("HH");
        sb.append(matcher.group(4));
        sb.append("mm");
        sb.append(matcher.group(5));
        sb.append("ss");
        if (matcher.groupCount() == 5) { // 秒,毫秒或时区为6
            String group5 = matcher.group(6);
            if (group5.length() == 1) {
                sb.append(matcher.group(6));
            }
            // 其他暂不处理

        }

        formatter = new SimpleDateFormat(sb.toString());
        return formatter;
    }

    /**
     * 获取分格式化对象
     * 
     * @Title getFormatTimeMinute
     * @author 吕凯
     * @date 2021年4月25日 上午10:07:48
     * @param source
     * @return SimpleDateFormat
     */
    private static SimpleDateFormat getFormatTimeMinute(String source) {
        SimpleDateFormat formatter;
        Matcher matcher = FORMAT_MINUTE_PATTERN.matcher(source);
        matcher.find();// 匹配字符串,匹配到的字符串可以在任何位置
        StringBuilder sb = new StringBuilder("yyyy");
        sb.append(matcher.group(1));
        sb.append("MM");
        sb.append(matcher.group(2));
        sb.append("dd");
        sb.append(matcher.group(3));
        sb.append("HH");
        sb.append(matcher.group(4));
        sb.append("mm");
        if (matcher.groupCount() > 5) {
            String group5 = matcher.group(5);
            if (group5.length() == 1) {
                sb.append(matcher.group(5)); // 分
            }
            // 其他暂不处理

        }

        formatter = new SimpleDateFormat(sb.toString());
        return formatter;
    }

    /**
     * 获取日期格式化对象
     * 
     * @Title getFormatDate
     * @author 吕凯
     * @date 2021年4月25日 上午10:08:00
     * @param source
     * @return SimpleDateFormat
     */
    private static SimpleDateFormat getFormatDate(String source) {
        SimpleDateFormat formatter;
        Matcher matcher = FORMAT_DATE_PATTERN.matcher(source);
        matcher.find();// 匹配字符串,匹配到的字符串可以在任何位置
        StringBuilder sb = new StringBuilder("yyyy");
        sb.append(matcher.group(1));
        sb.append("MM");
        sb.append(matcher.group(2));
        sb.append("dd");
        if (matcher.groupCount() > 3) {
            String group3 = matcher.group(3);
            if (group3.length() == 1) {
                sb.append(matcher.group(3)); // 秒
            }
            // 其他暂不处理

        }

        formatter = new SimpleDateFormat(sb.toString());
        return formatter;
    }

}
