package com.ld.shieldsb.common.core.util.notice.wechat.webhook;

import lombok.Builder;
import lombok.Data;

/**
 * webhook(机器人api)消息参数，主要为文本消息使用
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月19日 下午2:53:21
 *
 */
@Builder
@Data
public class EweChatWebHookMsgParams {
    private String content; // 内容
    private String noticeIds; // 通知的id，多个账号用逗号连接，
    private String noticeMobiles; // 通知的手机号，多个账号用逗号连接，
    private Boolean noticeAll; // 是否通知全部

}
