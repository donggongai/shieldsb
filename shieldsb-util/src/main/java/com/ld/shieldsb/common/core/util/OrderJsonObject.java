package com.ld.shieldsb.common.core.util;

import com.alibaba.fastjson.JSONObject;

/**
 * 有序JsonObject，原来的JSONObject无序，不满足一些对顺序有要求的场合
 * 
 * @ClassName OrderJsonObject
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年12月17日 上午11:19:33
 *
 */
public class OrderJsonObject extends JSONObject {
    /**
     * @Fields serialVersionUID
     */
    private static final long serialVersionUID = 2707258556013120878L;

    public OrderJsonObject() {
        super(true);
    }

}
