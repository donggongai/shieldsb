package com.ld.shieldsb.common.core.cache.caffeine;

import com.github.benmanes.caffeine.cache.Cache;
import com.ld.shieldsb.common.core.cache.CacheResult;

import lombok.Data;

@Data
public class CaffeineCacheResult<K, V> implements CacheResult {
    private Cache<K, V> cache;
}
