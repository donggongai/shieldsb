[TOC]

### 导出pdf模块

导出pdf，注意如果pdf中用到字体，本模块不提供，需要自己放到具体项目的class目录下新建fonts目录，把需要的字体放进去，程序会自动调取，调取时模板放到项目的class目录的templates下

#### 版本说明
##### v1.0.0（2019-10-16）
- 初版发布;
- ;

#### 参考资料

 https://www.w3cplus.com/css/designing-for-print-with-css.html 打印样式设计
 https://www.cnblogs.com/huashanqingzhu/p/4345698.html CSS控制print打印样式
 http://www.princexml.com/download/ prince工具，预览html导出pdf的效果
 http://www.princexml.com/samples/ prince提供的标准示例
 https://blog.csdn.net/qq_31980421/article/details/79662988 flying-saucer + iText + Freemarker生成pdf表格跨页问题
