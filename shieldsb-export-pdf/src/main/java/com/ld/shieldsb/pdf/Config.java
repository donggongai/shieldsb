package com.ld.shieldsb.pdf;

import lombok.Data;

@Data
public class Config {
    private boolean encrypt = true; // 是否加密
    private String encryptPwd = "11304ldi1ik6|y"; // 加密密码

    private boolean checkHtml = true; // 是否进行html校验

}
