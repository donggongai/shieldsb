package com.ld.shieldsb;

import java.util.HashMap;
import java.util.Map;

import com.ld.shieldsb.common.core.util.date.DateUtil;
import com.ld.shieldsb.pdf.PdfDocumentGenerator;
import com.ld.shieldsb.pdf.exception.DocumentGeneratingException;

public class Demo {
    public static void main(String[] args) {
        // 示例，注意因字体比较大，jar包中不提供，字体需要手动添加到项目classes的fonts目录中，模板中用到了SimHei和STXihei，如果不想引入可以修改为其他字体
        // 注意如果引用了网络图片，需要确定网络图片能访问，否则可能加载很长时间
        // 模板样式已经比较全，可以参考
        String template = "demo.html"; // 模板路径，相对于class目录的templates目录的相对路径
        // 生
        PdfDocumentGenerator pdfDocumentGenerator = new PdfDocumentGenerator();
        Map<String, Object> mapDataV = new HashMap<String, Object>();
        mapDataV.put("type", 1);
        mapDataV.put("bh", 12345678);
        mapDataV.put("realName", "潍坊测试用国内公司");
        mapDataV.put("purpose", "测试在水电费项目");
        mapDataV.put("tendercode", "潍项XAB-123456");
        mapDataV.put("tenderer", "潍坊绿盾");
        mapDataV.put("loanAmounts", "我是代理公司");
        mapDataV.put("printTime", DateUtil.getNowDateTime());
        mapDataV.put("creditLevel", "AAA");
        mapDataV.put("pingfen", "92.12");
        mapDataV.put("reportSiteUrl", "http://www.11315.com"); // 二维码
        mapDataV.put("address", "潍县中路口");
        mapDataV.put("setupDate", "2017-01-01");
        mapDataV.put("legalPerson", "法人代表是我");
        mapDataV.put("businessCall", "15200000000");
        mapDataV.put("fax", "123");
        mapDataV.put("registerFund", 10000);
        mapDataV.put("mainBrand", "士大夫为二浮士德发潍坊的说的分手费发");
        mapDataV.put("mainProduct", "士大夫为二浮士德发潍坊的说的分手费发");
        mapDataV.put("information", "士大夫为二浮士德发潍坊的说的分手费发，符文法师的，士大夫违法的事，封位符随碟附送的，为二浮士德发为二浮士德发是舒服舒服我士大夫士大夫 ");

        Map<String, Object> icpJibenModel = new HashMap<String, Object>();
        icpJibenModel.put("xinyongdaima", "91621202MA71T45H0W");
        icpJibenModel.put("realname", "潍坊测试用国内公司");
        icpJibenModel.put("leixing", "有限责任公司");
        icpJibenModel.put("qiyefaren", "法人代表是我");
        icpJibenModel.put("zhuceziben", "1000万");
        icpJibenModel.put("zczb_bizhong", "");
        icpJibenModel.put("chengliriqi", "");
        icpJibenModel.put("yingyeqixianzi", "");
        icpJibenModel.put("yingyeqixianzhi", "");
        icpJibenModel.put("jingyingfanwei", "潍舍夫多娃额士大夫士大夫为二浮士德发，发是为神坊测试用国内公司");
        icpJibenModel.put("dengjijiguan", "潍坊水电所");
        icpJibenModel.put("realname", "潍坊测试用国内公司");
        mapDataV.put("icpJiben", true);
        mapDataV.put("icpJibenModel", icpJibenModel);

        mapDataV.put("icpGudongNum", 0);
        mapDataV.put("zyryListNo", 0);

        String outputFile = "reports/test.pdf";
        try {
            pdfDocumentGenerator.generate(template, mapDataV, outputFile);
        } catch (DocumentGeneratingException e) {
            e.printStackTrace();
        }
    }

}
