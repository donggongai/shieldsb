package com.ld.shieldsb.jgit.model;

import java.util.List;

import lombok.Data;

/**
 * 提交记录hash
 * 
 * @ClassName CommitHashModel
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年6月7日 下午4:11:48
 *
 */
@Data
public class BranchCommitHashModel {
    private String branch;
    private List<CommitHashModel> list;

}
