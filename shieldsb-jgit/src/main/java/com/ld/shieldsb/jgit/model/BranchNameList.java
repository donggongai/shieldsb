package com.ld.shieldsb.jgit.model;

import java.util.List;

import lombok.Data;

/**
 * 分支名称
 * 
 * @ClassName BranchNameList
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年6月11日 下午1:52:56
 *
 */
@Data
public class BranchNameList {
    private List<BranchNameModel> localBranchs; // 本地分支
    private List<BranchNameModel> remoteBranchs; // 远程分支
    private List<BranchNameModel> tags; // 标签名称
}
