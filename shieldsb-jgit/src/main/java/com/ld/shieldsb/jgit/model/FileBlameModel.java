package com.ld.shieldsb.jgit.model;

import org.eclipse.jgit.lib.PersonIdent;

import lombok.Data;

/**
 * 文件责任分析model
 * 
 * @ClassName FileBlameModel
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年6月10日 下午4:19:35
 *
 */
@Data
public class FileBlameModel {
    private PersonIdent persion; // 责任人信息
    private GitCommitInfoModel info; // 基本信息，包括提交人信息等
    private String content;// 内容
}
