package com.ld.shieldsb.jgit.model;

import java.util.List;

import lombok.Data;

/**
 * git变化记录对象
 * 
 * @ClassName GitCount
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年6月7日 下午4:33:14
 *
 */
@Data
public class GitCommitInfoModel {
    private String id; // gitid
    private String author; // 作者
    private String email; // 邮件

    private String project; // 项目名称
    private List<String> branch; // 分支

    private String commitTime; // 提交时间
    private String commitMessage; // 提交信息

    private int addLine; // 添加行数
    private int removeLine; // 移除行数
    private int isMerge;// 是否合并

    private List<String> parentId; // 父类id，即上一条记录
    private List<String> childId; // 子类id，即下一条记录

    private String object; // 文件内容

}
