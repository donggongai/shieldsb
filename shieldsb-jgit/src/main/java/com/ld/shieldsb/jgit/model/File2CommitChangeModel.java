package com.ld.shieldsb.jgit.model;

import lombok.Data;

/**
 * 文件两次提交的变化
 * 
 * @ClassName File2CommitChangeModel
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年6月10日 下午3:00:47
 *
 */
@Data
public class File2CommitChangeModel {
    private String newContent; // 最新提交的内容
    private String oldContent; // 旧提交的内容
}
