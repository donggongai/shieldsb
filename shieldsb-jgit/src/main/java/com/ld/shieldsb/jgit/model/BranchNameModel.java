package com.ld.shieldsb.jgit.model;

import lombok.Data;

/**
 * 名称封装类
 * 
 * @ClassName BranchNameModel
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年6月11日 下午1:54:43
 *
 */
@Data
public class BranchNameModel {
    private String fullName; // 完整名称
    private String shortName; // 简短名称

}
