package com.ld.shieldsb.jgit.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class QueryParams {
    private String branch; // 分支
    private String commiter;// 提交人
    private String message; // 消息
    private Integer pageNum; // 页数
    private Integer pageSize; // 每页条数

}
