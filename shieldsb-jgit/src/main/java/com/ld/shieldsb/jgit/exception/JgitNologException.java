package com.ld.shieldsb.jgit.exception;

public class JgitNologException extends Exception {

    /**
     * @Fields serialVersionUID
     */
    private static final long serialVersionUID = -7944861817462994119L;

    public JgitNologException() {
        super();
    }

    public JgitNologException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public JgitNologException(String message, Throwable cause) {
        super(message, cause);
    }

    public JgitNologException(String message) {
        super(message);
    }

    public JgitNologException(Throwable cause) {
        super(cause);
    }

}
