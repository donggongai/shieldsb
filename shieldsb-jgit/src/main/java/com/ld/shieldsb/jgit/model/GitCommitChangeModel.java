package com.ld.shieldsb.jgit.model;

import java.util.List;

import org.eclipse.jgit.diff.DiffEntry;

import lombok.Data;

/**
 * git变化记录对象
 * 
 * @ClassName GitCount
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年6月7日 下午4:33:14
 *
 */
@Data
public class GitCommitChangeModel {
    private GitCommitInfoModel info; // 基本信息
    private List<DiffEntry> diff; // 改变信息

}
