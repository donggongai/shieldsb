package com.ld.shieldsb.jgit.model;

import lombok.Data;

/**
 * 增删行数
 * 
 * @ClassName LineDiff
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年6月7日 下午4:29:09
 *
 */
@Data
public class LineDiff {
    private int addLine = 0; // 增加行数
    private int removeLine = 0; // 删除行数
}
