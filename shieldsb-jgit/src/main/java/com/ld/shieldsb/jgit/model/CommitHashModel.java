package com.ld.shieldsb.jgit.model;

import java.util.List;

import org.eclipse.jgit.revwalk.RevCommit;

import lombok.Data;

/**
 * 提交记录hash
 * 
 * @ClassName CommitHashModel
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年6月7日 下午4:11:48
 *
 */
@Data
public class CommitHashModel {
    private RevCommit commitHash;
    private List<RevCommit> commitParentHash;

}
