package com.ld.shieldsb.jgit;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.lib.FileMode;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.ld.shieldsb.common.core.collections.ListUtils;
import com.ld.shieldsb.common.core.util.PathUtil;
import com.ld.shieldsb.common.core.util.StringUtils;
import com.ld.shieldsb.jgit.model.File2CommitChangeModel;
import com.ld.shieldsb.jgit.model.GitCommitInfoModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JgitTest {
    private String repositoryUrl = "http://lk@119.191.58.20:8881/r/utils/shieldjs.git";
    private String username = "lk";
    private String password = "11235813";

    private String versionCommitId = "be62e4b4bf2e1de5bb875451e23543be9b7f3334";// 需要分析的Commit Hash
    private String softwareCenterModulesPath = "F:\\workspace2021\\newframework\\software-center-modules";// 对应项目在本地Repo的路径

    @Test(priority = 0) // 不指定顺序时默认按字母顺序执行
    public void testDiffFile() {
        String path = softwareCenterModulesPath;// 对应项目在本地Repo的路径
        FileRepositoryBuilder builder = new FileRepositoryBuilder();
        builder.setMustExist(true);
        builder.addCeilingDirectory(new File(path));
        builder.findGitDir(new File(path));

        try (Repository repo = builder.build(); RevWalk walk = new RevWalk(repo);) {

            ObjectId versionId = repo.resolve(versionCommitId);
            RevCommit verCommit = walk.parseCommit(versionId);
            List<DiffEntry> diffFix = GitUtil.getChangedFileList(verCommit, repo);
            for (DiffEntry entry : diffFix) {
                log.warn("---差异实体类路径: {}", entry.getChangeType());
            }
            System.out.println("=================== 只传路径和提交id ======================");
            List<DiffEntry> diffFix2 = GitUtil.getChangedFileList(path, versionCommitId).getDiff();
            diffFix2.forEach(System.out::println);
            for (DiffEntry entry : diffFix2) {
                System.out.println("===差异比较开始: " + entry + ", 从: " + entry.getOldId() + ", 到: " + entry.getNewId());
                try (DiffFormatter formatter = new DiffFormatter(System.out)) {
                    formatter.setRepository(repo);
                    formatter.format(entry);
                }
                System.out.println("===差异比较结束: " + entry + ", 从: " + entry.getOldId() + ", 到: " + entry.getNewId());
            }
            Assert.assertEquals(diffFix.size(), diffFix2.size());
        } catch (IOException e) {
            log.warn("", e);
        }
    }

    /**
     * 获取所有的分支
     * 
     * @Title testBranchs
     * @author 吕凯
     * @date 2021年6月9日 上午9:31:57 void
     */
    @Test(priority = 1) // 不指定顺序时默认按字母顺序执行
    public void testBranchs() {
        List<String> branchs = GitUtil.getRemoteBranchs(repositoryUrl, username, password);
        log.warn("{}", branchs);
        Assert.assertTrue(ListUtils.isNotEmpty(branchs));
    }

    @Test(priority = 2) // 不指定顺序时默认按字母顺序执行
    public void testLocalVersion() throws IOException, GitAPIException, Exception {
        long start = System.currentTimeMillis();
        // 输出远程仓库的最新版本号
        String localPath = PathUtil.getTmpDir(); // 临时文件夹
        String projectName = "";
        if (new File(localPath).exists()) {
            projectName = repositoryUrl.substring(repositoryUrl.lastIndexOf('/') + 1, repositoryUrl.lastIndexOf('.'));
            localPath += File.separator + ".git_remote" + File.separator + projectName;
        }
        String localRepository = localPath;
        List<GitCommitInfoModel> list = GitUtil.getGitVersions(localRepository);
        long end = System.currentTimeMillis();
        log.warn("spend:{}", end - start);
        log.warn("{}", list);
        log.warn("size:{}", list.size());
    }

    /**
     * 获取远程仓库的所有修改记录
     * 
     * @Title testCommits
     * @author 吕凯
     * @date 2021年6月9日 上午9:34:51
     * @throws IOException
     * @throws GitAPIException
     * @throws Exception
     *             void
     */
    @Test(priority = 3) // 不指定顺序时默认按字母顺序执行
    public void testCommits() throws IOException, GitAPIException, Exception {

        String localPath = PathUtil.getTmpDir(); // 临时文件夹
        String projectName = "";
        String projectBranch = "master";
        if (new File(localPath).exists()) {
            projectName = repositoryUrl.substring(repositoryUrl.lastIndexOf('/') + 1, repositoryUrl.lastIndexOf('.'));
            localPath += ".git_remote" + File.separator + projectName;
        }

        List<String> branchs = GitUtil.getRemoteBranchs(repositoryUrl, username, password);
        log.warn("分支：：{}", branchs);
        projectBranch = branchs.get(0);

        File file = new File(localPath);
        // 如果文件夹不存在则创建，否则直接pull
        if (!file.exists() && !file.isDirectory()) {
            log.warn(projectName + "项目不存在！正在进行创建并克隆此项目。。。");
            file.mkdirs(); // 级联创建父目录
            GitUtil.gitClone(username, password, repositoryUrl, projectBranch, localPath);
            log.warn(projectName + "项目创建并克隆完成！");
        } else {
            GitUtil.gitPull(username, password, localPath, projectBranch);
            log.warn(projectName + "项目pull完成！");
        }

        // 获取git统计数据
        List<GitCommitInfoModel> list = GitStaticUtil.commitResolver(localPath, projectName, projectBranch);
        log.warn("统计{}项目数据完成！", projectName);
        // 循环批量插入数据库
        for (GitCommitInfoModel gitCount : list) {
            log.warn("model:::{}", gitCount);
        }

    }

    /**
     * 查询某个文件的提交记录
     * 
     * @Title testFileVersions
     * @author 吕凯
     * @date 2021年6月9日 下午2:51:14
     * @throws IOException
     * @throws GitAPIException
     * @throws Exception
     *             void
     */
    @Test(priority = 4) // 不指定顺序时默认按字母顺序执行
    public void testFileVersions() throws IOException, GitAPIException, Exception {
        long start = System.currentTimeMillis();
        // 输出远程仓库的最新版本号
        String localPath = PathUtil.getTmpDir(); // 临时文件夹
        String projectName = "";
        if (new File(localPath).exists()) {
            projectName = repositoryUrl.substring(repositoryUrl.lastIndexOf('/') + 1, repositoryUrl.lastIndexOf('.'));
            localPath += File.separator + ".git_remote" + File.separator + projectName;
        }
        String localRepository = localPath;
        List<GitCommitInfoModel> list = GitUtil.getFileVersions(localRepository,
                "src/main/resources/META-INF/resources/webjars/touchjs/touchjs.core.js"); // 文件路径为相对项目的路径
        long end = System.currentTimeMillis();
        log.warn("spend:{}", end - start);
        log.warn("{}", list);
        log.warn("size:{}", list.size());
    }

    /**
     * 列出文件
     * 
     * @Title testListFilesOfCommitAndTag
     * @author 吕凯
     * @date 2021年6月10日 上午11:46:06
     * @throws IOException
     * @throws GitAPIException
     * @throws Exception
     *             void
     */
    @Test(priority = 4) // 不指定顺序时默认按字母顺序执行
    public void testListFilesOfCommitAndTag() throws IOException, GitAPIException, Exception {
        long start = System.currentTimeMillis();
        String path = softwareCenterModulesPath;// 对应项目在本地Repo的路径
        FileRepositoryBuilder builder = new FileRepositoryBuilder();
        builder.setMustExist(true);
        builder.addCeilingDirectory(new File(path));
        builder.findGitDir(new File(path));

        try (Repository repository = builder.build();) {
            List<String> paths = readElementsAt(repository, versionCommitId,
                    "software-center-worktask/src/main/resources/META-INF/resources/WEB-INF/view/a/worktask/test/report");

            System.out.println("Had paths for commit: " + paths);

            String tag = "master";
            String dirpath = "software-center-worktask/src/main/resources/META-INF/resources/WEB-INF/view/a/worktask/test/report";
            final ObjectId testbranch = repository.resolve(tag);
            paths = readElementsAt(repository, testbranch.getName(), dirpath);

            log.warn("分支{}的目录{}存在文件 Had paths for tag: ", tag, dirpath, paths);
        }
        long end = System.currentTimeMillis();
        log.warn("spend:{}", end - start);
    }

    @Test(priority = 4) // 不指定顺序时默认按字母顺序执行
    public void testListFilesOfCommit() throws IOException, GitAPIException, Exception {
        long start = System.currentTimeMillis();

        String path = softwareCenterModulesPath;// 对应项目在本地Repo的路径
        FileRepositoryBuilder builder = new FileRepositoryBuilder();
        builder.setMustExist(true);
        builder.addCeilingDirectory(new File(path));
        builder.findGitDir(new File(path));

        try (Repository repository = builder.build();) {
            try (Git git = new Git(repository)) {
                // to compare against the "previous" commit, you can use
                // the caret-notation
                // ^表示前几个父提交 ^前一次的对比
                GitUtil.listDiff(repository, git, versionCommitId + "^", versionCommitId);
            }
        }
        long end = System.currentTimeMillis();
        log.warn("spend:{}", end - start);
    }

    @Test(priority = 6) // 不指定顺序时默认按字母顺序执行
    public void readFileFromCommit() throws IOException, GitAPIException, Exception {
        long start = System.currentTimeMillis();

        String path = softwareCenterModulesPath;// 对应项目在本地Repo的路径
        String filePath = "software-center-worktask/src/main/resources/META-INF/resources/worktask/css/worktask.css";
        GitUtil.readFile2OutputStreamFromCommit(path, versionCommitId, filePath, System.out);
        // 上一次提交
        GitUtil.readFile2OutputStreamFromCommit(path, versionCommitId + "^", filePath, System.out);
        long end = System.currentTimeMillis();
        log.warn("spend:{}", end - start);

        File2CommitChangeModel contents = GitUtil.readFile2ContentFromCommit(path, versionCommitId + "^", versionCommitId, filePath);
        log.warn("{}", contents);
        Assert.assertTrue(StringUtils.isNotEmpty(contents.getNewContent())); // 期望结果：：true，新内容不为空
        Assert.assertTrue(StringUtils.isNotEmpty(contents.getOldContent()));// 期望结果：：true，旧内容不为空
        long end1 = System.currentTimeMillis();
        log.warn("spend:{}", end1 - end);
    }

    @Test(priority = 7) // 不指定顺序时默认按字母顺序执行
    public void testBlameFile() throws IOException, GitAPIException, Exception {
        long start = System.currentTimeMillis();

        String path = softwareCenterModulesPath;// 对应项目在本地Repo的路径
        String filePath = "software-center-worktask/src/main/resources/META-INF/resources/worktask/css/worktask.css";
        GitUtil.blameFile(path, filePath);
        long end = System.currentTimeMillis();
        log.warn("spend:{}", end - start);

    }

    // =================================================================================================================
    private static List<String> readElementsAt(Repository repository, String commit, String path) throws IOException {
        RevCommit revCommit = buildRevCommit(repository, commit);

        // and using commit's tree find the path
        RevTree tree = revCommit.getTree();
        // System.out.println("Having tree: " + tree + " for commit " + commit);

        List<String> items = new ArrayList<>();

        // shortcut for root-path
        if (path.isEmpty()) {
            try (TreeWalk treeWalk = new TreeWalk(repository)) {
                treeWalk.addTree(tree);
                treeWalk.setRecursive(false);
                treeWalk.setPostOrderTraversal(false);

                while (treeWalk.next()) {
                    items.add(treeWalk.getPathString());
                }
            }
        } else {
            // now try to find a specific file
            try (TreeWalk treeWalk = buildTreeWalk(repository, tree, path)) {
                if ((treeWalk.getFileMode(0).getBits() & FileMode.TYPE_TREE) == 0) {
                    throw new IllegalStateException("Tried to read the elements of a non-tree for commit '" + commit + "' and path '" + path
                            + "', had filemode " + treeWalk.getFileMode(0).getBits());
                }

                try (TreeWalk dirWalk = new TreeWalk(repository)) {
                    dirWalk.addTree(treeWalk.getObjectId(0));
                    dirWalk.setRecursive(false);
                    while (dirWalk.next()) {
                        items.add(dirWalk.getPathString());
                    }
                }
            }
        }

        return items;
    }

    private static RevCommit buildRevCommit(Repository repository, String commit) throws IOException {
        // a RevWalk allows to walk over commits based on some filtering that is defined
        try (RevWalk revWalk = new RevWalk(repository)) {
            return revWalk.parseCommit(ObjectId.fromString(commit));
        }
    }

    private static TreeWalk buildTreeWalk(Repository repository, RevTree tree, final String path) throws IOException {
        TreeWalk treeWalk = TreeWalk.forPath(repository, path, tree);

        if (treeWalk == null) {
            throw new FileNotFoundException("Did not find expected file '" + path + "' in tree '" + tree.getName() + "'");
        }

        return treeWalk;
    }

}
