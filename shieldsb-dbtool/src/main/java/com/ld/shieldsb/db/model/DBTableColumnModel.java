package com.ld.shieldsb.db.model;

import java.io.Serializable;

import com.ld.shieldsb.db.service.DBTabelService;

import lombok.Data;

@Data
public class DBTableColumnModel implements Serializable {

    private static final long serialVersionUID = 7392984578665152894L;
    private String tableName; // 表名
    private String columnName; // 列名
    private String columnType; // 列类型（要求包含类长度，不同库有区别，不包含长度的需要手动处理）
    private String dataType; // 列类型（不包含类长度）
    private Integer datasize; // 大小
    private Integer digits; // 精度
    private Boolean nullable; // 是否可为空
    private Object defaultValue; // 默认值
    private String remarks; // 注释
    private String collation; // mysql中的检查

    private String javaDataType; // 转为列类型
    private String exportColName;// 导出字段名
    private Boolean isPrimaryKey; // 是否为主键

    public String getJavaDataType() {
        if (javaDataType == null) {
            if (dataType == null) {
                return "";
            }
            Integer digitsV = digits;
            if (digitsV == null) {
                digitsV = 0;
            }
            Integer datasizeV = datasize;
            if (datasizeV == null) {
                datasizeV = 0;
            }
            return DBTabelService.dbType2JavaType(dataType.toLowerCase(), digitsV < 0 ? 0 : digitsV,
                    datasizeV > 0 ? datasizeV : Math.abs(digitsV));
        } else {
            return javaDataType;
        }
    }

    /**
     * 导出名称
     * 
     * @Title getExportColName
     * @author 吕凯
     * @date 2018年12月19日 上午11:44:23
     * @return String
     */
    public String getExportColName() {
        return exportColName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        DBTableColumnModel other = (DBTableColumnModel) obj;
        if (columnName == null) {
            if (other.columnName != null) {
                return false;
            }
        } else if (!columnName.equals(other.columnName)) {
            return false;
        }
        if (tableName == null) {
            if (other.tableName != null) {
                return false;
            }
        } else if (!tableName.equals(other.tableName)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((columnName == null) ? 0 : columnName.hashCode());
        result = prime * result + ((tableName == null) ? 0 : tableName.hashCode());
        return result;
    }

}
