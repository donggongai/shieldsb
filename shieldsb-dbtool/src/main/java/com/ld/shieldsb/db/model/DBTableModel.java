package com.ld.shieldsb.db.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class DBTableModel implements Serializable {

    /**
     * @Fields serialVersionUID
     */
    private static final long serialVersionUID = 860860609774868712L;
    private String tableName; // 表名
    private Long dataSize; // 数据量
    private String tableComments; // 表注释

    private List<DBTableColumnModel> columns = new ArrayList<>();

}
