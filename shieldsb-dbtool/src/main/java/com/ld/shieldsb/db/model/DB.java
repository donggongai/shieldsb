package com.ld.shieldsb.db.model;

/**
 * 数据库类
 * 
 * @ClassName DB
 * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
 * @date 2016年8月29日 上午9:13:39
 *
 */
public class DB {

    private DB() {
        super();
    }

    public static final String ORACLE = "oracle";
    public static final String MYSQL = "mysql";
    public static final String SQLSERVER = "sqlserver";
    public static final String DB2 = "db2";
    public static final String INFORMIX = "Informix";
    public static final String SYBASE = "Sybase";
    public static final String POSTGRESQL = "PostgreSQL";
    public static final String TERADATA = "Teradata";
    public static final String NETEZZA = "Netezza";

    /**
     * 获取数据库类型
     * 
     * @Title getDBType
     * @author 吕凯
     * @date 2016年8月29日 上午9:10:02
     * @return String
     */
    public static String getDBType(String dbUrl) {
        String dbType = "";
        if (dbUrl != null) {
            if (dbUrl.contains("jdbc:mysql:")) {
                dbType = DB.MYSQL;
            } else if (dbUrl.contains("jdbc:oracle:")) {
                dbType = DB.ORACLE;
            } else if (dbUrl.contains("jdbc:microsoft:sqlserver:")) {// SQLServer 2000
                dbType = DB.SQLSERVER;
            } else if (dbUrl.contains("jdbc:sqlserver:")) { // SQL Server 2005
                dbType = DB.SQLSERVER;
            } else if (dbUrl.contains("jdbc:db2:")) { // IBM DB2
                dbType = DB.DB2;
            } else if (dbUrl.contains("jdbc:informix-sqli:")) { // Informix
                dbType = DB.INFORMIX;
            } else if (dbUrl.contains("jdbc:sybase:")) { // Sybase
                dbType = DB.SYBASE;
            } else if (dbUrl.contains("jdbc:postgresql:")) { // PostgreSQL
                dbType = DB.POSTGRESQL;
            } else if (dbUrl.contains("jdbc:teradata:")) { // Teradata
                dbType = DB.TERADATA;
            } else if (dbUrl.contains("jdbc:netezza:")) { // Netezza
                dbType = DB.NETEZZA;
            }

        }
        return dbType;
    }
}