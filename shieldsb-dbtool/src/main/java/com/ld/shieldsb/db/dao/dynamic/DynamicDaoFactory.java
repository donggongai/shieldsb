package com.ld.shieldsb.db.dao.dynamic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DynamicDaoFactory {
    private static final Map<String, DynamicBaseDao> DYNAMIC_DAOS_MAP = new HashMap<>();
    private static final Map<String, String> DYNAMIC_LINK_PWD_MAP = new HashMap<>();

    /**
     * 
     * 获取dao
     * 
     * @Title getDynamicDao
     * @author 吕凯
     * @date 2018年2月3日 上午11:08:52
     * @param url
     * @param userName
     * @param password
     * @return DynamicBaseDao
     */
    public static DynamicBaseDao getDynamicDao(String url, String userName, String password) {
        return getDynamicDao(url, userName, password, null);
    }

    public static DynamicBaseDao getDynamicDao(String url, String userName, String password, Map<String, String> exitenProperties) {
        String key = url + userName;
        DynamicBaseDao dao = DYNAMIC_DAOS_MAP.get(key);
        String linkPwd = DYNAMIC_LINK_PWD_MAP.get(key);
        if (linkPwd == null || !linkPwd.equals(password) || dao == null) { // 密码变化需要重新赋值
            synchronized (DYNAMIC_DAOS_MAP) {
                dao = DYNAMIC_DAOS_MAP.get(key);
                linkPwd = DYNAMIC_LINK_PWD_MAP.get(key);
                if (linkPwd == null || !linkPwd.equals(password) || dao == null) {
                    dao = new DynamicBaseDao(url, userName, password, exitenProperties);
                    DYNAMIC_DAOS_MAP.put(key, dao);
                    DYNAMIC_LINK_PWD_MAP.put(key, password);
                }
            }
        }
        return dao;
    }

    /**
     * 移除dao
     * 
     * @Title removeDao
     * @author 吕凯
     * @date 2018年2月3日 下午12:35:05
     * @param url
     * @param userName
     * @param password
     * @return DynamicBaseDao
     */
    public static DynamicBaseDao removeDao(String url, String userName, String password) {
        String key = url + userName;
        DynamicBaseDao dao = DYNAMIC_DAOS_MAP.get(key);
        if (dao != null) {
            // 删除
            DYNAMIC_DAOS_MAP.remove(key);
            DYNAMIC_LINK_PWD_MAP.put(key, password);
        }
        return dao;
    }

    /**
     * 清空所有dao
     * 
     * @Title clear
     * @author 吕凯
     * @date 2018年2月24日 上午9:07:57 void
     */
    public static void clear() {
        synchronized (DYNAMIC_DAOS_MAP) {
            DYNAMIC_DAOS_MAP.clear();
            DYNAMIC_LINK_PWD_MAP.clear();
        }
    }

    // [start] zhushi

    public static void main(String[] args) throws Exception {
//        YtDatabaseSetModel dataBase = MYSQL_BASE_DAO.findById(YtDatabaseSetModel.class, "1518337675633");
        // ConnectionUtil.init(dataBase.getDbdriver(), dataBase.getDburl(), dataBase.getDbusername(), dataBase.getDbpassword());
        // System.out.println(System.currentTimeMillis());
//        DynamicBaseDao baseDao = DynamicDaoFactory.getDynamicDao("jdbc:Access:///d:\\Database1.accdb", "", "");
//        System.err.println(baseDao.getCon());
        // System.out.println(baseDao);
        // System.out.println(System.currentTimeMillis());
        // List<String> list = baseDao.getAllFields("SELECT content_Id as id,title,content,sex,phone FROM content_test");

        // System.out.println(list);
        // System.out.println(System.currentTimeMillis());
        // =============sqlite
//        DynamicBaseDao baseDao = DynamicDaoFactory.getDynamicDao("jdbc:sqlite:///G:\\sqlLite\\test.db", "", "");
//        System.err.println(baseDao.getCon());
////        List<Map<String, Object>> listMaps = baseDao.getSqlMapList("PRAGMA table_info(['test'])");
//
//        DynaBean list = baseDao.getOne("PRAGMA table_info([test])");
//        System.exit(0);
        // ====sybase ==
        DynamicBaseDao baseDao = DynamicDaoFactory.getDynamicDao("jdbc:sybase:Tds:192.168.1.233:5000/master", "sa", "fanfan");
        log.warn(baseDao.getCon() + "");
//        String tableSql = "select name as TABLE_NAME from  sysobjects where type=\"U\" and crdate > (select crdate from sysobjects where name=\"jdbc_function_escapes\" )";
        String testSql = "select * from test";
        List<Map<String, Object>> listMaps = baseDao.getSqlMapList(testSql);
        log.warn(listMaps + "");
        // System.out.println(System.currentTimeMillis());
        System.exit(0);

    }
    // [end]

}
