package com.ld.shieldsb.annotation.field.link;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * model中多对多关系，用于有中间表的对象处理<br>
 * 示例：<br>
 * 2个表，用户表对象User，角色表对象Role，中间表对象UserRoles<br>
 * User中有字段id表示主键，Role中有字段id主键，中间表user_roles保存两个表关系，roleId对应Role对象的id，userId对应User对象的id，在该属性上定义注解<br>
 * 
 * <pre>
 * 
 * &#64;OnlyShow <br>
 * &#64;Many2Many(relation = SysUserRolesModel.class, relationFrom = "userId", relationTo = "roleId") <br>
 * private List<SysRoleModel> userRoles;
 * 
 * <pre>
 * 
 * @ClassName Many2Many
 * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
 * @date 2018年8月14日 下午2:13:19
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Many2Many {

    public String from() default "id"; // 当前类通过该类哪个字段与需要关联对象进行关联【当前类属性名】

    public String relationFrom(); // 当前类通过from字段与中间对象的哪个字段关联【中间类属性名】

    public Class<?> relation(); // 中间类

    public String to() default "id"; // 目标对象的管理字段名称，默认id【目标类属性名】

    public String relationTo(); // 目标类to字段与中间对象的哪个字段关联【中间类属性名】

}
