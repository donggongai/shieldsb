package com.ld.shieldsb.annotation.mask.impl;

import com.ld.shieldsb.annotation.mask.Masker;
import com.ld.shieldsb.common.core.util.StringUtils;

public class IdCardMasker implements Masker {

    @Override
    public String mask(String str) {
        return mask(str, false, "*");
    }

    /**
     * 
     * 所有字符可以被替换成指定字符
     * 
     * @Title mask
     * @author 刘金浩
     * @date 2020年1月7日 上午9:11:21
     * @param mobile
     * @param all
     * @param maskStr
     * @return
     * @see com.ld.shieldsb.annotation.mask.Masker#mask(java.lang.String, boolean, java.lang.String)
     */
    @Override
    public String mask(String str, boolean all, String maskStr) {

        if (str == null || (str.length() < 6)) {
            return str;
        }
        if (StringUtils.isEmpty(maskStr)) {
            maskStr = "*";
        }
        if (all) {
            return str.replaceAll("(\\w)", maskStr);
        } else {
            // 身份证前三后三脱敏
            if (str.length() == 15 || str.length() == 18) {
                return str.replaceAll("(?<=\\w{3})\\w(?=\\w{3})", maskStr); // 匹配前面有3个字符后面有3个字符的中间部分
            } else {
                // 其他前2后3位脱敏，护照一般为8或9位
                return str.substring(0, 2) + new String(new char[str.length() - 5]).replace("\0", maskStr)
                        + str.substring(str.length() - 3);
            }
        }
    }

    @Override
    public String mask(String str, String maskStr) {
        return mask(str, false, maskStr);
    }

}
