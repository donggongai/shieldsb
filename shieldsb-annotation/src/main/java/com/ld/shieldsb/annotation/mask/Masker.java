package com.ld.shieldsb.annotation.mask;

/**
 * 脱敏器
 * 
 * @ClassName Masker
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2018年9月21日 上午9:09:42
 *
 */
public interface Masker {

    String mask(String str);

    String mask(String str, boolean all, String maskStr);

    String mask(String str, String maskStr);
}