package com.ld.shieldsb.annotation.field;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 用于处理的字段
 * 
 * @ClassName DealField
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2019年6月26日 下午5:46:50
 *
 */
public class DealField {

    /**
     * 是否生成图片信息
     * 
     * @ClassName OutImg
     * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
     * @date 2014-4-21 上午9:29:40
     * 
     */
    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface OutImg {
        public boolean value() default true;
    }

    /**
     * 保存或修改时整个对象时该属性对应的图片需要索引（用于普通图片）
     * 
     * @ClassName IndexImg
     * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
     * @date 2014-4-21 上午9:29:40
     * 
     */
    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface IndexImg {
        public boolean value() default true;

        /** 描述 */
        public String desp() default "";
    }

    /**
     * 保存或修改时整个对象时该属性对应的图片需要索引（用于档案图片）
     * 
     * @ClassName IndexImgArchives
     * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
     * @date 2014-4-21 上午9:29:40
     * 
     */
    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface IndexImgArchives {
        public boolean value() default true;

        /** 描述 */
        public String desp() default "";
    }

    /**
     * 保存或修改时整个对象时该属性对应的图片需要索引（用于公司logo图片）
     * 
     * @ClassName IndexImgCompany
     * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
     * @date 2014-4-21 上午9:29:40
     * 
     */
    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface IndexImgCompany {
        public boolean value() default true;

        /** 描述 */
        public String desp() default "";
    }

    /**
     * 保存或修改时整个对象时该属性对应的附件需要索引
     * 
     * @ClassName IndexAffix
     * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
     * @date 2014-4-21 上午9:29:40
     * 
     */
    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface IndexAffix {
        public boolean value() default true;

        /** 描述 */
        public String desp() default "";
    }

    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Field {
        // 展现名称
        public String name() default "";
    }
}
