package com.ld.shieldsb.annotation.duplicate;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 提交拦截，用于防止重复提交，CSRF拦截等
 * 
 * @ClassName DuplicateSubmitToken
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2019年7月4日 下午2:31:33
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface SubmitRequestIntercept {
    /** 一次请求完成之前防止重复提交 */
    public static final int REQUEST = 1;
    /** 一次会话中防止重复提交 */
    public static final int SESSION = 2;

    /** 使用csrf拦截 */
    boolean csrf() default true;

    /** 使用频率判断 */
    boolean frequency() default true;

    /** 是否保存 */
    boolean save() default true;

    /** 间隔时间，即多长时间后可提交,单位秒 */
    int interval() default 3;

    /** 防止重复提交类型，默认：一次请求完成之前防止重复提交 */
    int type() default REQUEST;

    String desp() default "";
}
