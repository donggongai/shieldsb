package com.ld.shieldsb.annotation.field.db;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 字段不重复（新增修改时判断）
 * 
 * @ClassName Unique
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2019年1月16日 下午2:48:41
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Unique {
    // 联合其他字段不重复的字段名，注意只加到一个字段上即可，注意填的是数据库字段名，多个字段用逗号或空格连接
    public String compsite() default "";
}