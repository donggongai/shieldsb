package com.ld.shieldsb.annotation.duplicate;

public class TextConstants {
    public static final String REQUEST_REPEAT = "========this is a duplicate submit exception=====";

    private TextConstants() {
    }
}
