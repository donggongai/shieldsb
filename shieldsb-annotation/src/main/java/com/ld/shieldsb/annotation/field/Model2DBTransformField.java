package com.ld.shieldsb.annotation.field;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * model与数据库中字段转换注解
 * 
 * @ClassName ModelParseField
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2019年6月26日 下午5:45:26
 *
 */
public class Model2DBTransformField {

    /**
     * 配置model中为date行，而页面使用时间控件只能获取String型的时间字段，传值为fmt
     * 
     * @title String2Date
     * @author 张和祥
     * @date 2016年9月8日
     */
    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface String2Date {
        public String value() default "yyyy-MM-dd";
    }
}
