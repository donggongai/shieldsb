package com.ld.shieldsb.annotation.dao;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 进行html过滤（用于dao方法拦截如保存更新方法，暂未使用，将model参数过滤html标签）
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
 * @date 2016年11月3日 下午3:34:06
 *
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EscapeHtml {
    public String value() default "";
}