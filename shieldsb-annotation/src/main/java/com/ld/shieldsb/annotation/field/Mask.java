package com.ld.shieldsb.annotation.field;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.ld.shieldsb.annotation.mask.Masker;
import com.ld.shieldsb.annotation.mask.Type;

/**
 * -字段是否脱敏
 * 
 * @ClassName Mask
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2018年9月20日 下午2:13:22
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Mask {
    public Class<Masker> masker() default Masker.class;

    public Type type() default Type.NULL;

    public boolean value() default true;
}