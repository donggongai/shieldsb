package com.ld.shieldsb.annotation.field.link;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * model中关联对象，用于查询时获取关联对象，从表查主表<br>
 * 示例：<br>
 * 2个表，用户基本信息User，用户详细新UserInfo<br>
 * User中有字段id表示主键，UserInfo中有字段userId关联User表中的id，则User中定义UserInfo属性，在该属性上定义注解<br>
 * 
 * <pre>
 * 
 * &#64;Link(field="id",targetField="userId") <br>
 * private UserInfo userInfo;
 * 
 * <pre>
 * 
 * @ClassName Link
 * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
 * @date 2019年7月24日 下午2:13:19
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Link {

    public String field(); // 当前类通过该类哪个字段与需要关联对象进行关联【当前类属性名】

    public String targetField() default "id"; // 目标对象的管理字段名称，默认id

    public String queryFields() default "*"; // 查询那些字段，默认查询所有，用,号连接

    public String queryFieldsExclude() default ""; // 查询字段排除哪些字段，用,号连接，TODO 暂时处理不了

}
