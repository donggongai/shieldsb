package com.ld.shieldsb.annotation.json;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 指定返回json中的字段(多个配置项时)，该注解配合 @ResponseBody 一起使用来细粒度定制返回的json
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2020年12月1日 上午9:22:15
 *
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MultiResponseJsonField {

    ResponseJsonField[] value() default {};

}
