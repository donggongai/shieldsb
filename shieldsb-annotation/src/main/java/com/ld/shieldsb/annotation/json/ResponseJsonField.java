package com.ld.shieldsb.annotation.json;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.apache.commons.lang.ObjectUtils.Null;

/**
 * 指定返回json中的字段，该注解配合 @ResponseBody 一起使用来细粒度定制返回的json
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2020年12月1日 上午9:09:28
 *
 */
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ResponseJsonField {
    Class<?> clazz() default Null.class;

    /**
     * 
     * 返回的json包含哪些字段
     * 
     * @Title includes
     * @author 吕凯
     * @date 2020年12月1日 上午9:16:20
     * @return String[]
     */
    String[] includes() default {};

    /**
     * 返回的json排除哪些字段
     * 
     * @Title excludes
     * @author 吕凯
     * @date 2020年12月1日 上午9:16:03
     * @return String[]
     */
    String[] excludes() default {};

}
