package com.ld.shieldsb.annotation.model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * * 注解相关
 * 
 * @ClassName Annotation
 * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
 * @date 2016年8月26日 上午10:50:17
 *
 */
public class ModelAnno {

    /**
     * * 数据库表名
     * 
     * @ClassName TableName
     * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
     * @date 2016年8月26日 上午10:50:29
     *
     */
    @Inherited // 可继承
    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface TableName {
        public String value() default "";
    }

    /**
     * * 类注解相关
     * 
     * @ClassName Model
     * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
     * @date 2016年8月26日 上午10:50:17
     *
     */
    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Model {
        public String name() default "";
    }

}
