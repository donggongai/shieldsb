package com.ld.shieldsb.annotation.mask;

/**
 * 脱敏类型
 * 
 */
public enum Type {
    /**
     * 表示不存在
     */
    NULL,
    /**
     * id身份证号及护照号
     */
    ID_CARD,
    /**
     * phone电话
     */
    PHONE

}
