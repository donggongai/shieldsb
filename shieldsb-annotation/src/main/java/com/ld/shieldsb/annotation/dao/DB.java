package com.ld.shieldsb.annotation.dao;

/**
 * 数据库类型
 * 
 */
public enum DB {

    /**
     * oracle
     */
    ORACLE,
    /**
     * mysql
     */
    MYSQL

}
