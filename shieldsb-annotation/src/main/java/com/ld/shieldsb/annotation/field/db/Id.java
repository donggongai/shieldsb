package com.ld.shieldsb.annotation.field.db;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.ld.shieldsb.annotation.dao.DB;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Id {
    public DB db() default DB.ORACLE;

    public String seq() default "";
}