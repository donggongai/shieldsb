package com.ld.shieldsb.annotation.duplicate;

public class SubmitRequestException extends Exception {

    /**
     * @Fields serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    public SubmitRequestException(String msg) {
        super(msg);
    }
}