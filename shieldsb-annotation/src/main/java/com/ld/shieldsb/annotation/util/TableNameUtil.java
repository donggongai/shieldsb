package com.ld.shieldsb.annotation.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.ld.shieldsb.annotation.model.ModelAnno.TableName;

public class TableNameUtil {
    private TableNameUtil() {
    }

    private static final Map<Class<?>, String> CLAZZ_TABLES = new HashMap<>(); // 缓存类和表名的对应关系

    /**
     * 扩展类与表名的对应关系
     * 
     * @Title getTableName
     * @author 吕凯
     * @date 2019年5月27日 下午1:14:02
     * @param extendsTables
     *            void
     */
    public static void extendsCls2TabName(Map<Class<?>, String> extendsTables) {
        CLAZZ_TABLES.putAll(extendsTables);
    }

    /**
     * 扩展类与表名的对应关系
     * 
     * @Title extendsCls2TabName
     * @author 吕凯
     * @date 2019年5月27日 下午1:18:28
     * @param cls
     * @param tableName
     *            void
     */
    public static void extendsCls2TabName(Class<?> cls, String tableName) {
        CLAZZ_TABLES.put(cls, tableName);
    }

    /**
     * 根据类信息获取表名
     * 
     * @Title getTableName
     * @author 吕凯
     * @date 2019年5月27日 下午1:07:43
     * @param classOfT
     * @return String
     */
    public static String getTableName(Class<?> classOfT) {
        String tableName = CLAZZ_TABLES.get(classOfT);
        if (tableName == null) {
            TableName tableNameTag = classOfT.getAnnotation(TableName.class);
            if (tableNameTag != null && StringUtils.isNotBlank(tableNameTag.value())) {
                tableName = tableNameTag.value().trim();
            } else {
                tableName = classOfT.getSimpleName(); // 默认取类名
            }
            CLAZZ_TABLES.put(classOfT, tableName);
        }
        return tableName;
    }

    /**
     * 根据model获取表名
     * 
     * @Title getTableName
     * @author 吕凯
     * @date 2019年5月27日 下午1:08:05
     * @param obj
     * @return String
     */
    public static String getTableName(Object obj) {
        Class<?> modelType = obj.getClass();
        return getTableName(modelType);
    }

}
