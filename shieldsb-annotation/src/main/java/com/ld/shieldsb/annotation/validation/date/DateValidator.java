package com.ld.shieldsb.annotation.validation.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

public class DateValidator implements ConstraintValidator<LdDate, String> {

    // 默认值_false，用于接收注解上自定义的 required
    private boolean required = false;

    private String format = "yyyy-MM-dd";

    // 1、初始化方法：通过该方法我们可以拿到我们的注解
    public void initialize(LdDate constraintAnnotation) {

        required = constraintAnnotation.required();
        format = constraintAnnotation.format();
    }

    // 2、逻辑处理
    public boolean isValid(String value, ConstraintValidatorContext context) {
        // 2.1、如果允许为空的话，直接返回结果
        if (required) {
            if (StringUtils.isEmpty(value)) {
                return true;
            } else {
                return isValidDate(value, format);
            }
        } else {
            if (StringUtils.isEmpty(value)) {
                return false;
            } else {
                return isValidDate(value, format);
            }
        }
    }

    /**
     * 验证日期格式是否满足要求
     * 
     * @param str
     *            需要验证的日期格式
     * @param formatString
     *            验证的标准格式，如：（yyyy/MM/dd HH:mm:ss）
     * @return 返回验证结果
     */
    public static boolean isValidDate(String str, String formatString) {
        // 指定日期格式，注意yyyy/MM/dd区分大小写；
        SimpleDateFormat format = new SimpleDateFormat(formatString);
        try {
            // 设置lenient为false.
            // 否则SimpleDateFormat会比较宽松地验证日期，比如2007/02/29会被接受，并转换成2007/03/01
            format.setLenient(false);
            format.parse(str);
        } catch (ParseException e) {
            // e.printStackTrace();
            // 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
            return false;
        }
        return true;
    }

}