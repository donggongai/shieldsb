package com.ld.shieldsb.annotation.field.db;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 是否clob字段
 * 
 * @ClassName CLOB
 * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
 * @date 2015-3-6 上午9:55:04
 * 
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CLOB {
    public boolean value() default true;
}