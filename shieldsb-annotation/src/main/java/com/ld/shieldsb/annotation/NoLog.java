package com.ld.shieldsb.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * 不记录日志
 * 
 * @ClassName NoLog
 * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
 * @date 2019年7月17日 下午2:34:57
 *
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface NoLog {
    public String value() default "";
}