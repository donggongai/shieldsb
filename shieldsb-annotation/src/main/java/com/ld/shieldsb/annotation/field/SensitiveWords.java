package com.ld.shieldsb.annotation.field;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.ld.shieldsb.common.core.util.sensitiveWord.DefaultSensitiveWordAfterHandler;

/**
 * 
 * 默认表单需要进行过滤敏感词标签
 * 
 * @ClassName NoFilterHtmlTag
 * @author <a href="mailto:donggongai@126.com" target="_blank">liujinhao</a>
 * @date 2016年8月25日 下午2:14:09
 *
 */
@Target({ ElementType.TYPE, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface SensitiveWords {
    public String type() default "simple"; // 过滤方式 simple，text,html

    public String replaceStr() default "*"; // 替换字符

    Class<?> classes() default DefaultSensitiveWordAfterHandler.class;
}