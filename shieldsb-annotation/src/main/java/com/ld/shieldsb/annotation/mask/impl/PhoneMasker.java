package com.ld.shieldsb.annotation.mask.impl;

import com.ld.shieldsb.annotation.mask.Masker;

public class PhoneMasker implements Masker {

    @Override
    public String mask(String mobile) {

        return mask(mobile, false, "*");
    }

    public static void main(String[] args) {
        String ss = "4516038";
        System.out.println(new PhoneMasker().mask(ss));
    }

    /**
     * 
     * 所有字符可以被替换成指定字符
     * 
     * @Title mask
     * @author 刘金浩
     * @date 2020年1月7日 上午9:11:21
     * @param mobile
     * @param all
     * @param maskStr
     * @return
     * @see com.ld.shieldsb.annotation.mask.Masker#mask(java.lang.String, boolean, java.lang.String)
     */
    @Override
    public String mask(String mobile, boolean all, String maskStr) {
        // 手机号码前三后四脱敏
        if (mobile == null || (mobile.length() < 4)) {
            return mobile;
        }
        if (all) {
            return mobile.replaceAll("(\\w)", maskStr);
        } else {
            int last = 4;
            if (mobile.length() < 8) {
                return mobile.replaceAll("(?<=\\w{2})\\w(?=\\w{2})", "*");
            }
            return mobile.replaceAll("(?<=\\w{3})\\w(?=\\w{" + last + "})", "*"); // 匹配前面有3个字符后面有3个字符的中间部分
        }

    }

    @Override
    public String mask(String str, String maskStr) {
        return mask(str, false, maskStr);
    }

}
