[TOC]

### 代码生成服务模块
![](https://img.shields.io/badge/release-1.0.0-blue.svg)
#### 使用说明


##### 使用示例

1、引入jar包

```html
<shieldsb.version>2.4.0-SNAPSHOT</shieldsb.version>

<!-- 代码生成器 -->
<dependency>
    <groupId>com.ld</groupId>
    <artifactId>shieldsb-codegen</artifactId>
    <version>${shieldsb.version}</version>
</dependency>
```
2、生成代码

```java
ExportParams model = // 定义参数
// 如果是预览把preview设为true,则不会生成文件
// model.setPreview(true);
List<String> outtypes = getList("outtype"); //导出类型，0：model，1：DAO,2：SERVICE ,3：JSP，4：CONTROLLER，
ExportResult result = codegenService.getCreateCodeStr(model, outtypes);
String flag = "共生成文件" + result.getFileSize() + "个。请到路径：" + outDic + "下查看！";
log.warn(flag);
// result中包含导出文件的名称和内容等信息
```

这样就可以了

#### 传入参数

传入参数使用ExportParams封装为对象，其中包含参数：
##### packagePrefix【String 】

包名package前缀， 如com.ld.admin（不包含model，dao，conroller等，生成代码时会自动添加） 

##### packageSuffix【String 】

包名package后缀, 如com.ld.model.system中的system

##### baseURL【String 】

基本url， 访问URL，不带search，show之类的具体路径，如：system/project，具体url会自动拼接

##### tableName【String 】

表名称

##### modelComments【String 】

model类注释

##### className【String 】

类名前缀, 此名称为前缀，不包含Model，DAO，Conroller等，生成代码时会自动添加 

##### outDir【String 】

输出根目录，与后面的java和jsp目录结合使用

##### javaMainDir 【String 】

java输出根目录，默认"/src/main/java"，完整路径为outDir+javaMainDir

##### jspMainDir 【String 】

jsp输出根目录，默认 "/src/main/webapp/WEB-INF/view"，完整路径为outDir+jspMainDir

##### jspDir【String 】
jsp模块路径，完整路径为outDir+jspMainDir+jspDir， 如a/system/project，jsp主目录下模块的目录，开头结尾不需要/ 

##### hasAudit【boolean】
是否带审核功能

##### hasImport【boolean 】
是否带导入功能

##### author【String 】
作者，用于注释

##### email【String 】
邮箱，用于注释

##### templDir【String 】
模板目录

##### preview【boolean 】
是否为预览
##### modelTempl 【String 】
model模板，默认 "model.html";
##### daoTempl 【String 】
dao模板，默认"dao.html"
##### serviceTempl  【String 】
service模板，默认 "service.html";
##### controllerTempl  【String 】
controller模板，默认"controller.html";
##### jspTempl  【String 】
jsp模板，默认"search.html,list.html,show.html,update.html";

#### 返回参数

返回参数包含所有的传入参数。

返回模板中的参数为map，将传入参数全部封装进去，并将一部分做了装换处理，即除了所有传入的参数还包含以下参数：

##### input Params
所有的传入参数

##### serialID 【String】
序列化的编号，如6003349284224232317L

##### fieldList 【List<ExportParamsModelFieldsModel>】

model属性对象，包含表信息，model信息，具体可参考ExportParamsModelFieldsModel。
##### modelName【String 】

model名称，处理后的model名称，在前缀基础上增加了Model，如TestModel

##### jspPrefix【String 】
jsp名称前缀，与modelName比少了Model，小写

##### pkgName【String 】
处理后的包名，如model为com.ld.admin.model,controller为com.ld.admin.controller

##### modelPackage【String 】
model的包名

##### controllerName【String 】

用于controller注解的名称，同名会有异常

##### nowTime【String 】

现在时间，用于注释类生成时间

##### servicePackage【String 】

service包名

##### serviceName【String 】

service类名

#### ExportResult
```java
private ExportFile modelFile; //model文件
private ExportFile daoFile; //dao文件
private ExportFile serviceFile; //service文件
private ExportFile controllerFile; //controller文件

public int getFileSize() ；
// 将文件压缩到指定路径中
public void getZipFile(String srcDirName, String outFile) throws FileNotFoundException {}；
```


#### 其他参数

#### 版本说明
##### v1.0.0（2019-10-17）
- 初版发布;
- ;
