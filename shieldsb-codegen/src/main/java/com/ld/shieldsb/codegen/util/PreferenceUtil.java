package com.ld.shieldsb.codegen.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

import com.ld.shieldsb.codegen.model.PreferenceModel;
import com.ld.shieldsb.common.composition.util.ConvertUtil;
import com.ld.shieldsb.common.core.io.FileUtils;
import com.ld.shieldsb.common.core.model.Result;
import com.ld.shieldsb.common.core.util.PathUtil;
import com.ld.shieldsb.common.core.util.ResultUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 个人偏好工具类
 * 
 * @ClassName PreferenceUtil
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2018年12月21日 下午12:16:34
 *
 */
@Slf4j
public class PreferenceUtil {
    public static final String PREFERENCE_FILE_NAME = "preference.sd";

    /**
     * 获取个人偏好设置
     * 
     * @Title getPreferenceModel
     * @author 吕凯
     * @date 2018年12月21日 下午1:16:01
     * @return PreferenceModel
     */
    public static PreferenceModel getPreferenceModel(String uId) {
        String comments = "尚未设置！";

        PreferenceModel model = new PreferenceModel();
        model.setComments(comments);
        model.setSaveDir(PathUtil.getShieldsbPreferenceDir().getAbsolutePath());

        File file = getPreferenceFile(uId);
        if (file != null) {
            try {
                String content = FileUtils.readFileToString(file, "UTF-8");
                model = ConvertUtil.jsonStr2Obj(content, PreferenceModel.class);
                model.setComments("最近修改时间：" + com.ld.shieldsb.common.core.util.date.DateUtil.getDateTimeString(new Date(file.lastModified())));
                model.setSaveDir(PathUtil.getShieldsbPreferenceDir().getAbsolutePath());
                return model;
            } catch (Exception e) {
                comments = "设置文件出错！" + e.getMessage();
                log.error("", e);
            }
        }
        return model;
    }

    /**
     * 获取个人偏好文件
     * 
     * @Title getPreferenceFile
     * @author 吕凯
     * @date 2018年12月21日 下午2:17:32
     * @return File
     */
    public static File getPreferenceFile(String uId) {
        File tmpDirFile = PathUtil.getShieldsbPreferenceDir();
        File file = new File(tmpDirFile, getFileName(uId));
        if (!file.exists()) {
            return null;
        }
        return file;
    }

    private static String getFileName(String uId) {
        return PREFERENCE_FILE_NAME + "_" + uId + ".sd";
    }

    /**
     * 保存个人偏好设置
     * 
     * @Title savePreferenceModel
     * @author 吕凯
     * @date 2018年12月21日 下午1:16:21
     * @param model
     * @return Result
     */
    public static Result savePreferenceModel(PreferenceModel model, String uId) {
//        model.setComments(null); //不保存注释
        String preference = ConvertUtil.obj2JsonStr(model);
        try {
            File tmpDirFile = PathUtil.getShieldsbPreferenceDir();
            File file = new File(tmpDirFile, getFileName(uId));
            FileUtils.writeStringToFile(file, preference, "UTF-8");
        } catch (IOException e) {
            log.error("", e);
            return ResultUtil.error("修改失败！" + e.getMessage());
        }
        return ResultUtil.success("修改成功！");
    }

    /**
     * 保存最近输入的根目录
     * 
     * @Title addRecentSrcDir
     * @author 吕凯
     * @date 2018年12月21日 下午1:18:36
     * @param recentSrcDir
     * @return Result
     */
    public static Result addRecentSrcDir(String recentSrcDir, String uId) {
        PreferenceModel model = getPreferenceModel(uId);
        List<String> recentSrcDirs = model.getRecentSrcDirs();
        recentSrcDirs.add(0, recentSrcDir);
        if (recentSrcDirs.size() > model.getMaxRecentSrcNum()) { // 只保留符合规定的长度
            recentSrcDirs = recentSrcDirs.subList(0, model.getMaxRecentSrcNum() - 1);
        }
        List<String> uniqueList = recentSrcDirs.stream().distinct().collect(Collectors.toList()); // 去重
        model.setRecentSrcDirs(uniqueList);
        return savePreferenceModel(model, uId);
    }

    /**
     * 删除最近打开目录
     * 
     * @Title delRecentSrcDir
     * @author 吕凯
     * @date 2018年12月21日 下午1:59:59
     * @param index
     * @return Result
     */
    public static Result delRecentSrcDir(int index, String uId) {
        if (index > 0) {
            index = index - 1;
            PreferenceModel model = getPreferenceModel(uId);
            List<String> recentSrcDirs = model.getRecentSrcDirs();
            recentSrcDirs.remove(index);
            List<String> uniqueList = model.getRecentSrcDirs().stream()
                    .collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>()), ArrayList::new)); // 去重
            model.setRecentSrcDirs(uniqueList);
            return savePreferenceModel(model, uId);
        } else {
            return ResultUtil.error("index参数不正确！");
        }
    }

    /**
     * 重置
     * 
     * @Title reset
     * @author 吕凯
     * @date 2018年12月21日 下午1:59:51
     * @param index
     * @return Result
     */
    public static Result reset(String uId) {
        boolean flag = FileUtils.deleteQuietly(getPreferenceFile(uId));
        if (!flag) {
            return ResultUtil.error("重置失败！");
        }
        return ResultUtil.success("重置成功！");
    }

}
