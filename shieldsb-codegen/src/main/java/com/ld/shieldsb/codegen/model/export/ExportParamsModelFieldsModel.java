package com.ld.shieldsb.codegen.model.export;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * 导出参数model中的field
 * 
 * @ClassName ExportParamsModelFieldsModel
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2018年12月24日 上午11:53:55
 *
 */
@Data
public class ExportParamsModelFieldsModel implements Serializable {

    private static final long serialVersionUID = 7392984578665152894L;
    private String tableName; // 表名
    private String columnName; // 列名
    private Object defaultValue; // 默认值

    private String fieldName;// 字段名
    private String fieldType; // 字段类型
    private String comments; // 显示名称
    private String dataSize; // 字段长度

    private Boolean canSearch = false; // 是否可搜索
    private String searchType; // 搜索类型
    private Boolean isShowInList = false; // 是否在列表页显示
    private Boolean isShowInUpdate = true; // 更新页是否显示
    private String inputType; // input类型
    private List<String> formvalidateMethods; // 表单验证方法

}
