package com.ld.shieldsb.codegen.model.export;

import java.util.ArrayList;
import java.util.List;

import com.ld.shieldsb.common.core.util.StringUtils;

import lombok.Data;

/**
 * 导出参数
 * 
 * @ClassName ExportParams
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2018年12月24日 下午3:39:52
 *
 */
@Data
public class ExportParams {
    // 前台传递参数
    private String packagePrefix; // package前缀
    private String packageSuffix; // package后缀
    private String classNamePrefix; // 类名前缀，未使用，实际用的是className
    private String baseURL; // 基本url
    private String tableName; // 表名称
    private String modelName; // model名称
    private String modelComments; // 表注释
    private String className; // 类名称
    private String outDir; // 输出根目录
    private String javaMainDir = "/src/main/java"; // java输出根目录，完整路径为outDir+javaMainDir
    private String jspMainDir = "/src/main/webapp/WEB-INF/view"; // jsp输出根目录，完整路径为outDir+jspMainDir
    private String jspDir; // jsp模块路径，完整路径为outDir+jspMainDir+jspDir

    private boolean hasBatchdel = false; // 是否批量删除功能
    private boolean hasAudit = false; // 是否带审核功能
    private boolean hasImport = false; // 是否带导入功能
    private String author; // 作者用于注释
    private String email; // 邮箱用于注释
    private String templDir; // 模板目录
    private boolean preview = false; // 预览

    private String modelTempl = "model.html";
    private String daoTempl = "dao.html";
    private String serviceTempl = "service.html";
    private String controllerTempl = "controller.html";
    private String jspTempl = "search.html,list.html,show.html,update.html";

    // 处理后的参数（参数中没有，需要重新处理）
    private String jspPrefix; // jsp名称前缀，实际与modelName一致，只是转换为了小写
    private String pkgName; // 包名处
    private String modelPackage; // model包
    private String controllerName; // 用于controller注解的名称，同名会有异常
    private String nowTime; // 现在时间，用于注释类生成时间
    private String servicePackage; // service包名
    private String serviceName; // service类名

    private List<ExportParamsModelFieldsModel> columns = new ArrayList<>();

    public String getModelPackage() {
        modelPackage = packagePrefix + ".model";
        if (StringUtils.isNotBlank(packageSuffix)) {
            modelPackage += "." + packageSuffix;
        }
        return modelPackage;
    }

    public String getJspPrefix() {
        if (StringUtils.isBlank(jspPrefix)) {
            jspPrefix = "";
            if (StringUtils.isNotBlank(modelName)) {
                jspPrefix = StringUtils.substringBeforeLast(modelName, "Model").toLowerCase();
            }
        }
        return jspPrefix;
    }

    /**
     * 作者没传时的处理
     * 
     * @Title getAuthor
     * @author 吕凯
     * @date 2018年12月29日 上午11:30:09
     * @return String
     */
    public String getAuthor() {
        if (StringUtils.isBlank(author)) {
            author = "忽然忘了我是谁";
        }
        return author;
    }

    /**
     * 获取service对象的名称
     * 
     * @Title getServiceObjName
     * @author 吕凯
     * @date 2018年12月29日 上午11:09:29
     * @return String
     */
    public String getServiceObjName() {
        if (StringUtils.isNotBlank(serviceName)) {
            return StringUtils.uncap(serviceName);
        }
        return null;
    }

}
