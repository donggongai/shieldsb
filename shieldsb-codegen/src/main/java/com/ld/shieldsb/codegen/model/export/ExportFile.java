package com.ld.shieldsb.codegen.model.export;

import lombok.Data;

/**
 * 导出文件
 * 
 * @ClassName ExportFile
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2018年12月24日 上午11:56:30
 *
 */
@Data
public class ExportFile {
    private String path;
    private String name;
    private String extension;
    private String content;

    public void setFileNotNull(ExportFile file) {
        if (file.getPath() != null) {
            this.setPath(file.getPath());
        }
        if (file.getName() != null) {
            this.setName(file.getName());
        }
        if (file.getExtension() != null) {
            this.setExtension(file.getExtension());
        }
        if (file.getContent() != null) {
            this.setContent(file.getContent());
        }

    }

}
