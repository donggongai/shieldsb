package com.ld.shieldsb.codegen.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/** 代码生成器-个人偏好设置，不保存到数据库，保存到本地 */
@Data
public class PreferenceModel implements Serializable {
    private static final long serialVersionUID = 6003349284224232317L;

    /* 配置文件存放路径 */
    private String saveDir;
    /* 源码路径 */
    private String srcDir;
    /* 说明 */
    private String comments;
    // 作者
    private String author;
    // 邮箱
    private String email;
    /* 最近使用的源码路径 */
    private List<String> recentSrcDirs = new ArrayList<>();

    private Integer maxRecentSrcNum = 10;

}
