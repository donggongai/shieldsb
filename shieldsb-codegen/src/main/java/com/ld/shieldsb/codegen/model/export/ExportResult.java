package com.ld.shieldsb.codegen.model.export;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.ld.shieldsb.common.core.collections.ListUtils;
import com.ld.shieldsb.common.core.io.FileUtils;
import com.ld.shieldsb.common.core.io.zip.FileZip;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 导出结果
 * 
 * @ClassName ExportResult
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2018年12月24日 上午11:54:22
 *
 */
@Data
@Slf4j
public class ExportResult {

    private ExportFile modelFile;
    private ExportFile daoFile;
    private ExportFile serviceFile;
    private ExportFile controllerFile;

    private List<ExportFile> jspFiles; // jsp

    public int getFileSize() {
        int flag = 0;
        if (modelFile != null) {
            flag++;
        }
        if (daoFile != null) {
            flag++;
        }
        if (serviceFile != null) {
            flag++;
        }
        if (controllerFile != null) {
            flag++;
        }
        if (ListUtils.isNotEmpty(jspFiles)) {
            flag += jspFiles.size();
        }
        return flag;
    }

    /**
     * 将文件压缩到指定路径中
     * 
     * @Title getZipFile
     * @author 吕凯
     * @date 2019年12月12日 下午3:27:10
     * @param srcDirName
     *            压缩到的根目录
     * @param outFile
     *            输出路径
     * @throws FileNotFoundException
     *             void
     */
    public void getZipFile(String srcDirName, String outFile) throws FileNotFoundException {
        if (getFileSize() > 0) {
            List<File> srcFiles = new ArrayList<>();
            if (getModelFile() != null) {
                File file = new File(getModelFile().getPath());
                srcFiles.add(file);
            }
            if (getDaoFile() != null) {
                File file = new File(getDaoFile().getPath());
                srcFiles.add(file);
            }
            if (getServiceFile() != null) {
                File file = new File(getServiceFile().getPath());
                srcFiles.add(file);
            }
            if (getControllerFile() != null) {
                File file = new File(getControllerFile().getPath());
                srcFiles.add(file);
            }
            if (ListUtils.isNotEmpty(jspFiles)) {
                for (ExportFile eFile : jspFiles) {
                    File file = new File(eFile.getPath());
                    srcFiles.add(file);
                }
            }
            FileZip.zipFiles(srcDirName, srcFiles, new FileOutputStream(outFile));
            for (File file : srcFiles) {
                FileUtils.deleteCaseParent(file.getAbsolutePath()); // 删除原始文件
            }
        }

    }

}
