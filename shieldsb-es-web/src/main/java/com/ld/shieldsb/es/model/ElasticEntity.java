package com.ld.shieldsb.es.model;

import lombok.Data;

/**
 * es实体类
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2020年8月7日 上午8:58:30
 *
 */
@Data
public class ElasticEntity {
    /**
     * 主键标识，用户ES持久化
     */
    private String id;

    /**
     * JSON对象，实际存储数据
     */
    private Object data;

}
