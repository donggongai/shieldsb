package com.ld.shieldsb.es.model.search;

import lombok.Data;

/**
 * 查询条件
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2020年6月2日 下午6:12:35
 *
 */
@Data
public class Condition {
    private String column; // 列名称
    private Type queryType; // 查询类型
    private Object values; // 值
    private String otherColumns; // 其他列名称，联合操作时用
    private Float boost; // 搜索权重

    private LinkType linkType = LinkType.AND; // and or

    public Condition() {
    }

    public Condition(String column, Type queryType, Object values) {
        this(column, queryType, values, null);
    }

    public Condition(String column, Type queryType, Object values, String otherColumns) {
        super();
        this.column = column;
        this.queryType = queryType;
        this.values = values;
        this.otherColumns = otherColumns;
    }

    public static enum Type {
        ANALYSIS/*分词*/, KEYWORD/*text的精确搜索*/, EQUAL, LIKE, LIKE_LEFT, LIKE_RIGHT, NOT_EQUAL, GT, GE, LT, LE, RANGE/*范围搜索*/, IN_SET/* in (x,x,x) */
    }

    public static enum LinkType {
        AND/*分词*/, OR/*text的精确搜索*/
    }

}
