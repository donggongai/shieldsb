package com.ld.shieldsb.es.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class ElasticSearchResult<T> {
    private Integer currentPage = 1;
    private Integer pageSize = 30;

    private Long totalCount = 0L;

    private boolean hasError = false;
    private String errMsg;

    private List<T> resultList = new ArrayList<>();

}
