package com.ld.shieldsb.es.util;

import com.ld.shieldsb.common.core.util.StringUtils;

public class ElasticSearchUtil {

    public static final String YEAR = "y";

    public static final String MONTH = "M";

    public static final String WEEK = "w";

    public static final String DAY = "d";

    public static final String HOUR = "h";

    public static final String HOUR24 = "H";
    public static final String MINUTE = "m";

    public static final String SECOND = "s";

    public static final String NOW = "now";

    /**
     * 获取本月搜索字符
     * 
     * @Title getThisMonthSearchStr
     * @author 吕凯
     * @date 2020年11月30日 下午3:07:38
     * @return String
     */
    public static String getThisMonthSearchStr() {
        return getThisUnitSearchStr(MONTH);
    }

    /**
     * 获取本年的搜索字符串
     * 
     * @Title getThisYearSearchStr
     * @author 吕凯
     * @date 2020年11月30日 下午3:39:24
     * @return String
     */
    public static String getThisYearSearchStr() {
        return getThisUnitSearchStr(YEAR);
    }

    /**
     * 获取本周的搜索字符串
     * 
     * @Title getThisWeekSearchStr
     * @author 吕凯
     * @date 2020年11月30日 下午3:39:45
     * @return String
     */
    public static String getThisWeekSearchStr() {
        return getThisUnitSearchStr(WEEK);
    }

    /**
     * 获取当天的搜索字符串
     * 
     * @Title getThisDaySearchStr
     * @author 吕凯
     * @date 2020年11月30日 下午3:40:29
     * @return String
     */
    public static String getThisDaySearchStr() {
        return getThisUnitSearchStr(DAY);
    }

    /**
     * 获取传入单位的搜索字符串
     * 
     * @Title getThisUnitSearchStr
     * @author 吕凯
     * @date 2020年11月30日 下午3:40:44
     * @param unit
     * @return String
     */
    public static String getThisUnitSearchStr(String unit) {
        return NOW + "/" + unit;
    }

    /**
     * 在当前时间的基础上增加
     * 
     * @Title addNow
     * @author 吕凯
     * @date 2020年11月30日 下午3:46:36
     * @param unit
     * @param addNum
     * @return String
     */
    public static String addNow(String unit, int addNum) {
        return addNow(unit, addNum, null);
    }

    /**
     * 在当前时间的基础上增加,并设定保留的单位（保留到年、月、日等）
     * 
     * @Title addNow
     * @author 吕凯
     * @date 2020年11月30日 下午3:47:29
     * @param unit
     *            添加单位
     * @param addNum
     *            添加值
     * @param stayUnit
     *            保留单位
     * @return String
     */
    public static String addNow(String unit, int addNum, String stayUnit) {
        StringBuilder sb = new StringBuilder(NOW);
        sb.append(addNum > 0 ? "+" : "");
        sb.append(addNum);
        sb.append(unit);
        if (StringUtils.isNotEmpty(stayUnit)) {
            sb.append("/").append(stayUnit);
        }
        return sb.toString();
    }

}
