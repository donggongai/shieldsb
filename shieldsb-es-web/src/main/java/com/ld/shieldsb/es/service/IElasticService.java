package com.ld.shieldsb.es.service;

import java.util.Collection;
import java.util.List;

import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;

import com.ld.shieldsb.common.core.model.Result;
import com.ld.shieldsb.dao.model.PageNavigationBean;
import com.ld.shieldsb.es.model.ElasticEntity;
import com.ld.shieldsb.es.model.ElasticSearchResult;
import com.ld.shieldsb.es.model.search.ElasticSearchQueryModel;

public interface IElasticService {

    public void createIndex(String idxName, String idxSQL);

    public void deleteIndex(String index);

    public Result addOrUpdate(String idxName, ElasticEntity entity);

    public Result update(String idxName, ElasticEntity entity);

    public Result insertBatch(String idxName, List<ElasticEntity> list);

    public <T> Result deleteBatch(String idxName, Collection<T> idList);

    public <T> ElasticSearchResult<T> search(String idxName, SearchSourceBuilder builder, Class<T> clazz);

    public default <T> PageNavigationBean<T> getPageBean(String idxName, Class<T> classOfT, ElasticSearchQueryModel queryModel,
            HighlightBuilder highlightBuilder, int pageNum, int pageSize) {
        return getPageBean(idxName, classOfT, queryModel, highlightBuilder, pageNum, pageSize, null);
    }

    public <T> PageNavigationBean<T> getPageBean(String idxName, Class<T> classOfT, ElasticSearchQueryModel queryModel,
            HighlightBuilder highlightBuilder, int pageNum, int pageSize, TimeValue timeout);

    public Result deleteByQuery(String idxName, QueryBuilder builder);

    public Result deleteById(String idxName, String docId);

}