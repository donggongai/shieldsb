package com.ld.shieldsb.es.model.search;

import lombok.Data;

/**
 * 排序字段
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2020年6月2日 下午4:15:11
 *
 */
@Data
public class Order {
    private String column; // 列名称
    private Boolean isDesc; // 是否倒序
    private Object missing; // 缺失时的默认值

    public static final String SCORE_SORT = "_inner_score_sort_order"; // 按得分排序

    public Order() {
    }

    public Order(String column, Boolean isDesc) {
        this(column, isDesc, null);
    }

    public Order(String column, Boolean isDesc, Object missing) {
        super();
        this.column = column;
        this.isDesc = isDesc;
        this.missing = missing;
    }

}
