[TOC]

# Elasticsearch模块

封装了es查询的基本方法，提供了springboot的ElasticConfig注册。 

## 参数说明

```properties
#是否可用
es.enabled=true
#ip
es.host=192.168.1.248
#端口
es.port=9200
#协议
es.scheme=http
#用户名（可为空，为空不验证用户）
es.username=elastic
#密码
es.password=elastic248test
```
服务类IElasticService提供以下方法：

## 封装方法

```java
/** 创建索引 */
public void createIndex(String idxName, String idxSQL);
/** 删除索引 */
public void deleteIndex(String index);
/** 添加或更新档案 */
public Result addOrUpdate(String idxName, ElasticEntity entity);
/** 更新档案 */
public Result update(String idxName, ElasticEntity entity);
/** 批量插入档案 */
public Result insertBatch(String idxName, List<ElasticEntity> list);
/** 批量删除档案 */
public <T> Result deleteBatch(String idxName, Collection<T> idList);
/** 搜索档案 */
public <T> ElasticSearchResult<T> search(String idxName, SearchSourceBuilder builder, Class<T> clazz);
/** 搜索档案 */
public default <T> PageNavigationBean<T> getPageBean(String idxName, Class<T> classOfT, ElasticSearchQueryModel queryModel, HighlightBuilder highlightBuilder, int pageNum, int pageSize) {
        return getPageBean(idxName, classOfT, queryModel, highlightBuilder, pageNum, pageSize, null);
}
/** 搜索档案 */
public <T> PageNavigationBean<T> getPageBean(String idxName, Class<T> classOfT, ElasticSearchQueryModel queryModel,HighlightBuilder highlightBuilder, int pageNum, int pageSize, TimeValue timeout);
/** 删除档案 */
public Result deleteByQuery(String idxName, QueryBuilder builder);
/** 删除档案 */
public Result deleteById(String idxName, String docId);
```
实现类有BaseElasticService，可用在具体的使用时再次封装，如重新定义高亮等，示例如下：
```java
package com.ld.bbs.v2.service.es;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.springframework.stereotype.Service;
import com.ld.shieldsb.dao.model.PageNavigationBean;
import com.ld.shieldsb.es.model.ElasticEntity;
import com.ld.shieldsb.es.model.search.ElasticSearchQueryModel;
import com.ld.shieldsb.es.service.BaseElasticService;
/** es搜索服务*/
@Service(value = "BBSElasticService_v2")
public class BbsElasticTopicService extends BaseElasticService {
    public static final String ES_INDEX_BBS = "bbs";

    /** 搜索并放入request中*/
    public <T> PageNavigationBean<T> getPageBean(HttpServletRequest request, Class<T> classOfT, ElasticSearchQueryModel queryModel,
            int pageNum, int pageSize) {
        PageNavigationBean<T> pageBean = getPageBean(classOfT, queryModel, pageNum, pageSize);
        request.setAttribute("pageBean", pageBean);
        return pageBean;
    }

    /** 搜索*/
    public <T> PageNavigationBean<T> getPageBean(Class<T> classOfT, ElasticSearchQueryModel queryModel, int pageNum, int pageSize) {
        // =====定义高亮查询========
//        if (queryModel != null && StringUtils.isNotEmpty(queryModel.getTitle())) {
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        // 设置需要高亮的字段
        String highlightFieldName = "title,content";
        String[] fieldNames = highlightFieldName.split(",");
        for (int i = 0; i < fieldNames.length; i++) {
            highlightBuilder.field(fieldNames[i]);
        }
        // 设置前缀、后缀
        highlightBuilder.preTags("<span class='highlight_field'>").postTags("</span>")
        // 显示原文
        /*
         * .fragmentSize(800000)// 最大高亮分片数 .numOfFragments(0)// 从第一个分片开始获取高亮片段 .requireFieldMatch(false)
         */;
//        }
        PageNavigationBean<T> pageBean = super.getPageBean(ES_INDEX_BBS, classOfT, queryModel, highlightBuilder, pageNum, pageSize);
        return pageBean;
    }

    /** 返回自动补全信息 */
    public List<String> getSearchSuggest(String columnName, String columnValue, ElasticSearchQueryModel queryModel) {
        return super.getSearchSuggest(ES_INDEX_BBS, columnName, columnValue, null, queryModel);
    }

    public void addOrUpdate(String idxName, ElasticEntity entity, boolean update) {
        if (update) {
            super.update(idxName, entity);
        } else {
            super.addOrUpdate(idxName, entity);
        }
    }
    /** 删除文档    */
    public void del(String idxName, String docId) {
        super.deleteById(idxName, docId);
    }
}
```

## 使用说明
查询需要使用ElasticSearchQueryModel
```java
ElasticSearchQueryModel esQueryModel = new  ElasticSearchQueryModel();
```
然后调用对应的方法。
### 中文分词搜索
进行中文分词搜索的字段，定义时就需要设置为分词字段。
#### 1. 分词查询的符号

| 符号        | 含义         |
| ----------- | ------------ |
| match       |              |
| multi_match | 多个字段搜索 |

ElasticSearchQueryModel类中与之对应的方法

| 方法                                                         | 说明                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| andAnalysis(String fieldName, String value, String otherFieldNames) | fieldName为搜索字段，value为搜索值，otherFieldNames为其他搜索字段，多个字段用英文逗号分隔，这几个字段中只要有一个能搜索到即返回，如： <br />esQueryModel.andAnalysis("title^2", key, "content"); // title^5表示title权重为5，默认为1  esQueryModel.andAnalysis("title", key, "content"); // title，content中搜索 |
| andAnalysis(String fieldName, String value)                  |                                                              |
| andAnalysis(Property<T, ?> property, String value, String otherFieldNames) | property为类::属性 格式的搜索字段名，其他参数同上            |
| andAnalysis(Property<T, ?> property, String value)           | 同上                                                         |
| andAnalysis(String fieldName, String value, String otherFieldNames, Float boost) |                                                              |
| andAnalysis(String fieldName, String value, Float boost)     | boost为搜索权重                                              |
| andAnalysis(Property<T, ?> property, String value, String otherFieldNames, Float boost) |                                                              |
| andAnalysis(Property<T, ?> property, String value, Float boost) |                                                              |



#### 2. 示例

需求: 查询title或者content中包含key的文档，并且title符合条件的优先显示:

```java
esQueryModel.andAnalysis("title^2", key, "content"); // title^5表示title权重为5，默认为1
```

如果不考虑权重

```java
esQueryModel.andAnalysis("title", key, "content");
```

如果只查询title

```java
esQueryModel.andAnalysis("title", key);
// 或者 如果类为BBSPosterModel
esQueryModel.andAnalysis(BBSPosterModel::getTitle, key);
```




### 精确搜索
#### 1. 查询的符号

| 符号  | 含义       |
| ----- | ---------- |
| term  |            |
| terms | 匹配任一值 |
| ids   | id查询     |

ElasticSearchQueryModel类中与之对应的方法

| 方法                                                        | 说明                                                         |
| ----------------------------------------------------------- | ------------------------------------------------------------ |
| andEq(String fieldName, Object value)                       | fieldName=value，fieldName为搜索字段，value为搜索值，value可为空 |
| andEq(Property<T, ?> property, Object value)                | property为类::属性 格式的搜索字段名                          |
| andEq(String fieldName, Object value, Float boost)          | boost为搜索权重                                              |
| andEq(Property<T, ?> property, Object value, Float boost)   | 同上                                                         |
| andUnEq(String fieldName, Object value)                     | fieldName!=value                                             |
| andUnEq(Property<T, ?> property, Object value)              | 同上                                                         |
| andUnEq(String fieldName, Object value, Float boost)        | 同上                                                         |
| andUnEq(Property<T, ?> property, Object value, Float boost) | 同上                                                         |


orEq也存在上面参数的方法，不再单独列出。
#### 2. 示例
需求: 查询BBSPosterModel对象中isBest=1（精华）的文档：

```java
esQueryModel.andEq(BBSPosterModel::getIsBest, 1);
// 或者
esQueryModel.andEq("isBest", 1);
```

查询BBSPosterModel对象中isBest!=1（非精华）的文档：

```java
esQueryModel.andUnEq(BBSPosterModel::getIsBest, 1);
// 或者
esQueryModel.andUnEq("isBest", 1);
```

查询BBSPosterModel对象中isBest不为空的文档：

```java
esQueryModel.andUnEq(BBSPosterModel::getIsBest, null);
// 或者
esQueryModel.andUnEq("isBest", null);
```




#### 3. 数组字段包含某个值

如果要查询的字段在es中保存为数组，查询其中任意一个值匹配应该使用如下方法：
查询BBSPosterModel对象中标签tags（数组，存储在es中是["标签1","标签2","标签3"]）包含tag（tags contains tag）的文档：

```java
esQueryModel.andEq("tags.keyword", tag);
```

tags在文档定义的结构：

```json
"tags": {
    "type": "text",
    "fields": {
        "keyword": {
            "type": "keyword"
        }
    }
},
```

说明：
● text类型

1. 支持分词（存储时自动分词），全文检索,支持模糊、精确查询，不支持聚合、排序操作;
2. 最大支持的字符长度无限制,适合大字段存储；

● keyword
1. 不进行分词，直接索引，支持模糊、精确匹配，支持聚合、排序操作。
2. 最大支持的长度为——32766个UTF-8类型的字符，可以通过设置ignore_above指定自持字符长度，超过给定长度后的数据将不被索引，无法通过term精确匹配检索返回结果。
### 模糊搜索
说明：模糊查询未经过测试，可能有问题
#### 1. 模糊查询的符号

| 符号     | 含义     |
| -------- | -------- |
| wildcard | 模糊搜索 |

ElasticSearchQueryModel类中与之对应的方法

| 方法                                                | 说明                                |
| --------------------------------------------------- | ----------------------------------- |
| andLeftLike(String fieldName, String value)         |                                     |
| andLeftLike(Property<T, ?> property, String value)  | property为类::属性 格式的搜索字段名 |
| andRightLike(String fieldName, String value)        |                                     |
| andRightLike(Property<T, ?> property, String value) |                                     |
| andLike(String fieldName, String value)             |                                     |
| andLike(Property<T, ?> property, String value)      |                                     |

#### 2. 示例
需求: 查询BBSPosterModel对象中的文档：

### IN搜索
即查询字段的值满足给定值的任意一个值的搜索
#### 1. 查询的符号

| 符号  | 含义             |
| ----- | ---------------- |
| terms | 任一值符合即满足 |
| ids   | id查询           |

ElasticSearchQueryModel类中与之对应的方法

| 方法                                               | 说明                                                     |
| -------------------------------------------------- | -------------------------------------------------------- |
| andInSet(String fieldName, Object[] values)        | fieldName in values，fieldName为搜索字段，values为搜索值 |
| andInSet(Property<T, ?> property, Object[] values) | property为类::属性 格式的搜索字段名                      |
| andInSet(Property<T, ?> property, Set<?> values)   | 同上                                                     |
| andInSet(String fieldName, Set<?> values)          | 同上                                                     |


orInSet也存在上面参数的方法，不再单独列出。
#### 2. 示例
需求: 查询BBSPosterModel对象中type值为15或16（type in （15,16））的文档：

```java
esQueryModel.andInSet(BBSPosterModel::getType, new Long[] {15L,16L});
// 或者
esQueryModel.andEq("type", new Long[] {15L,16L});
```




### 范围搜索
#### 1. 范围查询的符号

| 符号 | 含义                                 |
| ---- | ------------------------------------ |
| gte  | greater-than or equal to, 大于或等于 |
| gt   | greater-than, 大于                   |
| lte  | less-than or equal to, 小于或等于    |
| lt   | less-than, 小于                      |

ElasticSearchQueryModel类中与之对应的方法

| 方法                                                         | 说明                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| andRange(String fieldName, Object value1, Object value2, Float boost) | fieldName为搜索字段，value1<=搜索值<=value2,boost为权重，value1、value2可以为空 |
| andRange(Property<T, ?> property, Object value1, Object value2, Float boost) | property为类::属性 格式的搜索字段名，其他参数同上            |
| andRange(String fieldName, Object value1, Object value2)     | 同上，gte value1 and lte value2                              |
| andRegion(Property<T, ?> property, Object value1, Object value2) | 同上                                                         |
| andGt(String fieldName, Object value)                        | gt，>value                                                   |
| andGt(Property<T, ?> property, Object value)                 | gt，>value                                                   |
| andGe(String fieldName, Object value)                        | gte，>=value                                                 |
| andGe(Property<T, ?> property, Object value)                 | gte，>=value                                                 |
| andLt(String fieldName, Object value)                        | lt，<value                                                   |
| andLt(Property<T, ?> property, Object value)                 | lt，<value                                                   |
| andLe(String fieldName, Object value)                        | lte, <=value                                                 |
| andLe(Property<T, ?> property, Object value)                 | lte, <=value                                                 |

#### 2. 数值范围查询
需求: 查询商品中40 <= price <= 80的文档:

```java
ElasticSearchQueryModel esQueryModel = new ElasticSearchQueryModel();
esQueryModel.andRange("price ", 40, 80);
```

原始语法：

```json
GET shop/_search
{
    "query": {
        "range": {
            "price": {
                "gte": 40,
                "lte": 80,
                "boost": 2.1	// 设置得分的权重值(提升值), 默认是1.0
            }
        }
    }
}
```

如果不包含80呢？
#### 3. 时间范围查询
#####  3.1 简单查询示例

需求: 查询网站中最近一天发布的帖子:

```java
ElasticSearchQueryModel esQueryModel = new ElasticSearchQueryModel();
// 修改时间大于xx的，注意时区
esQueryModel.andRange("createTime", DateUtil.str2dateTime("2020-09-10 14:00:00"), null); 
// 大于一天前，减1天，假设是2020-09-17 14:53:20秒 则是大于2020-09-16 14:53:20
esQueryModel.andRange("createTime", "now-1d", null);

// 注意区别于大于一天前，减1天，假设是2020-09-17 14:53:20秒 则是大于2020-09-16 00:00:00
esQueryModel.andRange("createTime", "now-1d/d", null);
```

es的原生语法：

```json
GET bbs/_search
{
    "query": {
        "range": {
            "post_date": {
            	"gte": "now-1d/d",	// 当前时间的上一天, 四舍五入到最近的一天
            	"lt":  "now/d"		// 当前时间, 四舍五入到最近的一天
        	}
        }
    }
}
```

##### 3.2 关于时间的数学表达式(date-math)

Elasticsearch中时间可以表示为now, 也就是系统当前时间, 也可以是以||结尾的日期字符串表示.
在日期之后, 可以选择一个或多个数学表达式:
 +1h —— 加1小时;
 -1d  —— 减1天;
 /d    —— 四舍五入到最近的一天.
下面是Elasticsearch支持数学表达式的时间单位:

| 表达式 | 含义 | 表达式 | 含义 |
| ------ | ---- | ------ | ---- |
| y      | 年   | M      | 月   |
| w      | 星期 | d      | 天   |
| h      | 小时 | H      | 小时 |
| m      | 分钟 | s      | 秒   |

说明: 假设系统当前时间now = 2020-10-11 12:00:00 :
 now+1h: now的毫秒值 + 1小时, 结果是: 2020-10-11 13:00:00.
 now-1h: now的毫秒值 - 1小时, 结果是: 2020-10-11 11:00:00.
 now-1h/d: now的毫秒值 - 1小时, 然后四舍五入到最近的一天的起始, 结果是: 2020-10-11 00:00:00.
 2020.10.11||+1M/d: 2020-10-11的毫秒值 + 1月, 再四舍五入到最近一天的起始, 结果是: 2020-11-11 00:00:00.

##### 3.3 关于时间的四舍五入

对日期中的日、月、小时等 进行四舍五入时, 取决于范围的结尾是包含(include)还是排除(exclude).
向上舍入: 移动到舍入范围的最后一毫秒;
向下舍入: 移动到舍入范围的第一毫秒.

举例说明:
① "gt": "2018-12-18||/M" —— 大于日期, 需要向上舍入, 结果是2018-12-31T23:59:59.999, 也就是不包含整个12月.
② "gte": "2018-12-18||/M" —— 大于或等于日期, 需要向下舍入, 结果是 2018-12-01, 也就是包含整个12月.
③ "lt": "2018-12-18||/M" —— 小于日期, 需要向上舍入, 结果是2018-12-01, 也就是不包含整个12月.
④ "lte": "2018-12-18||/M" —— 小于或等于日期, 需要向下舍入, 结果是2018-12-31T23:59:59.999, 也就是包含整个12月.
#### 4. 日期格式化范围查询(format)
格式化日期查询时, 将默认使用日期field中指定的格式进行解析, 当然也可以通过format参数来覆盖默认配置.
示例:

```json
GET website/_search
{
    "query": {
        "range": {
            "post_date": {
                "gte": "2/1/2018", 
                "lte": "2019",
                "format": "dd/MM/yyyy||yyyy"
            }
        }
    }
}
```



注意: 如果日期中缺失了部分年、月、日, 缺失的部分将被填充为unix系统的初始值, 也就是1970年1月1日.
比如, 将dd指定为format, 像"gte": 10将转换为1970-01-10T00:00:00.000Z.
#### 5. 时区范围查询(time_zone)
如果日期field的格式允许, 也可以通过在日期值本身中指定时区, 从而将日期从另一个时区的时间转换为UTC时间, 或者为其指定特定的time_zone参数.
示例:

```json
GET website/_search
{
    "query": {
        "range": {
            "post_date": {
                "gte": "2018-01-01 00:00:00",
                "lte": "now",
                "format": "yyyy-MM-dd hh:mm:ss",
                "time_zone": "+1:00"
            }
        }
    }
}
```


ES中的日期类型必须按照UTC时间格式存储, 所以, 上述的2018-01-01 00:00:00将被转换为2017-12-31T23:00:00 UTC.
另外需要注意的是, now是不受time_zone影响的.

参考资料：
 https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-range-query.html#querying-range-fields 

### 排序

#### 1. 排序符号

| 符号 | 含义                                 |
| ---- | ------------------------------------ |
| gte  | greater-than or equal to, 大于或等于 |
| gt   | greater-than, 大于                   |
| lte  | less-than or equal to, 小于或等于    |
| lt   | less-than, 小于                      |

ElasticSearchQueryModel类中与之对应的方法

| 方法                                                         | 说明                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| setOrder(String fieldName, boolean isDesc, Object missing)   | 设置排序，如果原来设置过，此方法会清空之前设置的排序规则，fieldName为排序字段，isDesc是否为倒序，missing为字段值缺失时的默认值 |
| setOrder(Property<T, ?> property, boolean isDesc, Object missing) | property为类::属性 格式的搜索字段名，其他参数同上            |
| setOrder(Property<T, ?> property, boolean isDesc)            |                                                              |
| addOrder(String fieldName, boolean isDesc, Object missing, Integer index) | 添加一个排序规则，一般addOrder系列方法使用较多               |
| addOrder(Property<T, ?> property, boolean isDesc, Object missing) | 同上                                                         |
| addOrder(Property<T, ?> property, boolean isDesc)            | 同上                                                         |
| insertOrder(Property<T, ?> property, boolean isDesc, Object missing, int index) | 插入一个排序规则到指定位置                                   |
| insertOrder(Property<T, ?> property, boolean isDesc, int index) | 同上                                                         |
| insertOrder(String fieldName, boolean isDesc, Object missing, int index) | 同上                                                         |
| unshiftOrder(String fieldName, boolean isDesc, Object missing) | 插入到第一个排序                                             |
| unshiftOrder(String fieldName, boolean isDesc)               | 同上                                                         |
| unshiftOrder(Property<T, ?> property, boolean isDesc, Object missing) | 同上                                                         |
| unshiftOrder(Property<T, ?> property, boolean isDesc)        | 同上                                                         |


如果按得分排序，字段名可以设置为Order.SCORE_SORT（com.ld.shieldsb.es.model.search包）
#### 2. 示例
需求：按创建时间倒序排列

```java
esQueryModel.addOrder(BBSPosterModel::getCreateTime, true);
```

需求：按置顶（1）倒序排列，如果置顶字段值为空，则默认当做0（即非置顶）来处理

```java
esQueryModel.addOrder(BBSPosterModel::getIsTop, true, 0); // 0表示isTop值不存在时默认当做0来排序
```

需求: 假设已经设置了一般排序规则，比如先按置顶再按创建时间，但是存在搜索条件，如果搜索条件为空时按原先条件搜索，搜索条件不为空时应该按匹配程度（即得分）优先排序：

```java
// 一般搜索条件
esQueryModel.addOrder(BBSPosterModel::getIsTop, true, 0); // 0表示isTop值不存在时默认当做0来排序
esQueryModel.addOrder(BBSPosterModel::getCreateTime, true);
// 如果搜索条件不为空
esQueryModel.unshiftOrder(Order.SCORE_SORT, true); // 按得分优先排序
```



## 版本说明

### v1.0.0（2020-08-12）
- 初版发布;
- ;
