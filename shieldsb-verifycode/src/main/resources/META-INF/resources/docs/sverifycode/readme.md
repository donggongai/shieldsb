[TOC]

### 验证码模块
![](https://img.shields.io/badge/release-1.0.0-blue.svg)
#### 使用说明
项目中引入后，内置了servlet（使用了servlet3的注解形式，如果为springboot注意在主类上增加@ServletComponentScan注解启用）路径为 /verifyCode（注意不要与主项目的url冲突），页面中使用img元素，src中增加路径即可引用
```html
<img id="verifyCode" style="float: right" alt="验证码" onclick = "this.src='${ctxPath }/verifyCode?d='+new Date()*1" src="${ctxPath }/verifyCode" width="105" height="40" />
```
##### 验证
使用com.ld.shieldsb.verifycode.VerifyCodeUtil类的checkVerifyCode方法进行验证。

```java
// 参数名为verifyCode
VerifyCodeUtil.checkVerifyCode(HttpServletRequest request);  
// 第2个参数为输入的值
VerifyCodeUtil.checkVerifyCode(HttpServletRequest request, String verifyCodeInput); 
// successRemove为验证成功后是否移除，ajax验证时不需要移除
checkVerifyCode(HttpServletRequest request, String verifyCodeInput, boolean successRemove);
// minutesTimeout为超时时间，默认5分钟
checkVerifyCode(HttpServletRequest request, String sessionKeyCode, String verifyCodeInput, boolean successRemove, int minutesTimeout)
```
##### 配置
目前支持google的kaptcha工具包和EasyCaptcha（https://gitee.com/whvse/EasyCaptcha）两种验证码。
默认使用的是google的kaptcha工具包，可以通过配置文件修改，在项目配置文件的根路径下增加authcode.properties文件，对需要覆盖的属性进行覆盖。
支持的属性：
```properties
### 验证码配置【默认配置】
#类型，支持google（kaptcha）和easy（EasyCaptcha）两种（公共）
authcode.type=google
# 是否显示边框（google）
kaptcha.border=no
# 边框颜色（RGB）（google）
kaptcha.border.color=105,179,90
# 图像宽度（公共）
kaptcha.image.width=110
# 图像高度（公共）
kaptcha.image.height = 40
# 字体大小（公共）
kaptcha.textproducer.font.size=30
# 字符长度（公共）
kaptcha.textproducer.char.length= 4
# session中存放字段（公共）
kaptcha.session.key = key-authCode
# session中存放日期字段（公共）
kaptcha.session.date = key-authCode-createtime
# 图像处理类型（阴影）支持：水纹 com.google.code.kaptcha.impl.WaterRipple 鱼眼 com.google.code.kaptcha.impl.FishEyeGimpy 阴影 com.google.code.kaptcha.impl.ShadowGimpy（google）
kaptcha.obscurificator.impl = com.google.code.kaptcha.impl.ShadowGimpy
# 去掉干扰线（google）
kaptcha.noise.impl = com.google.code.kaptcha.impl.NoNoise
# 文本集合，验证码值从此集合中获取abcde2345678gfynmnpwx（google）
#kaptcha.textproducer.char.string = abcde2345678gfynmnpwx诚朴勇毅
# 字体（google）
kaptcha.textproducer.font.names = 宋体,微软雅黑,楷体
# 字体颜色（google）
kaptcha.textproducer.font.color = blue

# EasyCaptcha的配置，为了统一便于记忆easy.kaptcha前缀中使用了kaptcha而非captcha
#  类型，支持gif、chinese[中文]、arith[算术],为空表示使用默认的等
easy.kaptcha.imgtype=gif
# 默认长度，对于普通的png图片或者gif生效，如果算术类则为级别，如2x3是2级，2x3x2是3级，汉字不需要设置根据图片宽度自动计算
# easy.kaptcha.length=kaptcha.textproducer.char.length
# 默认字体下标，-1表示随机，"actionj.ttf", "epilog.ttf", "fresnel.ttf", "headache.ttf", "lexo.ttf", "prefix.ttf", "progbot.ttf", "ransom.ttf", "robot.ttf", "scandal.ttf"
# 可以写多个下标随机产生
easy.kaptcha.font=-1
```

#### 效果展示
##### Google Kaptcha

---
**水纹：** 
kaptcha.obscurificator.impl = com.google.code.kaptcha.impl.WaterRipple
![验证码](https://s2.ax1x.com/2019/10/24/KNMbb8.jpg)
**阴影：**
kaptcha.obscurificator.impl = com.google.code.kaptcha.impl.ShadowGimpy
![验证码](https://s2.ax1x.com/2019/10/24/KNKh60.jpg)

##### EasyCaptcha
---
**普通：**
![验证码](https://s2.ax1x.com/2019/08/23/msFrE8.png) 
&emsp;&emsp;
![验证码](https://s2.ax1x.com/2019/08/23/msF0DP.png)
&emsp;&emsp;
![验证码](https://s2.ax1x.com/2019/08/23/msFwut.png)
**Gif：**
easy.kaptcha.imgtype=gif
![验证码](https://s2.ax1x.com/2019/08/23/msFzVK.gif) 
&emsp;&emsp;
![验证码](https://s2.ax1x.com/2019/08/23/msFvb6.gif)
&emsp;&emsp;
![验证码](https://s2.ax1x.com/2019/08/23/msFXK1.gif)

**算术类型：**
easy.kaptcha.imgtype=arith
![验证码](https://s2.ax1x.com/2019/08/23/mskKPg.png)
&emsp;&emsp;
![验证码](https://s2.ax1x.com/2019/08/23/msknIS.png)
&emsp;&emsp;
![验证码](https://s2.ax1x.com/2019/08/23/mskma8.png)

**中文类型：**
easy.kaptcha.imgtype=chinese
![验证码](https://s2.ax1x.com/2019/08/23/mskcdK.png)
&emsp;&emsp;
![验证码](https://s2.ax1x.com/2019/08/23/msk6Z6.png)
&emsp;&emsp;
![验证码](https://s2.ax1x.com/2019/08/23/msksqx.png)

**内置字体：**
easy.kaptcha.font=-1随机
easy.kaptcha.font=0 #actionj.ttf
![验证码](https://s2.ax1x.com/2019/10/24/KNQGPH.png)
easy.kaptcha.font=1 #epilog.ttf
![验证码](https://s2.ax1x.com/2019/10/24/KNQHzR.png)
easy.kaptcha.font=2 #fresnel.ttf
![验证码](https://s2.ax1x.com/2019/10/24/KNQDIg.png)
easy.kaptcha.font=3 #headache.ttf
![验证码](https://s2.ax1x.com/2019/10/24/KNQ2Mq.png)
easy.kaptcha.font=4 #lexo.ttf
![验证码](https://s2.ax1x.com/2019/10/24/KNQ5oF.png)
easy.kaptcha.font=5 #prefix.ttf
![验证码](https://s2.ax1x.com/2019/10/24/KNluWj.png)
easy.kaptcha.font=6 #progbot.ttf
![验证码](https://s2.ax1x.com/2019/10/24/KNldp9.png)
easy.kaptcha.font=7 #ransom.ttf
![验证码](https://s2.ax1x.com/2019/10/24/KNlckD.png)
easy.kaptcha.font=8 #robot.ttf
![验证码](https://s2.ax1x.com/2019/10/24/KNl2fH.png)
easy.kaptcha.font=9 #scandal.ttf
![验证码](https://s2.ax1x.com/2019/10/24/KNlf1A.png)

---


#### 版本说明
##### v1.0.0（2019-10-17）
- 初版发布;
- ;