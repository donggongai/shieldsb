package com.ld.shieldsb.verifycode;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 使用WebServlet注解是为了，兼容springboot和普通的servlet容器
 * 
 * @ClassName VerifyCodeController
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年10月18日 上午8:26:46
 *
 */
@WebServlet("/verifyCode")
public class VerifyCodeController extends HttpServlet {
    /**
     * @Fields serialVersionUID
     */
    private static final long serialVersionUID = -637772820452461037L;
    public static final Logger log = LoggerFactory.getLogger(VerifyCodeController.class);

    // 验证码
    @Override
    public void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        // 生成验证码，并将验证码保存到session中
        try {
            VerifyCodeUtil.out(httpServletRequest, httpServletResponse);
        } catch (Exception e) {
            log.error("", e);
        }
    }

}
