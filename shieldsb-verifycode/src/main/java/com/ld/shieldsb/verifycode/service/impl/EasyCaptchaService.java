package com.ld.shieldsb.verifycode.service.impl;

import java.awt.FontFormatException;
import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.RandomStringUtils;

import com.ld.shieldsb.common.core.reflect.ClassUtil;
import com.ld.shieldsb.common.core.util.StringUtils;
import com.ld.shieldsb.verifycode.service.VerifyCodeService;
import com.wf.captcha.ArithmeticCaptcha;
import com.wf.captcha.ChineseCaptcha;
import com.wf.captcha.ChineseGifCaptcha;
import com.wf.captcha.GifCaptcha;
import com.wf.captcha.SpecCaptcha;
import com.wf.captcha.base.Captcha;
import com.wf.captcha.utils.CaptchaUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 验证码工具
 * 
 * @ClassName VerifyCodeUtil
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2019年6月25日 上午8:37:08
 *
 */
@Slf4j
public class EasyCaptchaService extends VerifyCodeService {

    public boolean out(HttpServletRequest request, HttpServletResponse response, String sessionKeyCode)
            throws IOException, FontFormatException {
        CaptchaUtil.setHeader(response);

        // 三个参数分别为宽、高、位数
        Captcha specCaptcha = new SpecCaptcha(); // 默认

        String type = CONFIG.getString("easy.kaptcha.imgtype", "").toLowerCase();
        if (type.contains("gif") && type.contains("chinese")) {
            specCaptcha = new ChineseGifCaptcha(); // 中文gif
        } else if (type.contains("chinese")) {
            specCaptcha = new ChineseCaptcha(); // 中文gif
        } else if (type.contains("arith")) { // 算术
            specCaptcha = new ArithmeticCaptcha();
        } else if (type.contains("gif")) { // gif
            specCaptcha = new GifCaptcha();
        } else {
            specCaptcha = new SpecCaptcha(); // 默认
        }
        specCaptcha.setWidth(CONFIG.getWidth());
        specCaptcha.setHeight(CONFIG.getHeight());
        specCaptcha.setLen(CONFIG.getTextProducerCharLength());
        // 设置字体
        if (!type.contains("chinese")) { // 为英文字体不支持中文
            String fontIndexArrs = CONFIG.getString("easy.kaptcha.font", "-1");
            int fontSize = CONFIG.getTextProducerFontSize(); // 字体
            if (StringUtils.isBlank(fontIndexArrs) || "-1".equals(fontIndexArrs)) {
                int fontIndex = ClassUtil.obj2int(RandomStringUtils.random(1, "0123456789"));
                specCaptcha.setFont(fontIndex, fontSize);
            } else {
                int fontIndex = ClassUtil.obj2int(RandomStringUtils.random(1, fontIndexArrs));
                specCaptcha.setFont(fontIndex, fontSize);
            }
        }

        // specCaptcha.setFont(new Font("Verdana", Font.PLAIN, 32)); // 有默认字体，可以不用设置"Arial", Font.BOLD, 32
        // 设置类型，纯数字、纯字母、字母数字混合
        // specCaptcha.setCharType(Captcha.TYPE_ONLY_NUMBER);

        // 验证码存入session
//            request.getSession().setAttribute(sessionKeyCode, specCaptcha.text().toLowerCase());
        saveVerifyCode2Session(request.getSession(), specCaptcha.text(), sessionKeyCode);

        // 输出图片流
        try (ServletOutputStream out = response.getOutputStream()) {
            specCaptcha.out(out);
            out.flush();
            return true;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return false;
    }

}