package com.ld.shieldsb.verifycode;

public enum VerifyType {
    GOOGLE("google"), EASY("easy");

    private final String value;

    VerifyType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
