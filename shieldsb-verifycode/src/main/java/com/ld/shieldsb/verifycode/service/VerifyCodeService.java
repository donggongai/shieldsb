package com.ld.shieldsb.verifycode.service;

import java.awt.FontFormatException;
import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ld.shieldsb.common.core.util.StringUtils;
import com.ld.shieldsb.verifycode.util.ConfigExtend;

/**
 * 验证码服务类，保存验证码到session，输出验证码到流
 * 
 * @ClassName VerifyCodeUtil
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2019年6月25日 上午8:37:08
 *
 */
public class VerifyCodeService {
    public static final ConfigExtend CONFIG = new ConfigExtend(null); // 默认读取classpath下的authcode.properties文件

    /**
     * 保存验证码到session
     * 
     * @Title saveVerifyCode2Session
     * @author 吕凯
     * @date 2019年6月27日 下午2:30:46
     * @param session
     * @param capText
     * @return boolean
     */
    public boolean saveVerifyCode2Session(HttpSession session, String capText) {
        return saveVerifyCode2Session(session, capText, null);
    }

    /**
     * 保存验证码到session
     * 
     * @Title saveVerifyCode2Session
     * @author 吕凯
     * @date 2019年6月27日 下午2:30:32
     * @param session
     * @param capText
     *            验证码
     * @param sessionKeyCode
     *            session中的验证码保存的key
     * @return boolean
     */
    public boolean saveVerifyCode2Session(HttpSession session, String capText, String sessionKeyCode) {
        String sessionKeycodeDate = null; // session中的验证码生成日期的key
        if (StringUtils.isBlank(sessionKeyCode)) {
            sessionKeyCode = CONFIG.getSessionKey();
            sessionKeycodeDate = CONFIG.getSessionDate();
        } else {
            sessionKeycodeDate = sessionKeyCode + "_date";
        }
        session.setAttribute(sessionKeyCode, capText);
        session.setAttribute(sessionKeycodeDate, new Date());
        return true;
    }

    public boolean out(HttpServletRequest request, HttpServletResponse response) throws IOException, FontFormatException {
        return out(request, response, null);
    }

    public boolean out(HttpServletRequest request, HttpServletResponse response, String sessionKeyCode)
            throws IOException, FontFormatException {
        return false;
    }
}