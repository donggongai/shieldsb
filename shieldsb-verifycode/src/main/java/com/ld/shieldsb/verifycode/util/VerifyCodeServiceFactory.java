package com.ld.shieldsb.verifycode.util;

import com.ld.shieldsb.common.core.ServiceFactory;
import com.ld.shieldsb.verifycode.VerifyType;
import com.ld.shieldsb.verifycode.service.VerifyCodeService;
import com.ld.shieldsb.verifycode.service.impl.EasyCaptchaService;
import com.ld.shieldsb.verifycode.service.impl.GoogleKaptchaService;

public class VerifyCodeServiceFactory extends ServiceFactory<VerifyCodeService> {

    static {
        // 注意工厂类不要一开始就注册对象，否则可能缺少必要的jar包不能实例化报错，应该注册类
        getInstance().registerServiceClass(VerifyType.GOOGLE.getValue(), GoogleKaptchaService.class);
        getInstance().registerServiceClass(VerifyType.EASY.getValue(), EasyCaptchaService.class);
    }

    public static ServiceFactory<VerifyCodeService> getInstance() {
        return getInstance(VerifyCodeServiceFactory.class);
    }

}
