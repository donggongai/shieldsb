[TOC]

### ip地址解析
![](https://img.shields.io/badge/release-1.0.0-blue.svg)
#### 说明
使用了开源库ip2region做ip解析，准确率比较高。

[ip2region]: https://gitee.com/lionsoul/ip2region
注意，此库需要依赖于数据文件，下载地址

[ip库文件]: https://gitee.com/lionsoul/ip2region/tree/master/data



#### 使用说明

静态调用，返回IpInfo对象。

```java
System.out.println(Ip2regionSearcher.search("122.4.233.133"));
System.out.println(Ip2regionSearcher.search("124.42.8.133"));
System.out.println(Ip2regionSearcher.search("47.99.161.176"));
// 使用ip2region2.x结果如下
//IpInfo(cityId=null, country=中国, region=中国|0|山东省|潍坊市|电信, province=山东省, city=潍坊市, isp=电信, dataPtr=0)
//IpInfo(cityId=null, country=中国, region=中国|0|北京|北京市|电信, province=北京, city=北京市, isp=电信, dataPtr=0)
//IpInfo(cityId=null, country=中国, region=中国|0|浙江省|杭州市|阿里云, province=浙江省, city=杭州市, isp=阿里云, dataPtr=0)
// 使用ip2region1.x结果如下
//IpInfo(cityId=1616, country=中国, region=中国|0|山东省|潍坊市|电信, province=山东省, city=潍坊市, isp=电信, dataPtr=228814)
//IpInfo(cityId=215, country=中国, region=中国|0|北京|北京市|电信, province=北京, city=北京市, isp=电信, dataPtr=18118)
//IpInfo(cityId=1132, country=中国, region=中国|0|浙江省|杭州市|阿里云, province=浙江省, city=杭州市, isp=阿里云, dataPtr=70202)
```
IpInfo包含字段
```java
/** 城市id */
private Integer cityId;
/** 国家 */
private String country;
/** 区域 */
private String region;
/** 省 */
private String province;
/** 城市 */
private String city;
/** 运营商 */
private String isp;
```



#### 版本说明
##### v1.0.0（2020-08-26）
- 初版发布;
- ;
##### v1.0.0（2022-07-23）
- 升级ip2region到2.6.5;
- ;