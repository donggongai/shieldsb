package com.ld.shieldsb.ip2region;

import com.ld.shieldsb.ip2region.core.Ip2regionSearcher;

/**
 * Unit test for simple App.
 */
public class AppTest {
    public static void main(String[] arg) {
        System.out.println(Ip2regionSearcher.search("122.4.233.133"));
        System.out.println(Ip2regionSearcher.search("124.42.8.133"));
        System.out.println(Ip2regionSearcher.search("47.99.161.176"));
    }
}
