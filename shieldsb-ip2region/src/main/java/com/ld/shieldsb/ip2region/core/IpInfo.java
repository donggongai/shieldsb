package com.ld.shieldsb.ip2region.core;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;

import lombok.Data;

@Data
public class IpInfo implements Serializable {

    /**
     * @Fields serialVersionUID
     */
    private static final long serialVersionUID = -1979095556207821518L;
    /**
     * 城市id
     */
    private Integer cityId;
    /**
     * 国家
     */
    private String country;
    /**
     * 区域
     */
    private String region;
    /**
     * 省
     */
    private String province;
    /**
     * 城市
     */
    private String city;
    /**
     * 运营商
     */
    private String isp;
    /**
     * region ptr in the db file
     */
    private int dataPtr;

    /**
     * 拼接完整的地址
     *
     * @return address
     */
    public String getAddress() {
        Set<String> regionSet = new LinkedHashSet<>();
        regionSet.add(country);
        regionSet.add(region);
        regionSet.add(province);
        regionSet.add(city);
        regionSet.add(isp);
        regionSet.removeIf(Objects::isNull);
        return String.join(" ", country, region, province, city, city, isp);
    }
}
