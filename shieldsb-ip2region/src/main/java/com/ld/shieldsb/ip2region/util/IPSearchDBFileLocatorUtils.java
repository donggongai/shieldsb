package com.ld.shieldsb.ip2region.util;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

import org.apache.commons.configuration2.io.AbsoluteNameLocationStrategy;
import org.apache.commons.configuration2.io.BasePathLocationStrategy;
import org.apache.commons.configuration2.io.ClasspathLocationStrategy;
import org.apache.commons.configuration2.io.CombinedLocationStrategy;
import org.apache.commons.configuration2.io.DefaultFileSystem;
import org.apache.commons.configuration2.io.FileLocationStrategy;
import org.apache.commons.configuration2.io.FileLocator;
import org.apache.commons.configuration2.io.FileLocator.FileLocatorBuilder;
import org.apache.commons.configuration2.io.FileLocatorUtils;
import org.apache.commons.configuration2.io.FileSystemLocationStrategy;
import org.apache.commons.configuration2.io.HomeDirectoryLocationStrategy;
import org.apache.commons.configuration2.io.ProvidedURLLocationStrategy;

import com.ld.shieldsb.common.core.util.PathUtil;
import com.ld.shieldsb.common.core.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class IPSearchDBFileLocatorUtils {

    private static FileLocationStrategy initDefaultLocationStrategy() {
        /*ClasspathLocationStrategy：从classpath下去加载文件**（常用）**
        AbsoluteNameLocationStrategy：绝对路径。所以直接使用new File(locator.getFileName())去加载
        HomeDirectoryLocationStrategy：从user.home里去查找
        BasePathLocationStrategy：使用basePath+fileName的方式new File()
        FileSystemLocationStrategy：使用文件系统定位。比如windows是带盘符的
        ProvidedURLLocationStrategy：直接是URL的方式。所以它也可以存在于网络上~
        CombinedLocationStrategy：真正使用的。它是一个聚合，这些实现类可以构成一个扫描链来进行按照其顺序进行组合扫描，之前讲过很多类似的设计模式了*/
        final FileLocationStrategy[] subStrategies = new FileLocationStrategy[] { new ProvidedURLLocationStrategy(),
                new FileSystemLocationStrategy(), new AbsoluteNameLocationStrategy(), new BasePathLocationStrategy(),
                new HomeDirectoryLocationStrategy(true), new HomeDirectoryLocationStrategy(false), new ClasspathLocationStrategy() };
        return new CombinedLocationStrategy(Arrays.asList(subStrategies));
    }

    public static void main(String[] args) {
        String fileName = "data/ip2region.xdb";
        URL url = locate(fileName);
        System.out.println(url);

        File file = new File(url.getFile());
        url = locate(file);
        System.out.println(url);
    }

    /**
     * 根据文件名获取文件的url
     * 
     * @Title locate
     * @author 吕凯
     * @date 2020年8月26日 上午11:31:45
     * @param fileName
     * @return URL
     */
    public static URL locate(String fileName) {
        FileLocatorBuilder builder = FileLocatorUtils.fileLocator().fileName(fileName);
        FileLocator locator = new FileLocator(builder);
        URL url = initDefaultLocationStrategy().locate(new DefaultFileSystem(), locator);
        return url;
    }

    /**
     * 根据文件对象获取文件的url
     * 
     * @Title locate
     * @author 吕凯
     * @date 2020年8月26日 上午11:32:15
     * @param file
     * @return URL
     */
    public static URL locate(File file) {
        URL url = null;
        try {
            url = file.toURI().toURL();
        } catch (MalformedURLException e) {
            log.error("", e);
        }
        return url;
    }

    public static File getExtDefaultFile(String fileName) {
        String classRootPath = PathUtil.getClassRootPath(true);
        if (!StringUtils.isEmpty(classRootPath)) {
            File configFile = new File(classRootPath, fileName); // 判断文件是否存在
            return configFile;
        }
        return null;
    }

}