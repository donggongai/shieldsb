[TOC]

### ip转区域模块
![](https://img.shields.io/badge/release-1.0.0-blue.svg)
#### 使用说明
使用了开源项目ip2region，[https://gitee.com/lionsoul/ip2region](https://gitee.com/lionsoul/ip2region) 提供ip地址转为区域名称的方法。准确率99.9%的离线IP地址定位库，0.0x毫秒级查询，数据库文件大小只有1.5M，提供了java,php,c,python,nodejs,golang,c#等查询绑定和Binary,B树,内存三种查询算法。
##### 使用
```java
// 数据文件默认在jar包data/ip2region.db中，使用时会默认解压到当前工作目录或classpath路径下，也可直接使用最新的替换，数据文件的生成参考开源项目
System.out.println(Ip2regionSearcher.search("122.4.233.133"));
```

---

#### 版本说明
##### v1.0.0（2020-08-26）
- 初版发布;
- ;