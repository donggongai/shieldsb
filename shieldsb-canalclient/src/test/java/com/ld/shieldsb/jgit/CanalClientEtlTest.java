package com.ld.shieldsb.jgit;

import java.net.InetSocketAddress;
import java.sql.SQLException;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.otter.canal.client.CanalConnector;
import com.alibaba.otter.canal.client.CanalConnectors;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.ld.shieldsb.canalclient.client.AbstractCanalClient;
import com.ld.shieldsb.canalclient.etl.DatasourceConfig;
import com.ld.shieldsb.canalclient.handler.config.MappingConfig;
import com.ld.shieldsb.canalclient.handler.config.MappingConfig.DbMapping;
import com.ld.shieldsb.canalclient.handler.config.OuterAdapterConfig;
import com.ld.shieldsb.canalclient.handler.impl.Data2LoggerHandler;
import com.ld.shieldsb.canalclient.handler.impl.Data2RDbHandler;
import com.ld.shieldsb.canalclient.model.EtlConditions;
import com.ld.shieldsb.common.core.model.Result;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CanalClientEtlTest {
    public static final String key = "245source";

    public static void main(String[] args) {
        // 注意一个服务端只能有一个客户端生效，其他的都会阻塞
        String destination = "mysql245";
        CanalConnector canalConnector = CanalConnectors.newSingleConnector(new InetSocketAddress("192.168.1.248", 11111), destination,
                "canal", "admin31511cs");

        // 获取CanalServer 连接
        final AbstractCanalClient clientTest = new AbstractCanalClient(destination, canalConnector);

        // 第1个输出器
        OuterAdapterConfig config = new OuterAdapterConfig();
        config.setDestination(destination);
        config.setName(Data2RDbHandler.TYPE_NAME);
        config.setKey("mydb"); // 全局唯一标识，不可重复
        config.setProperties(ImmutableMap.of("jdbc.url",
                "jdbc:mysql://192.168.1.248:3306/software_center_develop?useUnicode=true&characterEncoding=UTF-8&useSSL=false&serverTimezone=Asia/Shanghai",
                "jdbc.username", "develop", "jdbc.password", "develop251"));
        Data2RDbHandler dbHandler = new Data2RDbHandler();

        // 输出的映射配置，不设置则不输出
        MappingConfig mc = new MappingConfig();
        String sourceDatabase = "software_center"; // 源库
        String sourceTable = "t_user"; // 源表
        String targetDatabase = "software_center_develop"; // 目标库
        String targetTable = "t_user"; // 目标表
        mc.setDestination(destination);
        mc.setOuterAdapterKey("mydb");

        DbMapping dbm = new DbMapping();
        dbm.setMapAll(true); // 所有字段匹配，不是所有字段映射的话设置targetColumns属性
        dbm.setTargetPk(ImmutableMap.of("id", "id")); // 主键
        dbm.setDatabase(sourceDatabase); // 源库名
        dbm.setTable(sourceTable); // 源表名
        dbm.setTargetDb(targetDatabase); // 库名
        dbm.setTargetTable(targetTable); // 目标表名
        mc.setDbMapping(dbm);
        config.setConfigs(ImmutableList.of(mc));
        dbHandler.init(config);

        // 第2个输出器
        OuterAdapterConfig config2 = new OuterAdapterConfig();
        config2.setDestination(destination);
        config2.setName(Data2RDbHandler.TYPE_NAME);
        config2.setKey("mydb2"); // 唯一标识,不可重复
        config2.setProperties(ImmutableMap.of("jdbc.url",
                "jdbc:mysql://192.168.1.211:3306/software_center_develop?useUnicode=true&characterEncoding=UTF-8&useSSL=false&serverTimezone=Asia/Shanghai",
                "jdbc.username", "kevin", "jdbc.password", "kevin"));

        MappingConfig mc2 = new MappingConfig();

        /* ****************注意需要设置数据源key，用于找到源数据*******************  */
        mc2.setDataSourceKey(key);

        mc2.setDestination(destination);
        mc2.setOuterAdapterKey("mydb2");
        DbMapping dbm2 = new DbMapping();
        dbm2.setMapAll(true); // 所有字段匹配，不是所有字段映射的话设置targetColumns属性
        dbm2.setTargetColumns(ImmutableMap.of("username", "username", "email", "email")); // 设置了所有字段匹配，则该项不起作用
        dbm2.setTargetPk(ImmutableMap.of("id", "id")); // 主键
        dbm2.setDatabase(sourceDatabase); // 源库名
        dbm2.setTable(sourceTable); // 源表名
        dbm2.setTargetDb(targetDatabase); // 库名
        dbm2.setTargetTable(targetTable); // 目标表名
        mc2.setDbMapping(dbm2);
        config2.setConfigs(ImmutableList.of(mc2));
        Data2RDbHandler dbHandler2 = new Data2RDbHandler();
        dbHandler2.init(config2);

        Data2LoggerHandler loggerHandler = new Data2LoggerHandler();
        OuterAdapterConfig loggerConfig = new OuterAdapterConfig();
        loggerConfig.setKey("logger");
        loggerHandler.init(loggerConfig);

        // 添加处理器，注意顺序
        clientTest.addHandler(loggerHandler); // 日志输出
        clientTest.addHandler(dbHandler); // 数据库输出
        clientTest.addHandler(dbHandler2);

        /* ***************注册数据源*********** */
        String sourceUrl = "jdbc:mysql://192.168.1.245:3306/software_center?useUnicode=true&characterEncoding=UTF-8&useSSL=false&serverTimezone=Asia/Shanghai";
        String sourceUsername = "software";
        String sourcePassword = "software251";
        setDateSource(key, sourceUrl, sourceUsername, sourcePassword);

        Result result = dbHandler2.etl(destination, sourceDatabase, sourceTable, targetDatabase, targetTable, new EtlConditions(), con -> {
        });
        log.warn("{}", result);

        // 启动客户端
//        clientTest.start();

    }

    public static void setDateSource(String key, DatasourceConfig datasourceConfig) {
        // 加载数据源连接池
        DruidDataSource ds = new DruidDataSource();
        ds.setDriverClassName(datasourceConfig.getDriver());
        ds.setUrl(datasourceConfig.getUrl());
        ds.setUsername(datasourceConfig.getUsername());
        ds.setPassword(datasourceConfig.getPassword());
        ds.setInitialSize(1);
        ds.setMinIdle(1);
        ds.setMaxActive(datasourceConfig.getMaxActive());
        ds.setMaxWait(60000); // 最大等待时间
        ds.setTimeBetweenEvictionRunsMillis(60000);
        ds.setMinEvictableIdleTimeMillis(300000);
        ds.setValidationQuery("select 1");
        try {
            ds.init();
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        DatasourceConfig.DATA_SOURCES.put(key, ds);
    }

    public static void setDateSource(String key, String url, String username, String password) {
        // 加载数据源连接池
        DatasourceConfig datasourceConfig = new DatasourceConfig();
        datasourceConfig.setUrl(url);
        datasourceConfig.setUsername(username);
        datasourceConfig.setPassword(password);

        setDateSource(key, datasourceConfig);
    }

}
