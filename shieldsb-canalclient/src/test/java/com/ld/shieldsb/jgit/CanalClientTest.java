package com.ld.shieldsb.jgit;

import java.net.InetSocketAddress;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.otter.canal.client.CanalConnector;
import com.alibaba.otter.canal.client.CanalConnectors;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.ld.shieldsb.canalclient.client.AbstractCanalClient;
import com.ld.shieldsb.canalclient.handler.config.MappingConfig;
import com.ld.shieldsb.canalclient.handler.config.MappingConfig.DbMapping;
import com.ld.shieldsb.canalclient.handler.config.OuterAdapterConfig;
import com.ld.shieldsb.canalclient.handler.impl.Data2LoggerHandler;
import com.ld.shieldsb.canalclient.handler.impl.Data2RDbHandler;
import com.ld.shieldsb.canalclient.handler.impl.Data2RollingFileHandler;
import com.ld.shieldsb.canalclient.handler.impl.Data2SolrHandler;
import com.ld.shieldsb.canalclient.model.Dml;
import com.ld.shieldsb.canalclient.model.RecordHandlerDmlModel;
import com.ld.shieldsb.canalclient.model.RecordModel;
import com.ld.shieldsb.canalclient.recoder.RecorderHandler;
import com.ld.shieldsb.common.core.queue.QueueEntry;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CanalClientTest {

    public static void main(String[] args) {
        // 注意一个服务端只能有一个客户端生效，其他的都会阻塞
        String destination = "mysql245";
        CanalConnector canalConnector = CanalConnectors.newSingleConnector(new InetSocketAddress("192.168.1.248", 11111), destination,
                "canal", "admin31511cs");

        // 获取CanalServer 连接
        final AbstractCanalClient clientTest = new AbstractCanalClient(destination, canalConnector);

        // 第1个输出器
        OuterAdapterConfig config = new OuterAdapterConfig();
        config.setDestination(destination);
        config.setName(Data2RDbHandler.TYPE_NAME);
        config.setKey("mydb"); // 全局唯一标识，不可重复
        config.setProperties(ImmutableMap.of("jdbc.url",
                "jdbc:mysql://192.168.1.248:3306/software_center_develop?useUnicode=true&characterEncoding=UTF-8&useSSL=false&serverTimezone=Asia/Shanghai",
                "jdbc.username", "develop", "jdbc.password", "develop251"));
        Data2RDbHandler dbHandler = new Data2RDbHandler();

        // 输出的映射配置，不设置则不输出
        MappingConfig mc = new MappingConfig();
        String sourceDatabase = "software_center"; // 源库
        String sourceTable = "t_user"; // 源表
        String targetDatabase = "software_center_develop"; // 目标库
        String targetTable = "t_user"; // 目标表
        mc.setDestination(destination);
        mc.setOuterAdapterKey("mydb");

        DbMapping dbm = new DbMapping();
        dbm.setMapAll(true); // 所有字段匹配，不是所有字段映射的话设置targetColumns属性
        dbm.setTargetPk(ImmutableMap.of("id", "id")); // 主键
        dbm.setDatabase(sourceDatabase); // 源库名
        dbm.setTable(sourceTable); // 源表名
        dbm.setTargetDb(targetDatabase); // 库名
        dbm.setTargetTable(targetTable); // 目标表名
        mc.setDbMapping(dbm);
        config.setConfigs(ImmutableList.of(mc));
        dbHandler.init(config);

        // 第2个输出器
        OuterAdapterConfig config2 = new OuterAdapterConfig();
        config2.setDestination(destination);
        config2.setName(Data2RDbHandler.TYPE_NAME);
        config2.setKey("mydb2"); // 唯一标识,不可重复
        config2.setProperties(ImmutableMap.of("jdbc.url",
                "jdbc:mysql://192.168.1.211:3306/software_center_develop?useUnicode=true&characterEncoding=UTF-8&useSSL=false&serverTimezone=Asia/Shanghai",
                "jdbc.username", "kevin", "jdbc.password", "kevin"));

        MappingConfig mc2 = new MappingConfig();
        mc.setDataSourceKey(targetTable);//
        mc2.setDestination(destination);
        mc2.setOuterAdapterKey("mydb2");
        DbMapping dbm2 = new DbMapping();
        dbm2.setTargetColumns(ImmutableMap.of("username", "username", "email", "email")); // 设置匹配匹配，目标在前源在后
        dbm2.setTargetPk(ImmutableMap.of("id", "id")); // 主键
        dbm2.setDatabase(sourceDatabase); // 源库名
        dbm2.setTable(sourceTable); // 源表名
        dbm2.setTargetDb(targetDatabase); // 库名
        dbm2.setTargetTable(targetTable); // 目标表名
        mc2.setDbMapping(dbm2);
        config2.setConfigs(ImmutableList.of(mc2));
        Data2RDbHandler dbHandler2 = new Data2RDbHandler();
        dbHandler2.init(config2);

        // solr输出器
        OuterAdapterConfig solrConfig = new OuterAdapterConfig();
        solrConfig.setDestination(destination);
        config.setName(Data2SolrHandler.TYPE_NAME);
        solrConfig.setKey("mysolr"); // 全局唯一标识，不可重复
        solrConfig
                .setProperties(ImmutableMap.of("url", "http://192.168.1.247:8983/rlos", "username", "admin", "password", "11235813Admin@"));
        Data2SolrHandler solrHandler = new Data2SolrHandler();

        // 输出的映射配置，不设置则不输出
        MappingConfig solrMc = new MappingConfig();
        String solrMcsourceDatabase = "software_center"; // 源库
        String solrMcsourceTable = "t_user"; // 源表
        String solrMctargetTable = "z_kevintest"; // 目标表
        solrMc.setDestination(destination);
        solrMc.setOuterAdapterKey("mysolr");

        DbMapping solrdbm = new DbMapping();
//        solrdbm.setMapAll(true); // 所有字段匹配，不是所有字段映射的话设置targetColumns属性
        solrdbm.setTargetColumns(ImmutableMap.of("username", "username", "email", "email")); // 设置匹配匹配，目标在前源在后
        solrdbm.setTargetPk(ImmutableMap.of("id", "id")); // 主键
        solrdbm.setDatabase(solrMcsourceDatabase); // 源库名
        solrdbm.setTable(solrMcsourceTable); // 源表名
        solrdbm.setTargetTable(solrMctargetTable); // 目标表名
        solrMc.setDbMapping(solrdbm);
        solrConfig.setConfigs(ImmutableList.of(solrMc));

        solrHandler.init(solrConfig);

        Data2LoggerHandler loggerHandler = new Data2LoggerHandler();
        OuterAdapterConfig loggerConfig = new OuterAdapterConfig();
        loggerConfig.setKey("logger");
        loggerHandler.init(loggerConfig);

        Data2RollingFileHandler fileHandler = new Data2RollingFileHandler();
        OuterAdapterConfig fileConfig = new OuterAdapterConfig();
        fileConfig.setKey("file");
        fileConfig.setProperties(ImmutableMap.of("file.path", "D:\\Desktop\\toufile.txt"));
        fileHandler.init(fileConfig);

        // 添加处理器，注意顺序
        clientTest.addHandler(loggerHandler); // 日志输出
        clientTest.addHandler(fileHandler); // 日志输出
        clientTest.addHandler(dbHandler); // 数据库输出
        clientTest.addHandler(dbHandler2);
        clientTest.addHandler(solrHandler);

        // 记录数据处理器，用于记录接收到的数据和同步数据，便于统计，如果不需要统计可不设置
        clientTest.setRecorderHandler(new RecorderHandler(1000, 100, 100) {

            @Override
            protected void takeSyncRecord(QueueEntry<RecordModel> recordModel) {
                System.out.println("接收同步记录：" + JSONObject.toJSONString(recordModel));
            }

            @Override
            protected void takeDmlRecord(QueueEntry<Dml> dml) {
                System.out.println("接收Dml：" + JSONObject.toJSONString(dml));
            }

            @Override
            protected void takeValidDmlRecord(QueueEntry<RecordHandlerDmlModel> dml) {
                System.out.println("接收处理器Dml：" + JSONObject.toJSONString(dml));

            }
        });

        // 启动客户端
        clientTest.start();

        // System.out.println(Util.testConn("192.168.1.248", 11112, "", "")); //测试连接

        // 获取CanalServer 连接
        /*final AbstractCanalClient clientTest2 = new AbstractCanalClient(destination, canalConnector2);
        // 注意顺序
        clientTest2.addHandler(new Data2LoggerHandler()); // 日志输出
        
        clientTest2.addHandler(dbHandler2); // 数据库输出
        clientTest2.start();*/

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                log.info("## stop the canal client");
                clientTest.stop();
                // clientTest2.stop();
            } catch (Throwable e) {
                log.warn("##something goes wrong when stopping canal:", e);
            } finally {
                log.info("## canal client is down.");
            }
        }));
    }

}
