package com.ld.shieldsb.canalclient.model;

import java.util.List;

import lombok.Data;

/**
 * ETL条件
 * 
 * @ClassName EtlConditions
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2022年1月13日 下午1:59:40
 *
 */
@Data
public class EtlConditions {
    private String etlCondition; // etl通用条件sql
    private String etlDelta; // etl增量条件
    private List<String> params; // 参数，注意增量条件要放到第一个
    private int type; // 0增量(默认) 、1全量
    private String uniqueKey; // 唯一主键，批次号
    private String dataSourceKey; // 数据源key

    public static final Integer TYPE_DELTA = 0; // 增量
    public static final Integer TYPE_FULL = 1; // 全量
}
