package com.ld.shieldsb.canalclient.etl;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import javax.sql.DataSource;

import com.ld.shieldsb.canalclient.handler.config.AdapterConfig.AdapterMapping;

import lombok.AllArgsConstructor;
import lombok.Builder.Default;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class EtlConsumer {
    private DataSource srcDS;
    private String sql;
    private List<Object> values;
    private AdapterMapping mapping;
    private AtomicLong impCount; // 导入成功的条数
    private List<String> errMsg;
    @Default
    private Boolean success = true; // 默认值

    private Long count; // 总条数
    private Long beginTime; // 开始时间毫秒数
    private Long endTime; // 结束数据毫秒数

    private Integer processState; // 进程状态

    public static final Integer PROCESS_STATE_BEGIN = 0;// 开始
    public static final Integer PROCESS_STATE_RUNNING = 1; // 处理中
    public static final Integer PROCESS_STATE_END = 2; // 结束
    public static final Integer PROCESS_STATE_GETCOUNT = 3; // 获取总条数
}
