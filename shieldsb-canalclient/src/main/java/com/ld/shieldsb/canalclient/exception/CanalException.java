package com.ld.shieldsb.canalclient.exception;

/**
 * 需要捕获的异常
 * 
 * @ClassName CanalException
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年12月24日 下午5:43:06
 *
 */
public class CanalException extends Exception {

    /**
     * @Fields serialVersionUID
     */
    private static final long serialVersionUID = -726171781636541972L;

    public CanalException() {
        super();
    }

    public CanalException(String message) {
        super(message);
    }

    public CanalException(String message, Throwable cause) {
        super(message, cause);
    }

}
