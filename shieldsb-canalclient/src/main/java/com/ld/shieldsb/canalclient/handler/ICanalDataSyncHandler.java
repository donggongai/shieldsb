package com.ld.shieldsb.canalclient.handler;

import java.util.List;
import java.util.function.Consumer;

import com.ld.shieldsb.canalclient.etl.EtlConsumer;
import com.ld.shieldsb.canalclient.etl.EtlServiceUtil;
import com.ld.shieldsb.canalclient.exception.CanalException;
import com.ld.shieldsb.canalclient.exception.CanalNoImplementException;
import com.ld.shieldsb.canalclient.handler.config.OuterAdapterConfig;
import com.ld.shieldsb.canalclient.model.Dml;
import com.ld.shieldsb.canalclient.model.EtlConditions;
import com.ld.shieldsb.canalclient.recoder.Recorder;
import com.ld.shieldsb.common.core.model.Result;

public interface ICanalDataSyncHandler {
    /**
     * 获取类型
     * 
     * @Title getType
     * @author 吕凯
     * @date 2021年12月2日 下午5:13:44
     * @return String
     */
    public String getType();

    /**
     * 初始化
     * 
     * @Title init
     * @author 吕凯
     * @date 2021年12月2日 上午11:34:46
     * @param configuration
     *            void
     */
    public void init(OuterAdapterConfig configuration);

    /**
     * 
     * 事件消息处理函数
     * 
     * @Title sync
     * @author 吕凯
     * @date 2021年11月13日 下午3:43:31
     * @param innerBinlogEntries
     *            void
     */
    public default void handle(List<Dml> dmls, Recorder recorder) {
        if (getInit()) { // 保证已经初始化才能进行同步
            sync(dmls, recorder);
        }

    }

    void sync(List<Dml> dmls, Recorder recorder);

    /**
     * 同步单条数据
     * 
     * @Title sync
     * @author 吕凯
     * @date 2021年12月25日 下午4:23:06
     * @param index
     * @param config
     * @param dml
     * @param recorder
     *            void
     */
    Result syncSingle(Dml dml, int dataIndex, String targetDb, String targetTable, Recorder recorder);

    /**
     * 传输数据，用于目标库的导入
     * 
     * @Title etl
     * @author 吕凯
     * @date 2022年1月11日 下午3:30:46
     * @param destination
     * @param database
     * @param table
     * @param targetDb
     * @param targetTable
     * @param params
     *            etl的参数
     * @param DbMapping
     *            etl的参数
     * @return Result
     * @throws CanalException
     */
    default Result etl(String destination, String database, String table, String targetDb, String targetTable, EtlConditions etlConditions,
            Consumer<EtlConsumer> con) throws CanalNoImplementException {
        throw new CanalNoImplementException("不支持的操作！");
    }

    /**
     * 获取etl数据的数目
     * 
     * @Title getEtlDataNum
     * @author 吕凯
     * @date 2022年1月13日 下午3:50:51
     * @param dataSourceKey
     * @param database
     * @param table
     * @param targetDb
     * @param targetTable
     * @param etlConditions
     * @return Result
     */
    default Result getEtlDataNum(String dataSourceKey, String database, String table, String targetDb, String targetTable,
            EtlConditions etlConditions) throws CanalNoImplementException {
        return EtlServiceUtil.getImportDataNum(dataSourceKey, database, targetTable, etlConditions);
    }

    /**
     * 测试连接
     * 
     * @Title testConn
     * @author 吕凯
     * @date 2021年12月22日 上午9:06:21
     * @return Result
     */
    public Result testConn();

    /**
     * 销毁方法,进行资源释放
     */
    public void destroy();

    /**
     * 获取是否初始化，不初始化不能使用
     * 
     * @Title getInit
     * @author 吕凯
     * @date 2021年12月7日 上午11:30:19
     * @return boolean
     */
    public default boolean getInit() {
        return false;
    }

    /**
     * 获取唯一标识符，一个客户端内唯一
     * 
     * @Title getKey
     * @author 吕凯
     * @date 2021年12月25日 下午4:19:16
     * @return String
     */
    public String getKey();

}