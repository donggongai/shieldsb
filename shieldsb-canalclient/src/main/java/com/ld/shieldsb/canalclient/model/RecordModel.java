package com.ld.shieldsb.canalclient.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder.Default;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 记录的model
 * 
 * @ClassName RecordModel
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年12月16日 上午11:54:55
 *
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class RecordModel {
    private String handlerKey;
    @Default
    private Date datetime = new Date(); // 数据发送时间
    private SingleDml singleDml;

    private String targetDb;
    private String targetTable;

    private Integer handState; // 处理状态0失败1成功2忽略
    private String msg; // 处理信息,如错误提示

}
