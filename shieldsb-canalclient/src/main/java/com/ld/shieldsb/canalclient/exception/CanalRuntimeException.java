package com.ld.shieldsb.canalclient.exception;

/**
 * 运行时异常
 * 
 * @ClassName CanalRuntimeException
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年12月24日 下午5:42:44
 *
 */
public class CanalRuntimeException extends RuntimeException {

    /**
     * @Fields serialVersionUID
     */
    private static final long serialVersionUID = -726171781636541971L;

    public CanalRuntimeException() {
        super();
    }

    public CanalRuntimeException(String message) {
        super(message);
    }

    public CanalRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

}
