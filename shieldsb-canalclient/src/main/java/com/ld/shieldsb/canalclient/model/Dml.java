package com.ld.shieldsb.canalclient.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import lombok.Data;

/**
 * DML操作转换对象
 *
 * @author rewerma 2018-8-19 下午11:30:49
 * @version 1.0.0
 */
@Data
public class Dml implements Serializable {

    private static final long serialVersionUID = 2611556444074013421L;

    private String uuid = UUID.randomUUID().toString(); // 生成uuid

    private String destination; // 对应canal的实例或者MQ的topic
    private String groupId; // 对应mq的group id
    private String database; // 数据库或schema
    private String table; // 表名
    private List<String> pkNames;
    private Boolean isDdl;
    private String type; // 类型: INSERT UPDATE DELETE
    // binlog executeTime
    private Long es; // 执行时间
    // dml build timeStamp
    private Long ts; // 同步时间
    private String sql; // 执行的sql, dml sql为空
    private List<Map<String, Object>> data; // 数据列表
    private List<Map<String, Object>> old; // 旧数据列表, 用于update, size和data的size一一对应

}
