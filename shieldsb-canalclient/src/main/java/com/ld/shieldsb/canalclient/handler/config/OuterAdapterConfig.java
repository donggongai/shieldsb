package com.ld.shieldsb.canalclient.handler.config;

import java.util.List;
import java.util.Map;

import lombok.Data;

/**
 * 外部适配器配置信息类
 *
 * @author rewerma 2018-8-18 下午10:15:12
 * @version 1.0.0
 */
@Data
public class OuterAdapterConfig {

    private String destination; // 实例
    private String name; // 适配器名称, 如: logger, hbase, es
    private String key; // 适配器唯一键
    private String hosts; // 适配器内部的地址, 比如对应es该参数可以填写es的server地址
    private String zkHosts; // 适配器内部的ZK地址, 比如对应HBase该参数可以填写HBase对应的ZK地址
    private Map<String, String> properties; // 其余参数, 可填写适配器中的所需的配置信息

    private List<AdapterConfig> configs; // 适配器配置，可能多个

}
