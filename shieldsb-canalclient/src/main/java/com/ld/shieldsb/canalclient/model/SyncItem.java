package com.ld.shieldsb.canalclient.model;

import com.ld.shieldsb.canalclient.handler.config.MappingConfig;

import lombok.Data;

/**
 * 同步条目
 * 
 * @ClassName SyncItem
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年12月6日 上午9:43:23
 *
 */
@Data
public class SyncItem {

    private MappingConfig config;
    private SingleDml singleDml;

    public SyncItem(MappingConfig config, SingleDml singleDml) {
        this.config = config;
        this.singleDml = singleDml;
    }

}