package com.ld.shieldsb.canalclient.exception;

/**
 * 需要捕获的异常，方法未实现
 * 
 * @ClassName CanalException
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年12月24日 下午5:43:06
 *
 */
public class CanalNoImplementException extends Exception {

    /**
     * @Fields serialVersionUID
     */
    private static final long serialVersionUID = -726171781636541972L;

    public CanalNoImplementException() {
        super();
    }

    public CanalNoImplementException(String message) {
        super(message);
    }

    public CanalNoImplementException(String message, Throwable cause) {
        super(message, cause);
    }

}
