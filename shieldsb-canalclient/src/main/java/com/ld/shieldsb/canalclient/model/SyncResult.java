package com.ld.shieldsb.canalclient.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SyncResult {
    @Default
    private boolean success = true;
    @Default
    private int state = 1; // 处理状态0失败1成功2忽略
    private String msg; // 处理信息,如错误提示

    public static final Integer HAND_STATE_SUCCESS = 1;
    public static final Integer HAND_STATE_ERROR = 0;
    public static final Integer HAND_STATE_IGNORE = 2;

}
