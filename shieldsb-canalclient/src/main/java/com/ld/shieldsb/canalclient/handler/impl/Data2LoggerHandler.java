package com.ld.shieldsb.canalclient.handler.impl;

import java.util.List;

import com.ld.shieldsb.canalclient.handler.ICanalDataSyncHandler;
import com.ld.shieldsb.canalclient.handler.config.OuterAdapterConfig;
import com.ld.shieldsb.canalclient.model.Dml;
import com.ld.shieldsb.canalclient.recoder.Recorder;
import com.ld.shieldsb.common.core.model.Result;
import com.ld.shieldsb.common.core.util.ResultUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 打印日志处理器
 * 
 * @ClassName LoggerHandler
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年11月13日 下午4:21:18
 *
 */
@Slf4j
public class Data2LoggerHandler implements ICanalDataSyncHandler {
    public static final String PROPERTIES_DESP = "不需要参数"; // OuterAdapterConfig中properties 参数说明，必须有
    public static final String TYPE_NAME = "logger";
    private boolean inited = false; // 初始化字段标识
    private String key; // 唯一标识

    @Override
    public void sync(List<Dml> dmls, Recorder recorder) {
        log.warn("接收到信息条数：{}", dmls.size());
        dmls.stream().forEach(msg -> log.warn("cmessages={}", msg));
    }

    @Override
    public void init(OuterAdapterConfig configuration) {
        key = configuration.getKey();
        if (log.isWarnEnabled()) {
            log.warn("日志处理器[key:{}]初始化", configuration.getKey());
        }
        inited = true; // 标记已经初始化
    }

    @Override
    public void destroy() {
        if (log.isTraceEnabled()) {
            log.trace("日志处理器销毁");
        }
    }

    @Override
    public String getType() {
        return TYPE_NAME;
    }

    @Override
    public boolean getInit() {
        return inited;
    }

    @Override
    public Result testConn() {
        return ResultUtil.success("");
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Result syncSingle(Dml dml, int dataIndex, String targetDb, String targetTable, Recorder recorder) {
        return ResultUtil.error("该处理器无需单条同步");
    }

}
