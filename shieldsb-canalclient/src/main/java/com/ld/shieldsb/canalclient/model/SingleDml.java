package com.ld.shieldsb.canalclient.model;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import lombok.Data;

/**
 * 每个更新对象，一个Dml中可能包含多个对象
 * 
 * @ClassName SingleDml
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年12月6日 上午9:30:25
 *
 */
@Data
public class SingleDml {

    private String destination;
    private String database;
    private String table;
    private String type;
    private Map<String, Object> data;
    private Map<String, Object> old;

    private String dmlUuid; // dml的uuid
    private int index = 0; // 在Dml中的下标

    /**
     * Dml对象转SingleDmls对象
     * 
     * @Title dml2SingleDmls
     * @author 吕凯
     * @date 2021年12月6日 上午9:29:10
     * @param dml
     * @param caseInsensitive
     *            忽略大小写
     * @return List<SingleDml>
     */
    public static List<SingleDml> dml2SingleDmls(Dml dml, boolean caseInsensitive) {
        List<SingleDml> singleDmls = new ArrayList<>();
        if (dml.getData() != null) {
            int size = dml.getData().size(); // 是不是批量操作有多条？
            for (int i = 0; i < size; i++) {
                SingleDml singleDml = new SingleDml();
                singleDml.setDmlUuid(dml.getUuid()); // 设置uuid，识别批次
                singleDml.setIndex(i); // 索引值
                singleDml.setDestination(dml.getDestination());
                singleDml.setDatabase(dml.getDatabase());
                singleDml.setTable(dml.getTable());
                singleDml.setType(dml.getType());
                Map<String, Object> data = dml.getData().get(i);
                if (caseInsensitive) {
                    data = toCaseInsensitiveMap(data);
                }
                singleDml.setData(data);
                if (dml.getOld() != null) {
                    Map<String, Object> oldData = dml.getOld().get(i);
                    if (caseInsensitive) {
                        oldData = toCaseInsensitiveMap(oldData);
                    }
                    singleDml.setOld(oldData);
                }
                singleDmls.add(singleDml);
            }
        } else if ("TRUNCATE".equalsIgnoreCase(dml.getType())) {
            SingleDml singleDml = new SingleDml();
            singleDml.setDmlUuid(dml.getUuid()); // 设置uuid，识别批次
            singleDml.setDestination(dml.getDestination());
            singleDml.setDatabase(dml.getDatabase());
            singleDml.setTable(dml.getTable());
            singleDml.setType(dml.getType());
            singleDmls.add(singleDml);
        }
        return singleDmls;
    }

    public static List<SingleDml> dml2SingleDmls(Dml dml) {
        return dml2SingleDmls(dml, false);
    }

    /**
     * 转为key大小写不敏感的map
     * 
     * @Title toCaseInsensitiveMap
     * @author 吕凯
     * @date 2021年12月6日 上午9:27:50
     * @param <V>
     * @param data
     * @return LinkedCaseInsensitiveMap<V>
     */
    private static <V> Map<String, V> toCaseInsensitiveMap(Map<String, V> data) {
        Map<String, V> map = new LinkedHashMap<>();
        data.forEach((key, value) -> map.put(key.toLowerCase(), value));
        return map;
    }
}
