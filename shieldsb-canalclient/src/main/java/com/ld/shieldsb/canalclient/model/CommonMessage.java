package com.ld.shieldsb.canalclient.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.alibaba.otter.canal.protocol.CanalEntry.Entry;

import lombok.Data;

@Data
public class CommonMessage implements Serializable {

    private static final long serialVersionUID = 2611556444074013268L;

    private String database; // 数据库或schema
    private String table; // 表名
    private List<String> pkNames;
    private Boolean isDdl;
    // 类型:INSERT/UPDATE/DELETE
    private String type;
    // binlog executeTime, 执行时间
    private Long es;
    // dml build timeStamp, 同步时间
    private Long ts;
    // 执行的sql,dml sql为空
    private String sql;
    // 数据列表
    private List<Map<String, Object>> data;
    // 旧数据列表,用于update,size和data的size一一对应
    private List<Map<String, Object>> old;

    /**
     * canal原生的Entry
     */
    private Entry entry;

    public void clear() {
        database = null;
        table = null;
        type = null;
        ts = null;
        es = null;
        data = null;
        old = null;
        sql = null;
    }

    @Override
    public String toString() {
        return "CommonMessage{" + "database='" + database + '\'' + ", table='" + table + '\'' + ", pkNames=" + pkNames + ", isDdl=" + isDdl
                + ", type='" + type + '\'' + ", es=" + es + ", ts=" + ts + ", sql='" + sql + '\'' + ", data=" + data + ", old=" + old + '}';
    }
}
