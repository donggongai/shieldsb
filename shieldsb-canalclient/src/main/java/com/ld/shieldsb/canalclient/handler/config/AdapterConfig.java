package com.ld.shieldsb.canalclient.handler.config;

public interface AdapterConfig {
    String getDataSourceKey();

    AdapterMapping getMapping();

    interface AdapterMapping {
        String getEtlCondition();

        // 增量条件
        String getEtlDelta();
    }
}
