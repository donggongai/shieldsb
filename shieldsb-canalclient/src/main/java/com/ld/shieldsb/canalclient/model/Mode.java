package com.ld.shieldsb.canalclient.model;

public enum Mode {
    LOCAL, // 本地模式
    DISTRIBUTED // 分布式
}
