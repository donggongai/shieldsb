package com.ld.userpay.model;

import lombok.Data;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
public class PaySendModel {
    private String money; // 字符串形式的钱数
    private String body; // 内容
    private String attach; // 备注
    private String outTradeNo; // 业务系统订单号
    private String ip; // 支付人ip

    private String type; // 支付类型，对应PayType里面的keyword

}
