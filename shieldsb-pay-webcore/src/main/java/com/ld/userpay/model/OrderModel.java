package com.ld.userpay.model;

import java.util.Date;

import lombok.Data;

/** 订单信息表 */
@Data
public class OrderModel implements java.io.Serializable {

    private static final long serialVersionUID = -8843579307741077396L;
    private Long id;

    private Long createBy; // 操作人id
    private String createName;

    private Double payMoney; // 金额
    private Double balance; // 余额
    private String meno; // 描述

    private Long companyId;
    private String companyName;

    private Date createTime; // 创建时间

    private Integer state; // 状态,参见Static类
    private String orderId; // 充值订单号
    private String orderInnerId; // 内部订单号（支付渠道订单号）

    private String relatedOrder; // 关联订单
    private Integer payType; // 支付渠道0余额1支付宝2微信
    private Integer back; // 是否退款0否1是

    // 更新者
    private String updateBy;
    // 更新名称
    private String updateName;
    // 更新时间
    private Date updateTime; // 更新时间用于重新充值

    // 支付时间
    private Date payTime; // 更新时间用于重新充值

    // 支付ip
    private String payIp;
}