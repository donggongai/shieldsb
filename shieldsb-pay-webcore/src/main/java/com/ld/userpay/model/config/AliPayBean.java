package com.ld.userpay.model.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@PropertySource("classpath:/alipay.properties")
@ConfigurationProperties(prefix = "alipay")
@Data
public class AliPayBean implements IPayConfigModel {
    private String appId;
    private String privateKey;
    private String publicKey;
    private String appCertPath;
    private String aliPayCertPath;
    private String aliPayRootCertPath;
    private String serverUrl;
    private String domain;

    @Override
    public String toString() {
        return "AliPayBean{" + "appId='" + appId + '\'' + ", privateKey='" + privateKey + '\'' + ", publicKey='" + publicKey + '\''
                + ", appCertPath='" + appCertPath + '\'' + ", aliPayCertPath='" + aliPayCertPath + '\'' + ", aliPayRootCertPath='"
                + aliPayRootCertPath + '\'' + ", serverUrl='" + serverUrl + '\'' + ", domain='" + domain + '\'' + '}';
    }

}