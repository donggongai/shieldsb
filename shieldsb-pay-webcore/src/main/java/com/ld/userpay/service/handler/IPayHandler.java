package com.ld.userpay.service.handler;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.Pair;

import com.ld.shieldsb.common.core.model.PropertiesModel;
import com.ld.shieldsb.common.core.model.Result;
import com.ld.userpay.util.pay.PayUtil.PayType;

/**
 * 回复处理器
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年4月23日 下午5:05:22
 *
 */
public interface IPayHandler {
    /**
     * 处理器类型类型
     *
     * @return 枚举类
     */
    PayType type();

    /**
     * 获取二维码地址，如weixin://wxpay/bizpayurl?pr=87csiY9zz
     * 
     * @Title getCodeUrl
     * @author 吕凯
     * @date 2022年4月25日 下午4:01:36
     * @param params
     * @return String
     */
    public Result getCodeUrl(HttpServletRequest request, HttpServletResponse response, String orderid, String body, String attach);

    /**
     * PC支付
     * 
     * @Title pcPay
     * @author 吕凯
     * @date 2022年4月26日 下午12:18:56
     * @param request
     * @param response
     * @param orderid
     * @param subject
     * @param body
     * @return Result
     */
    public Result pcPay(HttpServletRequest request, HttpServletResponse response, String orderid, String body, String attach);

    /**
     * wap支付
     * 
     * @Title wapPay
     * @author 吕凯
     * @date 2022年4月26日 下午12:19:07
     * @param request
     * @param response
     * @param orderid
     * @param subject
     * @param body
     * @return Result
     */
    public Result wapPay(HttpServletRequest request, HttpServletResponse response, String orderid, String body, String attach);

    /**
     * 支付完成异步回调方法
     * 
     * @Title payNotify
     * @author 吕凯
     * @date 2022年4月26日 下午12:18:33
     * @param request
     * @param response
     * @param type
     *            void
     */
    public void payNotify(HttpServletRequest request, HttpServletResponse response);

    /**
     * 支付完成同步回调方法
     * 
     * @Title payReturn
     * @author 吕凯
     * @date 2022年4月26日 下午12:18:16
     * @param request
     * @param response
     * @param type
     * @return Pair<Result,Map<String,String>>
     */
    public Pair<Result, Map<String, String>> payReturn(HttpServletRequest request, HttpServletResponse response);

    /**
     * 退款
     * 
     * @Title refund
     * @author 吕凯
     * @date 2022年4月26日 下午12:17:36
     * @param outTradeNo
     *            业务订单号（自己项目内实现的订单号）
     * @param transactionId
     *            渠道订单号（支付宝、微信的订单号）
     * @param monny
     *            退款钱数
     * @return String
     */
    public Result refund(HttpServletRequest request, HttpServletResponse response, String outTradeNo, String transactionId, Double monny);

    // 异步通知路径，/alipay/pay/notify ,/wxpay/pay/notify
    public default String getNotifyUrl() {
        return "/paycallback/" + type().getKeyword().concat(PropertiesModel.APPLICATION.getString("pay.notify_url", "/pay/notify"));
    }

    // 同步通知路径，/alipay/pay/return_url ,/wxpay/pay/return_url
    public default String getReturnUrl() {
        return "/paycallback/" + type().getKeyword().concat(PropertiesModel.APPLICATION.getString("pay.notify_url", "/pay/return_url"));
    }

}
