package com.ld.userpay.service.handler;

import com.ld.shieldsb.common.core.ServiceFactory;
import com.ld.userpay.service.handler.impl.WxPayHandler;
import com.ld.userpay.util.pay.PayUtil.PayType;

public class PayHandlerServiceFactory extends ServiceFactory<IPayHandler> {

    static {
        // 注意工厂类不要一开始就注册对象，否则可能缺少必要的jar包不能实例化报错，应该注册类
        getInstance().registerServiceClass(PayType.ALIPAY.getKeyword(), WxPayHandler.class); // 支付
        getInstance().registerServiceClass(PayType.WXPAY.getKeyword(), WxPayHandler.class); // 微信
    }

    public static ServiceFactory<IPayHandler> getInstance() {
        return getInstance(PayHandlerServiceFactory.class);
    }

    /**
     * 移除处理器
     * 
     * @Title removeHandler
     * @author 吕凯
     * @date 2021年4月25日 上午10:50:34
     * @param key
     * @return boolean
     */
    public boolean removeHandler(String key) {
        return removeService(key);
    }

    /**
     * 添加处理器
     * 
     * @Title addHandler
     * @author 吕凯
     * @date 2021年4月25日 上午10:52:12
     * @param key
     * @param clses
     *            void
     */
    public void addHandler(String key, Class<? extends IPayHandler> clses) {
        registerServiceClass(key, clses);
    }

}
