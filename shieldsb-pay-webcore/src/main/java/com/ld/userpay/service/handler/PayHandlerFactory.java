package com.ld.userpay.service.handler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ld.shieldsb.common.core.collections.ListUtils;
import com.ld.userpay.util.pay.PayUtil.PayType;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PayHandlerFactory {
    // 使用 Spring 的依赖注入特性，可以注入一个接口的多个实现，这里采用 List<IEventBusMessageHandler> 的形式注入，Spring 也支持通过 Map<String,IEventBusMessageHandler> 的形式注入，如果使用
    // Map 注入，那么key就是类名
    @Autowired
    private List<IPayHandler> handlerList;
    private boolean checked = false; // 是否检查过

    /**
     * 获取处理器
     * 
     * @Title getHandler
     * @author 吕凯
     * @date 2021年5月24日 下午1:38:27
     * @param type
     * @return IEventBusMessageHandler
     */
    public IPayHandler getHandler(String type) throws NoHandlerException {
        Optional<IPayHandler> groupSelectOptional = handlerList.stream().filter(t -> t.type().getKeyword().equals(type)).findAny();
        return groupSelectOptional.orElseThrow(() -> new NoHandlerException("暂不支持该类型的支付"));
    }

    public void check() {
        if (ListUtils.isNotEmpty(handlerList)) {
            // 方法2.type计数
            Map<PayType, Long> typeCountMap = handlerList.stream().collect(Collectors.groupingBy(IPayHandler::type, Collectors.counting()));
            log.warn("所有的type：" + typeCountMap);
            // 方法3，对重复的type保留计数
            List<Map<String, Long>> repeatTypeList = typeCountMap.keySet().stream().filter(key -> typeCountMap.get(key) > 1).map(key -> {
                Map<String, Long> map = new HashMap<>();
                map.put(key.toString(), typeCountMap.get(key));
                return map;
            }).collect(Collectors.toList());
            if (ListUtils.isNotEmpty(repeatTypeList)) {
                log.warn("\n");
                log.warn("\n");
                log.warn("==================检测到type存在重复！！！！！！！！！！==================重复type及个数：" + repeatTypeList);
                log.warn("\n");
                log.warn("\n");
            }
            checked = true;
        }
    }
}