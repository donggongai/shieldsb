package com.ld.userpay.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ld.shieldsb.common.core.model.Result;
import com.ld.shieldsb.common.core.util.ResultUtil;
import com.ld.shieldsb.common.core.util.StringUtils;
import com.ld.userpay.service.handler.NoHandlerException;
import com.ld.userpay.service.handler.PayHandlerFactory;

import lombok.extern.slf4j.Slf4j;

/**
 * 支付服务类对外提供统一服务
 * 
 * @ClassName PayService
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2022年4月24日 上午11:44:46
 *
 */
@Slf4j
@Service
public class PayService {
    @Autowired
    protected PayHandlerFactory payHandlerFactory;

    /**
     * 获取二维码支付的地址，在前台生成支付二维码
     * 
     * @Title getCodeUrl
     * @author 吕凯
     * @date 2022年4月25日 下午5:55:46
     * @param request
     * @param response
     * @param orderid
     * @param body
     * @param attach
     * @param type
     * @return Object
     */
    public Result getCodeUrl(HttpServletRequest request, HttpServletResponse response, String orderid, String body, String attach,
            String type) {
        // 在页面上显示
        try {
            return payHandlerFactory.getHandler(type).getCodeUrl(request, response, orderid, body, attach);
        } catch (NoHandlerException e) {
            log.warn("", e);
        }
        return ResultUtil.error("生成二维码错误");
    }

    /**
     * 浏览器支付，打开浏览器页面
     * 
     * @Title browserPay
     * @author 吕凯
     * @date 2022年4月24日 下午3:57:41
     * @param request
     * @param response
     * @param orderid
     * @param subject
     * @param body
     *            void
     */
    public void browserPay(HttpServletRequest request, HttpServletResponse response, String orderid, String subject, String body,
            String type) {
        // 头信息
        String userAgent = request.getHeader("user-agent");
        if (StringUtils.isNotBlank(userAgent)) {
            userAgent = userAgent.toLowerCase();
            if (userAgent.indexOf(" mobile") > 0) { // 手机端,这个地方肯定得大于0
                try {
                    payHandlerFactory.getHandler(type).wapPay(request, response, orderid, subject, body);
                } catch (NoHandlerException e) {
                    log.warn("", e);
                }
                return;
            }
        }
        try {
            payHandlerFactory.getHandler(type).pcPay(request, response, orderid, subject, body);
        } catch (NoHandlerException e) {
            log.warn("", e);
        }
    }

    /**
     * 异步通知
     * 
     * @Title payNotify
     * @author 吕凯
     * @date 2022年4月24日 上午11:15:54
     * @param request
     * @param response
     * @param type
     *            void
     */
    public void payNotify(HttpServletRequest request, HttpServletResponse response, String type) {
        try {
            payHandlerFactory.getHandler(type).payNotify(request, response);
        } catch (NoHandlerException e) {
            log.warn("", e);
        }
    }

    /**
     * 同步通知, session可取到支付人的session信息，与异步的处理可以不同
     */
    public Pair<Result, Map<String, String>> payReturn(HttpServletRequest request, HttpServletResponse response, String type) {
        try {
            return payHandlerFactory.getHandler(type).payReturn(request, response);
        } catch (NoHandlerException e) {
            log.warn("", e);
        }
        return Pair.of(ResultUtil.error("无效的支付渠道"), null);
    }

    /**
     * 退款
     * 
     * @Title refund
     * @author 吕凯
     * @date 2022年4月26日 下午4:40:53
     * @param type
     * @param outTradeNo
     * @param transactionId
     * @param monny
     * @return String
     */
    public Result refund(HttpServletRequest request, HttpServletResponse response, String type, String outTradeNo, String transactionId,
            Double monny) {
        try {
            return payHandlerFactory.getHandler(type).refund(request, response, outTradeNo, transactionId, monny);
        } catch (NoHandlerException e) {
            log.warn("", e);
            return ResultUtil.error(e.getMessage());
        }
    }

}
