package com.ld.userpay.service;

import javax.servlet.http.HttpServletRequest;

public interface IDebugChecker {
    public double getDebugMoney();

    public boolean checkPayIsDebug(HttpServletRequest request);

}
