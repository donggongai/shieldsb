package com.ld.userpay.service.handler;

/**
 * 没有匹配的处理器
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2021年5月24日 下午2:13:18
 *
 */
public class NoHandlerException extends Exception {

    /**
     * @Fields serialVersionUID
     */
    private static final long serialVersionUID = -6184902107341097565L;

    public NoHandlerException() {
        super("没有匹配的处理器");
    }

    public NoHandlerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public NoHandlerException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoHandlerException(String message) {
        super(message);
    }

    public NoHandlerException(Throwable cause) {
        super(cause);
    }

}
