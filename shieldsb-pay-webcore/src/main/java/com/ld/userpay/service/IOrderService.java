package com.ld.userpay.service;

import java.util.Date;

import com.ld.shieldsb.common.core.model.Result;
import com.ld.userpay.model.OrderModel;

/**
 * 需要在具体模块中继承实现
 * 
 * @ClassName IChargeService
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2022年4月24日 下午12:16:05
 *
 */
public interface IOrderService {

    /**
     * 获取微信扫码支付生成的支付的地址，在前台生成支付二维码
     */
    public OrderModel getOrderInfo(String orderid);

    /**
     * 更新付费之后的操作,包括更新订单以及其他后续的相关业务操作，需要自己实现
     * 
     * @Title payNotifyCallback
     * @author 吕凯
     * @date 2022年4月23日 上午9:40:03
     * @param outTradeNo
     * @param transactionId
     * @param totalFee
     *            void
     */
    public Result payNotifyCallback(boolean payResult, String outTradeNo, String transactionId, String payType, Double totalFee,
            Date payTime, String ip);

    /**
     * 退款回调
     * 
     * @Title refundCallback
     * @author 吕凯
     * @date 2022年4月27日 上午11:41:18
     * @param refundResult
     * @param outTradeNo
     *            系统订单号
     * @param transactionId
     *            支付订单号
     * @param payType
     *            支付类型
     * @param totalFee
     *            退款钱数
     * @param refundTime
     *            退款时间
     * @return Result
     */
    public Result refundCallback(boolean refundResult, String outTradeNo, String transactionId, String payType, Double totalFee,
            Date refundTime, String ip);

}
