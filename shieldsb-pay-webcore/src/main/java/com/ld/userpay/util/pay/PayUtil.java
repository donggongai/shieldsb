package com.ld.userpay.util.pay;

import java.util.Arrays;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.ld.shieldsb.common.core.collections.ListUtils;
import com.ld.userpay.service.IDebugChecker;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PayUtil { // 业务静态工具类
    private static IDebugChecker debugChecker;

    public static IDebugChecker getDebugCheck() {
        return debugChecker;
    }

    public static void setDebugCheck(IDebugChecker checker) {
        debugChecker = checker;
    }

    /**
     * 支付类型【待整合】
     * 
     * @ClassName PayType
     * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
     * @date 2022年4月23日 下午1:11:50
     *
     */
    public enum PayType {
        ALIPAY("支付宝", "alipay"), WXPAY("微信", "wxpay");

        private String name; // 名称
        private String keyword; //

        private PayType(String name, String keyword) {
            this.name = name;
            this.keyword = keyword;
        }

        public String getName() {
            return name;
        }

        public String getKeyword() {
            return keyword;
        }

    }

    /**
     * 获取支付类型Map，key为值，value为名称，enum转位value和name的键值对map
     * 
     * @Title getPayTypes
     * @author 吕凯
     * @date 2022年4月22日 上午9:43:16
     * @return Map<Integer,String>
     */
    public static Map<String, String> getPayTypes() {
        return ListUtils.listField2Map(Arrays.asList(PayType.values()), PayType::getKeyword, PayType::getName);
        // 等同于下面代码
        // return Arrays.stream(PayType.values()).collect(Collectors.toMap(PayType::getValue, PayType::getName));
    }

    /**
     * 获取实际支付钱数，如果为调试模式则返回调试的钱数
     * 
     * @Title getPayMoney
     * @author 吕凯
     * @date 2022年4月22日 上午10:28:50
     * @param money
     * @param type
     * @param request
     * @return Double
     */
    public static Double getPayMoney(Double money, HttpServletRequest request) {
        IDebugChecker debugChecker = getDebugCheck();
        if (debugChecker != null && debugChecker.checkPayIsDebug(request)) {
            return debugChecker.getDebugMoney();
        }
        return money;
    }

    public static void main(String[] args) {
        // System.out.println(BussinessUtil.getAllBussinessType());
    }
}
