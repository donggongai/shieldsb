package com.ld.shieldsb.mvc.checker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import com.ld.shieldsb.common.core.reflect.ClassUtil;
import com.ld.shieldsb.common.core.validate.IDCardChecker;
import com.ld.shieldsb.mvc.checker.model.CheckerModel;

/**
 * 
 * 后台Validate(验证)相关
 * 
 * @ClassName FormChecker
 * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
 * @date 2016年8月25日 下午2:22:43
 *
 */
public abstract class FormChecker {
    Pattern patternNumber = Pattern.compile("^[-+]?(([0-9]+)([.]([0-9]+))?|([.]([0-9]+))?)$"); // 数字
    Pattern patternEmail = Pattern.compile("^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$"); // 邮箱

    private List<CheckerModel> checkList = new ArrayList<CheckerModel>();
    protected HttpServletRequest request;

    public FormChecker() {
    }

    /**
     * 
     * 初始化
     * 
     * @Title init
     * @param request
     *            void
     */
    public void init(HttpServletRequest request) {
        this.request = request;
    }

    public abstract void setCheck();

    // 是否为空
    protected void addCheckEmpty(String key, String showMessage) {
        CheckerModel km = new CheckerModel(key, "notnull", showMessage);
        checkList.add(km);
    }

    // 检测最大长度
    protected void addCheckMaxLen(String key, String showMesage, int maxlen) {
        CheckerModel km = new CheckerModel(key, "maxlen", showMesage, maxlen);
        checkList.add(km);
    }

    // 检测长度范围
    protected void addCheckRangeLen(String key, String showMesage, int minlen, int maxlen) {
        CheckerModel km = new CheckerModel(key, "rangelen", showMesage, minlen, maxlen);
        checkList.add(km);
    }

    // 检测是否是数字
    protected void addCheckIsNum(String key, String showMesage) {
        CheckerModel km = new CheckerModel(key, "isnum", showMesage);
        checkList.add(km);
    }

    // 检测是否是有效的电子邮箱
    protected void addCheckIsEamil(String key, String showMesage) {
        CheckerModel km = new CheckerModel(key, "email", showMesage);
        checkList.add(km);
    }

    // 检测是否是有效的身份证号
    protected void addCheckIDCard(String key, String showMesage) {
        CheckerModel km = new CheckerModel(key, "IDCard", showMesage);
        checkList.add(km);
    }

    /**
     * 获取字符串的长度，如果有中文，则每个中文字符计为2位
     * 
     * @param value
     *            指定的字符串
     * @return 字符串的长度
     */
    public static int length(String value) {
        value = value.trim();
        int valueLength = 0;
        String chinese = "[\u0391-\uFFE5]";
        /* 获取字段值的长度，如果含中文字符，则每个中文字符长度为2，否则为1 */
        for (int i = 0; i < value.length(); i++) {
            /* 获取一个字符 */
            String temp = value.substring(i, i + 1);
            /* 判断是否为中文字符 */
            if (temp.matches(chinese)) {
                /* 中文字符长度为2 */
                valueLength += 2;
            } else {
                /* 其他字符长度为1 */
                valueLength += 1;
            }
        }
        return valueLength;
    }

    /**
     * 
     * 获取错误字符串
     * 
     * @Title getErrorStr
     * @return
     * @throws ServletException
     * @throws IOException
     *             String
     */
    public String getErrorStr() throws ServletException, IOException {
        for (CheckerModel checkModel : checkList) {
            String key = checkModel.getKey();
            String value = checkModel.getValue();
            String showMessage = checkModel.getShowMessage();
            String param = request.getParameter(key);
            if (param == null || param.isEmpty()) {
                if (value.equals("notnull")) {
                    return showMessage + "不能为空";
                }
            } else {
                if (value.equals("maxlen")) {
                    int maxlen = checkModel.getMaxlen();
                    int intmaxlen = ClassUtil.obj2int(length(param));
                    if (intmaxlen > maxlen) {
                        return showMessage + "超出最大长度" + maxlen;
                    }
                } else if (value.equals("rangelen")) {
                    int minlen = checkModel.getMinlen();
                    int maxlen = checkModel.getMaxlen();
                    int intmaxlen = ClassUtil.obj2int(length(param));
                    if (intmaxlen > maxlen || intmaxlen < minlen) {
                        return showMessage + "最少" + minlen + " 最多 " + maxlen;
                    }
                } else if (value.equals("isnum")) {
                    if (!patternNumber.matcher(param).matches()) {
                        return showMessage + "不是数字";
                    }
                } else if (value.equals("email")) {
                    if (!patternEmail.matcher(param).matches()) {
                        return showMessage + "不是有效的电子邮箱";
                    }
                } else if (value.equals("IDCard")) {
                    return IDCardChecker.validate(param).getMessage();
                }
            }
        }
        return null;
    }
}
