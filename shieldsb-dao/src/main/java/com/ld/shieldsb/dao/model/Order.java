package com.ld.shieldsb.dao.model;

import lombok.Data;

/**
 * 排序字段
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2020年6月2日 下午4:15:11
 *
 */
@Data
public class Order {
    private String column; // 列名称
    private Boolean isDesc; // 是否倒序

    public Order() {
    }

    public Order(String column, Boolean isDesc) {
        super();
        this.column = column;
        this.isDesc = isDesc;
    }

}
