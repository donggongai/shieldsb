package com.ld.shieldsb.dao.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.ld.shieldsb.common.core.model.PropertiesModel;
import com.ld.shieldsb.common.core.util.StackTraceUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LogUtil {

    static {
        String out = PropertiesModel.CONFIG.getString("log4j.out", "false");
        String outDao = PropertiesModel.CONFIG.getString("log4j.out.dao", "false");
        String outDaoExclude = PropertiesModel.CONFIG.getString("log4j.out.dao.exclude", "");
        String outDaoTrace = PropertiesModel.CONFIG.getString("log4j.out.dao.trace", "false");
        String outAction = PropertiesModel.CONFIG.getString("log4j.out.action", "false");
        String outActionTrace = PropertiesModel.CONFIG.getString("log4j.out.action.trace", "false");
        String outActionExclude = PropertiesModel.CONFIG.getString("log4j.out.action.exclude", "");
        String jspAction = PropertiesModel.CONFIG.getString("log4j.out.jsp", "false");
        log.debug("检查调试输出日志工具参数（config.properties文件，修改请到此文件）");
        log.debug("调试输出日志工具" + (out.equals("true") ? "开启" : "关闭") + ",如需改动请修改log4j.out参数");
        log.debug("调试输出日志工具Dao输出" + (outDao.equals("true") ? "开启" : "关闭") + ",如需改动请修改log4j.out.dao参数");
        log.debug("调试输出日志工具Dao的trace输出" + (outDaoTrace.equals("true") ? "开启" : "关闭") + ",如需改动请修改log4j.out.dao.trace参数");
        log.debug("调试输出日志工具Dao输出的排除类设置：" + outDaoExclude + ",如需改动请修改log4j.out.dao.exclude参数");
        log.debug("调试输出日志工具Action输出" + (outAction.equals("true") ? "开启" : "关闭") + ",如需改动请修改log4j.out.action参数");
        log.debug("调试输出日志工具Action的trace输出" + (outActionTrace.equals("true") ? "开启" : "关闭") + ",如需改动请修改log4j.out.action.trace参数");
        log.debug("调试输出日志工具Action输出的排除类设置：" + outActionExclude + ",如需改动请修改log4j.out.action.exclude参数");
        log.debug("调试输出日志工具Jsp输出" + (jspAction.equals("true") ? "开启" : "关闭") + ",如需改动请修改log4j.out.jsp参数");
    }

    /**
     * DAO输出
     * 
     * @Title daoDebug
     * @author 吕凯
     * @date 2016年10月13日 上午8:28:07
     * @param msg
     *            void
     */
    public static void daoDebug(String msg) {
        daoDebug(msg, 2); // 0为daoDebug(String msg, int traceEleIndex)1为本方法，从2开始为调用者信息
    }

    public static void daoDebug(String msg, int traceEleIndex) {
        String out = PropertiesModel.CONFIG.getString("log4j.out", "false");
        String outDao = PropertiesModel.CONFIG.getString("log4j.out.dao", "false");
        if (out.equals("true") && outDao.equals("true")) {
            String trace = StackTraceUtil.getTrace();
            String exclude = PropertiesModel.CONFIG.getString("log4j.out.dao.exclude", "");
            Pattern regex = Pattern.compile(".*?(" + exclude + ")", Pattern.DOTALL);// 共(\\d.)页
            Matcher regexMatcher = regex.matcher(trace);
            if (StringUtils.isBlank(exclude) || !regexMatcher.find()) { // 非排除的
                String outDaoTrace = PropertiesModel.CONFIG.getString("log4j.out.dao.trace", "false");
                if (outDaoTrace.equals("true")) {
                    msg = trace + "\n" + msg;
                }

                log.debug("dao调用输出：" + msg + " 调用者： " + StackTraceUtil.getTraceEle(traceEleIndex));
            }
        }
    }

    public static void main(String[] args) {
        String ss = "called by {" + " com.ld.log.LogUtil.debug(LogUtil.java:14)" + " com.dao.BaseDao.getList(BaseDao.java:356)"
                + " com.dao.analysis.back.AccessUrllimitDao.getAllURLSets(AccessUrllimitDao.java:34)"
                + " com.service.task.BackAcctimeListTask$1.run(BackAcctimeListTask.java:72)";
        String exclude = ".*?(com.dao.analysis.back.AccessUrllimitDao.*|)";
        System.out.println(ss.matches(exclude));
    }

}
