package com.ld.shieldsb.dao.model;

import java.lang.reflect.Field;

import com.alibaba.druid.sql.ast.statement.SQLJoinTableSource.JoinType;

import lombok.Data;

@Data
public class JoinLinkedTableModel {
    private Field field;
    private String tableName; // 表名
    private String tableAlias; // 表别名
    private QueryModel queryModel;

    private String linkfromTable; // 连哪个表
    private String linkfromCulumn; // 连xx表的哪个字段

    private String linkThisCulumn; // 连本表的哪个字段

    private JoinType joinType = JoinType.JOIN; // 连接类型

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }

        JoinLinkedTableModel linked = (JoinLinkedTableModel) obj;
        if (tableAlias == null && linked.getTableAlias() == null) {
            return true;
        }
        if (tableAlias != null) {
            return tableAlias.equals(linked.getTableAlias()); // 别名不能重复
        }
        return false;
    }

}
