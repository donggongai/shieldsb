package com.ld.shieldsb.dao.exception;

/**
 * 
 * 格式化sql错误（继承自Exception，运行时抛出强制捕获）
 * 
 * @ClassName SqlFormatException
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2022年1月13日 上午8:55:18
 *
 */
public class SqlFormatException extends Exception {

    /**
     * @Fields serialVersionUID
     */
    private static final long serialVersionUID = -1714203956986661668L;

    /*无参构造函数*/
    public SqlFormatException() {
        super();
    }

    // 用详细信息指定一个异常
    public SqlFormatException(String message) {
        super(message);
    }

    // 用指定的详细信息和原因构造一个新的异常
    public SqlFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    // 用指定原因构造一个新的异常
    public SqlFormatException(Throwable cause) {
        super(cause);
    }

}
