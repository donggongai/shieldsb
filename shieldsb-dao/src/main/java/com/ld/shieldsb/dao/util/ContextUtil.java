package com.ld.shieldsb.dao.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.ld.shieldsb.annotation.mask.Masker;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 全局工具类
 * 
 * @ClassName ContextUtil
 * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
 * @date 2016年8月26日 上午11:36:07
 *
 */
@Slf4j
public class ContextUtil {

    /*  脱敏器 */
    private final static Map<String, Masker> MASKER_MAP = new ConcurrentHashMap<>();

    /**
     * 根据类获取脱敏器
     * 
     * @Title getMasker
     * @author 吕凯
     * @date 2018年9月21日 上午9:38:21
     * @param miClass
     * @return Masker
     */
    public static Masker getMasker(Class<? extends Masker> miClass) {
        Masker maker = null;
        if (MASKER_MAP != null) {
            String miKey = miClass.getName();
            maker = getMasker(miKey);
            if (maker == null) {
                if (Masker.class.isAssignableFrom(miClass)) {
                    maker = addMasker((Class<? extends Masker>) miClass);
                    if (maker != null) {
                        log.error("懒加载拦截器：：" + miKey);
                    }
                    return maker;
                } else {
                    log.error("拦截器不存在：：" + miKey);
                }
            }
        }
        return maker;
    }

    /**
     * 添加脱敏器
     * 
     * @Title addMasker
     * @author 吕凯
     * @date 2018年9月21日 上午9:37:19
     * @param miClass
     * @return Masker
     */
    private static Masker addMasker(Class<? extends Masker> miClass) {
        if (!miClass.isInterface()) { // 不是接口
            try {
                String miKey = miClass.getName();
                if (MASKER_MAP.containsKey(miKey)) {
                    log.warn("脱敏器重名：" + miKey);
                    log.warn("【重复】class:" + miClass.getName());
                    Masker tempModel = MASKER_MAP.get(miKey);
                    log.warn("【重复】class:" + tempModel.getClass().getName());
                } else {
                    Masker miObj = miClass.newInstance();
                    MASKER_MAP.put(miKey, miObj);
                    return miObj;
                }
            } catch (Exception e) {
                log.warn("加载脱敏器出错", e);
            }
        }
        return null;
    }

    /**
     * 根据拦截器的key获取脱敏器（没有则直接返回null）
     * 
     * @Title getMasker
     * @author 吕凯
     * @date 2018年9月21日 上午9:35:39
     * @param miKey
     * @return Masker
     */
    public static Masker getMasker(String miKey) {
        if (MASKER_MAP != null) {
            return MASKER_MAP.get(miKey);
        }
        return null;
    }

    public static class Set {
        /**
         * * 是否忽略请求url的大小写
         */
        public static final boolean IGNORE_URL_CASE = false;
        /**
         * * 数据脱敏
         */
        public static boolean MASK = false;

    }
}
