package com.ld.shieldsb.dao.util;

import java.util.List;

import com.alibaba.druid.sql.SQLUtils;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.parser.ParserException;
import com.alibaba.druid.sql.parser.SQLParserUtils;
import com.alibaba.druid.sql.parser.SQLStatementParser;
import com.ld.shieldsb.dao.exception.SqlFormatException;

public class SqlUtil {
    /**
     * 格式化mysql的sql语句，可用于sql校验
     * 
     * @Title formatMysql
     * @author 吕凯
     * @date 2022年1月13日 上午8:49:50
     * @param sql
     * @return String
     * @throws SqlFormatException
     */
    public static String formatMysql(String sql) throws SqlFormatException {
        return format(sql, "mysql");
    }

    /**
     * 格式化oracle的sql语句，可以用于sql校验
     * 
     * @Title formatOracle
     * @author 吕凯
     * @date 2022年1月13日 上午8:50:22
     * @param sql
     * @return String
     * @throws SqlFormatException
     */
    public static String formatOracle(String sql) throws SqlFormatException {
        return format(sql, "oracle");
    }

    /**
     * 格式化指定数据库的sql语句，可用于sql校验
     * 
     * @Title format
     * @author 吕凯
     * @date 2022年1月13日 上午8:50:41
     * @param sql
     * @param type
     * @return String
     * @throws SqlFormatException
     */
    public static String format(String sql, String type) throws SqlFormatException {
        List<SQLStatement> statementList = null;
        SQLStatementParser parser = null;
        try {
            parser = SQLParserUtils.createSQLStatementParser(sql, type);
            statementList = parser.parseStatementList();
        } catch (ParserException e) {
            throw new SqlFormatException(e.getMessage());
        }
        return SQLUtils.toSQLString(statementList, type);
    }

}
