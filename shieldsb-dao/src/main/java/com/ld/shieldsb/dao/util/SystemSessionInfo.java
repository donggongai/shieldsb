package com.ld.shieldsb.dao.util;

/**
 * 系统session 信息
 *
 */
public final class SystemSessionInfo {
    private static SessionUser sessionUser;

    private SystemSessionInfo() {
        throw new AssertionError("No SystemSessionInfo instances for you!");
    }

    /**
     * @param sessionUser
     *            接口
     */
    public static void setSessionUser(SessionUser sessionUser) {
        SystemSessionInfo.sessionUser = sessionUser;
    }

    /**
     * 获取当前操作session 用户名
     *
     * @return 用户名
     */
    public static String getUserName() {
        if (sessionUser == null) {
            return "";
        }
        return sessionUser.getUserName();
    }

    /**
     * 获取当前操作session 用户id
     *
     * @return id
     */
    public static Long getUserId() {
        if (sessionUser == null) {
            return -1L;
        }
        return sessionUser.getUserId();
    }

    public static String userIdGetName(int userId) {
        if (sessionUser == null) {
            return "";
        }
        return sessionUser.userIdGetName(userId);
    }

    /**
     * 获取session 信息接口
     *
     */
    public interface SessionUser {

        String getUserName();

        Long getUserId();

        String userIdGetName(int userId);
    }
}
