package com.ld.shieldsb.dao;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.ld.shieldsb.common.core.model.PropertiesModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DaoParams {
    // 采用线程安全的类
    private static Map<String, Boolean> DATA_SOURCE_MAP = new ConcurrentHashMap<String, Boolean>();

    public static Boolean getCanSave(String key) {
        Boolean canSave = readProperty(key, "canSave", true);
        return canSave;
    }

    public static Boolean getCanRead(String key) {
        Boolean canRead = readProperty(key, "canRead", true);
        return canRead;
    }

    /**
     * 获取参数
     * 
     * @Title readProperty
     * @author 吕凯
     * @date 2017年5月19日 下午2:46:36
     * @param key
     *            数据源前缀 db0_
     * @param propertyName
     *            参数名
     * @param defaultVal
     *            默认值
     * @return Boolean
     */
    private static Boolean readProperty(String key, String propertyName, boolean defaultVal) {
        Boolean canRead = DATA_SOURCE_MAP.get(key);
        if (canRead == null) {
            synchronized (DATA_SOURCE_MAP) {
                canRead = DATA_SOURCE_MAP.get(key);
                if (canRead == null) {
                    String canSaveStr = PropertiesModel.CONFIG.getString(key + propertyName, defaultVal + "");
                    try {
                        canRead = Boolean.parseBoolean(canSaveStr);
                    } catch (Exception e) {
                    }
                    DATA_SOURCE_MAP.put(key, canRead);
                }
            }
            log.warn("加载数据库参数，key:" + key + "，" + propertyName + ":" + canRead);
        }
        return canRead;
    }

}
