package com.ld.shieldsb.dao;

import java.util.List;
import java.util.Map;

import com.ld.shieldsb.common.core.reflect.FunctionUtil;
import com.ld.shieldsb.dao.model.PageNavigationBean;
import com.ld.shieldsb.dao.model.QueryModel;

/**
 * 此类为了兼容MySQL和Oracle而新增的类，具体实现都在子类中。目前包括MySqlBaseDao和OracleBaseDao
 *
 * @author 张和祥
 * @date 2020/3/19 14:28
 * @version 1.0
 */
public abstract class TransactionImplBaseDao extends TransactionDao {

    public TransactionImplBaseDao(String key) {
        super(key);
    }

    public <T> T findById(Class<T> classOfT, Object id) {
        return findById(classOfT, "id", id);
    }

    public <T> List<T> getListBySql(Class<T> classOfT, String sql, Object... params) {
        return getList(classOfT, sql, params);
    }

    public abstract <T> Map<String, Object> getMap(Class<T> classOfT, QueryModel queryModel);

    public Map<String, Object> getMapSql(String sql, Object... params) {
        return getMap(sql, params);
    }

    public List<Map<String, Object>> getListMapSql(String sql, Object... params) {
        return getMapList(sql, params);
    }

    public abstract <T> List<T> getList(Class<T> classOfT, QueryModel queryModel, int size);

    public abstract <T> PageNavigationBean<T> getPageNavigationBean(Class<T> classOfT, QueryModel queryModel, int pageNum, int pageSize);

    public abstract <T> PageNavigationBean<Map<String, Object>> getPageNavigationMap(Class<T> classOfT, QueryModel queryModel, int pageNum,
            int pageSize);

    public abstract <T> T findById(Class<T> classOfT, String key, Object value);

    public abstract <T> T getOne(Class<T> classOfT, QueryModel queryModel);

    public abstract <T> boolean exists(Class<T> classOfT, String key, Object value);

    public abstract <T> TransactionImplBaseDao initJoin(Class<T> classOfT, QueryModel queryModel);

    public abstract <T> TransactionImplBaseDao join(FunctionUtil.Property<T, ?> property, QueryModel queryModel) throws Exception;

    public abstract <T> TransactionImplBaseDao join(FunctionUtil.Property<T, ?> property, QueryModel middleQueryModel,
            QueryModel queryModel) throws Exception;

    public abstract <T> PageNavigationBean<T> getPageNavigationBeanByJoin(int pageNum, int pageSize) throws Exception;

    public abstract <T> List<T> getListByJoin() throws Exception;
}
