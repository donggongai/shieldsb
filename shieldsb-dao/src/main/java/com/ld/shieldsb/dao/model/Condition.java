package com.ld.shieldsb.dao.model;

import lombok.Data;

/**
 * 查询条件
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2020年6月2日 下午6:12:35
 *
 */
@Data
public class Condition {
    private String column; // 列名称
    private Type queryType; // 查询类型
    private Object values; // 值
    private Object otherValues; // IN_STR或SQL_STR中的参数

    public Condition() {
    }

    public Condition(String column, Type queryType, Object values) {
        super();
        this.column = column;
        this.queryType = queryType;
        this.values = values;
    }

    public Condition(String column, Type queryType, Object values, Object otherValues) {
        super();
        this.column = column;
        this.queryType = queryType;
        this.values = values;
        this.otherValues = otherValues;
    }

    public static enum Type {
        /** 模糊查询 */
        LIKE,
        /** 精确查询 */
        EQUAL,
        /** 左模糊，左边一样，如查询a abc符合bca不符合 */
        LIKE_LEFT,
        /** 右模糊，右边一样，如查询a bac符合abc不符合 */
        LIKE_RIGHT,
        /** 右模糊 */
        NOT_EQUAL,
        /** 大于 */
        GT,
        /** 大于等于 */
        GE,
        /** 小于 */
        LT,
        /** 小于等于 */
        LE,
        /** 范围 */
        BETWEEN,
        /** in ()sql字符串 */
        IN_STR,
        /** not in sql字符串 */
        NOT_IN_STR,
        /** in set */
        IN_SET,
        /** not in set */
        NOT_IN_SET/*不在set中*/,
        /** exists */
        EXISTS,
        /** not exists */
        NOT_EXISTS,
        /** sql语句 */
        SQL_STR
    }

}
