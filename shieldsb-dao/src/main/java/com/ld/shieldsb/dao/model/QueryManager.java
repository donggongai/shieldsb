package com.ld.shieldsb.dao.model;

import javax.management.Query;

import com.ld.shieldsb.annotation.util.AnnotationUtil;
import com.ld.shieldsb.common.core.model.Description;
import com.ld.shieldsb.common.core.reflect.ClassUtil;
import com.ld.shieldsb.common.core.reflect.FunctionUtil;
import com.ld.shieldsb.common.core.reflect.FunctionUtil.Property;

public class QueryManager {
    public static final boolean QUERY_LAMBDAS_SUPPORT = FunctionUtil.QUERY_LAMBDAS_SUPPORT; // 是否是java8以上，lambda表达式需要java8以上

    public static <T> LambdaQuery<T> lambdaQuery(Class<T> cls) {
        if (QUERY_LAMBDAS_SUPPORT) {
            return new LambdaQuery<>();
        } else {
            throw new UnsupportedOperationException("需要Java8以上");
        }
    }

    public static <T> Query query() {
        return new Query();
    }

    /**
     * 获取Function的名称，有ColumnName注解的会自动匹配数据库对应字段
     * 
     * @Title getFunctionName
     * @author 吕凯
     * @date 2019年7月1日 上午11:10:44
     * @param property
     * @return String
     */
    public static <T> String getFunctionName(Property<T, ?> property) {
        String[] resultArrs = FunctionUtil.getFunctionClassMethod(property).split(" ");
        // 获取数据库的
        Description fieldDesp = AnnotationUtil.getFieldDescription(ClassUtil.getClass(resultArrs[0]),
                FunctionUtil.getFieldNameByMethodName(resultArrs[1]));
        return fieldDesp.getDbName();

    }

    public static void main(String[] args) {
        getFunctionName(QueryModel::getCnds);
    }

}
