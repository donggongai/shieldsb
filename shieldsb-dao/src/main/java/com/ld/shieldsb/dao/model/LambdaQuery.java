package com.ld.shieldsb.dao.model;

public class LambdaQuery<T> extends QueryModel {

    public static void main(String[] args) {
        LambdaQuery<QueryModel> queryModel = QueryManager.lambdaQuery(QueryModel.class);
        queryModel.addEq(QueryModel::getJoinType, "123"); //
    }

}
