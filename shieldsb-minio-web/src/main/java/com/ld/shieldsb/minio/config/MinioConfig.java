package com.ld.shieldsb.minio.config;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.ld.shieldsb.common.core.model.PropertiesModel;

import io.minio.MinioClient;
import lombok.Data;

@Data
@Component
public class MinioConfig {

    @Bean
    public MinioClient getMinioClient() {
        String endpoint = PropertiesModel.CONFIG.getString("minio.endpoint", "http://127.0.0.1");
        int port = PropertiesModel.CONFIG.getInt("minio.port", 9000);
        String accessKey = PropertiesModel.CONFIG.getString("minio.accessKey", "minioadmin");
        String secretKey = PropertiesModel.CONFIG.getString("minio.secretKey", "minioadmin");
        Boolean secure = PropertiesModel.CONFIG.getBoolean("minio.secure", false);
        MinioClient minioClient = MinioClient.builder().credentials(accessKey, secretKey).endpoint(endpoint, port, secure).build();
        return minioClient;
    }
}