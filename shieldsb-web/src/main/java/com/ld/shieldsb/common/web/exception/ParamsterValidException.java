package com.ld.shieldsb.common.web.exception;

/**
 * 参数非法异常（继承自RuntimeException，运行时抛出不强制捕获）
 * 
 * @ClassName ParamsterValidException
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2019年8月22日 下午2:14:30
 *
 */
public class ParamsterValidException extends RuntimeException {

    /**
     * @Fields serialVersionUID
     */
    private static final long serialVersionUID = -1714203956986661668L;

    /*无参构造函数*/
    public ParamsterValidException() {
        super();
    }

    // 用详细信息指定一个异常
    public ParamsterValidException(String message) {
        super(message);
    }

    // 用指定的详细信息和原因构造一个新的异常
    public ParamsterValidException(String message, Throwable cause) {
        super(message, cause);
    }

    // 用指定原因构造一个新的异常
    public ParamsterValidException(Throwable cause) {
        super(cause);
    }

}
