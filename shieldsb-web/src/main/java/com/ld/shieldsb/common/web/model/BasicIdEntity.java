package com.ld.shieldsb.common.web.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 带id的基本类
 * 
 * @ClassName SysArea
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2018年7月3日 下午2:13:54
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class BasicIdEntity extends BasicEntity {

    private static final long serialVersionUID = 3393951536897604671L;
    // id
    private Long id;

    public BasicIdEntity() {
    }

}
