package com.ld.shieldsb.common.web.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * * 属性注解相关
 * 
 * @ClassName Annotation
 * @author <a href="mailto:donggongai@126.com" target="_blank">kevin</a>
 * @date 2016年8月26日 上午10:50:17
 *
 */
public class DB {
    /**
     * 数据源
     * 
     * @ClassName DataSource
     * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
     * @date 2019年3月12日 上午8:43:35
     *
     */
    @Inherited // 可继承
    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface DataSource {
        // 数据源前缀名称
        public String value() default "db0.";
    }

}
