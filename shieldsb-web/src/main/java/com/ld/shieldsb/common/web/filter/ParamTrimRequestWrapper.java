package com.ld.shieldsb.common.web.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import com.ld.shieldsb.common.core.util.StringUtils;

/**
 * 过滤参数前后空格的过滤器
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2020年7月7日 下午2:33:47
 *
 */
public class ParamTrimRequestWrapper extends HttpServletRequestWrapper {

    private HttpServletRequest orgRequest = null;
    private String richTextNameRegex = null; // 富文本编辑器的name名称的正则表达式

    public ParamTrimRequestWrapper(HttpServletRequest request, String richTextName) {
        super(request);
        orgRequest = request;
        this.richTextNameRegex = richTextName;
    }

    /**
     * 覆盖getParameter方法，将参数值做trim过滤。<br/>
     * 如果需要获得原始的值，则通过super.getParameterValues(name)来获取<br/>
     */
    @Override
    public String getParameter(String name) {
        String value = super.getParameter(name);
        value = trim(value, isIgnoreParamName(name));
        return value;
    }

    /**
     * 
     * 根据参数名判断是否是忽略字段
     * 
     * @Title isIgnoreParamName
     * @author 吕凯
     * @date 2020年7月7日 下午2:28:24
     * @param name
     * @return boolean
     */
    private boolean isIgnoreParamName(String name) {
        if (name == null) {
            return false;
        }
        return StringUtils.isNotBlank(richTextNameRegex) && name.matches(richTextNameRegex);
    }

    @Override
    public String[] getParameterValues(String name) {
        String[] arr = super.getParameterValues(name);
        if (arr != null) {
            for (int i = 0; i < arr.length; i++) {
                arr[i] = trim(arr[i], isIgnoreParamName(name));
            }
        }
        return arr;
    }

    /**
     * 覆盖getHeader方法，将参数值做trim过滤。<br/>
     * 如果需要获得原始的值，则通过super.getHeaders(name)来获取<br/>
     * getHeaderNames 也可能需要覆盖
     */
    @Override
    public String getHeader(String name) {
        String value = super.getHeader(name);
        value = trim(value, false);
        return value;
    }

    /**
     * 获取最原始的request
     * 
     * @return
     */
    public HttpServletRequest getOrgRequest() {
        return orgRequest;
    }

    /**
     * 清除xss
     * 
     * @Title cleanXSS
     * @author 吕凯
     * @date 2018年11月24日 上午11:36:46
     * @param value
     * @param isRichText
     *            是否为富文本（保留部分html标签）
     * @return String
     */
    private String trim(String value, boolean isRichText) {
        if (StringUtils.isNotBlank(value) && !isRichText) { // 富文本编辑器需要保留部分html标签
            value = value.trim();
        }
        return value;

    }

}