package com.ld.shieldsb.common.web.util.params;

import com.ld.shieldsb.dao.model.QueryModel;

import lombok.Data;

@Data
public class ParamsHandleResult {

    private String paramCondition; // 参数处理方法前缀，如equal_,like_
    private String paramName; // 参数名，不带前缀
    private Object paramValue; // 参数值
    private QueryModel queryModel; // 处理后的QueryModel

}
