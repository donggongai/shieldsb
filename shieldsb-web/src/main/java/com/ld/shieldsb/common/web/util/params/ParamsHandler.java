package com.ld.shieldsb.common.web.util.params;

import com.ld.shieldsb.dao.model.QueryModel;

// 函数式接口
@FunctionalInterface
public interface ParamsHandler {
    /**
     * 处理参数
     * 
     * @Title deal
     * @author 吕凯
     * @date 2019年7月26日 上午8:03:23
     * @param paramName
     * @param paramValue
     *            一定不为空，但考虑可能性极小的特殊情况未进行trim处理
     * @param queryModel
     * @return QueryModel
     */
    public ParamsHandleResult deal(String paramName, String paramValue, QueryModel queryModel);
}
