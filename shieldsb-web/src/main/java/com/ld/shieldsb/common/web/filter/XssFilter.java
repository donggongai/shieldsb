package com.ld.shieldsb.common.web.filter;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.ld.shieldsb.common.core.util.SimplePathMatcher;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class XssFilter implements Filter {

    FilterConfig filterConfig = null;

    private List<String> urlExclusion = null; // 排除不过滤的的url
    protected SimplePathMatcher matcherSimple = new SimplePathMatcher(); // 简单过滤器
    private String richTextNameRegex = "html\\_.+"; // 富文本编辑器的字段name名称的正则表达式
    private String ignoreParamNameRegex = "password"; // 不处理的字段name名称的正则表达式

    public XssFilter() {
        super();
    }

    public XssFilter(String richTextNameRegex) {
        super();
        this.richTextNameRegex = richTextNameRegex;
    }

    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    public void destroy() {
        this.filterConfig = null;
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String servletPath = httpServletRequest.getServletPath();

        if (urlExclusion != null && isExclusion(servletPath)) {
            chain.doFilter(request, response);
        } else {
            chain.doFilter(new XssRequestWrapper((HttpServletRequest) request, richTextNameRegex, ignoreParamNameRegex), response);
        }
    }

    public boolean isExclusion(String requestURI) {
        if (urlExclusion == null || requestURI == null) {
            return false;
        }
        for (String pattern : urlExclusion) {
            if (matcherSimple.matches(pattern, requestURI)) {
                log.warn("Xss过滤器排除：" + requestURI);
                return true;
            }
        }

        return false;
    }

    public List<String> getUrlExclusion() {
        return urlExclusion;
    }

    public void setUrlExclusion(List<String> urlExclusion) {
        this.urlExclusion = urlExclusion;
    }

    public String getRichTextNameRegex() {
        return richTextNameRegex;
    }

    public void setRichTextNameRegex(String richTextNameRegex) {
        this.richTextNameRegex = richTextNameRegex;
    }

    public String getIgnoreParamNameRegex() {
        return ignoreParamNameRegex;
    }

    public void setIgnoreParamNameRegex(String ignoreParamNameRegex) {
        this.ignoreParamNameRegex = ignoreParamNameRegex;
    }

}