package com.ld.shieldsb.common.composition.util.web;

import com.ld.shieldsb.common.core.io.PropertiesUtils;
import com.ld.shieldsb.common.core.model.PropertiesModel;
import com.ld.shieldsb.common.core.reflect.ClassUtil;
import com.ld.shieldsb.common.core.util.properties.ConfigFile;
import com.ld.shieldsb.common.web.util.Web.Attr;
import com.ld.shieldsb.common.web.util.Web.Parameter;
import com.ld.shieldsb.common.web.util.Web.Property;
import com.ld.shieldsb.common.web.util.Web.Request;
import com.ld.shieldsb.common.web.util.Web.Response;
import com.ld.shieldsb.common.web.util.Web.Util;
import com.ld.shieldsb.common.web.util.server.WebServerUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 全局配置类，读取配置参数（建议项目启动时打印一下项目名称调用该类），常量，session属性，上下文属性等
 * 
 * @ClassName Global
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2018年11月14日 上午11:31:30
 *
 */
@Slf4j
public class Global {

    public static final ConfigFile APPLICATION = PropertiesModel.APPLICATION; // 非特殊情况不建议使用，因为有些默认值可能在shieldsb-core.propertes中
    public static final ConfigFile CONFIG = PropertiesModel.CONFIG; // 建议将可能变动的参数放入此文件中，从该属性获取，PropertiesUtils暂不支持动态获取

    static {
        log.warn("添加配置文件：：" + Global.CONFIG.getFilePath());
        PropertiesUtils.addConfigFile(Global.CONFIG.getFilePath()); // 添加不在默认文件里的配置文件
    }

    // 前台访问地址,简称
    public static final String PROJECT_FRONT_S = getProperty(PROPERTY.CONFIG_FRONTPATH);
    // 后台访问地址,简称
    public static final String PROJECT_ADMIN_S = getProperty(PROPERTY.CONFIG_ADMINPATH);

    // 项目端口(tomcat等外部容器不能直接从配置文件获取)
    protected static int projectPort = ClassUtil.obj2int(getProperty("server.port", "80"));
    // 项目名称(tomcat等外部容器不能直接从配置文件获取)
    protected static String projectName = getProperty("server.servlet.context-path", "");
    // 项目访问基本路径(tomcat等外部容器不能直接从配置文件获取)
    protected static String projectBaseUrl = "http://" + WebServerUtil.getHost() + ":" + projectPort + projectName;
    // 前台访问地址,完整地址(tomcat等外部容器不能直接从配置文件获取)
    protected static String projectFrontUrl = projectBaseUrl + PROJECT_FRONT_S;
    // 后台访问地址,完整地址(tomcat等外部容器不能直接从配置文件获取)
    protected static final String projectAdminUrl = projectBaseUrl + PROJECT_ADMIN_S;

    /**
     * 从参数工具类获取参数
     * 
     * @Title getProperty
     * @author 吕凯
     * @date 2018年11月15日 下午5:58:10
     * @param key
     * @return String
     */
    public static String getProperty(String key) {
        return getProperty(key, null);
    }

    /**
     * 从参数工具类获取参数
     * 
     * @Title getProperty
     * @author 吕凯
     * @date 2018年11月15日 下午5:57:54
     * @param key
     * @param defaultValue
     * @return String
     */
    public static String getProperty(String key, String defaultValue) {
        return PropertiesUtils.getInstance().getProperty(key, defaultValue);
    }

    // 将web工具类聚合进来
    public static class ATTR extends Attr {
    }

    // 将web工具类聚合进来
    public static class PROPERTY extends Property {
    }

    // 将web工具类聚合进来
    public static class REQUEST extends Request {
    }

    // 将web工具类聚合进来
    public static class RESPONSE extends Response {
    }

    // 将web工具类聚合进来
    public static class UTIL extends Util {
    }

    // 将web工具类聚合进来
    public static class PARAMETER extends Parameter {
    }

    public static int getProjectPort() {
        return projectPort;
    }

    public static String getProjectName() {
        return projectName;
    }

    public static String getProjectBaseURL() {
        return "http://" + WebServerUtil.getHost() + ":" + getProjectPort() + getProjectName();
    }

    public static String getProjectFrontURL() {
        return getProjectBaseURL() + PROJECT_FRONT_S;
    }

    public static String getProjectAdminURL() {
        return getProjectBaseURL() + PROJECT_ADMIN_S;
    }

    public static String getProjectBaseUrl() {
        return projectBaseUrl;
    }

    public static void setProjectBaseUrl(String projectBaseUrl) {
        Global.projectBaseUrl = projectBaseUrl;
    }

    public static String getProjectFrontUrl() {
        return projectFrontUrl;
    }

    public static void setProjectFrontUrl(String projectFrontUrl) {
        Global.projectFrontUrl = projectFrontUrl;
    }

    public static String getProjectadminurl() {
        return projectAdminUrl;
    }

    public static void setProjectPort(int projectPort) {
        Global.projectPort = projectPort;
    }

    public static void setProjectName(String projectName) {
        Global.projectName = projectName;
    }

}
