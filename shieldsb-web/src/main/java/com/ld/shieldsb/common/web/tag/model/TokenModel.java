package com.ld.shieldsb.common.web.tag.model;

import java.io.Serializable;

import lombok.Data;

/**
 * token类
 * 
 * @ClassName TokenModel
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2019年8月7日 上午8:56:43
 *
 */
@Data
public class TokenModel implements Serializable {

    /**
     * @Fields serialVersionUID
     */
    private static final long serialVersionUID = 8715234104137538922L;
    private String token;
    private String parameterName = "csft-token"; // request中的参数名
    private String headerName = "csft-token"; // header中的参数名

    public TokenModel(String token, String parameterName, String headerName) {
        super();
        this.token = token;
        this.parameterName = parameterName;
        this.headerName = headerName;
    }

    public TokenModel(String token) {
        super();
        this.token = token;
    }

}
