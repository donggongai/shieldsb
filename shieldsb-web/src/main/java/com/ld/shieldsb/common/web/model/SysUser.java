package com.ld.shieldsb.common.web.model;

import java.util.Date;

import com.ld.shieldsb.annotation.field.DealField.Field;
import com.ld.shieldsb.annotation.field.db.Unique;
import com.ld.shieldsb.annotation.model.ModelAnno.Model;
import com.ld.shieldsb.annotation.model.ModelAnno.TableName;
import com.ld.shieldsb.common.core.reflect.ClassUtil;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/*
 * 
 * gen by beetlsql 2018-04-26
 */
@Model(name = "用户")
@TableName("sys_user")
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class SysUser extends BasicIdEntity {
    private static final long serialVersionUID = -4568635605081702587L;

    // 登录账号
    @Unique
    @Field(name = "登录账号")
    private String loginCode;
    // 用户昵称
    private String userName;
    // 登录密码
    private String password;
    // 头像路径
    private String avatar;
    // 电子邮箱
    private String email;
    // 冻结时间
    private Date freezeTime;
    // 冻结原因
    private String freezeCause;
    // 最后登陆IP
    private String lastLoginIp;
    // 最后登陆时间
    private Date lastLoginTime;
    // 登录总次数
    private Integer loginCount;
    // 管理员类型（0非管理员 1系统管理员 2二级管理员）
    private Integer mgrType;
    // 手机号码
    private String mobile;
    // 绑定的手机串号
    private String mobileImei;
    // 办公电话
    private String phone;
    // 密保问题
    private String pwdQuestion;
    // 密保问题2
    private String pwdQuestion2;
    // 密保问题3
    private String pwdQuestion3;
    // 密保问题答案
    private String pwdQuestionAnswer;
    // 密保问题答案2
    private String pwdQuestionAnswer2;
    // 密保问题答案3
    private String pwdQuestionAnswer3;
    // 密码安全级别（0初始 1很弱 2弱 3安全 4很安全）
    private Integer pwdSecurityLevel;
    // 密码修改记录
    private String pwdUpdateRecord;
    // 用户类型引用编号
    private String refCode;
    // 用户类型引用姓名
    private String refName;
    // 备注信息
    private String remarks;
    // 用户性别
    private Integer sex; // 0女1男
    // 个性签名
    private String sign;
    // 归属集团Code
    private String corpCode;
    // 归属集团Name
    private String corpName;
    // 用户类型
    private Integer userType;
    // 用户权重（降序）
    private Integer userWeight;
    // 绑定的微信号
    private String wxOpenid;
    // 密码问题修改时间
    private Date pwdQuestUpdateTime;
    // 密码最后更新时间
    private Date pwdUpdateTime;

    public SysUser() {
    }

    public SysUser(String loginCode, String password) {
        super();
        this.loginCode = loginCode;
        this.password = password;
    }

    public int getLoginCount() {
        return ClassUtil.obj2int(loginCount); // 防止空指针
    }

}
