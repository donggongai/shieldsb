package com.ld.shieldsb.common.web.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import com.ld.shieldsb.common.core.util.StringUtils;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 父类
 * 
 * @ClassName ListFieldTag
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2019年3月28日 下午3:54:16
 *
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class TagSupportBasicTag extends TagSupport {

    /**
     * @Fields serialVersionUID
     */
    private static final long serialVersionUID = 5718857174848876590L;
    protected String scope = null;// 作用域
    protected String var = null;// 赋值给变量

    @Override
    public int doStartTag() throws JspException {
        return EVAL_BODY_INCLUDE;
    }

    protected void setAttrOrOut(JspWriter out, String sb) throws IOException {
        if (StringUtils.isNotEmpty(var)) {
            if ("application".equals(scope)) {
                pageContext.getServletContext().setAttribute(var, sb);
            } else if ("session".equals(scope)) {
                pageContext.getSession().setAttribute(var, sb);
            } else if ("page".equals(scope)) {
                pageContext.setAttribute(var, sb);
            } else { // request
                pageContext.getRequest().setAttribute(var, sb);
            }
        } else { // 未赋值给var则直接显示
            out.print(sb);
        }
    }

}
