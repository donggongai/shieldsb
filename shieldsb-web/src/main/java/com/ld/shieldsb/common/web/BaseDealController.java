package com.ld.shieldsb.common.web;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ld.shieldsb.common.core.model.Result;
import com.ld.shieldsb.common.core.util.StringUtils;

@ConditionalOnProperty(prefix = "basedeal.controller", name = "enable", havingValue = "true", matchIfMissing = true)
@Controller
@RequestMapping("/scbase")
public class BaseDealController extends BaseController {

    /**
     * 返回json结果
     * 
     * @Title jsonResult
     * @author 吕凯
     * @date 2021年4月14日 上午10:16:21
     * @param message
     * @param success
     * @return Object
     */
    @RequestMapping(value = "/jsonresult", method = { RequestMethod.POST, RequestMethod.GET })
    // 返回json格式
    @ResponseBody
    public Object jsonResult(String message, Boolean success) {
        Result result = (Result) request.getAttribute("result");
        if (StringUtils.isNotEmpty(message)) {
            result = new Result();
            result.setMessage(message);
            if (success != null) {
                result.setSuccess(success);
            } else {
                result.setSuccess(true);
            }
        }
        return result;
    }

}
