package com.ld.shieldsb.common.web.model;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

/**
 * 基本类
 * 
 * @ClassName BasicEntity
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2018年7月3日 下午2:13:54
 *
 */
@Setter
@Getter
public class BasicEntity implements Serializable {

    private static final long serialVersionUID = -173139163965507059L;
    // 创建人
    @JsonIgnore
    private Long createBy;
    // 修改人
    @JsonIgnore
    private Long updateBy;
    // 创建时间
    @JsonIgnore
    private Date createTime;
    // 修改时间
    @JsonIgnore
    private Date updateTime;
    // 状态（0正常 -1删除 2停用）
    @JsonIgnore
    private Integer state;

    public BasicEntity() {
        super();
    }

}
