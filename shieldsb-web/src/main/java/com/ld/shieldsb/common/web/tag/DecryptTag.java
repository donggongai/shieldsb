package com.ld.shieldsb.common.web.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.ld.shieldsb.common.core.model.PropertiesModel;
import com.ld.shieldsb.common.web.exception.ParamsterValidException;
import com.ld.shieldsb.common.web.util.Web;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

/**
 * 解密标签,使用只读的加密key(会保存的不要使用)
 * 
 * @ClassName DecryptTag
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2019年3月28日 下午3:54:16
 *
 */
@EqualsAndHashCode(callSuper = false)
@Data
@Slf4j
public class DecryptTag extends SimpleTagSupport {

    private String value;// 传入的参数名

    @Override
    public void doTag() throws JspException, IOException {
        JspWriter out = this.getJspContext().getOut();// 指定输入流，用于页面输出分页信息、
        StringBuffer sb = new StringBuffer();// 构建StringBuffer对象，用户拼接分页标签
        boolean encryptd = PropertiesModel.CONFIG.getBoolean("shieldsb.encrypttag.enable", true);
        if (encryptd) {
            try {
                sb.append(Web.Parameter.decrypt(value));
            } catch (ParamsterValidException e) {
                log.error("", e);
            }
        } else {
            sb.append(value);
        }
        out.print(sb);
    }

}
