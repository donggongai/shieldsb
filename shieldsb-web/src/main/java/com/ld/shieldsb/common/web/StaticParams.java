package com.ld.shieldsb.common.web;

/**
 * 静态参数，具体项目中可能需要另外定义
 *
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @ClassName StaticParams
 * @date 2019年1月16日 下午4:57:40
 */
public class StaticParams {
    // 数据库常用字段
    public static final String DB_FILED_CREATE_BY = "createBy";
    public static final String DB_FILED_CREATE_NAME = "createName";
    public static final String DB_FILED_CREATE_TIME = "createTime";
    public static final String DB_FILED_UPDATE_BY = "updateBy";
    public static final String DB_FILED_UPDATE_NAME = "updateName";
    public static final String DB_FILED_UPDATE_TIME = "updateTime";
    public static final String DB_FILED_APPROVED_BY = "approvedBy";
    public static final String DB_FILED_APPROVED_NAME = "approvedName";
    public static final String DB_FILED_APPROVED_TIME = "approvedTime";
    public static final String DB_FILED_STATE = "state";
    public static final String DB_FILED_ISSTART = "isStart";

    // 状态，待迁移
    public static final Integer STATE_DELETE = -1;
    public static final Integer STATE_WAIT_SUBMIT = -1; // 待提交
    public static final Integer STATE_NORMAL = 0;
    public static final Integer STATE_DISABLE = 2; // 停用
    public static final Integer STATE_FREEZE = 3; // 冻结
    public static final Integer STATE_ENABLE = -2; // 启用
    public static final Integer STATE_UNFREEZE = -3; // 解除冻结

    // 显示
    public static final Integer SHOW_YES = 1; // 显示
    public static final Integer SHOW_NO = 0; // 不显示

    public static final int AUDIT_PASS = 0; // 通过
    public static final int AUDIT_WAIT = 1; // 待审核
    public static final int AUDIT_BACK = 2; // 退回

    public static final int UPDATE_TRUE = 1; // 修改
    public static final int UPDATE_FALSE = 0; // 不修改

    public interface ATTFIX {

        public static final int ATTFIX_ONLINE = 1; // 文件栏目类型 1 是远程
        public static final int ATTFIX_LOCAL = 2; // 文件栏目类型 2 是本地
        public static final int ATTFIX_ONLINE_LOCAL = 3; // 文件栏目类型 3 是远程本地

    }

    /**
     * 动态条件拼接 或者 or
     */
    public static final int DYNAMIC_CONDITION_OR = 1;

    /**
     * 动态条件 分组用，确定是否有左右括号
     */
    public static final int DYNAMIC_CONDITION_BRACKETS = 1;

    public static final String CONFIG_ADMIN_PATH_KEY = "shieldsb.adminPath"; // 后台地址前缀，如/a
    public static final String CONFIG_FRONT_PATH_KEY = "shieldsb.frontPath"; // 前台地址前缀，如/f

}
