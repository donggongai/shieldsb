package com.ld.shieldsb.common.core.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * Spring的ApplicationContext的持有者,可以用静态方法的方式获取spring容器中的bean
 *
 * @author fengshuonan
 * @date 2016年11月27日 下午3:32:11
 */
//在Spring Boot可以扫描的包下,实现ApplicationContextAware接口，并加入Component注解，不在扫描包下需要引入。参考：https://www.cnblogs.com/s648667069/p/6489557.html
@Component
@Slf4j
public class SpringContextHolder implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringContextHolder.applicationContext = applicationContext;
    }

    public static ApplicationContext getApplicationContext() {
        assertApplicationContext();
        return applicationContext;
    }

    /**
     * 根据bean名称获取注册的Bean
     * 
     * @Title getBean
     * @author 吕凯
     * @date 2019年5月17日 上午9:41:28
     * @param beanName
     * @return T
     */
    @SuppressWarnings("unchecked")
    public static <T> T getBean(String beanName) {
        assertApplicationContext();
        try {
            return (T) applicationContext.getBean(beanName);
        } catch (BeansException e) {
            return null;
        }
    }

    /**
     * 根据Bean类型获取注册的bean
     * 
     * @Title getBean
     * @author 吕凯
     * @date 2019年5月17日 上午9:41:07
     * @param requiredType
     * @return T
     */
    public static <T> T getBean(Class<T> requiredType) {
        assertApplicationContext();
        try {
            return (T) applicationContext.getBean(requiredType);
        } catch (BeansException e) {
            return null;
        }
    }

    /**
     * 根据class的名称获取bean
     * 
     * @Title getBeanByClassName
     * @author 吕凯
     * @date 2020年9月30日 上午8:49:28
     * @param <T>
     * @param requiredType
     * @return T
     */
    public static <T> T getBeanByClassName(Class<T> requiredType) {
        return getBean(StringUtils.uncap(requiredType.getSimpleName()));
    }

    /**
     * 根据bean名称及类型获取注册的Bean
     * 
     * @Title getBean
     * @author 吕凯
     * @date 2019年5月17日 上午9:40:46
     * @param beanName
     * @param requiredType
     * @return T
     */
    public static <T> T getBean(String beanName, Class<T> requiredType) {
        assertApplicationContext();
        try {
            return (T) applicationContext.getBean(beanName, requiredType);
        } catch (BeansException e) {
            return null;
        }
    }

    private static void assertApplicationContext() {
        if (SpringContextHolder.applicationContext == null) {
            throw new RuntimeException("applicaitonContext属性为null,请检查是否注入了SpringContextHolder!");
        }
    }

    /**
     * 延时获取类对象
     * 
     * @Title getInstance
     * @author 吕凯
     * @date 2021年4月26日 上午10:51:39
     * @param classType
     * @return Object
     */
    public static <T> T getInstance(Class<T> classType) {
        T object = null;
        long startTime = System.currentTimeMillis();
        while (true) {
            try {
                object = SpringContextHolder.getBean(classType);
            } catch (Exception e) {
                log.error("", e);
            }
            long endTime = System.currentTimeMillis();
            if (object != null || (endTime - startTime) > 1000) {
                break;
            }
        }
        return object;
    }

}
