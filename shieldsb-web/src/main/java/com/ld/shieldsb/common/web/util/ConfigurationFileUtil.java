package com.ld.shieldsb.common.web.util;

import java.io.File;
import java.io.IOException;

import com.ld.shieldsb.common.composition.util.ConvertUtil;
import com.ld.shieldsb.common.composition.util.web.Global;
import com.ld.shieldsb.common.core.io.FileUtils;
import com.ld.shieldsb.common.core.model.Result;
import com.ld.shieldsb.common.core.util.PathUtil;
import com.ld.shieldsb.common.core.util.ResultUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 配置文件工具类
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2020年6月4日 下午4:52:34
 *
 */
@Slf4j
public class ConfigurationFileUtil {
    private ConfigurationFileUtil() {
        // 工具类不提供构造方法
    }

    /**
     * 获取配置文件(文件不一定存在，调用saveObj2ConfigFile时保存)
     * 
     * @Title getConfigFile
     * @author 吕凯
     * @date 2020年6月4日 下午4:30:40
     * @param prefix
     * @return File
     */
    public static File getConfigFile(String prefix) {
        File tmpDirFile = PathUtil.getShieldsbPreferenceDir();
        String fileName = getConfigFileName(prefix);
        return new File(tmpDirFile, fileName);
    }

    /**
     * 获取配置文件名称（不带上级路径）
     * 
     * @Title getConfigFileName
     * @author 吕凯
     * @date 2020年6月4日 下午4:28:49
     * @param prefix
     * @return String
     */
    public static String getConfigFileName(String prefix) {
        return Global.getProjectName().replace("/", "") + "_" + Global.getProjectPort() + "_" + prefix + ".sd"; // 配置信息文件名称
    }

    /**
     * 获取配置文件路径
     * 
     * @Title getConfigFilePath
     * @author 吕凯
     * @date 2020年6月4日 下午4:30:52
     * @param prefix
     * @return String
     */
    public static String getConfigFilePath(String prefix) {
        return getConfigFile(prefix).getAbsolutePath();
    }

    /**
     * 获取配置文件里的类对象
     * 
     * @Title readObjFromConfigFile
     * @author 吕凯
     * @date 2020年6月4日 下午5:00:05
     * @param <T>
     * @param prefix
     * @param classOfT
     * @return T
     */
    public static <T> T readObjFromConfigFile(String prefix, Class<T> classOfT) {
        File file = getConfigFile(prefix);
        if (file.exists()) {
            try {
                String content = FileUtils.readFileToString(file, "UTF-8");
                // 转换字符串
                T model = ConvertUtil.jsonStr2Obj(content, classOfT);
                return model;
            } catch (Exception e) {
                log.error("", e);
            }
        }
        return null;
    }

    /**
     * 保存对象到配置文件中
     * 
     * @Title saveObj2ConfigFile
     * @author 吕凯
     * @date 2020年6月4日 下午5:03:17
     * @param <T>
     * @param prefix
     * @param model
     * @return Result
     */
    public static <T> Result saveObj2ConfigFile(String prefix, T model) {
        String preference = ConvertUtil.obj2JsonStr(model);
        try {
            File file = ConfigurationFileUtil.getConfigFile(prefix);
            FileUtils.write(file, preference, "UTF-8");
        } catch (IOException e) {
            log.error("", e);
            return ResultUtil.error("保存配置失败！" + e.getMessage());
        }
        return ResultUtil.success("保存配置成功！");
    }

}
