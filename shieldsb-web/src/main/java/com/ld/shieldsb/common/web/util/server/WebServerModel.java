package com.ld.shieldsb.common.web.util.server;

import lombok.Data;

@Data
public class WebServerModel {
    private String host;
    private String projectName = "";
    private int port;

    public String getUrl() {
        String ipadd = "http" + "://" + host + ":" + port + projectName;
        return ipadd;
    }

}
