package org.shieldspring.common.util;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ld.shieldsb.common.core.util.StringUtils;

public class StringUtilTest {
    @Test
    public void bytes() {
        String ss = "test";
        byte[] bs = StringUtils.getBytes(ss);
        String ss1 = StringUtils.toString(bs);
        Assert.assertEquals(ss, ss1);
    }
}
