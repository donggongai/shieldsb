[TOC]

### 二维码模块
![](https://img.shields.io/badge/release-1.0.0-blue.svg)
#### 使用说明
使用了google的zxing，提供二维码的生成和解码功能。
##### 生成
```java
// 普通二维码
// a、生成到文件中
// 不带logo,destPath表示输出文件路径
QRCodeCommonUtil.encode(text, 500, destPath);
// 带logo
QRCodeCommonUtil.encode(text, logoPath, 500, 120, destPath);
// b、生成BufferedImage，然后通过输出到流中
BufferedImage bi= QRCodeCommonUtil.encode(String content, int width);
BufferedImage bi= QRCodeCommonUtil.encode(String content, String logoImagePath, int width, int logowidth);
ImageIO.write(bi, "jpg", outputStream);
// 1、将图像输出到文件
try (FileOutputStream fileOut = new FileOutputStream(filePath)){
	ImageIO.write(bi, "jpg", fileOut);
	fileOut.flush();
}
// 2、将图像输出到Servlet输出流中。
try (ServletOutputStream sos = response.getOutputStream()){
	ImageIO.write(bi, "jpg", sos);
} 

// 彩色二维码
// 不带logo
QRCodeColorUtil.encode(text, 522, destPath);
// 带logo
QRCodeColorUtil.encode(text, logoPath, 522, 170, destPath);
// 以上为输出到文件中，输出到流中参考上面普通二维码的生成代码
```
##### 解码
```java
// 传本地路径
QRCodeUtil.decode(String path); 
String str = QRCodeUtil.decode("D:/Desktop/captcha/qrcode.jpg");
String str = QRCodeUtil.decode("http://24657430.11315.cn/mallqrcodebigshow/1?bh=24657430");
// 传file
QRCodeUtil.decode(File file); 
// 传图片对象解码
QRCodeUtil.decode(BufferedImage image);

//或者使用具体的工具类获取service解码
QRCodeCommonUtil.getService().decode(File file);
QRCodeCommonUtil.getService().decode(String path);
QRCodeCommonUtil.getService().decode(BufferedImage image);
QRCodeColorUtil.getService().decode(File file);
……

```


#### 效果展示
仅展示显示效果，因清晰度下降可能不能扫描。

##### 普通

---
**普通：** 
![验证码](http://test.11315.com/ueditorgroup/softwarecenter/ueditor/jsp/upload/image/2019/10/25/1571991627949058265.jpg)
**带logo：**
![验证码](http://test.11315.com/ueditorgroup/softwarecenter/ueditor/jsp/upload/image/2019/10/25/1571991539450083063.jpg)

##### 彩色
---
**普通：**
![验证码](http://test.11315.com/ueditorgroup/softwarecenter/ueditor/jsp/upload/image/2019/10/25/1571991606938049534.jpg) 
**带logo：**
![验证码](http://test.11315.com/ueditorgroup/softwarecenter/ueditor/jsp/upload/image/2019/10/25/1571991495512025972.jpg) 

---


#### 版本说明
##### v1.0.0（2019-10-17）
- 初版发布;
- ;