package com.ld.shieldsb.qrcode.writer.model;

import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.Version;

import lombok.Data;

/**
 * 二维码数据
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2019年10月25日 上午11:21:11
 *
 */
@Data
public class QRCodeData {
    private Integer qrWidth; // 二维码的宽
    private Integer qrHeight; // 二维码的高

    private BitMatrix matrix; // 得到每个模块长度
    private Version version; // 得到每个模块长度

    private Integer modelWidth; // 得到每个模块长度
    private Integer totalModelNum; // 获取单边模块数

    private Location leftTopArea; // 左上角
    private Location rightTopArea; // 右上角
    private Location leftBottomArea; // 左下角

    public Integer getTotalModelNum() {
        if (totalModelNum == null) {
            totalModelNum = (version.getVersionNumber() - 1) * 4 + 5 + 16;
        }
        return totalModelNum;
    }

    public Integer getQrWidth() {
        if (qrWidth == null) {
            qrWidth = matrix.getWidth();
        }
        return qrWidth;
    }

    public Integer getQrHeight() {
        if (qrHeight == null) {
            qrHeight = matrix.getHeight();
        }
        return qrHeight;
    }

    public QRCodeData(BitMatrix matrix, Version version, int startModel, int endModel) {
        int qrWidth = matrix.getWidth();
        int qrHeight = matrix.getHeight();

        int[] tl = matrix.getTopLeftOnBit(); // getTopLeftOnBit()返回两个值top，left边界值

        int totalModelNum = (version.getVersionNumber() - 1) * 4 + 5 + 16; // 获取单边模块数
        int resultWidth = qrWidth - 2 * (tl[0]);
        int modelWidth = resultWidth / totalModelNum; // 得到每个模块长度
        Location leftTopArea = new Location();
        leftTopArea.setX(tl[0] + modelWidth * startModel);
        leftTopArea.setY(tl[0] + modelWidth * startModel);
        leftTopArea.setWidth(modelWidth * (endModel - startModel));
        leftTopArea.setHeight(modelWidth * (endModel - startModel));
        Location rightTopArea = new Location();
        rightTopArea.setX((totalModelNum - endModel) * modelWidth + tl[0]);
        rightTopArea.setY(tl[0] + modelWidth * startModel);
        rightTopArea.setWidth(qrWidth - modelWidth * startModel - (totalModelNum - endModel) * modelWidth);
        rightTopArea.setHeight(qrWidth - modelWidth * startModel - (totalModelNum - endModel) * modelWidth);
        Location leftBottomArea = new Location();
        leftBottomArea.setX(tl[0] + modelWidth * endModel);
        leftBottomArea.setY(qrHeight - modelWidth * endModel - tl[1]);
        leftBottomArea.setWidth(modelWidth * (endModel - startModel));
        leftBottomArea.setHeight(modelWidth * (endModel - startModel));
        // 赋值
        this.setMatrix(matrix);
        this.setVersion(version);
        this.setQrWidth(qrWidth);
        this.setQrHeight(qrHeight);
        this.setTotalModelNum(totalModelNum);
        this.setModelWidth(modelWidth);

        this.setLeftTopArea(leftTopArea);
        this.setRightTopArea(rightTopArea);
        this.setLeftBottomArea(leftBottomArea);
    }

}
