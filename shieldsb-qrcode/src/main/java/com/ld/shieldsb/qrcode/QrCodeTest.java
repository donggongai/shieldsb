package com.ld.shieldsb.qrcode;

import com.ld.shieldsb.qrcode.util.QRCodeColorUtil;
import com.ld.shieldsb.qrcode.util.QRCodeCommonUtil;
import com.ld.shieldsb.qrcode.util.QRCodeUtil;

public class QrCodeTest {

    public static void main(String[] args) throws Exception {
        // 存放在二维码中的内容
        String text = "http://24657430.11315.cn/imgamalltdcodebig?bh=24657430";
        // 嵌入二维码的图片路径
//        String imgPath = "D:/图片视频/092002.92725844_620X620.jpg";
        String imgPath = "http://www.11315.cn/images/mallTdlogoNew.png";
        // 生成的二维码的路径及名称
        String destPath = "D:\\Desktop\\captcha/jam.jpg";
        // 生成二维码
        QRCodeCommonUtil.encode(text, imgPath, 200, 60, destPath);
        // 解析二维码
        String str = QRCodeUtil.decode(destPath);
        // 打印出解析出的内容
        System.out.println(str);

        destPath = "D:/Desktop/captcha/jam1.jpg";
        QRCodeColorUtil.encode(text, imgPath, 200, 60, destPath);
        str = QRCodeUtil.decode(destPath);
        // 打印出解析出的内容
        System.out.println(str);
    }

}
