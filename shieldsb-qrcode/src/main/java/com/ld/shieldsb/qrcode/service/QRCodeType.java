package com.ld.shieldsb.qrcode.service;

public enum QRCodeType {
    COMMON("common"), COLOR("color");

    private final String value;

    QRCodeType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
