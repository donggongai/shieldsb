package com.ld.shieldsb.qrcode.writer;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.google.zxing.common.BitMatrix;
import com.ld.shieldsb.qrcode.writer.model.QRCodeData;

/**
 * 圆角输出器，重写的二维码输出器
 * 
 * @ClassName ColorQRCodeWriter
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2017年8月11日 上午8:32:24
 *
 */

public class QRCodeRoundedWriter extends QRCodeDataFillWriter {

    // 填充剩余区域
    @Override
    public void fillOthers(BitMatrix matrix, int qrWidth, int[] pixels, int y, int x) {
        // 二维码颜色（RGB,渐变）
        int colorInt = getColorVal(matrix, y);
        pixels[getPixelsIndex(qrWidth, y, x)] = matrix.get(x, y) ? colorInt : Color.WHITE.getRGB();
    }

    // 填充左下角颜色
    @Override
    public Integer fillLeftBottom(Integer bottomLeftColor, BitMatrix matrix, int qrWidth, int[] pixels, int y, int x) {
        if (bottomLeftColor == null) {
            bottomLeftColor = getColorVal(matrix, y);
        }
        pixels[getPixelsIndex(qrWidth, y, x)] = matrix.get(x, y) ? bottomLeftColor : Color.WHITE.getRGB();
        return bottomLeftColor;
    }

    // 填充右上角颜色
    @Override
    public Integer fillRightTop(Integer topRightColor, BitMatrix matrix, int qrWidth, int[] pixels, int y, int x) {
        if (topRightColor == null) {
            topRightColor = getColorVal(matrix, y);
        }
        pixels[getPixelsIndex(qrWidth, y, x)] = matrix.get(x, y) ? topRightColor : Color.WHITE.getRGB();
        return topRightColor;
    }

    // 填充左上角颜色
    @Override
    public Integer fillLeftTop(Integer topLeftColor, BitMatrix matrix, int qrWidth, int[] pixels, int y, int x) {
        if (topLeftColor == null) {
//                            topLeftColor = Color.WHITE.getRGB();
            Color color = new Color(231, 144, 56);
            topLeftColor = color.getRGB();
        }
        pixels[getPixelsIndex(qrWidth, y, x)] = matrix.get(x, y) ? topLeftColor : Color.WHITE.getRGB();
        return topLeftColor;
    }

    @Override
    public BufferedImage extendDealImage(BufferedImage image, QRCodeData data) {
        changeFillShape(image, data.getModelWidth(), data.getLeftTopArea().getY(),
                data.getLeftTopArea().getY() + data.getLeftTopArea().getHeight());
        return image;
    }

    /**
     * 颜色填充
     * 
     * @Title changeFillShape
     * @author 吕凯
     * @date 2017年8月11日 下午1:55:17
     * @param image
     * @param modelWidth
     * @param topStartX
     * @param topStartY
     *            void
     */
    private void changeFillShape(BufferedImage image, int modelWidth, int topStartX, int topStartY) {
        // 读取二维码图片，并构建绘图对象
        Graphics2D g = image.createGraphics();
        // 开始绘制图片
        BasicStroke stroke = new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
        g.setStroke(stroke);// 设置笔画对象
        for (int i = 0; i < modelWidth; i++) {
            // 外框
            // 指定弧度的圆角矩形
            RoundRectangle2D.Float round = new RoundRectangle2D.Float((float) topStartX + i, (float) topStartY + i,
                    (float) modelWidth * 7 - 2 * i, (float) modelWidth * 7 - 2 * i, 30F, 30F);
            // 消除圆角的锯齿
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g.setColor(Color.blue);
            g.draw(round);// 绘制圆弧矩形
        }
        for (int i = 0; i < modelWidth * 3 / 2; i++) {
            // 内框
            // 指定弧度的圆角矩形
            RoundRectangle2D.Float roundInner = new RoundRectangle2D.Float((float) topStartX + 2 * modelWidth + i,
                    (float) topStartY + 2 * modelWidth + i, (float) modelWidth * 3 - 2 * i, (float) modelWidth * 3 - 2 * i, 20F, 20F);
            // 消除圆角的锯齿
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g.setColor(Color.blue);
            g.draw(roundInner);// 绘制圆弧矩形
        }

        g.dispose();
        image.flush();
    }

    public static void main(String[] args) {
        QRCodeDataFillWriter writer = new QRCodeDataFillWriter();
        System.out.println(Color.BLACK.getRGB() + " " + Color.WHITE.getRGB());
        BufferedImage bufferImg = writer.encodeBufImg("http://www.baidu.com/", 600, 600, true);
        // 指定生成图片的保存路径
        File file = new File("D:/Desktop/captcha/imooctt1.png");
        // 生成二维码
        try {
            ImageIO.write(bufferImg, "png", file);
        } catch (IOException e) {
            log.error("", e);
        }
    }

}