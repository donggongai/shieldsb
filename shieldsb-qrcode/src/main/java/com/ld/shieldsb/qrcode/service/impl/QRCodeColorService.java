package com.ld.shieldsb.qrcode.service.impl;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.ld.shieldsb.qrcode.service.QRCodeService;
import com.ld.shieldsb.qrcode.writer.QRCodeColorWriter;

/**
 * 带颜色的二维码
 * 
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2019年10月25日 上午8:19:39
 *
 */
public class QRCodeColorService extends QRCodeService {
    private QRCodeColorWriter writer = new QRCodeColorWriter();

    @Override
    public BufferedImage createImage(String content, String logoImagePath, int width, int logowidth) throws Exception {
        BufferedImage bufferImg = writer.encodeBufImg(content, width);
        // 生成二维码
        try {
            overlapImage(bufferImg, logoImagePath, logowidth);
        } catch (IOException e) {
            log.error("", e);
        }
        return bufferImg;
    }

    @Override
    public void encode(String content, String logoImagePath, int width, int logowidth, String destImagePath) throws Exception {
        BufferedImage image = createImage(content, logoImagePath, width, logowidth);
        mkdirs(destImagePath);
        // String file = new Random().nextInt(99999999)+".jpg";
        // ImageIO.write(image, FORMAT_NAME, new File(destPath+"/"+file));
        ImageIO.write(image, FORMAT_NAME, new File(destImagePath));

    }

    @Override
    public BufferedImage encode(String content, String logoImagePath, int width, int logowidth) throws Exception {
        BufferedImage bufferImg = createImage(content, logoImagePath, width, logowidth);
        return bufferImg;
    }

}
