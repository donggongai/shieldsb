package com.ld.shieldsb.qrcode.util;

import java.awt.image.BufferedImage;
import java.io.File;

import com.ld.shieldsb.qrcode.service.QRCodeService;
import com.ld.shieldsb.qrcode.service.QRCodeType;

public class QRCodeUtil {

    /**
     * 
     * 根据传入类型获取实例
     * 
     * @Title getService
     * @author 吕凯
     * @date 2019年10月25日 下午12:01:23
     * @param type
     * @return QRCodeService
     */
    public static QRCodeService getService(QRCodeType type) {
        return QRCodeServiceFactory.getInstance().getService(type.getValue());
    }

    public static QRCodeService getService() {
        return getService(QRCodeType.COMMON);
    }

    /**
     * 解码二维码
     * 
     * @Title decode
     * @author 吕凯
     * @date 2019年10月24日 下午3:01:49
     * @param file
     * @return
     * @throws Exception
     *             String
     */
    public static String decode(File file) throws Exception {
        return getService().decode(file);
    }

    /**
     * 解码二维码
     * 
     * @Title decode
     * @author 吕凯
     * @date 2019年10月24日 下午3:01:25
     * @param path
     *            二维码路径,本地或网络地址
     * @return
     * @throws Exception
     *             String
     */
    public static String decode(String path) throws Exception {
        return getService().decode(path);
    }

    public static String decode(BufferedImage image) throws Exception {
        return getService().decode(image);
    }

}
