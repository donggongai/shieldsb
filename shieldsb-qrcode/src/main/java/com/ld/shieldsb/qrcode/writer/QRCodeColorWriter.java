package com.ld.shieldsb.qrcode.writer;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.google.zxing.common.BitMatrix;

import lombok.extern.slf4j.Slf4j;

/**
 * 彩色输出器，重写的二维码输出器
 * 
 * @ClassName ColorQRCodeWriter
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2017年8月11日 上午8:32:24
 *
 */
@Slf4j
public final class QRCodeColorWriter extends QRCodeDataFillWriter {

    // 填充剩余区域
    @Override
    public void fillOthers(BitMatrix matrix, int qrWidth, int[] pixels, int y, int x) {
        // 二维码颜色（RGB,渐变）
        int colorInt = getColorVal(matrix, y);
        pixels[getPixelsIndex(qrWidth, y, x)] = matrix.get(x, y) ? colorInt : Color.WHITE.getRGB();
    }

    // 填充左下角颜色
    @Override
    public Integer fillLeftBottom(Integer bottomLeftColor, BitMatrix matrix, int qrWidth, int[] pixels, int y, int x) {
        if (bottomLeftColor == null) {
            bottomLeftColor = getColorVal(matrix, y);
        }
        pixels[getPixelsIndex(qrWidth, y, x)] = matrix.get(x, y) ? bottomLeftColor : Color.WHITE.getRGB();
        return bottomLeftColor;
    }

    // 填充右上角颜色
    @Override
    public Integer fillRightTop(Integer topRightColor, BitMatrix matrix, int qrWidth, int[] pixels, int y, int x) {
        if (topRightColor == null) {
            topRightColor = getColorVal(matrix, y);
        }
        pixels[getPixelsIndex(qrWidth, y, x)] = matrix.get(x, y) ? topRightColor : Color.WHITE.getRGB();
        return topRightColor;
    }

    // 填充左上角颜色
    @Override
    public Integer fillLeftTop(Integer topLeftColor, BitMatrix matrix, int qrWidth, int[] pixels, int y, int x) {
        if (topLeftColor == null) {
//                            topLeftColor = Color.WHITE.getRGB();
            Color color = new Color(231, 144, 56);
            topLeftColor = color.getRGB();
        }
        pixels[getPixelsIndex(qrWidth, y, x)] = matrix.get(x, y) ? topLeftColor : Color.WHITE.getRGB();
        return topLeftColor;
    }

    public static void main(String[] args) {
        QRCodeDataFillWriter writer = new QRCodeDataFillWriter();
        System.out.println(Color.BLACK.getRGB() + " " + Color.WHITE.getRGB());
        BufferedImage bufferImg = writer.encodeBufImg("http://www.baidu.com/", 600, 600, true);
        // 指定生成图片的保存路径
        File file = new File("D:/Desktop/captcha/imooctt.png");
        // 生成二维码
        try {
            ImageIO.write(bufferImg, "png", file);
        } catch (IOException e) {
            log.error("", e);
        }
    }

}