package com.ld.shieldsb.qrcode.writer.model;

import lombok.Data;

@Data
public class Location {
    private int x;
    private int y;
    private int width;
    private int height;

}
