package com.ld.shieldsb.qrcode.util;

import com.ld.shieldsb.common.core.ServiceFactory;
import com.ld.shieldsb.qrcode.service.QRCodeService;
import com.ld.shieldsb.qrcode.service.QRCodeType;
import com.ld.shieldsb.qrcode.service.impl.QRCodeColorService;
import com.ld.shieldsb.qrcode.service.impl.QRCodeCommonService;

public class QRCodeServiceFactory extends ServiceFactory<QRCodeService> {

    static {
        // 注意工厂类不要一开始就注册对象，否则可能缺少必要的jar包不能实例化报错，应该注册类
        getInstance().registerServiceClass(QRCodeType.COMMON.getValue(), QRCodeCommonService.class);
        getInstance().registerServiceClass(QRCodeType.COLOR.getValue(), QRCodeColorService.class);
    }

    public static ServiceFactory<QRCodeService> getInstance() {
        return getInstance(QRCodeServiceFactory.class);
    }

}
