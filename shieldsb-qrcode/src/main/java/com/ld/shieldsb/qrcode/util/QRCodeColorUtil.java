package com.ld.shieldsb.qrcode.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import com.ld.shieldsb.qrcode.service.QRCodeService;
import com.ld.shieldsb.qrcode.service.QRCodeType;
import com.ld.shieldsb.qrcode.service.impl.QRCodeColorService;

/**
 * zxing带颜色输出
 * 
 * @ClassName ZxingQRCode
 * @author <a href="mailto:donggongai@126.com" target="_blank">吕凯</a>
 * @date 2017年8月10日 上午9:59:26
 *
 */
public class QRCodeColorUtil {
    private static final QRCodeColorService SERVICE = (QRCodeColorService) QRCodeServiceFactory.getInstance()
            .getService(QRCodeType.COLOR.getValue());

    /**
     * 
     * 为二维码图片增加logo
     * 
     * @Title overlapImage
     * @author 吕凯
     * @date 2019年10月25日 上午8:35:14
     * @param image
     * @param logoImagePath
     *            logo路径(本地或远程)
     * @param width
     *            logo大小尺寸
     * @throws IOException
     *             void
     */
    public static void overlapImage(BufferedImage image, String logoImagePath, int width) throws IOException {
        SERVICE.overlapImage(image, logoImagePath, width);
    }

    /**
     * 
     * 生成二维码到本地文件
     * 
     * @Title encode
     * @author 吕凯
     * @date 2019年10月25日 上午8:45:55
     * @param content
     *            内容
     * @param logoImagePath
     *            logo地址（本地或网络）
     * @param width
     *            二维码大小
     * @param logowidth
     *            logo大小
     * @param destImagePath
     *            输出地址
     * @throws Exception
     *             void
     */
    public static void encode(String content, String logoImagePath, int width, int logowidth, String destImagePath) throws Exception {
        SERVICE.encode(content, logoImagePath, width, logowidth, destImagePath);
    }

    /**
     * 
     * 生成二维码到流
     * 
     * @Title encode
     * @author 吕凯
     * @date 2019年10月25日 上午8:46:44
     * @param content
     *            内容
     * @param logoImagePath
     *            logo地址（本地或网络）
     * @param width
     *            二维码大小
     * @param logowidth
     *            logo大小
     * @return
     * @throws Exception
     *             BufferedImage
     */
    public static BufferedImage encode(String content, String logoImagePath, int width, int logowidth) throws Exception {
        BufferedImage bufferImg = SERVICE.encode(content, logoImagePath, width, logowidth);
        return bufferImg;
    }

    /**
     * 生成不带logo的二维码到流
     * 
     * @Title encode
     * @author 吕凯
     * @date 2019年10月25日 上午8:45:12
     * @param content
     * @param width
     * @return
     * @throws Exception
     *             BufferedImage
     */
    public static BufferedImage encode(String content, int width) throws Exception {
        BufferedImage bufferImg = SERVICE.encode(content, null, width, -1);
        return bufferImg;
    }

    /**
     * 解码二维码
     * 
     * @Title decode
     * @author 吕凯
     * @date 2019年10月24日 下午3:01:49
     * @param file
     * @return
     * @throws Exception
     *             String
     */
    public static String decode(File file) throws Exception {
        return SERVICE.decode(file);
    }

    public static QRCodeService getService() {
        return SERVICE;
    }

    /**
     * 解码二维码
     * 
     * @Title decode
     * @author 吕凯
     * @date 2019年10月24日 下午3:01:25
     * @param path
     *            二维码本地路径
     * @return
     * @throws Exception
     *             String
     */
    public static String decode(String path) throws Exception {
        return decode(new File(path));
    }

    public static void main(String[] args) throws Exception {
        // 依次为内容(不支持中文),宽,长,logo图标路径,储存路径
//        ZxingQRCode.encode("http://www.baidu.com/", 600, 150, "file:///C:/Users/lv/Desktop/新logo-03.png", "E:/QRCode/qrtest1.jpg");
        QRCodeColorUtil.encode("http://12085511.11315.com", "http://img.11315.com/images/11315logo/463x495.png", 522, 170,
                "D:/Desktop/captcha/qrtestc2.jpg");
    }
}